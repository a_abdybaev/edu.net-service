'use strict';

var app = angular.module("EduNet", ['ngRoute','LocalStorageModule','ngDialog','ui.mask'])
    .config(function (localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('EduNet')
            .setStorageType('sessionStorage')
            .setNotify(true, true)
    })
    .config(function($routeProvider){
        $routeProvider
            .otherwise({
                redirectTo : '/'
            })
    });
angular
    .module("EduNet")
    .controller("indexController", function ($location,$scope,ngDialog,appValidator,$http) {

       var addFirstRequest = function () {
           var data = {
               centerName : self.firstCenterName,
               name : self.firstName,
               phone : self.firstPhone
           };
           if (data.phone == null || data.phone == "") return;
           addRequest(data);


       };
       var addSecondRequest = function () {
           var data = {
               centerName : self.secondCenterName,
               name : self.secondName,
               phone : self.secondPhone
           };

           if (data.phone == null || data.phone == "") return;
           addRequest(data);
       };
       var addRequest = function (data) {
           $http
               .post('/api/landing/requests',data)
               .then(function (resp) {
                   if (resp.status == 201) return showAlert(false);
                   showAlert(true);
               },function (resp) {
                   showAlert(false);
               });
       };


       this.addFirstRequest = addFirstRequest;
       this.addSecondRequest = addSecondRequest;
       var showAlert = function (isSuccess) {
           ngDialog.open({
               template : "/app-landing/views/alerts/success-error.html",
               className : 'ngdialog-theme-default',
               data : {
                   isSuccess : isSuccess
               },
               width : 600
           });
       };




       // INIT
       var self = this;
       // self.firstName = "Aza";
       // self.firstPhone = "7000245132";
       // self.firstCenterName = "test request center name";




    });

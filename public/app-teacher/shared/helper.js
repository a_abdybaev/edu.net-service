var SHORTLIMIT = 200;
var LONGLIMIT = 10000;

app.factory('teacherHelper', function (appValues,$location,teacherConstants,teacherApi,ngDialog,localStorageService,$window) {
    return {
        getDayNames : function () {
            return ['ПОНЕДЕЛЬНИК','ВТОРНИК','СРЕДА','ЧЕТВЕРГ','ПЯТНИЦА','СУББОТА'];
        },
        getGroupSchedule : function (group) {
            var dayNames = this.getDayNames();
            var days = [];

            for (var i =0;i<6;i++) {
                days[i] = {
                    name : dayNames[i],
                    hasLesson : false
                }
            }
            var lessons = group.schedule;
            if (lessons === null || lessons.length === 0) {
                return days;
            } else {
                lessons.forEach(function (lesson) {
                    var index = lesson.day -1;
                    days[index].hasLesson = true;
                    var startTime = new Date();
                    startTime.setHours(lesson.start.hour);
                    startTime.setMinutes(lesson.start.minute);
                    var endTime = new Date();
                    endTime.setHours(lesson.end.hour);
                    endTime.setMinutes(lesson.end.minute);
                    days[index].startTime = startTime;
                    days[index].endTime = endTime;

                    days[index].classroomID = lesson.classroomID;
                });
                return days;
            }
        },
        getMonthNames : function () {
            return ["ЯНВАРЬ","ФЕВРАЛЬ","МАРТ","АПРЕЛЬ","МАЙ","ИЮНЬ","ИЮЛЬ","АВГУСТ","СЕНТЯБРЬ","ОКТЯБРЬ","НОЯБРЬ", "ДЕКАБРЬ"];
        },
        getMonthNamesDefault : function () {
            return ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"];
        },

        getHomeworkDate : function (homework) {
            var date = new Date();
            date.setDate(homework.date.day);
            date.setMonth(homework.date.month);
            return date;
        },
        setTeacherInfoData : function (teacher) {
            return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.USER_INFO,teacher);
        },
        setTeacherInfoHasGroups : function (value) {
            return localStorageService.set('TEACHER_INFO_HAS_GROUPS',value)
        },
        getTeacherInfoHasGroups : function () {
            return localStorageService.get('TEACHER_INFO_HAS_GROUPS');
        },
        getTeacherInfo : function () {
            return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.USER_INFO)
        },
        getGroupsFromListOfFiles : function (files) {
            if (files.length === 0) {
                return [];
            } else {
                var groups = [files[0].groupID];
                files.forEach(function (file) {
                    var indexMatch = groups.map(function (group) {
                        return group._id;
                    }).indexOf(file.groupID._id);
                    if (indexMatch <= -1) {
                        groups.push(file.groupID);
                    }
                });
                return groups;
            }

        },
        getGroupsFromListOfApplications : function (applications) {
            if (applications.length === 0) {
                return [];
            } else {
                var groups = [applications[0].groupID];
                applications.forEach(function (application) {
                    applications.forEach(function (application) {
                        var indexMatch = groups.map(function (group) {
                            return group._id;
                        }).indexOf(application.groupID._id);
                        if (indexMatch <= -1) {
                            groups.push(application.groupID);
                        }
                    });
                    return groups;
                });
            }
        },
        getListOfStudentsForAttendanceAddForm : function (students) {
            if (students.length >0) {
                students.forEach(function (student) {
                    student.attendance = -1;
                })
            } else {
                return [];
            }
        },
        prepareStudentsForAttendance : function (students) {
            if (students.length > 0) {
                var results =[];
                students.forEach(function (student) {
                    results.push({
                        value : student.attendance,
                        studentID : student._id
                    });
                });
                return results;
            } else {
                return [];
            }
        },
        //CHANNEL
        buildSchedule : function (lessons) {
            var self = this;
            var top = 230;
            var height = 40;
            var results = [[],[],[],[],[],[]];
            lessons.forEach(function (lesson) {
                lesson.style = {
                    'top' :  self.convertStartTimeToTopPosition(top,lesson.start) + 'px',
                    'height' : (self.convertTimeToMinutes(lesson.end) -self.convertTimeToMinutes(lesson.start)) * 5/6 + 'px',
                    'background-color' : lesson.groupID.color
                };
                results[lesson.day-1].push(lesson);
            });
            return results;
        },
        convertTimeToMinutes : function (time) {
            return parseInt(time.hour) * 60 + parseInt(time.minute);
        },
        convertStartTimeToTopPosition : function (top,start) {
            var difference = this.convertTimeToMinutes(start) - this.convertTimeToMinutes({
                    hour : 9,
                    minute : 0
                });
            return top + difference * 5/6;
        },
        getFilesFromListOfGroups : function (groups) {
            var results = [];
            groups.forEach(function (group) {
                results = results.concat(group.files);
            });
            return results;
        },
        getApplicationListFromGroups : function (groups) {
            var results = [];
            groups.forEach(function (group) {
                results = results.concat(group.applications);
            });
            return results;
        },
        logout : function () {
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.TOKEN);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.ACT_GROUP_ID);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.USER_INFO);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.ACT_GROUP_ID);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.IS_DEV);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.USER_INFO);
            $window.location.href = "/";
        },
        setActiveGroupID : function (groupID) {
            return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.ACT_GROUP_ID,groupID);
        },
        getActiveGroupID : function () {
            return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.ACT_GROUP_ID);
        },
        setActiveTestID : function (testID) {
            return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.ACT_TEST_ID,testID)
        },
        getActiveTestID : function () {
            return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.ACT_TEST_ID);
        }
    }
});
app.config(function($routeProvider){
    $routeProvider
        .when("/group-info",{
            templateUrl : "group-info/index.html"
        })
        .when("/list-of-groups",{
            templateUrl : "groups/index.html",
            controller : "listOfGroupsController"
        })

        .when("/list-of-applications",{
            templateUrl : "applications/index.html"
        })
        .when("/list-of-files",{
            templateUrl : "files/index.html"
        })
        .when("/schedule",{
            templateUrl : "schedule/index.html"
        })
        .when("/add-attendance",{
            templateUrl : "group-info/attendance/add-attendance-day.html",
            controller : "addAttendanceDayAlertController"
        })
        .when("/error404",{
            templateUrl : "errors/404.html",
            controller : "errorsController"
        })
        .when("/error500",{
            templateUrl : "errors/500.html",
            controller : "errorsController"
        })
        .when("/profile",{
            templateUrl : "profile/index.html"
        })
        .when("/list-of-tests",{
            templateUrl : "tests/index.html"
        })
        .when("/add-test",{
            templateUrl : "tests/add-test/index.html"
        })
        .when("/test-info",{
            templateUrl : "tests/test-info.html"
        })
        .otherwise({
            redirectTo : '/list-of-groups'
        })
});
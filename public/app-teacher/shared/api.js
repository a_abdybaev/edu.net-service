var TEACHER_URL = "/api/teachers",
    BASE_URL = "/api/teachers",
    TESTS_URL = BASE_URL + '/tests',
    GROUPS_URL = BASE_URL + '/groups',
    SCHEDULE_URL = BASE_URL + '/schedule',
    HOMEWORK_URL = BASE_URL + '/homeworks',
    CHANNEL_URL = BASE_URL + '/channel',
    APPLICATIONS_URL = BASE_URL + '/applications',
    FILES_URL = BASE_URL + '/files';


var createResource= function (response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 201) return cb(null);
    return cb(response);
};
var updateResource = function (response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null);
    return cb(response);
};
var deleteResource = function(response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null);
    return cb(response);
};
var getResource = function(response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null,response.data);
    return cb(response);
};
var errorCallback = function(response,cb) {
    var appHelper = angular.element(document.body).injector().get('appHelper'),
        appValues = angular.element(document.body).injector().get('appValues');
    appHelper.removeLoader();
    if (response.status == 403) {
        angular.element(document.querySelector('[ng-controller]')).injector().get('localStorageService').remove(appValues.LOCAL_STORAGE_KEYS.TOKEN);
        angular.element(document.querySelector('[ng-controller]')).injector().get('$window').location.href = "/";
        return;
    }

    return cb(response);
};
var successCallback = function (response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null);
    return cb(response);
};

app.factory('teacherApi', function (Upload,$http,localStorageService,$window,appHelper,appValues) {
    return {
        setToken : function (token) {
            return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.TOKEN,token);
        },
        getToken : function () {
            return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.TOKEN)
        },
        checkAuthorization : function () {
            var token = this.getToken();
            if (!token) {
                $window.location.href = "/";
            }
        },
        removeToken : function () {
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.TOKEN);
        },
        getGroupByID : function (groupID,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(GROUPS_URL + '/' + groupID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getGroups : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(GROUPS_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        addHomework : function (obj,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .post(HOMEWORK_URL,obj,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        deleteHomeworkByID : function (homeworkID,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .delete(HOMEWORK_URL + '/' + homeworkID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getHomeworkByID : function (homeworkID,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(HOMEWORK_URL + '/' + homeworkID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateHomeworkByID : function(homeworkID,obj,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .put(HOMEWORK_URL + '/' + homeworkID,obj,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        addPost : function (obj,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            Upload
                .upload({
                    url : CHANNEL_URL,
                    data : {
                        files : obj.files,
                        images : obj.images,
                        text : obj.text,
                        groupID : obj.groupID,
                        important : obj.important
                    },
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getPosts : function (obj,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .get(CHANNEL_URL,{
                    headers : {
                        token : token
                    },
                    params : obj
                })
                .then(function (response) {
                    return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        addApplication : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .post(APPLICATIONS_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getApplications : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .get(APPLICATIONS_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getApplicationsByGroupID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .get(APPLICATIONS_URL + '/groups/' + data.groupID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                   return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });

        },
        updateApplicationByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .put(APPLICATIONS_URL + '/'+ data.applicationID,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                })
        },
        deleteApplicationByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .delete(APPLICATIONS_URL + '/' + data.applicationID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        addFile : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            Upload
                .upload({
                    url : FILES_URL,
                    data : {
                        file : data.file,
                        groupID : data.groupID,
                        kind : data.kind
                    },
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getFiles : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .get(FILES_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        deleteFileByID : function (fileID,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .delete(FILES_URL + '/' + fileID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getFilesByGroupID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .get(FILES_URL + '/groups/' + data.groupID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                })
        },
        getTeacherInfo : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .get('/api/teachers/self',{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                })
        },
        getListOfGroups : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .get('/api/teachers/lists/groups',{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                })
        },
        getListOfStudentsByGroupID : function (groupID,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .post('/api/teachers/lists/students',{
                    groupID : groupID
                },{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        addAttendance : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .post('/api/teachers/attendance',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    if (response.status === 201) {
                        cb(null);
                    } else {
                        cb(response);
                    }
                },function (response) {
                    cb(response);
                });
        },
        addAppearance : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .post('/api/teachers/appearance',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                   return createResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        addComment : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .post(CHANNEL_URL + '/' + data.postID + '/comments',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        deletePostByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .delete(CHANNEL_URL + '/' + data.postID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updatePostByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            Upload
                .upload({
                    url : CHANNEL_URL + '/' + data.postID,
                    data : {
                        files : data.files,
                        images : data.images,
                        text : data.text,
                        deletingFiles : data.deletingFiles,
                        deletingImages : data.deletingImages,
                        name : [],
                        important : data.important
                    },
                    headers : {
                        token : token
                    },
                    method : 'put'
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response, cb);
                });
        },
        getSchedule : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .get(SCHEDULE_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getHomeworks : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .get(HOMEWORK_URL,{
                    headers : {
                        token : token
                    },
                    params : {
                        groupID : data.groupID
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getApplicationByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .get(APPLICATIONS_URL + '/' + data.applicationID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    getResource(response,cb);
                },function (response) {
                    errorCallback(response,cb);
                });
        },
        updateImage : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            $http
                .post(TEACHER_URL + '/self/image',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateProfile : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            $http
                .put(TEACHER_URL + '/self',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updatePassword : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            $http
                .put(TEACHER_URL + '/self/password',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },

        // START TESTS
        getTests : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(TESTS_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        addTest : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            Upload
                .upload({
                    url : TESTS_URL,
                    headers : {
                        token : token
                    },
                    data : data,
                    method : 'post'
                })
                .then(function (resp) {
                    return createResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        getTestByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(TESTS_URL + '/' + data.testID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return getResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        deleteTestByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .delete(TESTS_URL + '/' +  data.testID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        deleteTestSectionByIndex : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();

            appHelper.addLoader();
            return $http
                .delete(TESTS_URL + '/' + data.testID + '/sections/' + data.sectionIndex,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        assignTest : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .post(TESTS_URL + '/'+ data.testID + '/assign',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getAssignAvailableGroupsForTest : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(TESTS_URL + '/' + data.testID + '/assign',{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateTestByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .put(TESTS_URL + '/' + data.testID ,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateTestSectionByIndex : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            Upload
                .upload({
                    url : TESTS_URL + '/' + data.testID + '/sections/' + data.sectionIndex,
                    headers : {
                        token : token
                    },
                    data : data,
                    method : 'put'
                })
                .then(function (resp) {
                    return updateResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        getTestResults : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(TESTS_URL + '/' + data.testID + '/results',{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getTestResultsByGroupID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(GROUPS_URL  + '/' + data.groupID + '/tests/results',{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getStudentsResultsByGroupID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(GROUPS_URL  + '/' + data.groupID + '/tests/students/results',{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        }
    //    END TESTS

    }
});


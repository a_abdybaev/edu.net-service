app
    .filter("filesByGroupIDFilter",function () {
        return function (files,groupID) {
            if (groupID === '-') return files;
            var results = [];
            files.forEach(function (file) {
                if (file.groupID === groupID) results.push(file);
            });
            return results;
        }
    })
    .filter("applicationsByGroupIDFilter",function () {
        return function (applications,groupID) {
            if (groupID === '-') return applications;
            var results = [];
            applications.forEach(function (application) {
                if (application.groupID._id === groupID) results.push(application);
            });
            return results;
        }
    });





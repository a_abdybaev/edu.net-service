app
    .factory('teacherValidator', function (appValidator) {
        return {
            // NEW!
            isEmpty : function (field) {
                return appValidator.isEmpty(field);
            },
            isValidEmail : function (email) {
                if (appValidator.isEmpty(email)) return true;
                return validator.isEmail(email);
            },
            isValidEmailField : function (ctrl,email) {
                if (this.isValidEmail(email)) {
                    ctrl['emailError'] = false;
                    return true;
                } else {
                    ctrl['emailError'] = true;
                    return false;
                }
            },
            isValidProfile : function (ctrl) {
                appValidator.isValidField(ctrl,'firstName');
                appValidator.isValidField(ctrl,'lastName');
                this.isValidEmailField(ctrl,ctrl.email);
                return this.isValidEmailField(ctrl,ctrl.email) && appValidator.isValidField(ctrl,'lastName') && appValidator.isValidField(ctrl,'firstName');
            },

            isValidMonthNumber : function(monthNumber) {
                return (monthNumber >0 && monthNumber <=11);
            },
            isValidDayNumber : function(dayNumber) {
                return (dayNumber >=1 && dayNumber <=31);
            },


            // FROM OLD VALIDATION TO NEW
            isValidAddHomework : function (ctrl) {
                return appValidator.isValidField(ctrl,'content');
            }
            ,isValidAddAttendanceForm : function ($scope) {
                var students = $scope.students;
                if (students.length > 0) {
                    var result = true;
                    students.forEach(function (student) {
                        if (student.attendance == undefined || student.attendance <0 || student.attendance >2) {
                            result = false;
                        }
                    });
                    return result;
                } else {
                    return false;
                }
            }


        }
    });
app
    .config(function (localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('EduNet')
            .setStorageType('sessionStorage')
            .setNotify(true, true)
    })
    .config(function ($mdDateLocaleProvider) {
        $mdDateLocaleProvider.shortMonths = ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь", "Декабрь"];
        $mdDateLocaleProvider.shortDays = ['Вс','Пн','Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
        $mdDateLocaleProvider.firstDayOfWeek = 1;
        $mdDateLocaleProvider.formatDate = function(date) {
            return moment(date).format('DD-MM-YYYY');
        };
    })
    .run(function($rootScope, $route, $location,$http){
        moment.locale('ru');
    })
;



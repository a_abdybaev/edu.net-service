app
    .factory('teacherModalHelper', function (teacherValues) {
        return {
            deleteMessages : {
                application : {
                    text : "Вы точно хотите удалить текущую заявку",
                    comment : "Текст заявки будет удален навсегда"
                },
                post : {
                    text : "Вы точно хотите удалить текущий пост?",
                    comment : "Контент данного поста будет удален навсегда"
                },
                file : {
                    text : "Вы точно хотите удалить текущий файл?",
                    comment : "Файл будет потерян навсегда"
                },
                homework : {
                    text : "Вы точно хотите удалить домашнее задание?",
                    comment : "Домашнее задание будет удалено безвозвратно"
                }
            },
            successMessages : [{
                value : teacherValues.POST_DELETED,
                message : "Пост успешно удален!"
            },{
                value : teacherValues.COMMENT_CREATED,
                message : "Комментарий успешно добавлен!"
            },{
                value : teacherValues.COMMENT_DELETED,
                message : "Комментарий успешно удален!"
            },{
                value : teacherValues.POST_CREATED,
                message : "Пост успешно добавлен!"
            },{
                value : teacherValues.POST_UPDATED,
                message : "Пост успешно сохранен!"
            },{
                value : teacherValues.APPLICATION_CREATED,
                message : "Заявка успешно добавлена!"
            },{
                value : teacherValues.APPLICATION_UPDATED,
                message : "Заявка успешно сохранена!"
            },{
                value : teacherValues.APPLICATION_DELETED,
                message : "Заявка успешно удалена!"
            },{
                value : teacherValues.ATTENDANCE_DAY_CREATED,
                message : "День посещаемости успешно добавлен!"
            },{
                value : teacherValues.FILE_CREATED,
                message : "Файл успешно добавлен!"
            },{
                value : teacherValues.HOMEWORK_CREATED,
                message : "Домашнее задание успешно добавлено!"
            },{
                value : teacherValues.FILE_DELETED,
                message : "Файл успешно удален!"
            },{
                value : teacherValues.HOMEWORK_DELETED,
                message : "Домашнее успешно удалено!"
            },{
                value : teacherValues.HOMEWORK_UPDATED,
                message : "Домашнее успешно сохранено!"
            }],







            errorMessages : [{
                value : teacherValues.ATTENDANCE_DAY_EXISTED,
                message : 'У выбранной даты посещение уже заполнено!'
            }]

        }
    });
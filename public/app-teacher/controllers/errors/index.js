angular
    .module("EduNet")
    .controller("errorsController", function ($rootScope,$scope,$location) {
        $scope.goToMain = function () {
            $location.path('/');
        };
    });

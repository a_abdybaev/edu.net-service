angular
    .module("EduNet")
    .controller("listOfApplicationsController", function (appValues,appHelper,teacherConstants,ngDialog,teacherHelper,teacherApi,$rootScope,$scope) {
        var addApplication = function () {
            ngDialog.open({
                template : "/app-teacher/views/applications/alerts/add-application.html",
                className : 'ngdialog-theme-default',
                controller : "addApplicationController",
                width : 700
            });
        };
        var loadApplications = function () {
            teacherApi.getApplications(function (err,applications) {
                if (err) return appHelper.showError(err);
                if (!applications) return $scope.noApplications = true;
                self.groups = appHelper.getUserInfo().groups;
                self.applications = applications;
                self.info = appHelper.getUserInfo();
            });
        };
        var editApplicationByID = function (applicationID) {
            ngDialog.open({
                template : "/app-teacher/views/applications/alerts/edit-application.html",
                className : 'ngdialog-theme-default',
                data : {
                    applicationID : applicationID
                },
                width : 700
            });
        };
        var deleteApplicationByID = function (applicationID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deleteApplicationController",
                data : {
                    applicationID : applicationID
                }
            });
        };



        this.editApplicationByID = editApplicationByID;
        this.deleteApplicationByID = deleteApplicationByID;
        this.addApplication = addApplication;
        // INIT
        var self = this;

        this.hasGroups = teacherHelper.getTeacherInfoHasGroups();
        this.applications = [];
        this.noApplications = false;
        this.groups = appHelper.getUserInfo().groups;
        this.activeGroupID = "-";
        $rootScope.$on(appHelper.EVENTS.LOAD_APPLICATIONS,function (event,data) {
            loadApplications();
        });

        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,function () {
            if (this.hasGroups) loadApplications();
        });
        if ($rootScope.indexLoaded == true && this.hasGroups) {
            loadApplications();
        }
    });

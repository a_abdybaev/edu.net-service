angular
    .module("EduNet")
    .controller("addApplicationController", function (teacherValues,appConstants,appValidator,appHelper,teacherConstants,teacherHelper,teacherApi,$rootScope,$scope) {
        var loadListOfGroups = function () {
            teacherApi.getListOfGroups(function (err,groups) {
                if (err || groups.length === 0) {
                    $scope.closeThisDialog();
                    return appHelper.showFailAlertGeneralError();
                }
                self.groups = groups;
                self.selectedGroupID = groups[0]._id;
            });
        };
        var addApplication = function () {
            if(!appValidator.isValidField(self,'content')) return;
            var obj = {
                groupID : self.selectedGroupID,
                content : self.content
            };
            $scope.closeThisDialog();
            teacherApi.addApplication(obj,function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(teacherValues.APPLICATION_CREATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_APPLICATIONS);
            });
        };

        this.addApplication = addApplication;


        // INIT
        var self = this;
        this.groups = [appConstants.LOADING_SELECT_OBJECT];
        this.selectedGroupID = "-";
        loadListOfGroups();
    });

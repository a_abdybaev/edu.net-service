angular
    .module("EduNet")
    .controller("deleteApplicationController", function (teacherValues,appHelper,teacherModalHelper,teacherConstants,$rootScope,teacherApi,$scope) {

        var deleteApplication = function () {
            $scope.closeThisDialog();
            teacherApi.deleteApplicationByID({
                applicationID : applicationID
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(teacherValues.APPLICATION_DELETED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_APPLICATIONS);
            });
        };

        var applicationID = $scope.ngDialogData.applicationID;
        if(!applicationID) return $scope.closeThisDialog();
        var modal = {
            text :  teacherModalHelper.deleteMessages.application.text,
            comment :  teacherModalHelper.deleteMessages.application.comment,
            del : deleteApplication
        };
        $scope.modal = modal;
    });

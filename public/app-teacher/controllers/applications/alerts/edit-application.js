angular
    .module("EduNet")
    .controller("editApplicationController", function (teacherValues,appValidator,appHelper,teacherConstants,teacherHelper,teacherValidator,$rootScope,teacherApi,$scope) {
        var updateApplication = function () {
            if (!appValidator.isValidField(self,'content')) return;
            $scope.closeThisDialog();
            teacherApi.updateApplicationByID({
                applicationID : applicationID,
                content : self.content
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(teacherValues.APPLICATION_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_APPLICATIONS);
            });
        };
        var loadApplicationByID = function (applicationID) {
            teacherApi.getApplicationByID({
                applicationID : applicationID
            },function (err,application) {
                if (err) return appHelper.showError(err);
                self.content = application.content;
            });
        };

        this.updateApplication = updateApplication;

        // INIT
        var self = this;
        var applicationID = $scope.ngDialogData.applicationID;

        if(!applicationID) return $scope.closeThisDialog();
        loadApplicationByID(applicationID);

    });

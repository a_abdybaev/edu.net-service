angular
    .module("EduNet")
    .controller("listOfGroupsController", function (teacherConstants,appHelper,appConstants,teacherHelper,teacherApi,$rootScope,$scope,$location) {
        $scope.noGroups = false;
        $scope.properties = [];
        $scope.displayOption = appConstants.VIEW_STYLES.VIEW_STYLE_BLOCKS;
        $scope.displayOptions = appConstants.VIEW_STYLES;
        var setActiveGroup = function (index) {
            $scope.activeGroupIndex = index;
        };
        var unsetActiveGroup = function (index) {
            $scope.activeGroupIndex = null;
        };
        teacherApi.getGroups(function (err,groups) {
            if (err) return appHelper.showError(err);
            $scope.groups = groups;
            if (_.isEmpty(groups)) return $scope.noGroups = true;
        });
        $scope.goToGroupInfo = function (groupID) {
            appHelper.setActGroupID(groupID);
            $location.path('group-info');
        };

        $scope.changeDisplayOption = function (displayOption) {
            $scope.displayOption = displayOption;
        };
        $scope.setActiveGroup = setActiveGroup;
        $scope.unsetActiveGroup = unsetActiveGroup;
    });

angular
    .module("EduNet")
    .controller("groupInfoController", function (appHelper,teacherHelper,teacherApi,$rootScope,$scope) {
        console.log("HELLO");
        var loadGroupInfo = function (groupID) {
            teacherApi.getGroupByID(groupID, function (err,group) {
                if (err) return appHelper.showError(err);
                self.group = group;
                self.isLoadedGroupInfo = true;
                self.isLoaded = true;
                $rootScope.$broadcast(appHelper.EVENTS.GROUP_LOADED,group);

            });
        };
        // INIT
        var self = this;
        var groupID = appHelper.getActGroupID();
        if (!groupID) appHelper.showError();

        $rootScope.$on(appHelper.EVENTS.LOAD_GROUP,function () {
            self.isLoaded = false;
            groupID = appHelper.getActGroupID();
            if (!groupID) return appHelper.showError();
            loadGroupInfo(groupID);
        });
        loadGroupInfo(groupID);
        this.teacher = appHelper.getUserInfo().teacher;
        this.isLoaded = false;


    });


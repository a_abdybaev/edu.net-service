angular
    .module("EduNet")
    .controller("homeworkInfoController", function (appValues,ngDialog,teacherHelper,$rootScope,$scope) {
        var homework = $scope.ngDialogData.homework;
        console.log(homework);

        $scope.date = teacherHelper.getHomeworkDate(homework);

        $scope.deleteHomework = function () {
            $scope.closeThisDialog();
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                controller : "deleteHomeworkAlertController",
                data : {
                    homeworkID : homework._id
                }
            });
        };

        $scope.homework = homework;


        $scope.editHomework = function () {
            $scope.closeThisDialog();
            ngDialog.open({
                template : "/app-teacher/views/group-info/calendar/alerts/edit-homework.html",
                className : 'ngdialog-theme-default',
                controller : "editHomeworkController",
                data : {
                    homeworkID : homework._id
                },
                width : 700
            });
        };
    });

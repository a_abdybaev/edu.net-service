angular
    .module("EduNet")
    .controller("deleteHomeworkAlertController", function (teacherValues,appHelper,teacherConstants,teacherModalHelper,teacherApi,$rootScope,$scope) {

        var del = function () {
            teacherApi.deleteHomeworkByID(homeworkID,function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(teacherValues.HOMEWORK_DELETED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_HOMEWORK);
            });
        };
        var modal = {
            text :  teacherModalHelper.deleteMessages.homework.text,
            comment : teacherModalHelper.deleteMessages.homework.comment,
            del : del
        };
        $scope.modal = modal;

        // INIT

        var homeworkID = $scope.ngDialogData.homeworkID;
        if (!homeworkID) return appHelper.closeDialogAndShowGenError($scope);
    });

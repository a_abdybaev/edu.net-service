angular
    .module("EduNet")
    .controller("addHomeworkController", function (teacherValues,teacherValidator,appHelper,teacherConstants,teacherHelper,teacherApi,$rootScope,$scope) {
        var data = $scope.ngDialogData;
        $scope.day = data.day;
        $scope.month = data.month;
        var groupID = data.groupID;
        $scope.contentError = false;
        $scope.date = teacherHelper.getHomeworkDate({
            date : {
                day : data.day,
                month : data.month
            }
        });

        $scope.addHomework = function () {
            if (!teacherValidator.isValidAddHomework($scope)) return;
            var obj = {
                day : $scope.day,
                month : $scope.month,
                content : $scope.content,
                groupID : groupID,
                comment : $scope.comment
            };
            teacherApi.addHomework(obj,function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(teacherValues.APPLICATION_CREATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_HOMEWORK);
            });
        };
    });

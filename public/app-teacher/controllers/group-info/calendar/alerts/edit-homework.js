angular
    .module("EduNet")
    .controller("editHomeworkController", function (teacherValues,appValidator,appHelper,teacherConstants,teacherHelper,teacherApi,$rootScope,$scope) {
        var data = $scope.ngDialogData;
        var homeworkID = data.homeworkID;
        $scope.contentError = false;
        teacherApi.getHomeworkByID(homeworkID, function (err,homework) {
            if (err) return appHelper.showError(err);
            $scope.homework = homework;
            $scope.comment = homework.comment;
            $scope.content = homework.content;
            $scope.date = teacherHelper.getHomeworkDate(homework);
        });
        $scope.updateHomework = function () {
            if (!appValidator.isValidField($scope,'content')) return;
            var obj = {
                content : $scope.content
            };
            if (!appValidator.isEmpty($scope.comment)) {
                obj.comment = $scope.comment;
            }
            teacherApi.updateHomeworkByID(homeworkID,obj, function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(teacherValues.HOMEWORK_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_HOMEWORK);
            });
        };
    });

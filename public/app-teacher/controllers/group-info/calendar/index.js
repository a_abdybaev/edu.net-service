angular
    .module("EduNet")
    .controller("calendarControllerGroupInfo", function (ngDialog,teacherConstants,appHelper,appConstants,teacherHelper,teacherApi,$rootScope,$scope,$location) {
        var nextMonth = function () {
            if (self.activeMonthNumber < 11) {
                self.activeMonthNumber = self.activeMonthNumber+1;
            }
        };
        var previousMonth = function () {
            if (self.activeMonthNumber > 0) {
                self.activeMonthNumber =  self.activeMonthNumber-1;
            }
        };
        var loadHomework = function () {
            teacherApi.getHomeworks({
                groupID : ctrl.group._id
            },function (err,homeworks) {
                if (err) return appHelper.showFailAlertGeneralError();
                ctrl.group.homeworks = homeworks;
                getCalendar(ctrl.group)
            });
        };
        var addHomework = function (isLessonDay,day) {
            if (isLessonDay === true) {
                ngDialog.open({
                    template : "/app-teacher/views/group-info/calendar/alerts/add-homework.html",
                    className : 'ngdialog-theme-default',
                    controller : "addHomeworkController",
                    data : {
                        day : day,
                        month : self.activeMonthNumber,
                        groupID : ctrl.group._id
                    },
                    width : 700
                });
            }
        };
        var homeworkInfo = function (month,week,day) {
            var day = self.calendar[month].days[week][day];
            if (day.hasHomework) {
                ngDialog.open({
                    template : "/app-teacher/views/group-info/calendar/alerts/homework-info.html",
                    className : 'ngdialog-theme-default',
                    controller : "homeworkInfoController",
                    data : {
                        homework : day.homework
                    },
                    width : 600
                });
            }
        };
        var getGroupSchedule = function (group) {
            var dayNames = teacherHelper.getDayNames();
            var days = [];

            for (var i =0;i<6;i++) {
                days[i] = {
                    name : dayNames[i],
                    hasLesson : false
                }
            }
            var lessons = group.schedule;
            if (lessons === null || lessons.length === 0) {
                return days;
            } else {
                lessons.forEach(function (lesson) {
                    var index = lesson.day -1;
                    days[index].hasLesson = true;
                    var startTime = new Date();
                    startTime.setHours(lesson.start.hour);
                    startTime.setMinutes(lesson.start.minute);
                    var endTime = new Date();
                    endTime.setHours(lesson.end.hour);
                    endTime.setMinutes(lesson.end.minute);
                    days[index].startTime = startTime;
                    days[index].endTime = endTime;

                    days[index].classroomID = lesson.classroomID;
                });
                return days;
            }
        };
        var getCalendar = function (group) {
            var today = new Date();
            var notThisMonthStyle = "teacher-calendar-item",
                thisMonthStyle = "teacher-calendar-active-item",
                lessonDayStyle = "teacher-calendar-lesson-item";
            var currentMonth;
            self.calendar = [];
            var monthNames = teacherHelper.getMonthNames();
            var lessonDays = getLessonDaysByGroup(group);
            for(var i = 0; i < 12; i++) {
                var date = new Date(2017,i,1);
                var days = [];
                var dayOfWeek = date.getDay();
                if (dayOfWeek == 0) {
                    date.setDate(date.getDate()-6);
                }  else {
                    date.setDate(date.getDate()-dayOfWeek+1);
                }
                self.calendar.push({
                    monthName : monthNames[i],
                    days : []
                });
                for (var k=0;k<6;k++) {
                    self.calendar[i].days.push([]);
                    for (var j = 0; j < 7;j++){
                        var isLessonDay = false;
                        var style = notThisMonthStyle;
                        if (date.getMonth() == i) {
                            style = thisMonthStyle;
                            currentMonth = true;
                        } else {
                            currentMonth = false;
                        }
                        var index = lessonDays.indexOf(j);
                        if (index > -1 && currentMonth === true) {
                            isLessonDay = true;
                            style = lessonDayStyle;
                        }
                        var dayCSS = 'calendar-day-number';
                        if (i == self.today.getMonth() && date.getDate() == today.getDate() && currentMonth == true) {
                            dayCSS = 'calendar-day-number-active calendar-day-number';
                        }
                        self.calendar[i].days[k].push({
                            hasHomework : false,
                            style : style,
                            dayCSS : dayCSS,
                            day : date.getDate(),
                            currentMonth : currentMonth,
                            isLessonDay : isLessonDay
                        });
                        date.setDate(date.getDate()+1);
                    }
                }
            }
            addHomeworksForCalendar(group);
        };
        var addHomeworksForCalendar = function (group) {
            var homeworks = group.homeworks;
            if (homeworks.length >0) {
                homeworks.forEach(function (homework) {
                    var month = homework.date.month,
                        day = homework.date.day;
                    var days = self.calendar[month].days;
                    for (var weekNumber=0;weekNumber<6;weekNumber++) {
                        for (var weekDay = 0;weekDay<7;weekDay++) {
                            var currentDay = days[weekNumber][weekDay];
                            if (currentDay.currentMonth === true && currentDay.day === homework.date.day) {
                                self.calendar[month].days[weekNumber][weekDay].hasHomework = true;
                                self.calendar[month].days[weekNumber][weekDay].homework = homework;
                            }
                        }
                    }
                });
            }
        };
        var getLessonDaysByGroup = function (group) {
            var results = [];
            var lessons = group.schedule;
            if (lessons != undefined && lessons.length != 0) {
                lessons.forEach(function (lesson) {
                    results.push(lesson.day-1);
                });
            }
            return results;
        };

        // INIT
        var self = this;
        var ctrl = $scope.$parent.ctrl;
        this.previousMonth = previousMonth;
        this.nextMonth = nextMonth;
        this.addHomework = addHomework;
        this.homeworkInfo = homeworkInfo;
        var isLoaded = false;
        var initialize = function (group) {
            if (isLoaded) return;
            isLoaded = true;
            self.today = new Date();
            self.schedule = getGroupSchedule(group);
            self.activeMonthNumber = self.today.getMonth();
            getCalendar(group);
            $rootScope.$on(appHelper.EVENTS.LOAD_HOMEWORK,loadHomework);

        };
        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });
        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);



    });

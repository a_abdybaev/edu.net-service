angular
    .module("EduNet")
    .controller("resultsControllerGroupInfo", function (teacherConstants,appHelper,appConstants,teacherHelper,teacherApi,$rootScope,$scope,$location) {
        var setActiveStudent = function (index) {
            if (index == self.studentIndex) return;
            self.studentIndex = index;
            self.student = self.students[index];
            if (self.noResults) return;
            self.studentResults = self.results[index];
        };
        var initialize = function (group) {
            teacherApi.getStudentsResultsByGroupID({
                groupID : group._id
            },function (err,obj) {
                if (err) return appHelper.showError(err);
                self.noResults = obj.noResults;
                self.students = obj.students;
                self.results = obj.results;
                if (_.isEmpty(self.students)) return self.noStudents = true;
                setActiveStudent(0);

            });

        };
        this.setActiveStudent = setActiveStudent;


        // INIT
        var self = this;
        var ctrl = $scope.$parent.ctrl;
        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });



        if (ctrl.isLoaded) initialize(ctrl.group);



    });

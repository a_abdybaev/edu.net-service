angular
    .module("EduNet")
    .controller("applicationsControllerGroupInfo", function (appValues,ngDialog,teacherConstants,appHelper,appConstants,teacherHelper,teacherApi,$rootScope,$scope,$location) {
        var addApplication = function () {
            ngDialog.open({
                template : "/app-teacher/views/group-info/applications/alerts/add-application.html",
                className : 'ngdialog-theme-default',
                width : 700
            });
        };
        var editApplicationByID = function (applicationID) {
            ngDialog.open({
                template : "/app-teacher/views/applications/alerts/edit-application.html",
                className : 'ngdialog-theme-default',
                data : {
                    applicationID : applicationID
                },
                width : 700
            });
        };
        var loadApplications = function () {
            teacherApi.getApplicationsByGroupID({
                groupID : appHelper.getActGroupID()
            },function (err,applications) {
                if (err) return appHelper.showError(err);
                self.applications = applications;
            });
        };
        var deleteApplicationByID = function (applicationID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deleteApplicationController",
                data : {
                    applicationID : applicationID
                }
            });
        };
        this.addApplication = addApplication;
        this.editApplicationByID = editApplicationByID;
        this.deleteApplicationByID = deleteApplicationByID;

        // INIT
        var self = this;
        var ctrl = $scope.$parent.ctrl;
        var isLoaded = false;

        var initialize = function (group) {
            self.applications = group.applications;

        };

        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });
        $rootScope.$on(appHelper.EVENTS.LOAD_APPLICATIONS,function () {
            loadApplications();
        });
        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);

        // $rootScope.$broadcast(appHelper.EVENTS.LOAD_APPLICATIONS);

    });

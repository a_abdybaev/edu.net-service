angular
    .module("EduNet")
    .controller("addApplicationGroupTabController", function (teacherValues,appValidator,appHelper,teacherConstants,teacherHelper,teacherApi,$rootScope,$scope) {
        var addApplication = function () {
            if(!appValidator.isValidField(self,'content')) return;
            var obj = {
                groupID : appHelper.getActGroupID(),
                content : self.content
            };
            teacherApi.addApplication(obj, function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(teacherValues.APPLICATION_CREATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_APPLICATIONS);
            });
        };


        // INIT
        this.addApplication = addApplication;

        var self = this;



    });

angular
    .module("EduNet")
    .controller("deletePostAlertController", function (teacherModalHelper,appHelper,appValues,teacherValues,teacherHelper,$rootScope,teacherApi,$scope) {

        var deletePost = function () {
            $scope.closeThisDialog();
            teacherApi.deletePostByID({
                postID : postID
            }, function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(teacherValues.POST_DELETED);
                $rootScope.$broadcast(appValues.EVENTS.LOAD_CHANNEL);
            });
        };



        // INIT

        var postID = $scope.ngDialogData.postID;
        if (!postID) return appHelper.closeDialogAndShowGenError($scope);
        var modal = {
            text :  teacherModalHelper.deleteMessages.post.text,
            comment : teacherModalHelper.deleteMessages.post.comment,
            del : deletePost
        };
        $scope.modal = modal;






    });

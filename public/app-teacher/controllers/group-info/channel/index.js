var POST_KIND_ALL_POSTS = "ALL_POSTS",
    POST_KIND_HOMEWORKS = "HOMEWORKS",
    POST_KIND_FILES = "FILES",
    POST_KIND_ANOTHER = "ANOTHER",
    POST_KIND_IMPORTANT = "IMPORTANT";

angular
    .module("EduNet")
    .controller("channelControllerGroupInfo", function (appValues,teacherValues,ngDialog,appValidator,teacherConstants,appHelper,appConstants,teacherHelper,teacherApi,$rootScope,$scope,$location) {
        var addChannelFile = function () {
            for (var i=0; i<self.data.attachedFiles.length;i++) {
                if (self.data.attachedFiles[i] === false) {
                    self.data.attachedFiles[i] = true;
                    break;
                }
            }
        };
        var resolveFileName = function (index) {
            if (self.data.files[index] != undefined) {
                self.data.fileNames[index] = self.data.files[index].name;
            } else {
                self.data.fileNames[index] = fileName;
            }
        };
        var deleteFile = function (index) {
            self.data.attachedFiles[index] = false;
            self.data.files[index] = undefined;
            self.data.fileNames[index] = fileName;
        };
        var initAddPostForm = function () {
            return {
                text : "",
                important : false,
                images : [],
                thumbs : [false,false,false,false],
                attachedImages : false,

                files : [],
                attachedFiles : [false,false,false,false],
                fileCount : 0,
                fileNames :[fileName,fileName,fileName,fileName]
            };
        };
        var addPost = function () {
            var text = self.data.text,
                images = self.data.images,
                files = self.data.files;
            if (!appValidator.isValidField(self.data,'text')) return;
            var obj = {
                text : text,
                files : [],
                groupID :ctrl.group._id,
                images : [],
                important : self.data.important
            };
            if (images.length != 0) {
                images.forEach(function (image) {
                    obj.images.push(image);
                });
            }
            if (files.length != 0) {
                files.forEach(function (file) {
                    if (file != undefined) {
                        obj.files.push(file);
                    }
                });
            }
            teacherApi.addPost(obj, function (err) {
                if (err && err.status != 406) return appHelper.showFailAlertGeneralError();
                if (err && err.status == 406) return appHelper.showFailAlert(appValues.TARIFF_ERROR_MEMORY);
                appHelper.showSuccessAlert(teacherValues.POST_CREATED);
                self.processAddPost();

            });
        };
        var showImages = function () {
            self.data.attachedImages = true;
        };
        var hideImages = function () {
           self.data.attachedImages = false;
        };
        var showThumb = function (index) {
            self.data.thumbs[index] = true;
        };
        var readMore = function (index) {
            self.limits[index] = LONGLIMIT;
            self.moreButtons[index] = false;
        };
        var showComments = function (index) {
            self.posts[index].showComments = true;
        };
        var hideComments = function (index) {
            self.posts[index].showComments = false;
        };

        var addComment = function (index) {
            var data = {
                content : self.posts[index].comment,
                postID : self.posts[index]._id,
                groupID : teacherHelper.getActiveGroupID(),
                isReply : !!self.posts[index].isReply
            };
            if (!appValidator.isValidField(self.posts[index],'comment')) return;
            if (data.isReply) data.replyCommentID = self.posts[index].replyCommentID;
            teacherApi.addComment(data,function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(teacherValues.COMMENT_CREATED);
                self.posts[index].comment = null;
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_CHANNEL);

            });
        };
        var showPostImageDetailed = function (image) {
            ngDialog.open({
                template : "/app-teacher/views/group-info/channel/alerts/post-image-view.html",
                className : 'ngdialog-theme-default',
                appendClassName : 'post-image-view-modal',
                controller : "postImageDetailedAlertController",
                width : 800,
                data : {
                    image : image
                }
            });
        };
        var editPostClick = function (index) {
            editPost(self.posts[index]);
            ctrl.isEditPost = true;
        };
        var deletePost = function (index) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deletePostAlertController",
                data : {
                    postID : self.posts[index]._id
                }
            });
        };
        var loadMorePosts = function () {
            if (self.noAvailablePosts) return;
            query.page++;
            loadPosts();

        };
        var loadPostsByType = function (index) {
            if (CURRENT_POST_TYPE == index) return;
            self.postTypes[index].style = style;
            self.postTypes[CURRENT_POST_TYPE].style = null;
            CURRENT_POST_TYPE = index;
            query = Object.assign({},self.postTypes[index].query);
            query.page = 0;
            self.posts = [];
            self.moreButtons = [];
            self.limits = [];
            self.noAvailablePosts = false;
            loadPosts();
        };
        var processAddPost = function () {
            //LOAD POST
            self.data = initAddPostForm();
            self.postTypes[0].style = style;
            self.postTypes[CURRENT_POST_TYPE].style = null;
            CURRENT_POST_TYPE = 0;
            query = Object.assign({},self.postTypes[0].query);
            query.page = 0;
            self.posts = [];
            self.moreButtons = [];
            self.limits = [];
            self.noAvailablePosts = false;
            loadPosts();
        };

        var loadPosts = function () {
            if (self.noAvailablePosts) return;
            query.groupID = ctrl.group._id;
            teacherApi.getPosts(query,function (err,posts) {
                if (err) return appHelper.showError(err);
                if (posts.length == 0 || posts.length != appConstants.POSTS_PER_PAGE) self.noAvailablePosts = true;
                if (posts.length == 0) return;

                var data = buildPosts(posts);
                self.posts = self.posts.concat(data.posts);
                self.moreButtons = self.moreButtons.concat(data.moreButtons);
                self.limits = self.limits.concat(data.limits);
            });

        };
        var resolveComments = function (post) {
        };
        var buildPosts = function (posts) {

            var limits = [];
            var moreButtons = [];
            posts.forEach(function (post) {
                limits.push(SHORTLIMIT);
                resolveComments(post);
                if (post.homeworkID == null) {
                    if (post.text.length > SHORTLIMIT) {
                        moreButtons.push(true);
                    } else {
                        moreButtons.push(false);
                    }
                    if (post.images.length === 0) {
                        post.hasImages = false;
                    } else if (post.images.length === 1) {
                        post.hasImages = true;
                        post.hasOneImage = true;
                        post.image = {
                            original_url : post.images[0].url
                        };
                    } else {
                        post.hasImages = true;
                        post.gridImages = [];
                        post.images.forEach(function (image) {
                            post.gridImages.push({
                                original_url : image.url
                            });
                        });
                    }
                } else {
                    moreButtons.push(false);
                }
            });
            return {
                moreButtons : moreButtons,
                limits : limits,
                posts : posts
            };
        };
        var editPost = function (post) {

            var editPost = JSON.parse(JSON.stringify(post));
            var initEditPostForm = function () {
                return {
                    text : editPost.text,
                    images : [],
                    important : editPost.important,
                    thumbs : [false,false,false,false],
                    attachedImages : false,
                    files : editPost.files,
                    attachedFiles  : [false,false,false,false],
                    fileNames : [fileName,fileName,fileName,fileName],
                    hasFiles : [false,false,false,false],
                    hasImages : [false,false,false,false],
                    updatedImages : [false,false,false,false],
                    updatedFiles : [false,false,false,false]
                };
            };
            self.editPost = initEditPostForm();
            if (post.files.length >0) {
                editPost.files.forEach(function (file,i) {
                    self.editPost.attachedFiles[i] = true;
                    self.editPost.fileNames[i] = file.name;
                });
            }

            if (post.images.length != 0) {
                self.editPost.attachedImages = true;
                editPost.images.forEach(function (image,i) {
                    self.editPost.thumbs[i] = true;
                });
                self.editPost.images = editPost.images;

            } else {
                self.editPost.attachedImages = false;
            }

            self.editPost.deleteFile = function (index) {
                self.editPost.attachedFiles[index] = false;
                self.editPost.files[index] = undefined;
                self.editPost.fileNames[index] = fileName;
            };
            self.editPost.deleteImage = function (index) {
                self.editPost.thumbs[index] = false;
                self.editPost.images[index] = null;
            };
            self.editPost.showThumb = function (index) {
                self.editPost.thumbs[index] = true;
            };
            self.editPost.resolveFileName = function (index) {
                if (self.editPost.files[index] != undefined) {
                    self.editPost.fileNames[index] = self.editPost.files[index].name;
                } else {
                    self.editPost.fileNames[index] = fileName;
                }
            };
            self.editPost.addChannelFile = function () {
                for (var i=0; i<self.editPost.attachedFiles.length;i++) {
                    if (self.editPost.attachedFiles[i] === false) {
                        self.editPost.attachedFiles[i] = true;
                        break;
                    }
                }
            };
            self.editPost.showImages = function () {
                self.editPost.attachedImages = true;
            };
            self.updatePost = function () {
                var text = self.editPost.text,
                    images = self.editPost.images,
                    files = self.editPost.files;
                if (!appValidator.isValidField(self.editPost,'text')) return;
                var obj = {
                    text : text,
                    files : [],
                    groupID : post.groupID,
                    important : self.editPost.important,
                    images : [],
                    deletingImages : [],
                    deletingFiles : [],
                    postID : post._id
                };
                if (post.images.length != 0) {
                    post.images.forEach(function (image,i) {
                        if (images[i] == undefined || images[i]._id == undefined) {
                            obj.deletingImages.push(image._id);
                        }
                    });
                }
                if (post.files.length != 0) {
                    post.files.forEach(function (file,i) {
                        if (files[i] == undefined || files[i]._id == undefined) {
                            obj.deletingFiles.push(file._id);
                        }
                    });
                }
                if (images.length != 0) {
                    images.forEach(function (image) {
                        if (image != undefined && image._id == undefined) {
                            obj.images.push(image);
                        }
                    });
                }
                if (files.length != 0) {
                    files.forEach(function (file) {
                        if (file != undefined && file._id == undefined) {
                            obj.files.push(file);
                        }
                    });
                }
                teacherApi.updatePostByID(obj,function (err) {
                    if (err && err.status != 406) return appHelper.showFailAlertGeneralError();
                    if (err && err.status == 406) return appHelper.showFailAlert(appValues.TARIFF_ERROR_MEMORY);
                    appHelper.showSuccessAlert(teacherValues.POST_UPDATED);
                    ctrl.isEditPost = false;
                    $rootScope.$broadcast(appHelper.EVENTS.LOAD_CHANNEL);
                });
            };
            self.cancelEditPost = function () {
                ctrl.isEditPost = false;
            };
        };
        var answerComment = function (postIndex,commentIndex) {
            var comment = self.posts[postIndex].comment;
            self.posts[postIndex].isReply = true;
            self.posts[postIndex].replyCommentID = self.posts[postIndex].comments[commentIndex]._id;
            if (!_.isEmpty(comment)) return;
            self.posts[postIndex].comment = self.posts[postIndex].comments[commentIndex].user.value.firstName + ", ";
        };

        this.addChannelFile = addChannelFile;
        this.resolveFileName = resolveFileName;
        this.deleteFile = deleteFile;
        this.addPost = addPost;
        this.showImages = showImages;
        this.hideImages = hideImages;
        this.showThumb = showThumb;
        this.readMore = readMore;
        this.showComments = showComments;
        this.hideComments = hideComments;
        this.addComment = addComment;
        this.showPostImageDetailed = showPostImageDetailed;
        this.editPostClick = editPostClick;
        this.deletePost = deletePost;
        this.loadMorePosts = loadMorePosts;
        this.loadPostsByType = loadPostsByType;
        this.processAddPost = processAddPost;
        this.answerComment = answerComment;



        // INIT
        var self = this;
        var ctrl = $scope.$parent.ctrl;
        var fileName = appHelper.getFileEmptyName();

        this.TEACHER = appConstants.TEACHER;
        this.userID = appHelper.getUserInfo().teacher._id;
        this.STUDENT_AVATAR_DEFAULT_URL = appConstants.STUDENT_AVATAR_DEFAULT_URL;
        this.imageUrl = appHelper.getUserInfo().teacher.imageUrl || appConstants.TEACHER_AVATAR_DEFAULT_URL;



        var query = {
            page : 0
        };
        var CURRENT_POST_TYPE = 0;
        var style = {
            'background-color' : '#f5f9fc',
            'border-left' : '2px solid #2196F3'
        };
        self.postTypes = [{
            POST_TYPE : POST_KIND_ALL_POSTS,
            query : {

            },
            style : style
        },{
            POST_TYPE : POST_KIND_HOMEWORKS,
            query : {
                homework : true

            }
        },{
            POST_TYPE : POST_KIND_FILES,
            query : {
                onlyFiles : true
            }
        },{
            POST_TYPE : POST_KIND_IMPORTANT,
            query : {
                important : true
            }
        },{
            POST_TYPE : POST_KIND_ANOTHER,
            query : {
                another : true
            }
        }];



        $rootScope.$on(appHelper.EVENTS.LOAD_CHANNEL,function () {
            self.data = initAddPostForm();
            self.postTypes[0].style = style;
            self.postTypes[CURRENT_POST_TYPE].style = null;
            CURRENT_POST_TYPE = 0;
            query = Object.assign({},self.postTypes[0].query);
            query.page = 0;
            self.posts = [];
            self.moreButtons = [];
            self.limits = [];
            self.noAvailablePosts = false;
            loadPosts();
        });
        var initialize = function (group) {
            ctrl.isEditPost = false;
            var data = buildPosts(group.channel);
            self.posts = data.posts;
            self.limits = data.limits;
            self.moreButtons = data.moreButtons;
            self.data = initAddPostForm();
            self.gridOptions = {
                    onClicked : function(image) {
                        showPostImageDetailed(image);
                    }
                };
            if (data.posts.length  != appConstants.POSTS_PER_PAGE) self.noAvailablePosts = true;
            else self.noAvailablePosts = false;

        };
        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });
        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);



    });

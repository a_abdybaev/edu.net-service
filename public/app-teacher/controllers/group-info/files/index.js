angular
    .module("EduNet")
    .controller("filesControllerGroupInfo", function (appValues,ngDialog,teacherConstants,appHelper,appConstants,teacherHelper,teacherApi,$rootScope,$scope) {
        var addFile = function () {
            ngDialog.open({
                template : "/app-teacher/views/group-info/files/alerts/add-file.html",
                className : 'ngdialog-theme-default',
                width : 700
            });
        };
        var deleteFileByID = function (fileID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deleteFileAlertForGroupController",
                data : {
                    fileID : fileID
                }
            });
        };
        var loadFiles = function () {
            teacherApi.getFilesByGroupID({
                groupID : appHelper.getActGroupID()
            },function (err,files) {
                if (err) return appHelper.showError(err);
                self.files = files;
            });
        };
        this.addFile = addFile;
        this.deleteFileByID = deleteFileByID;



        // INIT
        var self = this;
        var ctrl = $scope.$parent.ctrl;
        var isLoaded = false;

        var initialize = function (group) {
            self.files = group.files;
        };

        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });
        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);
        $rootScope.$on(appHelper.EVENTS.LOAD_FILES,function () {
            loadFiles();

        });
        // $rootScope.$broadcast(appHelper.EVENTS.LOAD_FILES);



    });

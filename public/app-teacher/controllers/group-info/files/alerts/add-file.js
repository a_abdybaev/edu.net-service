angular
    .module("EduNet")
    .controller("addFileControllerGroupInfo", function (appValues,appHelper,teacherValues,teacherHelper,teacherApi,$rootScope,$scope) {
        var addFile = function () {
            //NEED VALIDATION
            if (!self.file) return;
            $scope.closeThisDialog();

            teacherApi.addFile({
                groupID :appHelper.getActGroupID(),
                file : self.file,
                kind : self.selectedFileKind
            },function (err) {
                if (err && err.status != 406) return appHelper.showFailAlertGeneralError();
                if (err && err.status == 406) return appHelper.showFailAlert(appValues.TARIFF_ERROR_MEMORY);
                appHelper.showSuccessAlert(teacherValues.FILE_CREATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_FILES);
            });
        };
        var resolveFileName = function () {
            if (self.file) self.fileName = self.file.name;
        };


        // INIT

        this.addFile = addFile;
        this.resolveFileName = resolveFileName;


        var self = this;
        this.fileName = appHelper.getFileEmptyName();
        this.kinds = appHelper.getFileKinds();
        this.selectedFileKind = this.kinds[0].value;
    });

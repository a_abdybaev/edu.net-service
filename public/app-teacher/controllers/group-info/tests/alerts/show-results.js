angular
    .module("EduNet")
    .controller("testResultsController", function (appHelper,teacherConstants,teacherHelper,teacherApi,$rootScope,$scope) {

        var loadTestResults = function (testID) {
            teacherApi.getTestResults({
                groupID : teacherHelper.getActiveGroupID(),
                testID : testID
            },function (err,data) {
                if (err) return appHelper.showFailAlertGeneralError();
                self.results = data;
                if (_.isEmpty(data)) return self.noResults = true;
                var sectionsNumber = data[0].sections;
                self.count = self.results.length;
                // for (var i=0; i < sectionsNumber;i++) {
                //     _.each(data[0].sections[0].results,function (section,) {
                //
                //     })
                // }


                self.statistics = [];
                _.each(data[0].sections,function (section,sectionIndex) {
                    self.statistics.push({
                        sectionNumber : section.sectionNumber,
                        answers : []
                    });
                    _.each(section.results,function (result) {
                        // self.statistics[sectionIndex].answers.push({
                        //     number : result.number,
                        //     rightAnswers : 0,
                        //     wrongAnswers : 0
                        // });
                        self.statistics[sectionIndex].answers[result.number-1] = {
                            right : 0,
                            wrong : 0,
                            number : result.number
                        }
                    });
                    _.each(data,function (testResult,testResultIndex) {
                        _.each(testResult.sections[sectionIndex].results,function (question,questionIndex) {
                           if (question.value) self.statistics[sectionIndex].answers[question.number-1].right++;
                           else self.statistics[sectionIndex].answers[question.number-1].wrong++;
                        });
                    });
                });









                _.each(data,function (testResult,testResultIndex) {
                    _.each(testResult.sections,function (section,sectionIndex) {
                        _.each(section.results,function (question,questionIndex) {
                            
                        })
                    })
                })


            })
        };
        // INIT
        var self = this;
        var test = $scope.ngDialogData.test;
        if (!test) return $scope.closeThisDialog();
        self.test = test;
        loadTestResults(test._id)
    });

angular
    .module("EduNet")
    .controller("testsControllerGroupInfo", function (ngDialog,teacherConstants,appHelper,appConstants,teacherHelper,teacherApi,$rootScope,$scope,$location) {
        var showResults = function (test) {
            ngDialog.open({
                template : "/app-teacher/views/group-info/tests/alerts/show-results.html",
                className : 'ngdialog-theme-default',
                width : 720,
                data : {
                    test : test
                }
            });
        };
        var deleteTest = function (testID) {

        };
        var initialize = function (group) {
            self.hasTests = !_.isEmpty(group.tests);
        };

        var self = this;
        this.showResults = showResults;
        this.deleteTest = deleteTest;
        var ctrl = $scope.$parent.ctrl;

        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });
        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);

    });

angular
    .module("EduNet")
    .controller("attendanceControllerGroupInfo", function (teacherConstants,appHelper,appConstants,teacherHelper,teacherApi,$rootScope,$scope,$location) {
        var buildAppearance = function (students) {
            var self = this;
            students.forEach(function (student) {
                student.calendar =  getAllDaysArray();
                if (student.appearance.length == 0) return;
                student.hasAttendance = true;
                student.appearance.forEach(function (appearance) {
                    student.calendar[appearance.month].hasAttendance = true;
                    student.calendar[appearance.month].days[appearance.day] = appearance.value;
                });
            });
            return students;
        };
        var buildStudents = function (students,attendances) {
            var self = this;
            var results = [];
            students.forEach(function (student,i) {
                student.hasAttendance = false;
                results.push(student);
                results[i].appearance = getAllDaysArray();
                attendances.forEach(function (attendance) {
                    var index = attendance.appearance.map(function (appearance) {
                        return appearance.studentID;
                    }).indexOf(student._id);
                    if (index < 0) return;
                    var value = attendance.appearance[index].value,
                        day = attendance.day,
                        month = attendance.month;
                    results[i].appearance[month].days[day] = value;
                    results[i].appearance[month].hasAttendance = true;
                    results[i].hasAttendance = true;
                });
            });
            return students;
        };
        var getAllDaysArray = function () {
            var months = appHelper.getMonthNamesWithYearLowerCase();
            var results = [];
            var year = 2017;
            months.forEach(function (month,i) {
                var num = 0;
                results.push({
                    name : month,
                    days : [],
                    hasAttendance : false
                });
                var daysInArray = new Date(year,i+1,0).getDate();
                for (var j = 1; j <= daysInArray;j++) {
                    results[i].days[j] = appConstants.STUDENT_APPEARANCE_EMPTY_DAY;
                }
            });
            return results;
        };
        var changeActiveStudentIndex = function (index) {
            if (index == self.activeStudentIndex) return;
            self.activeStudentIndex = index;
        };
        var addAttendanceDay = function () {
            $location.path('add-attendance');
        };
        var initialize = function (group) {
            // INIT
            if (group.students.length == 0) return self.noStudents = true;
            self.noStudents = false;
            self.students = buildAppearance(group.students);
            self.changeActiveStudentIndex(0);
        };
        // INIT
        var self = this;
        var ctrl = $scope.$parent.ctrl;
        self.STUDENT_APPEARANCE_EMPTY_DAY = appConstants.STUDENT_APPEARANCE_EMPTY_DAY;
        self.STUDENT_APPEARANCE_PRESENCE = appConstants.STUDENT_APPEARANCE_PRESENCE;
        self.STUDENT_APPEARANCE_ABSENCE = appConstants.STUDENT_APPEARANCE_ABSENCE;
        self.STUDENT_APPEARANCE_ABSENCE_REASON = appConstants.STUDENT_APPEARANCE_ABSENCE_REASON;
        this.addAttendanceDay = addAttendanceDay;
        this.changeActiveStudentIndex = changeActiveStudentIndex;
        self.noStudents = true;

        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });
        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);



    });

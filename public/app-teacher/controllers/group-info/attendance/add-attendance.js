angular
    .module("EduNet")
    .controller("addAttendanceDayAlertController", function (teacherValues,appConstants,teacherValidator,appHelper,teacherConstants,ngDialog,teacherHelper,teacherApi,$scope,$location) {
        var groupID = appHelper.getActGroupID();
        $scope.myDate = new Date();
        $scope.isOpen = false;

        $scope.STUDENT_APPEARANCE_PRESENCE = appConstants.STUDENT_APPEARANCE_PRESENCE;
        $scope.STUDENT_APPEARANCE_ABSENCE = appConstants.STUDENT_APPEARANCE_ABSENCE;
        $scope.STUDENT_APPEARANCE_ABSENCE_REASON = appConstants.STUDENT_APPEARANCE_ABSENCE_REASON;


        teacherApi.getListOfStudentsByGroupID(groupID,function (err,students) {
            if (err) return appHelper.showError(err);
            students = appHelper.getNamesFromUsers(students);
            $scope.students = appHelper.getNamesFromUsers(students);
            teacherHelper.getListOfStudentsForAttendanceAddForm(students)
        });

        $scope.setAttendance = function (index,value) {
            $scope.students[index].attendance = value;
        };

        $scope.addAttendance = function () {
            if (!teacherValidator.isValidAddAttendanceForm($scope)) return;
            var data = {
                appearance : teacherHelper.prepareStudentsForAttendance($scope.students),
                day : $scope.myDate.getDate(),
                month : $scope.myDate.getMonth(),
                groupID : groupID
            };
            teacherApi.addAppearance(data,function (err) {
                if (err) {
                    if (err.status == 406) return appHelper.showFailAlert(teacherValues.ATTENDANCE_DAY_EXISTED);
                    return appHelper.showFailAlertGeneralError();
                }
                appHelper.showSuccessAlert(teacherValues.ATTENDANCE_DAY_CREATED);
                $location.path("/group-info");
            });
        };

        $scope.cancel = function () {
            $location.path('group-info');
        };
    });

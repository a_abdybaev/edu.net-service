angular
    .module("EduNet")
    .controller("aboutControllerGroupInfo", function (teacherConstants,appHelper,appConstants,teacherHelper,teacherApi,$rootScope,$scope,$location) {
        var buildHomeworks = function (group) {
            group.homeworks.forEach(function (homework) {
                homework.onDate = new Date();
                homework.onDate.setMonth(homework.date.month);
                homework.onDate.setDate(homework.date.day);
            });
        };
        var buildSchedule = function (group) {
            var weekDays = appHelper.getWeekDaysWithoutSundayDefault();
            var results = [];
            weekDays.forEach(function (weekDay) {
                results.push({
                    name : weekDay,
                    hasLesson : false
                });
            });
            if (group.schedule.length == 0) return;
            group.schedule.forEach(function (lesson) {
                results[lesson.day-1].hasLesson = true;
                var startTime = new Date(),
                    endTime = new Date();
                startTime.setHours(lesson.start.hour);
                startTime.setMinutes(lesson.start.minute);
                endTime.setHours(lesson.end.hour);
                endTime.setMinutes(lesson.end.minute);

                results[lesson.day-1].lesson = {
                    classroomID : lesson.classroomID,
                    startTime : startTime,
                    endTime : endTime
                }
            });
            return results;
        };

        // INIT
        var self = this;
        var ctrl = $scope.$parent.ctrl;
        var isLoaded = false;
        var initialize = function (group) {
            if (isLoaded) return;
            isLoaded = true;
            self.schedule = buildSchedule(group);
            self.students  = group.students;
            self.studentAvatar = appConstants.STUDENT_AVATAR_DEFAULT_URL;
            self.homeworks = buildHomeworks(group);
        };
        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {

            initialize(group);
        });
        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);



    });

angular
    .module("EduNet")
    .controller("testsController", function ($location,appHelper,ngDialog,teacherHelper,teacherApi) {
        var loadTests = function () {
            teacherApi.getTests(function (err,tests) {
                if (err) return appHelper.showFailAlertGeneralError();
                if (_.isEmpty(tests)) return self.noTests = true;
                //    compute total duration and questions count
                _.each(tests,function (test) {
                    test.duration = 0;
                    test.questionsCount = 0;
                    _.each(test.sections,function (section) {
                        test.duration +=section.duration;
                        test.questionsCount +=section.questionsCount;
                    });
                });
                self.tests = tests;
            })
        };
        var assignTest = function (testID) {
            ngDialog.open({
                template : "/app-teacher/views/tests/alerts/assign-test.html",
                className : 'ngdialog-theme-default',
                width : 520,
                data : {
                    testID : testID
                }
            });
        };
        var addTest = function () {
            $location.path('add-test');
        };
        var goToTestInfo = function (testID) {
            teacherHelper.setActiveTestID(testID);
            $location.path('test-info');
        };


        var self = this;
        this.addTest = addTest;
        this.assignTest = assignTest;
        this.goToTestInfo = goToTestInfo;
        loadTests();
    });

angular
    .module("EduNet")
    .controller("addTestSecondStepController", function (testValidator,$scope,$location,appHelper,ngDialog,teacherHelper,teacherApi) {
        var cancel = function () {
            ctrl.isFirstStep = true;
            ctrl.isThirdStep = false;
            ctrl.isSecondStep = false;
        };
        var next = function () {
            if(!testValidator.isValidSecondStep(ctrl)) return;
            ctrl.isFirstStep = false;
            ctrl.isThirdStep = true;
            ctrl.isSecondStep = false;
        };

        // init
        var self = this;
        var ctrl = $scope.$parent.ctrl;


        this.next = next;
        this.cancel = cancel;
        // next();

    });

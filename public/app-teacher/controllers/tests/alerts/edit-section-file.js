angular
    .module("EduNet")
    .controller("editSectionFileController", function (appValues,$rootScope,appConstants,$scope,$location,appHelper,ngDialog,teacherHelper,teacherApi) {
        var updateFile = function () {
            if (!self.file) return;
            var data = {
                file : self.file,
                testID : testID,
                sectionIndex : sectionIndex,
                pagesFrom : parseInt(self.pagesFrom),
                pagesTo : parseInt(self.pagesTo)
            };
            if (!data.file || isNaN(data.pagesFrom) || isNaN(data.pagesTo)) return;
            $scope.closeThisDialog();
            teacherApi.updateTestSectionByIndex(data,function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(appValues.EVENTS.TEST_SECTION_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_TEST)
            });
        };



        var self = this;
        var testID = $scope.ngDialogData.testID,
            sectionIndex = $scope.ngDialogData.sectionIndex;
        if (!testID || sectionIndex == null) return $scope.closeThisDialog();
        this.updateFile = updateFile;

    });






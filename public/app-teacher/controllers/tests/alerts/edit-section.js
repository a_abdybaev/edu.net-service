angular
    .module("EduNet")
    .controller("editSectionController", function (appValues,$rootScope,appConstants,$scope,$location,appHelper,ngDialog,teacherHelper,teacherApi) {
        var loadTestByID = function (testID) {
            teacherApi.getTestByID({
                testID : testID
            },function (err,test) {
                if (err) {
                    appHelper.showFailAlertGeneralError();
                    $scope.closeThisDialog();
                }
                self.duration = test.sections[sectionIndex].duration / 60;
                self.questionsCount = test.sections[sectionIndex].questionsCount;
                self.test = test;

            });
        };
        var udpateSection = function () {
            var data = {
                duration : self.duration,
                testID : testID,
                sectionIndex : sectionIndex
            };
            if (!data.duration || isNaN(parseInt(data.duration)) || !self.questionsCount || isNaN(parseInt(self.questionsCount))) return;
            if (parseInt(self.questionsCount) == self.test.sections[sectionIndex].questionsCount) {
                teacherApi.updateTestSectionByIndex(data,function (err) {
                    $scope.closeThisDialog();
                    if (err) return appHelper.showFailAlertGeneralError();
                    appHelper.showSuccessAlert(appValues.TEST_SECTION_UPDATED);
                    $rootScope.$broadcast(appHelper.EVENTS.LOAD_TEST);
                });
            } else {
                $scope.closeThisDialog();
                data.questionsCount = self.questionsCount;
                ngDialog.open({
                    template : "/app-teacher/views/tests/alerts/edit-section-answers.html",
                    className : 'ngdialog-theme-default',
                    width : 900,
                    data : data
                });
            }
        };

        //     INIT
        var self = this,
            testID = $scope.ngDialogData.testID,
            sectionIndex = $scope.ngDialogData.sectionIndex;
        this.udpateSection = udpateSection;

        if (!testID) return $scope.closeThisDialog();
        loadTestByID(testID);
    });






"use strict";

angular
    .module("EduNet")
    .controller("deleteTestController", function (appValues,$scope,$location,appHelper,ngDialog,appModalHelper,teacherApi) {
        var deleteTest = function () {
            $scope.closeThisDialog();
            teacherApi.deleteTestByID({
                testID : testID
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError(err);
                appHelper.showSuccessAlert(appValues.TEST_DELETED);
                $location.path('/list-of-tests');

            })
        };
        //     INIT
        var modal = {
            text :  appModalHelper.deleteMessages.test.text,
            comment : appModalHelper.deleteMessages.test.comment,
            del : deleteTest
        };
        var testID = $scope.ngDialogData.testID;
        if (!testID) return $scope.closeThisDialog();
        $scope.modal = modal;
    });
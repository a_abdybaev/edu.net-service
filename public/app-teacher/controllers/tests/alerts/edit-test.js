angular
    .module("EduNet")
    .controller("editTestController", function (appValues,$rootScope,appValidator,appConstants,$scope,$location,appHelper,ngDialog,teacherHelper,teacherApi) {
        var editTest = function () {
            var data = {
                name : self.test.name,
                testID : testID
            };
            if (!appValidator.isValidField(self.test,'name')) return;
            teacherApi.updateTestByID(data,function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(appValues.TEST_UPDATED);
                $rootScope.$broadcast(appValues.EVENTS.LOAD_TEST);
            });
        };
        var loadTestByID = function (testID) {
            teacherApi.getTestByID({
                testID : testID
            },function (err,test) {
                if (err) {
                    appHelper.showFailAlertGeneralError();
                    $scope.closeThisDialog();
                    return;
                }
                self.test = test;
            });
        };
        // INIT
        var self = this;
        var testID = $scope.ngDialogData.testID;
        if (!testID) return $scope.closeThisDialog();
        this.editTest = editTest;
        loadTestByID(testID);
    });
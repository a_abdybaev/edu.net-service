"use strict";

angular
    .module("EduNet")
    .controller("deleteTestSectionController", function (appValues,$rootScope,appConstants,$scope,$location,appHelper,ngDialog,teacherHelper,teacherApi) {
        var deleteTestSection = function () {
            teacherApi.deleteTestSectionByIndex({
                sectionIndex : sectionIndex,
                testID : testID
            },function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError(err);
                appHelper.showSuccessAlert(appValues.TEST_SECTION_DELETED);
                $rootScope.$broadcast(appValues.EVENTS.LOAD_TEST);

            })
        };
        //     INIT

        var modal = {
            text :  teacherHelper.delMessage.applicationAnswers.text,
            comment : teacherHelper.delMessage.applicationAnswers.comment,
            del : deleteTestSection
        };

        var sectionIndex = $scope.ngDialogData.sectionIndex,
            testID = $scope.ngDialogData.testID;
        if (!_.isNumber(sectionIndex) || !testID) return $scope.closeThisDialog();
        $scope.delete = deleteTestSection;
    });
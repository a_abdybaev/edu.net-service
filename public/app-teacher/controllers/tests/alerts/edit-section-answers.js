angular
    .module("EduNet")
    .controller("editSectionAnswersController", function (appValues,$rootScope,testValidator,appConstants,$scope,$location,appHelper,ngDialog,teacherHelper,teacherApi) {
        var loadTestByID = function (testID) {
            teacherApi.getTestByID({
                testID : testID
            },function (err,test) {
                if (err) {
                    appHelper.showFailAlertGeneralError();
                    $scope.closeThisDialog();
                }
                var answers = test.sections[sectionIndex].answers;

                _.each(answers,function (answer) {
                    answer.value = "";
                    _.each(answer.values,function (value,i) {
                        answer.value +=value;
                        if (i < answer.values.length-1) {
                            answer.value+= "\r\n";
                        }
                    });
                });
                if (questionsCount) {
                    if (questionsCount > answers.length) {
                        self.alert = true;
                        self.oldAnswersCount = answers.length + 1;
                        for (var i = answers.length; i < questionsCount;i++) {
                            answers.push({
                                value : ""
                            })
                        }
                    }
                    if (questionsCount < answers.length) {
                        answers.splice(questionsCount,answers.length-questionsCount);
                    }
                }
                self.answers = answers;

            });
        };
        var updateTestSectionByIndex = function () {
            if(!testValidator.isValidUpdateTestSectionAnswers(self)) return;
            var answers = _.each(self.answers,function (answer) {
                return answer.value;
            });
            teacherApi.updateTestSectionByIndex({
                answers : answers,
                testID : testID,
                sectionIndex : sectionIndex,
                duration : duration
            },function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(appValues.TEST_SECTION_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_TEST);
            });
        };

    //     INIT
        this.updateTestSectionByIndex = updateTestSectionByIndex;
        var self = this,
            testID = $scope.ngDialogData.testID,
            sectionIndex = $scope.ngDialogData.sectionIndex,
            duration = $scope.ngDialogData.duration,
            questionsCount = parseInt($scope.ngDialogData.questionsCount);
        if (!testID) return $scope.closeThisDialog();
        loadTestByID(testID);


    });






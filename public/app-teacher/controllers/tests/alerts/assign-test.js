angular
    .module("EduNet")
    .controller("assignTest", function (appValues,appConstants,$scope,$location,appHelper,ngDialog,teacherHelper,teacherApi) {
       var loadAvailableGroupsForTest = function (testID) {
           teacherApi.getAssignAvailableGroupsForTest({
               testID : testID
           },function (err,groups) {
               if (err) {
                   $scope.closeThisDialog();
                   appHelper.showFailAlertGeneralError();
               } else {
                   if (groups.length == 0) return self.noGroups = true;
                   self.noGroups = false;
                   self.groups = groups;
                   self.groupID = groups[0]._id;
               }
           });
       };
       var assignTest = function () {
           teacherApi.assignTest({
               groupID : self.groupID,
               testID : testID
           },function (err) {
               $scope.closeThisDialog();
               if (err) return appHelper.showFailAlertGeneralError();
               appHelper.showSuccessAlert(appValues.TEST_ASSIGNED);
           });
       };

       // INIT
       var self = this;
       this.assignTest = assignTest;
       var testID = $scope.ngDialogData.testID;
       if (!testID) return;
       loadAvailableGroupsForTest(testID);


    });






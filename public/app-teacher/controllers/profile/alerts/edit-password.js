angular
    .module("EduNet")
    .controller("editPasswordController", function (appValues,appValidator,teacherConstants,appHelper,teacherValidator,appConstants,teacherHelper,teacherApi,$rootScope,$scope) {
        var self = this;
        var updatePassword = function () {
            if (!appValidator.isValidEditPassword(self)) return;
            $scope.closeThisDialog();
            teacherApi.updatePassword({
                oldPassword : self.oldPassword,
                newPassword : self.newPassword
            },function (err) {
                if (err && err.status == 404) return appHelper.showFailAlert(appValues.PASSWORDS_NOT_MATCHES);
                if (err) return appHelper.showError(err);
                appHelper.showSuccessAlert(appValues.PASSWORD_UPDATED);
            });
        };
        // INIT
        this.updatePassword = updatePassword;

    });

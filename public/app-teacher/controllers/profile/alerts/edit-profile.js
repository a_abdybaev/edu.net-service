angular
    .module("EduNet")
    .controller("editProfileController", function (appValues,appHelper,teacherValidator,appConstants,teacherHelper,teacherApi,$rootScope,$scope) {
        var self = this;
        var loadProfile = function () {
            teacherApi.getTeacherInfo(function (err,info) {
                if (err) return appHelper.showError(err);
                self.firstName = info.teacher.firstName;
                self.lastName = info.teacher.lastName;
                self.email = info.teacher.email;
            });
        };
        $scope.updateProfile = function () {
            if (!teacherValidator.isValidProfile(self)) return;
            var data = {
                firstName : self.firstName,
                lastName : self.lastName,
                email : self.email
            };
            teacherApi.updateProfile(data,function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(appValues.PROFILE_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_INDEX);

            });
        };



        loadProfile();
    });

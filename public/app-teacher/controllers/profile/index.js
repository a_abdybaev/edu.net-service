angular
    .module("EduNet")
    .controller("profileController", function (ngDialog,appHelper,teacherConstants,appConstants,teacherHelper,teacherApi,$rootScope,$scope) {
        var self = this;
        var loadTeacherInfo = function () {
            teacherApi.getTeacherInfo(function (err,info) {
                if (err) return appHelper.showError(err);
                var teacher =info.teacher,
                    groups = info.groups;
                $scope.teacher = teacher;
            });
        };
        var editPassword = function () {
            ngDialog.open({
                template : "/app-teacher/views/profile/alerts/edit-password.html",
                className : 'ngdialog-theme-default',
                width : 600

            });
        };


        var editProfile = function () {
            ngDialog.open({
                template : "/app-teacher/views/profile/alerts/edit-profile.html",
                className : 'ngdialog-theme-default',
                width : 600
            });
        };
        var showAvatarView = function () {
            $scope.myImage = '';
            $scope.myCroppedImage = '';
            $scope.imageUrl = null;
            $scope.isSettingAvatar = true;
        };
        var closeAvatarView = function () {
            $scope.isSettingAvatar = false;
        };
        var cancelAvatarView = function () {
            angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
            $scope.isSettingAvatar = false;
            $scope.myImage = undefined;
            $scope.myCroppedImage = undefined;
            $scope.imageUrl = undefined;
        };
        var handleFileSelect=function(evt) {
            var file=evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function($scope){
                    $scope.myImage=evt.target.result;
                });
            };
            reader.readAsDataURL(file);
        };
        var updateImage = function () {
            var myCroppedImage = $scope.myCroppedImage;
            if (!myCroppedImage) return;
            teacherApi.updateImage({
                image : myCroppedImage
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(appValues.AVATAR_UPDATED);
                closeAvatarView();
                loadTeacherInfo();

            })
        };

        // INIT
        $scope.myImage='';
        $scope.myCroppedImage='';
        angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
        $scope.cancelAvatar = function () {
            $scope.myImage = '';
            $scope.myCroppedImage = '';
            $scope.closeAvatarView();
            $scope.imageUrl = null;
        };
        $scope.showAvatarView = showAvatarView;
        $scope.closeAvatarView = closeAvatarView;
        $scope.cancelAvatarView = cancelAvatarView;
        $scope.updateImage = updateImage;
        $scope.editProfile = editProfile;
        $scope.editPassword = editPassword;
        $scope.isSettingAvatar = false;
        loadTeacherInfo();
        $rootScope.$on('load-profile',function () {
            loadTeacherInfo();
        });
        $scope.imageUrl = appConstants.TEACHER_AVATAR_DEFAULT_URL_BIG;
    });

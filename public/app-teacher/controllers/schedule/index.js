angular
    .module("EduNet")
    .controller("scheduleController", function (appHelper,ngDialog,teacherHelper,teacherApi) {
        var loadSchedule = function () {
            teacherApi.getSchedule(function (err,lessons) {
                if (err) return appHelper.showError(err);
                self.lessons = teacherHelper.buildSchedule(lessons);
            });
        };
        var groupScheduleInfo = function (lesson) {
            ngDialog.open({
                template : "/app-teacher/views/schedule/alerts/group-schedule.html",
                className : 'ngdialog-theme-default',
                controller : "groupScheduleCtrl",
                data : {
                    lesson : lesson
                },
                width : 400
            });
        };
        //INIT
        var self = this;
        this.days = appHelper.getDayNames();
        loadSchedule();
        this.groupScheduleInfo = groupScheduleInfo;


    });

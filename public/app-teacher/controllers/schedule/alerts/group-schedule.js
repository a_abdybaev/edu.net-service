angular
    .module("EduNet")
    .controller("groupScheduleCtrl", function (appHelper,$scope) {
        var lesson = $scope.ngDialogData.lesson;
        $scope.lesson = lesson;
        var startTime = new Date();
        startTime.setHours(lesson.start.hour);
        startTime.setMinutes(lesson.start.minute);
        var endTime = new Date();
        endTime.setHours(lesson.end.hour);
        endTime.setMinutes(lesson.end.minute);
        $scope.startTime = startTime;
        $scope.endTime = endTime;
        $scope.day = appHelper.getWeekDaysDefault()[lesson.day];
    });

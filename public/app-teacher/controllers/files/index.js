angular
    .module("EduNet")
    .controller("listOfFilesController", function (appValues,appHelper,ngDialog,teacherHelper,teacherApi,$rootScope,$scope,appDictionary) {

        var addFile = function () {
            ngDialog.open({
                template : "/app-teacher/views/files/alerts/add-file.html",
                className : 'ngdialog-theme-default',
                width : 700
            });
        };
        var loadFiles = function () {
            teacherApi.getFiles(function (err,files) {
                if (err) return appHelper.showError(err);
                self.noFiles = _.isEmpty(files);
                self.files = files;
                sortFilesByGroupID(appDictionary.EMPTY_ID);

            });
        };
        var deleteFileByID = function (fileID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deleteFileAlertForGroupController",
                data : {
                    fileID : fileID
                }
            });
        };
        var sortFilesByGroupID = function (groupID) {
            if (self.noFiles) return;
            self.actGroupID = groupID;
            if (groupID == appDictionary.EMPTY_ID) self.filteredFiles = self.files;
            if (groupID != appDictionary.EMPTY_ID) {
                self.filteredFiles = [];
                _.each(self.files,function (file) {
                    if (file.groupID == groupID) self.filteredFiles.push(file);
                });
            }
            self.noFilteredFiles = _.isEmpty(self.filteredFiles);
        };

        var initFiles = function () {
            teacherApi.getListOfGroups(function (err,groups) {
                if (err) return appHelper.showError(err);
                if (_.isEmpty(groups)) return self.noGroups = true;
                self.groups = groups;
                loadFiles();
            });
        };

        this.deleteFileByID = deleteFileByID;
        this.addFile = addFile;
        this.sortFilesByGroupID = sortFilesByGroupID;


        // INIT
        var self = this;

        this.allGroups = appDictionary.ADMIN.ALL_GROUPS;
        this.files = [];
        this.hasGroups = teacherHelper.getTeacherInfoHasGroups();



        $rootScope.$on(appHelper.EVENTS.LOAD_FILES,function (event,data) {
            loadFiles();
        });
        initFiles();
    });

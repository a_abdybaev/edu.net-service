angular
    .module("EduNet")
    .controller("addFileController", function (appValues,appHelper,teacherValues,teacherValidator,teacherHelper,teacherApi,$rootScope,$scope) {
        var resolveFileName = function () {
            if (self.file) return self.fileName = self.file.name;
        };
        var addFile = function () {
            if (!self.file) return;
            $scope.closeThisDialog();
            teacherApi.addFile({
                file : self.file,
                groupID : self.selectedGroupID
            },function (err) {
                if (err && err.status != 406) return appHelper.showFailAlertGeneralError();
                if (err && err.status == 406) return appHelper.showFailAlert(appValues.TARIFF_ERROR_MEMORY);
                appHelper.showSuccessAlert(teacherValues.FILE_CREATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_FILES);
            });
        };
        var loadGroups = function () {
            teacherApi.getListOfGroups(function (err,groups) {
                if (err) return appHelper.showError(err);
                self.groups = groups;
                self.selectedGroupID = groups[0]._id;
            });
        };

        // INIT
        this.resolveFileName = resolveFileName;
        this.addFile = addFile;


        var self = this;
        this.groups = [appHelper.getLoadingObject()];
        this.fileName = appHelper.getFileEmptyName();
        loadGroups();





    });
// d0da2de696342a0f898b

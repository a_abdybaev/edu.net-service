angular
    .module("EduNet")
    .controller("deleteFileAlertForGroupController", function (teacherValues,appHelper,teacherModalHelper,teacherApi,$rootScope,$scope) {
        var deleteFile = function () {
            teacherApi.deleteFileByID(fileID,function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(teacherValues.FILE_DELETED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_FILES);
            });
        };
        var fileID = $scope.ngDialogData.fileID;
        if (!fileID) return appHelper.closeDialogAndShowGenError($scope);
        var modal = {
            text :  teacherModalHelper.deleteMessages.file.text,
            comment : teacherModalHelper.deleteMessages.file.comment,
            del : deleteFile
        };
        $scope.modal = modal;



    });

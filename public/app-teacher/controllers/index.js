angular
    .module("EduNet")
    .controller("indexController", function ($mdSidenav,ngDialog,appHelper,appConstants,teacherHelper,teacherApi,$rootScope,$scope,$location) {
        var loadUserInfo = function () {
            teacherApi.getTeacherInfo(function (err,info) {
                if (err) return appHelper.showError(err);
                var teacher =info.teacher,
                    groups = info.groups;
                appHelper.createNameForUser(teacher);
                teacher.imageUrl = teacher.imageUrl || appConstants.TEACHER_AVATAR_DEFAULT_URL;
                appHelper.setUserInfo({
                    teacher : teacher,
                    groups : groups
                });
                self.teacher = teacher;
                self.hasGroups = true;
                self.groups = groups;

                if (_.isEmpty(self.groups)) self.hasGroups = false;
                teacherHelper.setTeacherInfoHasGroups(self.hasGroups);
                $rootScope.$broadcast(appHelper.EVENTS.INDEX_LOADED);
                $rootScope.indexLoaded = true;
            });
        };
        var goTo = function (path) {
            self.isSideNaVOpen = false;
            $location.path(path);
        };
        var buildToggler = function (componentId) {
            return function() {
                $mdSidenav(componentId).toggle();
            }
        };
        var goToGroupByID = function (groupID) {
            self.isSideNaVOpen = false;
            appHelper.setActGroupID(groupID);
            var path = $location.path();
            if (path == '/group-info') {
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_GROUP);
            }  else {
                $location.path('/group-info');
            }
        };
        var logout = function () {
            teacherHelper.logout();
        };
        var openUserNav = function () {
            self.userNav = true;
        };
        var hideUserNav = function () {
            self.userNav = false;
        };
        var editProfile = function () {
            ngDialog.open({
                template : "/app-teacher/views/profile/alerts/edit-profile.html",
                className : 'ngdialog-theme-default',
                width : 600
            });
        };
        this.goToGroupByID = goToGroupByID;
        this.openUserNav = openUserNav;
        this.hideUserNav = hideUserNav;
        this.editProfile = editProfile;
        this.toggleLeft = buildToggler('left');
        this.toggleRight = buildToggler('right');
        this.isSideNaVOpen = false;
        this.goTo = goTo;
        this.logout = logout;




        var self = this;

        // INIT

        $rootScope.loader = [];

        $rootScope.$on(appHelper.EVENTS.LOAD_INDEX,loadUserInfo);
        loadUserInfo();
    });

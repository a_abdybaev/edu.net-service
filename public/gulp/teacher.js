// TEACHER BUILDER
var mainDist = 'dist',
    dist =  mainDist + '/teacher',
    src = 'app-teacher',
    app = 'teacher_app',
    tasksNames = {
        build : {
            index_html : 'build:teacher:index_html',
            custom_styles : "build:teacher:app_styles",
            custom_js : "build:teacher:app_js",
            lib_js : "build:teacher:lib_js",
            lib_styles : "build:teacher:lib_styles",
            fonts : "build:teacher:fonts",
            templates : "build:teacher:templates",
            start : "build:teacher"
        },
        clean : "clean:teacher",
        revision : "revision:teacher",
        watch : "watch:teacher"
    },
    lib_styles = [
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'bower_components/angular-material/angular-material.min.css',
        'bower_components/angular/angular-csp.css',
        'bower_components/angular-ui-notification/dist/angular-ui-notification.css',
        'bower_components/font-awesom/css/font-awesome.min.css',
        'bower_components/angular-bootstrap/ui-bootstrap-csp.css',
        'bower_components/ng-dialog/css/ngDialog.min.css',
        'bower_components/ng-dialog/css/ngDialog-theme-default.min.css',
        'bower_components/ui-cropper/compile/unminified/ui-cropper.css'
    ],
    def = require('./build/constants'),
    files = def.files,
    path = def.path({
        dist : dist,
        src : src,
        files : def.files,
        path : path,
        lib_styles : lib_styles
    }),
    version = def.version,
    Task = require('./build/tasks')({
        dist : dist,
        tasks : tasksNames,
        files : files,
        path : path,
        version : version,
        app : app
    });
Task.register();

module.exports.tasks = tasksNames;

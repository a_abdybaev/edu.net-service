// MANUAL BUILDER
var mainDist = 'dist',
    dist =  mainDist + '/manual',
    src = 'app-manual',
    app = 'manual_app',
    tasksNames = {
        build : {
            index_html : "build:manual:index_html",
            custom_styles : "build:manual:app_styles",
            custom_js : "build:manual:app_js",
            lib_js : "build:manual:lib_js",
            lib_styles : "build:manual:lib_styles",
            fonts : "build:manual:fonts",
            templates : "build:manual:templates",
            start : "build:manual"
        },
        clean : "clean:manual",
        revision : "revision:manual",
        watch : "watch:manual"
    },
    lib_styles = [
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'bower_components/angular-material/angular-material.min.css',
        'bower_components/angular/angular-csp.css',
        'bower_components/font-awesom/css/font-awesome.min.css',
        'bower_components/angular-bootstrap/ui-bootstrap-csp.css'
    ],
    def = require('./build/constants'),
    files = def.files,
    path = def.path({
        dist : dist,
        src : src,
        files : def.files,
        path : path,
        lib_styles : lib_styles
    }),
    version = def.version,
    Task = require('./build/tasks')({
        dist : dist,
        tasks : tasksNames,
        files : files,
        path : path,
        version : version,
        app : app
    });
Task.register();
module.exports.tasks = tasksNames;
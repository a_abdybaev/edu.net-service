// TEACHER BUILDER
var mainDist = 'dist',
    dist =  mainDist + '/auth',
    src = 'app-auth',
    app = 'auth_app',
    tasksNames = {
        build : {
            index_html : 'build:auth:index_html',
            custom_styles : "build:auth:app_styles",
            custom_js : "build:auth:app_js",
            lib_js : "build:auth:lib_js",
            lib_styles : "build:auth:lib_styles",
            fonts : "build:auth:fonts",
            templates : "build:auth:templates",
            start : "build:auth"
        },
        clean : "clean:auth",
        revision : "revision:auth",
        watch : "watch:auth"
    },
    lib_styles = [
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'bower_components/angular-material/angular-material.min.css',
        'bower_components/angular/angular-csp.css',
        'bower_components/angular-ui-notification/dist/angular-ui-notification.css',
        'bower_components/font-awesom/css/font-awesome.min.css',
        'bower_components/angular-bootstrap/ui-bootstrap-csp.css',
        'bower_components/ui-cropper/compile/unminified/ui-cropper.css',
        'bower_components/ng-dialog/css/ngDialog.min.css',
        'bower_components/ng-dialog/css/ngDialog-theme-default.min.css'
    ],
    def = require('./build/constants'),
    files = def.files,
    path = def.path({
        dist : dist,
        src : src,
        files : def.files,
        path : path,
        lib_styles : lib_styles
    }),
    version = def.version,
    Task = require('./build/tasks')({
        dist : dist,
        tasks : tasksNames,
        files : files,
        path : path,
        version : version,
        app : app
    });
Task.register();

module.exports.tasks = tasksNames;
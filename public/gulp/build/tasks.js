var gulp = require('gulp'),
    wiredep = require('wiredep').stream,
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rimraf = require('rimraf'),
    rigger = require('gulp-rigger'),
    htmlmin = require('gulp-htmlmin'),
    minifyCss = require('gulp-minify-css'),
    watch = require('gulp-watch'),
    rev = require('gulp-rev'),
    runSequence = require('run-sequence'),
    tempCache= require('gulp-angular-templatecache'),
    gulpMerge = require('gulp-merge'),
    debug = require('gulp-debug'),
    filesize = require('gulp-filesize'),
    revdel = require('rev-del'),
    del = require('del'),
    collect = require('gulp-rev-collector'),
    rename = require('gulp-rename'),
    replace = require('gulp-replace'),
    livereload = require('gulp-livereload'),
    Lib = require('./index');


module.exports = function (data) {
    if (!data.path || !data.dist || !data.files || !data.version || !data.app) return (new Error());

    return {
        build : {
            index_html : function () {
                return del([data.path.build.src  + '/**/*.html','!'+data.path.build.index_html],function (err) {
                    if (err) return console.log(err);
                    return gulp
                        .src(data.path.src.index_html)
                        // .pipe(htmlmin({collapseWhitespace : true}))
                        .pipe(gulp.dest(data.path.build.src))
                        ;
                });
            },
            lib_js : function () {
                return gulp
                    .src(data.path.src.lib_js)
                    .pipe(rigger())
                    .pipe(gulp.dest(data.path.build.js))
                    ;
            },
            lib_styles : function () {
                return gulp
                    .src(data.path.src.lib_styles)
                    .pipe(concat(data.files.lib_styles))
                    .pipe(gulp.dest(data.path.build.styles))
                    ;
            },
            custom_styles : function () {
                return gulp
                    .src(data.path.src.custom_styles)
                    .pipe(gulp.dest(data.path.build.styles))
                    ;
            },
            templates : function () {
                return gulp
                    .src(data.path.src.templates)
                    .pipe(htmlmin({
                        collapseWhitespace : true
                    }))
                    .pipe(tempCache({
                        module : 'EduNet',
                        filename : data.files.templates
                    }))
                    .pipe(gulp.dest(data.path.build.js))
                    ;
            },
            start : function (callback) {
                return runSequence(
                    data.tasks.clean,
                    data.tasks.build.fonts,
                    data.tasks.build.index_html,
                    data.tasks.build.templates,
                    data.tasks.build.custom_styles,
                    data.tasks.build.lib_styles,
                    data.tasks.build.custom_js,
                    data.tasks.build.lib_js,
                    data.tasks.revision,


                    callback
                )
            },
            custom_js : function () {
                return gulp
                    .src(data.path.src.custom_js)
                    .pipe(concat(data.files.custom_js))
                    .pipe(gulp.dest(data.path.build.js))
                    ;
            },
            fonts : function () {
                return gulp.src(data.path.src.fonts)
                    .pipe(gulp.dest(data.path.build.fonts));
            }
        },
        clean : function (cb) {
            return  rimraf(data.dist, cb);
        },
        revision : function () {
            var version = Lib.getVersions();
            return gulp.src(data.path.build.index_html)
                .pipe(replace('lib_styles_build_version',version[data.app].lib_styles))
                .pipe(replace('custom_styles_build_version',version[data.app].custom_styles))
                .pipe(replace('index_styles_build_version',version[data.app].custom_styles))
                .pipe(replace('lib_js_build_version',version[data.app].lib_js))
                .pipe(replace('templates_build_version',version[data.app].templates))
                .pipe(replace('custom_js_build_version',version[data.app].custom_js))
                .pipe(rename('index-'+version[data.app].index_html+'.html'))
                .pipe(gulp.dest(data.path.build.src));
        },
        watch : function () {
            watch(data.path.watch.lib_js,function (event,cb) {
                Lib.update({
                    path : data.version.lib_js,
                    app : data.app
                },function (err) {
                    if (err) return console.log(err);
                    runSequence(data.tasks.build.index_html,data.tasks.build.lib_js,data.tasks.revision);
                });
            });
            watch(data.path.watch.custom_js,function (event,cb) {
                Lib.update({
                    path : data.version.custom_js,
                    app : data.app
                },function (err) {
                    if (err) return console.log(err);
                    runSequence(data.tasks.build.index_html,data.tasks.build.custom_js,data.tasks.revision);
                });
            });
            watch(data.path.watch.custom_styles,function (event,cb) {
                Lib.update({
                    path : data.version.custom_styles,
                    app : data.app
                },function (err) {
                    if (err) return console.log(err);
                    runSequence(data.tasks.build.index_html,data.tasks.build.custom_styles,data.tasks.revision);
                });
            });
            watch(data.path.watch.templates,function (event,cb) {
                Lib.update({
                    path : data.version.templates,
                    app : data.app
                },function (err) {
                    if (err) return console.log(err);
                    runSequence(data.tasks.build.index_html,data.tasks.build.templates,data.tasks.revision);
                });
            });
            watch(data.path.watch.index_html,function (event,cb) {
                Lib.update({
                    path : data.version.index_html,
                    app : data.app
                },function (err) {
                    if (err) return console.log(err);
                    runSequence(data.tasks.build.index_html,data.tasks.revision);
                });
            });
            watch(data.path.watch.fonts,function (event,cb) {
                gulp.start(data.tasks.build.fonts);
            });
        },
        register : function () {
            gulp.task(data.tasks.build.index_html,this.build.index_html);
            gulp.task(data.tasks.build.templates,this.build.templates);
            gulp.task(data.tasks.build.custom_js,this.build.custom_js);
            gulp.task(data.tasks.build.custom_styles,this.build.custom_styles);
            gulp.task(data.tasks.build.lib_js,this.build.lib_js);
            gulp.task(data.tasks.build.lib_styles,this.build.lib_styles);
            gulp.task(data.tasks.build.fonts,this.build.fonts);
            gulp.task(data.tasks.build.start,this.build.start);
            gulp.task(data.tasks.watch,this.watch);
            gulp.task(data.tasks.clean,this.clean);
            gulp.task(data.tasks.revision,this.revision);
        }
    };
};


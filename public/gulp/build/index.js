module.exports.generateHash = function (hash) {
    return crypto.randomBytes(3).toString('hex');
};
module.exports.getVersions = function () {
    decache('./index.json');
    return require('./index.json')
};
var applications = {
    admin_app : 'admin_app',
    teacher_app : 'teacher_app',
    student_app : 'student_app',
    auth_app : 'auth_app',
    manual_app : "manual_app"
};
module.exports.updatePath = function (data,cb) {
    if (!data.application || !data.path) return cb(new Error());
    var versions = exports.getVersions();
    versions[data.application][data.path] = exports.generateHash();
    fs.writeFile(path.join(__dirname,'index.json'),JSON.stringify(versions,null,2),cb);
};

module.exports.update = function (data,cb) {
    exports.updatePath({
        application : data.app,
        path : data.path
    },cb);
};



var crypto = require('crypto'),
    decache = require('decache'),
    path = require('path'),
    fs = require('fs');
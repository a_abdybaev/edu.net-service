var main_dist = 'dist',
    shared = 'shared';

module.exports.path = function (data) {
    if (!data.src || !data.files || !data.lib_styles) return (new Error());
    return {
        src : {
            templates : data.src + '/views/**/*.html',
            index_html : data.src + '/index.html',
            custom_styles : data.src + '/styles/**/*.*',
            fonts : data.src + '/fonts/**/*.*',
            lib_styles : data.lib_styles,
            lib_js : data.src + '/' + data.files.lib_js,
            custom_js : [
                data.src + '/**/*.js',
                shared + '/**/*.js',
                '!' + data.src +'/' + data.files.lib_js,
                '!' + data.src + '/application.js',
                '!' + data.src + '/templates.js'
            ]
        },
        build : {
            src : data.dist,
            styles : data.dist + '/css',
            js : data.dist + '/js',
            index_html : data.dist + '/index.html',
            fonts : data.dist + '/fonts'
        },
        watch : {
            templates : data.src +'/views/**/*.html',
            index_html : data.src + '/index.html',
            custom_styles : data.src + '/styles/**/*.*',
            lib_js : [data.src  + '/' +data.files.lib_js,data.src + '/application.js'],
            custom_js : [
                data.src + '/**/*.js',
                shared + '/**/*.*',
                '!' + data.src +'/' + data.files.lib_js,
                '!' + data.src + '/application.js'
            ],
            //MODIFIED
            fonts : data.src + '/fonts'
        }
    }
};
module.exports.version = {
    templates : "templates",
    custom_styles : "custom_styles",
    lib_styles : "lib_styles",
    lib_js : "lib_js",
    index_html : "index_html",
    custom_js : "custom_js"
};
module.exports.files = {
    custom_js : 'custom.js',
    lib_js : 'lib.js',
    templates : 'templates.js',
    lib_styles : 'lib.css',
    index_html : "index.html",
    index_styles : "index.css"
};

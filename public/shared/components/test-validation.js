app
    .factory('testValidator', function (appValidator,appConstants) {
        return {
            isValidThirdStep : function (ctrl) {
                var result = true;
                _.each(ctrl.sections,function (section,i) {
                    _.each(section.answers,function (answer,j) {
                        section.answersValidation[j] = appValidator.isEmpty(answer) == false;
                        if (appValidator.isEmpty(answer)) result = false
                    });
                });
                return result;
            },
            isValidSecondStep : function (ctrl) {
                var result = true;
                _.each(ctrl.sections,function (section) {
                    var questionsCount = parseInt(section.questionsCount),
                        minutesCount = parseInt(section.minutesCount);
                    if (isNaN(questionsCount) || questionsCount <=0 || isNaN(minutesCount) || minutesCount <= 0) {
                        result = false;
                    }
                    if (section.pages == appConstants.TEST_FILE_INTERVAL) {
                        var from = parseInt(section.pagesFrom);
                        var to = parseInt(section.pagesTo);
                        if (isNaN(from) || isNaN(to) || from > to) {
                            result = false;
                        }
                    }
                    if (section.file == null) {
                        result = false;
                    }
                    section.hasAudio = section.audioFile != null;
                });
                return result;
            },
            isValidFirstStep : function (ctrl) {
                return !appValidator.isEmpty(ctrl.name) && ctrl.sections.length >=1;
            },
            isValidUpdateTestSectionAnswers : function (ctrl) {
                ctrl.answersValidation = [];
                var result = true;
                _.each(ctrl.answers,function (answer,j) {
                    ctrl.answersValidation[j] = appValidator.isEmpty(answer.value) == false;
                    if (appValidator.isEmpty(answer.value)) return result = false;
                });
                return result;
            }
        }
    });

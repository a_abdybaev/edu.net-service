console.log("BEFOR EERROr");
app
    .value('HTMLIZE_CONVERSIONS', [
        { expr: /\n+?/g, value: '<br>' }
    ])
    .filter('htmlize', function(HTMLIZE_CONVERSIONS) {
        return function(string) {
            return HTMLIZE_CONVERSIONS.reduce(function(result, conversion) {
                return result.replace(conversion.expr, conversion.value);
            }, string || '');
        };
    })
    .filter("listOfGroupsSearch",function () {
        return function (groups,name) {
            var result = [];
            if (name == '' || name == null) return groups;
            if (_.isEmpty(groups)) return result;
            _.each(groups,function (group) {
                var regexp = new RegExp(name,'i');
                if (group.name.toLowerCase().search(regexp) > -1) result.push(group);
            });
            return result;
        }
    })
    .filter('searchByField',function () {
        return function (list,field,text) {
            var results = [];
            if (text == null || _.isEmpty(text)) return list;
            if (list.length == 0) return results;
            _.each(list,function (item) {
                var regexp = new RegExp(text,'i');
                if (item[field].toLowerCase().search(regexp) > -1) results.push(item);
            });
            return results;
        }
    })
    .filter("timeFilter",function () {
        return function (time) {
            return moment(time).format("D MMMM в HH:mm");
        }
    })
    .filter("timeFilterDate",function () {
        return function (time) {
            return moment(time).format("D MMMM YYYY");
        }
    })

    .filter("memoryUsageFilter",function () {
        return function (size) {
            var usage = (size / 1024 / 1024).toFixed(2) + ' Мб';
            return usage;
        }
    })
    .filter("memoryUsageFilterWithoutMB",function () {
        return function (size) {
            var usage = (size / 1024 / 1024).toFixed(2);
            return usage;
        }
    })
    .filter("listOfFilesFilter",function () {
        return function (files,name) {
            var result = [];
            if (name == '' || name == undefined) {
                return files;
            } else {
                if (files.length != 0) {
                    files.forEach(function (file) {
                        var index = file.name.search(name);
                        if (index > -1) {
                            result.push(file);
                        }
                    });
                    return result;
                } else {
                    return result;
                }
            }
        };
    })
    .filter('phoneFilter',function () {
        return function (phone) {
            if (!phone) return;
            phone = phone.toString();
            if (phone == null || phone.length != 10) return;
            return '+7 (' + phone[0] + phone[1] + phone[2] + ') '+ phone[3] + phone[4] + phone[5] + ' ' + phone[6] + phone[7] + ' ' + phone[8] + phone[9];
        };
    })
    .filter('nameFilter',function () {
        return function (user) {
            if (user == null) return;
            if (user.name) return user.name;
            return user.lastName + ' ' + user.firstName;
        }
    })
    .filter('nameFilterByVacation',function (appDictionary,$filter) {
        return function (user) {
            if (!user) return appDictionary.VACATION;
            return $filter('nameFilter')(user);
        }
    })
    .filter('namesFilterByVacation',function (appDictionary,$filter) {
        return function (users) {
            if (_.isEmpty(users)) return appDictionary.VACATION;
            return $filter('nameFilter')(users[0]);
        }
    })
    .filter('teacherImageUrlFilter',function (appConstants) {
        return function (teacher) {
            if (!teacher) return appConstants.TEACHER_AVATAR.MAN.DEFAULT;
            if (!teacher.imageUrl) return teacher.gender == appConstants.GENDER.WOMAN ? appConstants.TEACHER_AVATAR.WOMAN.DEFAULT : appConstants.TEACHER_AVATAR.MAN.DEFAULT;
            return teacher.imageUrl;
        }
    })
    .filter('teacherImageUrlBigFilter',function (appConstants) {
        return function (teacher) {
            if (teacher == null) return;
            if (teacher.imageUrl == null) return teacher.gender == appConstants.GENDER.WOMAN ? appConstants.TEACHER_AVATAR.WOMAN.BIG : appConstants.TEACHER_AVATAR.MAN.BIG;
            return teacher.imageUrl;
        }
    })
    .filter('emailFilter',function () {
        return function (email) {
            if (email == null || email =='') return 'Не указан';
            return email;
        };
    })
    .filter('testDurationFilter',function () {
        return function (section) {
            if (!section || !section.duration) return;
            return (section.duration / 60).toFixed(0);
        };
    })
    .filter('testTotalQuestionsCountFilter',function () {
        return function (test) {
            var count = 0;
            if (!test || !test.sections) return;
            _.each(test.sections,function (section) {
                count+= section.questionsCount;
            });
            return count;
        };
    })
    .filter('testTotalDurationFilter',function () {
        return function (test) {
            if (!test || !test.sections) return;
            var duration = 0;
            _.each(test.sections,function (section) {
                duration+= section.duration;
            });
            return (duration / 60) .toFixed(0);
        };
    })
    .filter('customCurrencyFilter',function ($filter) {
        return function (value) {
            return $filter('currency')(value,'',0,false) + ' KZT';
        }
    })
    .filter('customCurrencyFilterWithoutCurrency',function ($filter) {
        return function (value) {
            return $filter('currency')(value,'',0,false);
        }
    })
    .filter('backGroundColor',function () {
        return function (color) {
            return {
                'background-color' : color
            };
        }
    })
    .filter("groupLessonColor",function () {
        return function (color) {
            if (color == "#51baf2"){
                return "#31a1dd"
            }
            if(color == "#fd6461"){
                return "#f2413e"
            }
            if(color == "#71ca58"){
                return "#52bf33"
            }
            if(color == "#f7a650"){
                return "#f0912c"
            }
            else{
                return "#c56fd9"
            }
        }
    })







;




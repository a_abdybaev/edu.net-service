angular
    .module('EduNet')
    .factory('main',['appConstants','appDictionary','appHelper','appModalHelper','testValidator','appValidator','appValues',function (appConstants,appDictionary,appHelper,appModalHelper,testValidator,appValidator,appValues) {
        var self = this;
        return {
            constants : appConstants,
            dictionary : appDictionary,
            helper : appHelper,
            appModalHelper : appModalHelper,
            testValidator : testValidator,
            validator : appValidator,
            values : appValues
        }}]);
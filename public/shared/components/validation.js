app
    .factory('appValidator', function (appConstants) {
        return {
            isEmpty : function (field) {
                if (field == null) return true;
                field+="";
                return validator.isEmpty(field);
            },
            isValidField : function (ctrl,name) {
                var self = this;
                if (self.isEmpty(ctrl[name])) {
                    ctrl[name + appConstants.VALIDATION.ERROR_FIELD_PREFIX] = true;
                    return false;
                }
                ctrl[name + appConstants.VALIDATION.ERROR_FIELD_PREFIX] = false;
                return true;
            },
            isValidForm : function (form,stringFields) {
                var items = stringFields.split(' '),
                    self = this;
                var result = true;
                _.each(items,function (item) {
                    if (item =='email') result  = self.isValidEmailField(form,item) && result;
                    else result = self.isValidField(form,item) && result;
                });
                return result;
            },
            hideError : function (error) {
                error = false;
            },
            isEmail : function (string) {
                var self = this;
                return !self.isEmpty(string) && validator.isEmail(string);
            },
            isValidEmail : function (email) {
                if (this.isEmpty(email)) return true;
                return validator.isEmail(email);
            },
            isValidEmailField : function (ctrl,email) {
                if (this.isValidEmail(ctrl[email])) {
                    ctrl['emailError'] = false;
                    return true;
                } else {
                    ctrl['emailError'] = true;
                    return false;
                }

            },
            isValidEditPassword : function (ctrl) {
                var self = this;
                ctrl['oldPasswordError'] = false;
                ctrl['newPasswordError'] = false;
                ctrl['newPasswordConfirmError'] = false;
                ctrl['newPasswordConfirmInvalidError'] = false;
                ctrl['newPasswordInvalidError'] = false;
                if (!this.isValidField(ctrl,'oldPassword') || !this.isValidField(ctrl,'newPassword') || !this.isValidField(ctrl,'newPasswordConfirm')) return false;
                if (ctrl['newPassword'] != ctrl['newPasswordConfirm']) {
                    ctrl['newPasswordConfirmInvalidError'] = true;
                    return false;
                }
                ctrl['newPasswordConfirmInvalidError'] = true;
                if (ctrl['newPassword'].length < 6) {
                    ctrl['newPasswordInvalidError'] = true;
                    return false;
                }
                ctrl['newPasswordInvalidError'] = true;
                return true;
            }


        }
    });

app
    .factory('appModalHelper', function (appValues) {
        return {
            deleteMessages : {
                testSection : {
                   text : "Вы точно хотите удалить данный раздел теста?",
                   comment : "Данный раздел теста будет потерян навсегда"
                },
                test : {
                    text : "Вы точно хотите удалить данный тест?",
                    comment : "Данный тест будет потерян навсегда"
                }
            },
            successMessages : [{
                value : appValues.PROFILE_UPDATED,
                message : "Профиль успешно обновлен"
            },{
                message : "Пароль обновлен",
                value : appValues.PASSWORD_UPDATED
            },{
                value : appValues.TEST_CREATED,
                message : "Тест создан"
            },{
                value : appValues.TEST_UPDATED,
                message : "Тест успешно обновлен"
            },{
                value : appValues.TEST_DELETED,
                message : "Тест успешно удален"
            },{
                value : appValues.TEST_SECTION_DELETED,
                message : "Данные раздел теста удален"
            },{
                value : appValues.TEST_SECTION_UPDATED,
                message : "Текущий раздел теста успешно обновлен"
            },{
                value : appValues.TEST_ASSIGNED,
                message : "Данные тест успешно назначен в группу"
            },{
                value : appValues.GENERAL_SUCCESS,
                message : "Успешное действие"
            }],
            errorMessages : [{
                value : appValues.PASSWORDS_NOT_MATCHES,
                message : "Вы ввели неверный старый пароль"
            },{
                value : appValues.GENERAL_ERROR,
                message : "Произошла ошибка, попробуйте позже"
            },{
                value : appValues.COMMENT_ADDED,
                message : "Комментарий успешно добавлен"
            },{
                message : "Вы превысили колличество потребляемой памяти по данному тарифу",
                value : appValues.TARIFF_ERROR_MEMORY
            },{
                message : "Вы превысили число действующих активных студентов по данному тарифу",
                value : appValues.TARIFF_ERROR_STUDENTS
            }]
        }
    });
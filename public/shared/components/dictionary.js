app
    .factory('appDictionary', function (appConstants) {
        return {
            GENERAL : "Общая",
            INDIVIDUAL : "Индвидуальная",
            MORNING_TIME : "Утреннее",
            LUNCH_TIME : "Обеденное",
            EVENING_TIME : "Вечернее",
            NOT_SELECTED : "Не выбрано",
            STUDENT_CHILDREN : "Детская",
            STUDENT_TEENAGE : "Подростковая",
            STUDENT_YOUTH : "Молодежная",
            STUDENT_ADULT : "Взрослая",
            days : ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
            VACATION : "Вакансия",
            EMPTY_ID : "-",
            ADMIN : {
                ALL_GROUPS : "Все группы"
            },
            SELECT : {
                SELECT : "Выбрать",
                TEACHER : "Выбрать учителя"
            }
        }
    });
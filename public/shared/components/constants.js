
app
    .factory("appConstants",function () {
        var POST_TEXT_SHORT_LIMIT = 200,
            POST_TEXT_LONG_LIMIT = 10000,
            POSTS_PER_PAGE = 5,
            POST_KIND_ALL_POSTS_TEXT = "ALL_POSTS",
            POST_KIND_HOMEWORKS_TEXT = "HOMEWORKS",
            POST_KIND_FILES_TEXT = "FILES",
            POST_KIND_ANOTHER_TEXT = "ANOTHER",
            POST_KIND_IMPORTANT_TEXT = "IMPORTANT",
            TEACHER_AVATAR_DEFAULT_URL = "/static-files/images/empty-teacher-photo-small.png",
            TEACHER_AVATAR_DEFAULT_URL_BIG = "/static-files/images/empty-teacher-photo.png",
            STUDENT_AVATAR_DEFAULT_URL = "/static-files/images/empty-student-photo-small.png",
            STUDENT_AVATAR_DEFAULT_URL_BIG = "/static-files/images/empty-student-photo.png",

            CENTER_IMAGE_DEFAULT_URL = "/static-files/images/empty-center-icon.png",
            TEACHER = "TEACHER",
            STUDENT = "STUDENT",
            ADMIN = "ADMIN",
            TESTING = "TESTING",
            NOT_TESTING = "NOT_TESTING",
            GROUP_TYPE_INDIVIDUAL = "INDIVIDUAL",
            GROUP_TYPE_GENERAL = "GENERAL"




        ;
        var VIEW_STYLE_BLOCKS = "BLOCKS",
            VIEW_STYLE_LIST = "LIST",
            VACATION = "Вакансия",
            SELECTION = "Выберите",
            LOADING = "Загружается";

        var NOT_SELECTED = "Не выбрано";
        var EMPTY_ID = "-";
        var NOT_SELECTED_OBJECT = {
            _id : EMPTY_ID,
            name : NOT_SELECTED
        };
        var HELPERS = {
            NOT_SELECTED : NOT_SELECTED,
            NOT_SELECTED_OBJECT : NOT_SELECTED_OBJECT,
            EMPTY_ID : EMPTY_ID,
            getNotSelectedObject : function () {
                return _.clone(NOT_SELECTED_OBJECT);
            },
            VACATION : VACATION
        };




        // VALIDATION
        var VALIDATION = {
            REQUIRED_FIELD_SHORT : "Данное поле обязательно"
            , ERROR_FIELD_PREFIX : "Error"
        };





        // ATTENDANCE
        var STUDENT_APPEARANCE_PRESENCE = "PRESENCE",
            STUDENT_APPEARANCE_ABSENCE = "ABSENCE",
            STUDENT_APPEARANCE_ABSENCE_REASON = "ABSENCE_REASON",
            STUDENT_APPEARANCE_EMPTY_DAY = "APPEARANCE_EMPTY_DAY";


        // STUDENT_AGE


        var STUDENT_AGE_GROUP_CHILDREN = "STUDENT_CHILDREN",
            STUDENT_AGE_GROUP_TEENAGE = "STUDENT_TEENAGE",
            STUDENT_AGE_GROUP_YOUTH = "STUDENT_YOUTH",
            STUDENT_AGE_GROUP_ADULT = "STUDENT_ADULT";

        var STUDENT_AGE_GROUPS = [{
            name : HELPERS.NOT_SELECTED,
            value : HELPERS.EMPTY_ID
        },{
            name : 'Детская',
            value : STUDENT_AGE_GROUP_CHILDREN
        },{
            name : 'Подростковая',
            value : STUDENT_AGE_GROUP_TEENAGE
        },{
            name : 'Молодежная',
            value : STUDENT_AGE_GROUP_YOUTH
        },{
            name : 'Взрослая',
            value : STUDENT_AGE_GROUP_ADULT
        }];

        var STUDENT_STUDY_TIME_MORNING = "MORNING_TIME",
            STUDENT_STUDY_TIME_LUNCH = "LUNCH_TIME",
            STUDENT_STUDY_TIME_EVENING = "EVENING_TIME";
        var STUDENT_STUDY_TIMES = [STUDENT_STUDY_TIME_MORNING,STUDENT_STUDY_TIME_EVENING,STUDENT_STUDY_TIME_EVENING];

        var INTERVIEW = {
            OUT_OF_FIVE : "OUT_OF_FIVE",
            OUT_OF_TWO : "OUT_OF_TWO"
        };


        return {
            STUDENT_STUDY_TIME_MORNING : STUDENT_STUDY_TIME_MORNING,
            STUDENT_STUDY_TIME_LUNCH : STUDENT_STUDY_TIME_LUNCH,
            STUDENT_STUDY_TIME_EVENING : STUDENT_STUDY_TIME_EVENING,
            STUDENT_STUDY_TIMES : STUDENT_STUDY_TIMES,
            POST_TEXT_SHORT_LIMIT : POST_TEXT_SHORT_LIMIT,
            POSTS_PER_PAGE : POSTS_PER_PAGE,
            POST_KIND_ALL_POSTS_TEXT : POST_KIND_ALL_POSTS_TEXT,
            POST_TEXT_LONG_LIMIT : POST_TEXT_LONG_LIMIT,
            getPostTypesWithQueryObject : [{
                query : {

                },
                POST_TYPE : POST_KIND_ALL_POSTS_TEXT
            },{
                query : {
                    homework : true
                },
                POST_TYPE : POST_KIND_HOMEWORKS_TEXT
            },{
                POST_TYPE : POST_KIND_FILES_TEXT,
                query : {
                    onlyFiles : true
                }
            },{
                POST_TYPE : POST_KIND_IMPORTANT_TEXT,
                query : {
                    important : true
                }
            },{
                POST_TYPE : POST_KIND_ANOTHER_TEXT,
                query : {
                    another : true
                }
            }],

            VIEW_STYLES : {
                VIEW_STYLE_BLOCKS : VIEW_STYLE_BLOCKS,
                VIEW_STYLE_LIST : VIEW_STYLE_LIST
            },
            TEACHER_AVATAR : {
                MAN : {
                    DEFAULT : "/static-files/images/teacher-male-small-icon.png",
                    BIG : "/static-files/images/teacher-male-icon.png"
                },
                WOMAN : {
                    DEFAULT : "/static-files/images/teacher-female-small-icon.png",
                    BIG : "/static-files/images/teacher-female-icon.png"
                }
            },
            TEACHER_AVATAR_DEFAULT_URL : TEACHER_AVATAR_DEFAULT_URL,
            STUDENT_AVATAR_DEFAULT_URL : STUDENT_AVATAR_DEFAULT_URL,
            CENTER_IMAGE_DEFAULT_URL : CENTER_IMAGE_DEFAULT_URL


            , TEACHER_AVATAR_DEFAULT_URL_BIG : TEACHER_AVATAR_DEFAULT_URL_BIG
            , STUDENT_AVATAR_DEFAULT_URL_BIG : STUDENT_AVATAR_DEFAULT_URL_BIG

            ,LOADING_SELECT_OBJECT : {
                _id : '-',
                name : LOADING
            }
            , NOT_SELECTED_OBJECT : {
                _id : '-',
                name : 'Не указано'
            }
            , VACATION : VACATION
            , VALIDATION : VALIDATION
            , STUDENT : STUDENT
            , TEACHER : TEACHER
            , ADMIN : ADMIN,
            TESTING : TESTING,
            NOT_TESTING : NOT_TESTING,
            STUDENT_APPEARANCE_EMPTY_DAY : STUDENT_APPEARANCE_EMPTY_DAY,
            STUDENT_APPEARANCE_PRESENCE : STUDENT_APPEARANCE_PRESENCE,
            STUDENT_APPEARANCE_ABSENCE : STUDENT_APPEARANCE_ABSENCE,
            STUDENT_APPEARANCE_ABSENCE_REASON : STUDENT_APPEARANCE_ABSENCE_REASON,
            STUDENT_APPEARANCE : [STUDENT_APPEARANCE_PRESENCE,STUDENT_APPEARANCE_ABSENCE,STUDENT_APPEARANCE_ABSENCE_REASON],

            // GROUP TYPE
            GROUP_TYPE_INDIVIDUAL : GROUP_TYPE_INDIVIDUAL,
            GROUP_TYPE_GENERAL : GROUP_TYPE_GENERAL,
            GROUP_TYPES : [GROUP_TYPE_GENERAL,GROUP_TYPE_INDIVIDUAL],



            // STUDENT_AGE_GROUP_TYPES
            STUDENT_AGE_GROUP_CHILDREN : STUDENT_AGE_GROUP_CHILDREN,
            STUDENT_AGE_GROUP_TEENAGE : STUDENT_AGE_GROUP_TEENAGE,
            STUDENT_AGE_GROUP_YOUTH : STUDENT_AGE_GROUP_YOUTH,
            STUDENT_AGE_GROUP_ADULT : STUDENT_AGE_GROUP_ADULT,
            STUDENT_AGE_GROUPS : STUDENT_AGE_GROUPS,

            HELPERS : HELPERS,



        //    TEST
            TEST_FILE_ALL : "ALL",
            TEST_FILE_INTERVAL : "INTERVAL",

            CUSTOM : "CUSTOM",
            // INTERVIEW
            INTERVIEW : {
                INTERVIEW : INTERVIEW,
                OUT_OF_TWO : "OUT_OF_TWO",
                OUT_OF_FIVE : "OUT_OF_FIVE",
                QUESTIONS : {
                    OUT_OF_FIVE_UPPERCASE : ['ОЧ. ПЛОХО', 'ПЛОХО', 'СРЕДНЕ', 'ХОРОШО', 'ОТЛИЧНО'],
                    OUT_OF_FIVE : ['Оч. плохо','Плохо','Средне','Хорошо','Отлично'],
                    OUT_OF_TWO : ['ДА','НЕТ']
                }
            },
            NO_TITLE : "---",
            GENDER : {
                MAN : "MAN",
                WOMAN : "WOMAN"
            }

        };
    });




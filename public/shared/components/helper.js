app
    .factory('appHelper', function (appModalHelper,appValues,appConstants,$location,$rootScope,ngDialog,localStorageService) {
        return {
            showAlert : function (isSuccess,message) {
                ngDialog.open({
                    template : "/shared/views/alerts/index.html",
                    className : 'ngdialog-theme-default',
                    controller : "alertController",
                    width : 320,
                    data : {
                        isSuccess : isSuccess,
                        message : message
                    },
                    showClose : false
                });
            },
            showSuccessAlert : function (message) {
                this.showAlert(true,message);
            },
            showFailAlert : function (message) {
                this.showAlert(false,message);
            },
            showFailAlertGeneralError : function () {
                this.showFailAlert(appValues.GENERAL_ERROR);
            },
            closeDialogAndShowGenError : function (scope) {
                scope.closeThisDialog();
                this.showFailAlertGeneralError();
            },
            showGeneralSuccessAlert : function () {
                this.showAlert(true,appValues.GENERAL_SUCCESS);

            },
            // LOCAL STORAGE


            getUserInfo : function () {
                return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.USER_INFO);
            },
            setUserInfo : function (info) {
                return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.USER_INFO,info);
            },
            removeUserInfo : function () {
                return localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.USER_INFO);
            },
            setUserType : function (type) {
                return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.USER_TYPE,type);
            },
            getUserType : function (type) {
                return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.USER_TYPE);
            },
            getUserID : function () {
                return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.USER_ID);
            },
            setUserID : function (userID) {
                return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.USER_ID,userID);
            },
            getActGroupID : function () {
                return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.ACT_GROUP_ID);
            },
            setActGroupID : function (groupID) {
                return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.ACT_GROUP_ID,groupID);
            },
            setActBlocID : function (blocID) {
                return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.ACT_BLOC_ID,blocID);
            },
            getActBlocID : function () {
                return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.ACT_BLOC_ID);
            },
            removeActGroupID : function () {
                return localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.ACT_GROUP_ID);
            },
            removeActBlocID : function () {
                return localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.ACT_BLOC_ID);
            },

            showError : function (err) {
                if (this.isDevelopment()) console.log(err);
                localStorageService.set('ERROR_DATA',err);
                $location.path('/error500');
            },

            addLoader : function() {
                $rootScope.loader.push(true);
            },
            removeLoader : function () {
                $rootScope.loader.pop();
            },
            createNameForUser : function (user) {
                user.name = user.lastName + ' ' + user.firstName;
            },
            createNameForUsers : function (users) {
                var self = this;
                users.forEach(function (user) {
                    self.createNameForUser(user);
                });
            },
            getWeekDaysDefault : function () {
                return ["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота","Воскресенье"];
            },
            getMonthNamesWithYearLowerCase : function () {
                return ["Январь 2017","Февраль 2017","Март 2017","Апрель 2017","Май 2017","Июнь 2017","Июль 2017","Август 2017","Сентябрь 2017","Октябрь 2017","Ноябрь 2017","Декабрь 2017"];
            }
            ,getFileKinds : function () {
                return [{
                    value : "TASK",
                    text : "Задание"
                },{
                    value : "BOOK",
                    text : "Книги"
                },{
                    value : "PREZENTATION",
                    text : "Презентации"
                },{
                    value : "USEFUL_MATERIAL",
                    text : "Полезные материалы"
                },{
                    value : "OTHER",
                    text : "Прочие документы"
                }];
            }
            ,getLoadingObject : function () {
                return {
                    _id : '-',
                    name : 'Загружается'
                }
            }
            ,getFileEmptyName : function () {
                return "Выберите документ для загрузки";
            }
            ,getDayNames : function () {
                return ['ПОНЕДЕЛЬНИК','ВТОРНИК','СРЕДА','ЧЕТВЕРГ','ПЯТНИЦА','СУББОТА'];
            }
            ,getWeekDaysUpperCaseWithoutSunday : function() {
                return ['ПОНЕДЕЛЬНИК','ВТОРНИК','СРЕДА','ЧЕТВЕРГ','ПЯТНИЦА','СУББОТА'];
            }

            ,getNamesFromUsers : function(users) {
                users.forEach(function (user) {
                    user.name = user.lastName + ' ' + user.firstName;
                });
                return users;
            }
            , getNameForUser : function (user) {
                return user.lastName + ' ' + user.firstName;
            }
            , getWeekDaysWithoutSundayDefault : function () {
                return ['Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'];
            }
            , getMonthNamesLowerCase : function () {
                return ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"];
            }
            , resolveGroupTeacher : function (group) {
                if (!group.teacherID) group.teacherID = {
                    name : appConstants.VACATION,
                    _id : '-'
                };
                this.createNameForUser(group.teacherID);
            },
            computeTotalPaymentForStudents : function (students) {
                var self = this;
                if (students.length == 0) return;
                students.forEach(function (student) {
                    self.computePaymentForStudent(student);
                });
            },
            computePaymentForStudent : function (student) {
                student.totalPayment = 0;
                if (student.payments.length == 0) return;
                student.payments.forEach(function (payment) {
                    student.totalPayment+=payment.value
                });
            },
            isDevelopment : function () {
                return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.IS_DEV);
            },
            setDevelopment : function (env) {
                return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.IS_DEV,env);
            },
            computeTestDuration : function (test) {
                test.duration = 0;
                test.questionsCount = 0;
                _.each(test.sections,function (section) {
                    test.duration +=section.duration;
                    test.questionsCount += section.questionsCount;
                });
            },
            computeTestsDuration : function (tests) {
                var self = this;
                _.each(tests,function (test) {
                    self.computeTestDuration(test);
                });
            },
            getWeekDaysArrayWithoutSundayDefault : function () {
                var results = [];
                var weekDays = this.getWeekDaysWithoutSundayDefault();
                weekDays.forEach(function (day) {
                    results.push({
                        name : day,
                        hasLesson : false
                    });
                });
                return results;
            },
            setPrev : function (from) {
                return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.FROM,from);
            },
            clearPrev : function () {
                return localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.FROM);
            },
            getPrev : function () {
                return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.FROM);
            },
            EVENTS : appValues.EVENTS,
            values : appValues

        }
    });
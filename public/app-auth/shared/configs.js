app.config(function (localStorageServiceProvider) {
    localStorageServiceProvider
        .setPrefix('EduNet')
        .setStorageType('sessionStorage')
        .setNotify(true, true)
});


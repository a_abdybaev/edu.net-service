app.factory('userApi', function ($http,localStorageService,$window) {
    var EVENTS = {
        TOKEN : "TOKEN",
        USER_TYPE : "USER_TYPE",
        IS_DEV : "IS_DEV"
    };
    return {
        authUser : function (data,cb) {
            return $http
                .post('/api/auth/login-user',{
                    phone : data.phone,
                    password : data.password,
                    centerDomain : data.centerDomain
                })
                .then(function (response) {
                    if (response.status === 200) {
                        cb(null,response.data);
                    } else {
                        cb(response);
                    }
                }, function (response) {
                    cb(response);
                });
        },
        setToken : function (token) {
            return localStorageService.set(EVENTS.TOKEN,token);
        },
        getToken : function () {
            return localStorageService.get(EVENTS.TOKEN)
        },
        setUserType : function (user) {
            return localStorageService.set(EVENTS.USER_TYPE,user)
        },
        getUser : function () {
            return localStorageService.get(EVENTS.USER_TYPE);
        },
        checkAuthorization : function () {
            var token = this.getToken();
            if (!token) {
                $window.location.href = "/";
            }
        },
        removeToken : function () {
            return localStorageService.remove(EVENTS.TOKEN);
        },
        setDevelopment : function (isDev) {
            return localStorageService.set(EVENTS.IS_DEV,isDev);
        }

    }
});
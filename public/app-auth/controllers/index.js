
angular
    .module("EduNet")
    .controller("indexController", function (localStorageService,$location,appValidator,appConstants,$scope,$window,userApi) {
        $scope.emptyField = false;
        $scope.notFound = false;
        $scope.clearErrors = function () {
            $scope.emptyField = false;
            $scope.notFound = false;
        };

        $scope.login = function () {
            var authentication = {
                phone : $scope.phone,
                password : $scope.password,
                centerDomain : $location.host().split('.')[0]
            };
            if (!appValidator.isValidField($scope,'phone') || !appValidator.isValidField($scope,'password')) return $scope.emptyField = true;
            userApi.authUser(authentication, function (err,data) {
                if (err) return $scope.notFound = true;
                var token = data.token,
                    user = data.user;
                userApi.setUserType(user);
                userApi.setToken(token);
                var domain = $location.host().split('.')[0];
                userApi.setDevelopment(domain == 'interpress' || domain == 'mk');
                switch (user) {
                    case appConstants.STUDENT :
                        $window.location.href = "/student";
                        break;
                    case appConstants.TEACHER :
                        $window.location.href = "/teacher";
                        break;
                    case appConstants.ADMIN :
                        $window.location.href = "/admin";
                        break;
                    case appConstants.TESTING :
                        $window.location.href = "/test";
                        break;
                }
            });

        };
    });

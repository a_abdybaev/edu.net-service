"use strict";
angular
    .module("EduNet")
    .controller("groupInfoController", function (appHelper,$sce,ngDialog,adminScheduleHelper,adminHelper,$rootScope,$scope,localStorageService,adminApi) {
        var loadStudentsByGroupID = function () {
            adminApi.getStudentsByGroupID({
                groupID : self.group._id
            },function (err,students) {
                if (err) return appHelper.showFailAlertGeneralError();
                self.group.students = students;
                $rootScope.$broadcast(appHelper.EVENTS.INIT_STUDENTS);
                $rootScope.$broadcast(appHelper.EVENTS.INIT_PAYMENTS);
                $rootScope.$broadcast(appHelper.EVENTS.INIT_ATTENDANCE);


            });
        };
        var loadGroupByID = function (groupID) {
            adminApi.getGroupByID(groupID, function (err,data) {
                if (err) return appHelper.showError(err);
                self.group = data;
                self.isLoadedGroupInfo = true;
                $rootScope.$broadcast(appHelper.EVENTS.GROUP_LOADED,self.group);
            });
        };

        var initController = function () {
            groupID = appHelper.getActGroupID();
            if (!groupID) return adminHelper.goToMain();
            loadGroupByID(groupID);
        };
        // INIT
        var self = this,
            groupID;

        $rootScope.$on(appHelper.EVENTS.LOAD_STUDENTS,loadStudentsByGroupID);
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,initController);

        if ($scope.$parent.index.isLoad) {
            initController();
        }



    });
angular
    .module("EduNet")
    .controller("attendanceControllerGroupInfo", function (adminHelper,appHelper,ngDialog,$rootScope,$scope,$location,adminApi,appConstants) {
        var getAllDaysArray = function () {
            var months = appHelper.getMonthNamesWithYearLowerCase();
            var results = [];
            var year = 2017;
            months.forEach(function (month,i) {
                var num = 0;
                results.push({
                    name : month,
                    days : [],
                    hasAttendance : false
                });
                var daysInArray = new Date(year,i+1,0).getDate();
                for (var j = 1; j <= daysInArray;j++) {
                    results[i].days[j] = appConstants.STUDENT_APPEARANCE_EMPTY_DAY;
                }
            });
            return results;
        };
        var buildStudents = function (students,attendances) {
            var self = this;
            var results = [];
            students.forEach(function (student,i) {
                student.hasAttendance = false;
                results.push(student);
                results[i].appearance = self.getAllDaysArray();
                attendances.forEach(function (attendance) {
                    var index = attendance.appearance.map(function (appearance) {
                        return appearance.studentID;
                    }).indexOf(student._id);
                    if (index < 0) return;
                    var value = attendance.appearance[index].value,
                        day = attendance.day,
                        month = attendance.month;
                    results[i].appearance[month].days[day] = value;
                    results[i].appearance[month].hasAttendance = true;
                    results[i].hasAttendance = true;
                });
            });
            return students;
        };
        var buildAppearance = function (students) {
            var self = this;
            students.forEach(function (student) {
                student.calendar = getAllDaysArray();
                if (student.appearance.length == 0) return;
                student.hasAttendance = true;
                student.appearance.forEach(function (appearance) {
                    student.calendar[appearance.month].hasAttendance = true;
                    student.calendar[appearance.month].days[appearance.day] = appearance.value;
                });
            });
            return students;
        };
        var changeActiveStudentIndex = function (index) {
            self.activeStudent = self.students[index];
        };

        var initialize = function (group) {
            if (self.isLoaded) return;
            self.isLoaded = true;
            if (group.students.length == 0) return self.noStudents = true;
            self.noStudents = false;
            self.students = buildAppearance(group.students);
            changeActiveStudentIndex(0);


        };

        // INIT
        var self = this;
        var ctrl = $scope.$parent.ctrl;

        self.STUDENT_APPEARANCE_EMPTY_DAY = appConstants.STUDENT_APPEARANCE_EMPTY_DAY;
        self.STUDENT_APPEARANCE_PRESENCE = appConstants.STUDENT_APPEARANCE_PRESENCE;
        self.STUDENT_APPEARANCE_ABSENCE = appConstants.STUDENT_APPEARANCE_ABSENCE;
        self.STUDENT_APPEARANCE_ABSENCE_REASON = appConstants.STUDENT_APPEARANCE_ABSENCE_REASON;
        self.changeActiveStudentIndex = changeActiveStudentIndex;

        $rootScope.$on(appHelper.EVENTS.INIT_ATTENDANCE,function () {
            self.isLoaded = false;
            initialize(ctrl.group);
        });

        self.isLoaded = false;

        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });
        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);
    });
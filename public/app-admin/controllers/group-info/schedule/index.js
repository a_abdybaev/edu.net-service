angular
    .module("EduNet")
    .controller("scheduleControllerGroupInfo", function (adminHelper,appHelper,ngDialog,$rootScope,$scope,$location,adminApi,scheduleBuilder) {

        var initialize = function (group) {
            if (self.isLoaded || group.schedule.length == 0) return;
            self.isLoaded = true;
            _.each(group.schedule,function (lesson) {
                lesson.style = {
                    'top' :  scheduleBuilder.convertStartTimeToTopPosition(top,lesson.start) + 'px',
                    'height' : (scheduleBuilder.convertTimeToMinutes(lesson.end) -scheduleBuilder.convertTimeToMinutes(lesson.start)) * 2/3 + 'px',
                    'background-color' : group.color
                };
                self.schedule[lesson.day-1].push(lesson);
                var startTime = new Date();
                startTime.setHours(lesson.start.hour);
                startTime.setMinutes(lesson.start.minute);
                lesson.startTime = startTime;
                var endTime = new Date();
                endTime.setHours(lesson.end.hour);
                endTime.setMinutes(lesson.end.minute);
                lesson.endTime = endTime;
            });
        };
        // INIT

        var self = this;
        var ctrl = $scope.$parent.ctrl;
        this.isLoaded = false;
        this.schedule = [[],[],[],[],[],[]];
        this.days = appHelper.getWeekDaysUpperCaseWithoutSunday();
        var top = 439;
        var height = 40;


        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });
        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);
    });
angular
    .module("EduNet")
    .controller("applicationsControllerGroupInfo", function (appValues,adminHelper,appHelper,ngDialog,$rootScope,$scope,$location,adminApi) {


        var deleteApplicationAnswer = function (applicationID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deleteApplicationAnswerController",
                data : {
                    applicationID : applicationID
                }
            });
        };
        var editApplicationAnswer = function (applicationID) {
            ngDialog.open({
                template : "/app-admin/views/applications/alerts/edit-application-answer.html",
                className : 'ngdialog-theme-default',
                controller : "editApplicationAnswerController",
                data : {
                    applicationID : applicationID
                },
                width : 600
            });
        };
        var addApplicationAnswer = function (applicationID) {
            ngDialog.open({
                template : "/app-admin/views/applications/alerts/add-application-answer.html",
                className : 'ngdialog-theme-default',
                controller : "addApplicationAnswerController",
                data : {
                    applicationID : applicationID
                },
                width : 600
            });
        };
        var loadApplications = function () {
            adminApi.getApplications({
                groupID : ctrl.group._id
            },function (err,applications) {
                if (err) return appHelper.showFailAlertGeneralError();
                ctrl.group.applications = applications;
            });
        };
        var initialize = function (group) {
            if (isLoaded) return;
            isLoaded = true;
        };


        // INIT
        var self = this;
        var ctrl = $scope.$parent.ctrl;
        this.deleteApplicationAnswer = deleteApplicationAnswer;
        this.editApplicationAnswer = editApplicationAnswer;
        this.addApplicationAnswer = addApplicationAnswer;

        var isLoaded = false;
        $rootScope.$on(appHelper.EVENTS.LOAD_APPLICATIONS,loadApplications);






        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });
        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);
    });
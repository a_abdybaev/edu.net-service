angular
    .module("EduNet")
    .controller("aboutControllerGroupInfo", function (appValues,adminHelper,appHelper,adminConstants,ngDialog,$rootScope,$scope,$location,adminApi) {
        var editGroup = function () {
            $location.path('/edit-group');
        };

        var deleteGroup = function () {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deleteGroupController",
                data : {
                    groupID : ctrl.group._id
                }
            });
        };
        var addNote = function () {
            ngDialog.open({
                template : "/app-admin/views/group-info/about/alerts/add-note.html",
                className : 'ngdialog-theme-default',
                data : {
                    groupID : ctrl.group._id
                },
                width : 600
            });
        };
        var editNote = function (index) {
            if (index >= ctrl.group.notes.length) return;
            ngDialog.open({
                template : "/app-admin/views/group-info/about/alerts/edit-note.html",
                className : 'ngdialog-theme-default',
                data : {
                    groupID : ctrl.group._id,
                    noteID : ctrl.group.notes[index]._id
                },
                width : 600
            });
        };
        var deleteNote = function (index) {
            if (index >= ctrl.group.notes.length) return;
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deleteNoteController",
                data : {
                    groupID : ctrl.group._id,
                    noteID : ctrl.group.notes[index]._id,
                }
            });
        };
        var showNote = function (index) {
            self.activeNoteIndex = index;
        };
        var loadNotes = function () {
            adminApi.getNotesByGroupID({
                groupID : ctrl.group._id
            },function (err,notes) {
                if (err) return appHelper.showFailAlertGeneralError();
                ctrl.group.notes = notes;
                self.activeNoteIndex = 0;
            });
        };


        // INIT
        var self = this;
        var ctrl = $scope.$parent.ctrl;
        var isLoaded = false;
        this.showNote = showNote;
        this.editGroup = editGroup;
        this.deleteGroup = deleteGroup;
        this.addNote = addNote;
        this.editNote = editNote;
        this.deleteNote = deleteNote;
        $rootScope.$on(appHelper.EVENTS.LOAD_NOTES,loadNotes);
        var initialize = function (group) {
            if (isLoaded) return;
            isLoaded = true;
            self.activeNoteIndex = 0;
        };
        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });
        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);
    });
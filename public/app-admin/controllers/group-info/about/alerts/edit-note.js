"use strict";

angular
    .module("EduNet")
    .controller("editNoteController", function (adminValues,appHelper,appValidator,ngDialog,adminHelper,$rootScope,$scope,adminApi) {
        var updateNote = function () {
            if (!appValidator.isValidField(self,'content')) return;
            $scope.closeThisDialog();
            adminApi.updateNoteByID({
                noteID : noteID,
                groupID : groupID,
                content : self.content
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.NOTE_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_NOTES);
            });
        };
        var loadNoteByID = function (noteID) {
            adminApi.getNoteByID({
                groupID : groupID,
                noteID : noteID
            },function (err,note) {
                if (err) {
                    $scope.closeThisDialog();
                    appHelper.showFailAlertGeneralError();
                    return;
                }
                self.content = note.value;
            });
        };

        // INIT

        var self = this;
        var groupID = $scope.ngDialogData.groupID,
            noteID = $scope.ngDialogData.noteID;
        this.updateNote = updateNote;
        if (!groupID || !noteID) return $scope.closeThisDialog();
        loadNoteByID(noteID);
    });
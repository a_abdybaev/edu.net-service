"use strict";

angular
    .module("EduNet")
    .controller("deleteNoteController", function (adminValues,appHelper,adminValidator,ngDialog,adminHelper,$rootScope,$scope,adminApi) {
        var deleteNote = function () {
            $scope.closeThisDialog();
            adminApi.deleteNoteByID({
                groupID : groupID,
                noteID : noteID
            }, function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.NOTE_DELETED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_NOTES);
            });
        };

        var groupID = $scope.ngDialogData.groupID,
            noteID = $scope.ngDialogData.noteID;
        if (!groupID || !noteID) return appHelper.closeDialogAndShowGenError($scope);
        var modal = {
            text :  adminHelper.delMessage.note.text,
            comment : adminHelper.delMessage.note.comment,
            del : deleteNote
        };

        $scope.modal = modal;


    });
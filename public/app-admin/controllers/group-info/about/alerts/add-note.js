"use strict";

angular
    .module("EduNet")
    .controller("addNoteController", function (adminValues,appHelper,appValidator,adminValidator,ngDialog,adminHelper,adminScheduleHelper,$rootScope,$scope,adminApi) {
        var addNote = function () {
            if (!appValidator.isValidField(self,'content')) return;
            $scope.closeThisDialog();
            adminApi.addNoteByGroupID({
                groupID : groupID,
                content : self.content
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.NOTE_CREATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_NOTES);

            });
        };

        // INIT
        var self = this;
        this.addNote = addNote;
        var groupID = $scope.ngDialogData.groupID;
        if (!groupID) return $scope.closeThisDialog();


    });
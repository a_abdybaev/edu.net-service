angular
    .module("EduNet")
    .controller("interviewControllerGroupInfo", function (appConstants,adminHelper,appHelper,ngDialog,$rootScope,$scope,$location,adminApi) {
        var resolveAnswers = function () {
            _.each(self.answers,function (answer) {
                answer.analytics.push(0);
                answer.popularIndex = 0;
                answer.popularAnswer = answer.analytics[0];
                _.each(answer.analytics,function (value,index) {
                        if (answer.popularAnswer < value) {
                            answer.popularIndex = index;
                            answer.popularAnswer = value;
                        }
                });
            });
        };
        var initialize = function (group) {

            // NEED CHANGE IN FUTURE
            if (!group.interviewResultID) return self.hasInterview = false;
            self.hasInterview = true;
            self.questions = group.interviewResultID.interviewID.questions;
            self.answers = group.interviewResultID.questions;
            self.answers[0].analytics = [5,10,5,15,20];
            self.answers[1].analytics = [10,5,20,15,5];
            self.answers[2].analytics = [11,22,7,10,5];
            self.answers[3].analytics = [11,22,7,10,5];
            self.answers[4].analytics = [11,22,7,10,5];
            self.answers[5].analytics = [11,22,7,10,5];
            self.answers[6].analytics = [0,55];
            self.answers[7].analytics = [35,20];
            resolveAnswers();
        };

        var startInterview = function () {
            var result = confirm("Вы уверены что хотите начать опрос?");
            if (!result) return;
            adminApi.startInterview({
                groupID : ctrl.group._id
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showGeneralSuccessAlert();
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_GROUP)
            });
        };
        this.startInterview = startInterview;

        // INIT
        var self = this;
        var ctrl = $scope.$parent.ctrl;
        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);
        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });
        this.INTERVIEW = appConstants.INTERVIEW;
        this.CUSTOM = appHelper.CUSTOM;
        this.outOfFive = appConstants.INTERVIEW.QUESTIONS.OUT_OF_FIVE_UPPERCASE;
        this.outOfFiveLowerCase = appConstants.INTERVIEW.QUESTIONS.OUT_OF_FIVE;
        this.outOfTwo = appConstants.INTERVIEW.QUESTIONS.OUT_OF_TWO;
        this.labels = [];
        this.labels[appConstants.INTERVIEW.OUT_OF_TWO] = this.outOfTwo;
        this.labels[appConstants.INTERVIEW.OUT_OF_FIVE] = this.outOfFive;


    });
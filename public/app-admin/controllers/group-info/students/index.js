angular
    .module("EduNet")
    .controller("studentsControllerGroupInfo", function (appValues,adminHelper,appHelper,ngDialog,$rootScope,$scope,$location,adminApi) {

        var editStudentByID = function (studentID) {
            ngDialog.open({
                template : "/app-admin/views/students/active/alerts/edit-student.html",
                className : "ngdialog-theme-default",
                width : 600,
                data : {
                    studentID : studentID
                }
            });
        };
        var deleteStudentByID = function (studentID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deleteStudentFromGroupController",
                data : {
                    studentID : studentID,
                    groupID : ctrl.group._id
                }
            });
        };
        var addStudent = function () {
            ngDialog.open({
                template : "/app-admin/views/group-info/students/alerts/add-student.html",
                className : 'ngdialog-theme-default',
                controller : "addStudentAlertController",
                data : {
                    groupID : ctrl.group._id
                }
            });
        };

        var loadStudentsByGroupID = function () {
            adminApi.getStudentsByGroupID({
                groupID : ctrl.group._id
            },function (err,students) {
                if (err) return appHelper.showFailAlertGeneralError();
                ctrl.group.students = students;
            });
        };
        var initialize = function (group) {
            if (self.isLoaded) return;
            self.isLoaded = true;
            if (group.students.length == 0) return self.noStudents = true;
            self.noStudents = false;
        };

        var studentInfo = function (studentID) {
            ngDialog.open({
                template : "/app-admin/views/students/alerts/student-info/index.html",
                className : "ngdialog-theme-default",
                width : 1120,
                data : {
                    studentID : studentID
                }
            });
        };

        // INIT
        var self = this;
        var ctrl = $scope.$parent.ctrl;
        this.studentInfo = studentInfo;
        this.editStudentByID = editStudentByID;
        this.deleteStudentByID = deleteStudentByID;
        this.addStudent = addStudent;
        $rootScope.$on(appHelper.EVENTS.INIT_STUDENTS,function() {
            self.isLoaded = false;
            initialize(ctrl.group);
        });

        self.isLoaded = false;
        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });

        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);
    });
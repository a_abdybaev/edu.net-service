"use strict";

angular
    .module("EduNet")
    .controller("deleteStudentFromGroupController", function (adminValues,appHelper,adminHelper,$rootScope,$scope,adminApi) {
        var deleteStudent = function () {
            $scope.closeThisDialog();
            adminApi.deleteStudentFromGroup({
                groupID : groupID,
                studentID : studentID
            }, function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.STUDENT_DELETED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_STUDENTS);
            });
        };



        // INIT
        var studentID = $scope.ngDialogData.studentID,
            groupID = $scope.ngDialogData.groupID;
        if (!studentID || !groupID) return appHelper.closeDialogAndShowGenError($scope);

        var modal = {
            text :  adminHelper.delMessage.studentFromGroup.text,
            comment : adminHelper.delMessage.studentFromGroup.comment,
            del : deleteStudent
        };
        $scope.modal = modal;

    });
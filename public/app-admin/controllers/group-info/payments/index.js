angular
    .module("EduNet")
    .controller("paymentsControllerGroupInfo", function (appValues,adminHelper,appHelper,ngDialog,$rootScope,$scope,$location,adminApi) {
        var addPayment = function () {
            ngDialog.open({
                template : "/app-admin/views/payments/alerts/add-payment.html",
                className : 'ngdialog-theme-default',
                width : 600,
                data : {
                    groupID : appHelper.getActGroupID()
                }
            });
        };
        var editPaymentByID = function (paymentID) {
            ngDialog.open({
                template : "/app-admin/views/payments/alerts/edit-payment.html",
                className : 'ngdialog-theme-default',
                width : 600,
                data : {
                    paymentID : paymentID
                }
            });
        };
        var deletePaymentByID = function (paymentID) {
            ngDialog.open({
                template :appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deletePaymentController",
                data : {
                    paymentID : paymentID
                }
            });
        };
        var changeActiveStudentIndex = function (index) {
            if (self.activeStudentIndex == index) return;
            self.activeStudentIndex = index;
            self.activeStudent = ctrl.group.students[index];

        };
        var computePaymentForStudent = function (student) {
            student.totalPayment = 0;
            if (_.isEmpty(student.payments)) return;
            _.each(student.payments,function (payment) {
                student.totalPayment+=payment.value
            });
        };
        var computeTotalPaymentForStudents = function (students) {
            var self = this;
            if (_.isEmpty(students)) return;
            _.each(students,function (student) {
                computePaymentForStudent(student);
            });
        };


        this.addPayment = addPayment;
        this.editPaymentByID = editPaymentByID;
        this.changeActiveStudentIndex = changeActiveStudentIndex;
        this.deletePaymentByID = deletePaymentByID;

        var initialize = function (group) {
            if (self.isLoaded) return;
            self.isLoaded = true;
            if (_.isEmpty(group.students)) return self.noStudents = true;
            self.noStudents = false;
            computeTotalPaymentForStudents(group.students);
            self.activeStudent = group.students[0];
            self.activeStudentIndex = 0;
        };
        var self = this;
        var ctrl = $scope.$parent.ctrl;
        self.isLoaded = false;

        $rootScope.$on(appHelper.EVENTS.INIT_PAYMENTS,function () {
            self.isLoaded = false;
            initialize(ctrl.group);
        });
        $rootScope.$on(appHelper.EVENTS.LOAD_PAYMENTS,function () {
            $rootScope.$broadcast(appHelper.EVENTS.LOAD_STUDENTS);
        });

        $rootScope.$on(appHelper.EVENTS.GROUP_LOADED,function (event,group) {
            initialize(group);
        });
        if (ctrl.isLoadedGroupInfo) initialize(ctrl.group);
    });
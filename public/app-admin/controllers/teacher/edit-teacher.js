"use strict";

angular
    .module("EduNet")
    .controller("editTeacherController", function (adminValues,appHelper,adminValidator,adminConstants,ngDialog,$rootScope,$scope,$location,adminApi,adminHelper) {
        var showAvatarView = function () {
            $scope.myImage = '';
            $scope.myCroppedImage = '';
            $scope.imageUrl = null;
            $scope.isSettingAvatar = true;
        };
        var closeAvatarView = function () {
            $scope.isSettingAvatar = false;

        };
        var handleFileSelect=function(evt) {
            var file=evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function($scope){
                    $scope.myImage=evt.target.result;
                });
            };
            reader.readAsDataURL(file);
        };
        var updateTeacher = function () {
            if (!adminValidator.isValidEditTeacher($scope.teacher)) return;
            var obj = {
                firstName : $scope.teacher.firstName,
                lastName : $scope.teacher.lastName,
                email : $scope.teacher.email,
                teacherID : teacherID
            };
            var croppedImage = $scope.myCroppedImage;
            if (croppedImage!=undefined && croppedImage != '') {
                obj.image = croppedImage;
            }

            adminApi.updateTeacherByID(obj, function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.TEACHER_UPDATED);
                $location.path('/list-of-teachers');
            });
        };
        var cancelAvatar = function () {
            $scope.myImage = '';
            $scope.myCroppedImage = '';
            $scope.closeAvatarView();
            $scope.imageUrl = null;
        };
        var cancel = function () {
            $location.path(appHelper.getPrev() == adminHelper.values.PREV_PAGE.TEACHER_INFO  ? 'teacher-info' : 'list-of-teachers');
        };
        var loadTeacherByID = function (teacherID) {
            adminApi.getTeacherByID(teacherID, function (err,info) {
                if (err) return appHelper.showError(err);
                $scope.teacher = info.teacher;
            });

        };
        var initController = function () {

            $scope.teacher = {};
            $scope.isSettingAvatar = false;
            $scope.myImage='';
            $scope.myCroppedImage='';
            angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
            teacherID = adminHelper.getActTeacherID();
            if (!teacherID) return $location.path('/list-of-teachers');
            loadTeacherByID(teacherID);
        };
        $scope.showAvatarView = showAvatarView;
        $scope.closeAvatarView = closeAvatarView;
        $scope.updateTeacher = updateTeacher;
        $scope.cancelAvatar = cancelAvatar;
        $scope.cancel = cancel;


        // INIT
        var teacherID;

        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }



    });
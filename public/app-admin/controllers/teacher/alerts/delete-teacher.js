"use strict";

angular
    .module("EduNet")
    .controller("deleteTeacherAlertController", function (adminValues,appHelper,$rootScope,adminConstants,adminApi,adminHelper,$scope,$location) {
        var deleteTeacher = function () {
            $scope.closeThisDialog();
            adminApi.deleteTeacherByID(teacherID, function (err) {
                if (err) return appHelper.showFailAlertGeneralError();

                appHelper.showSuccessAlert(adminValues.TEACHER_DELETED);
                if (path != undefined && path != '') {
                    $location.path(path);
                } else {
                    $rootScope.$broadcast(appHelper.EVENTS.LOAD_TEACHERS);
                }
            });
        };


        // INIT

        var modal = {
            text :  adminHelper.delMessage.teacher.text,
            comment : adminHelper.delMessage.teacher.comment,
            del : deleteTeacher
        };

        var teacherID = $scope.ngDialogData.teacherID,
            path = $scope.ngDialogData.path;
        if (!teacherID) return appHelper.closeDialogAndShowGenError($scope);
        $scope.modal = modal;

    });
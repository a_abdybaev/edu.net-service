"use strict";

angular
    .module("EduNet")
    .controller("teacherInfoController", function (appHelper,scheduleBuilder,ngDialog,adminHelper,adminScheduleHelper,$rootScope,$scope,$location,adminApi) {

        var loadTeacherByID = function () {
            adminApi.getTeacherByID(self.teacherID, function (err,info) {
                if (err) return appHelper.showError(err);
                self.teacher = info.teacher;
                self.groups = info.groups;
                if (_.isEmpty(info.groups)) return self.noGroups = true;
                adminApi.getSchedule({
                    teacherID : self.teacherID
                },function (err,lessons) {
                    if (err) return appHelper.showError(err);
                    self.lessons = scheduleBuilder.buildScheduleForTeacherInfo(lessons);
                });

            });
        };
        var editTeacher = function () {
            appHelper.setPrev(adminHelper.values.PREV_PAGE.TEACHER_INFO);
            $location.path('/edit-teacher');
        };
        var deleteTeacher = function () {
            ngDialog.open({
                template : "/app-admin/views/alerts/delete-confirm.html",
                className : 'ngdialog-theme-default',
                controller : "deleteTeacherAlertController",
                data : {
                    teacherID : self.teacherID,
                    path : 'list-of-teachers'
                }
            });
        };
        var getTeacherScheduleInfo = function (groupID) {
            return;
            ngDialog.open({
                template : "/app-admin/views/teacher/alerts/teacher-schedule-info.html",
                className : 'ngdialog-theme-default',
                data : {
                    groupID : groupID
                },
                width : 600
            });
        };
        var initController = function () {
            self.teacherID = adminHelper.getActTeacherID();
            if (!self.teacherID) return $location.path('/list-of-teachers');
            self.noGroups = false;
            loadTeacherByID();


        };
        this.editTeacher = editTeacher;
        this.deleteTeacher = deleteTeacher;
        this.getTeacherScheduleInfo = getTeacherScheduleInfo;



        // INIT
        var self = this;
        this.days = adminScheduleHelper.getDayNames();
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }



    });
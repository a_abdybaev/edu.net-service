"use strict";

angular
    .module("EduNet")
    .controller("addTeacherController", function (appValues,adminValues,appConstants,adminValidator,adminConstants,ngDialog,appHelper,adminHelper,$rootScope,$scope,$location,adminApi) {
        var showAvatarView = function () {
            $scope.myImage = '';
            $scope.myCroppedImage = '';
            $scope.imageUrl = null;
            $scope.isSettingAvatar = true;
        };
        var closeAvatarView = function () {
            $scope.isSettingAvatar = false;
        };
        var cancelAvatarView = function () {
            angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
            $scope.isSettingAvatar = false;
            $scope.myImage = undefined;
            $scope.myCroppedImage = undefined;
            $scope.imageUrl = undefined;
        };
        var addTeacher = function () {
            if (!adminValidator.isValidAddTeacher($scope)) return;
            var obj = {
                firstName : $scope.firstName,
                lastName : $scope.lastName,
                phone : $scope.phone,
                email : $scope.email,
                gender : $scope.gender
            };

            var croppedImage = $scope.myCroppedImage;
            if (croppedImage!=undefined && croppedImage != '') {
                obj.image = croppedImage;
            }
            adminApi.addTeacher(obj, function (err) {
                if (err && err.status === 406) return appHelper.showFailAlert(appValues.DUPLICATE_LOGIN_ERROR);
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.TEACHER_CREATED);
                $location.path("/list-of-teachers");
            });
        };
        var cancel = function () {
            $location.path('/list-of-teachers');
        };
        var cancelAvatar = function () {
            $scope.myImage = '';
            $scope.myCroppedImage = '';
            $scope.closeAvatarView();
            $scope.imageUrl = null;
        };
        $scope.showAvatarView = showAvatarView;
        $scope.closeAvatarView = closeAvatarView;
        $scope.cancelAvatarView = cancelAvatarView;
        $scope.addTeacher = addTeacher;
        $scope.cancel = cancel;
        $scope.cancelAvatar = cancelAvatar;
        var handleFileSelect=function(evt) {
            var file=evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function($scope){
                    $scope.myImage=evt.target.result;
                });
            };
            reader.readAsDataURL(file);
        };


        // INIT

        $scope.isSettingAvatar = false;
        $scope.myImage='';
        $scope.myCroppedImage='';
        $scope.genders = {
            man : appConstants.GENDER.MAN,
            woman : appConstants.GENDER.WOMAN
        };
        $scope.gender = appConstants.GENDER.WOMAN;

        angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);




    });
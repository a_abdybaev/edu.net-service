"use strict";

angular
    .module("EduNet")
    .controller("listOfTeachersController", function (appValues,appConstants,appHelper,adminConstants,ngDialog,adminHelper,$rootScope,$scope,$location,adminApi) {
        var changeViewStyle = function (viewStyle) {
            self.viewStyle = viewStyle;
        };
        var showProperties = function (index) {
            self.properties[index] = true;
        };
        var hideProperties = function (index) {
            self.properties[index] = false;
        };
        var changeDisplayOption = function (option) {
            self.displayOption = option;
        };
        var deleteTeacherByID = function (teacherID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deleteTeacherAlertController",
                data : {
                    teacherID : teacherID
                }
            });
        };
        var editTeacherByID = function (teacherID) {
            adminHelper.setActTeacherID(teacherID);
            $location.path('/edit-teacher');
        };
        var goToTeacherInfo = function (teacherID) {
            adminHelper.setActTeacherID(teacherID);
            $location.path('/teacher-info');
        };
        var addTeacher = function () {
            $location.path('/add-teacher');
        };
        var loadTeachers = function () {
            adminApi.getTeachers(function (err,teachers) {
                if (err) return appHelper.showError(err);
                if (teachers.length == 0) return self.noTeachers = true;
                _.each(teachers,function (teacher) {
                    self.properties.push(false);
                });

                self.teachers = teachers;
            });
        };
        var initController = function () {
            self.properties = [];
            self.noTeachers = false;
            loadTeachers();
        };

        this.changeViewStyle = changeViewStyle;
        this.showProperties = showProperties;
        this.hideProperties = hideProperties;
        this.changeDisplayOption = changeDisplayOption;
        this.deleteTeacherByID = deleteTeacherByID;
        this.editTeacherByID = editTeacherByID;
        this.goToTeacherInfo = goToTeacherInfo;
        this.addTeacher = addTeacher;

        // INIT
        var self = this;


        this.isBlocsCtrl = true;
        this.viewStyle = adminConstants.VIEW_STYLES.VIEW_STYLE_BLOCKS;
        this.viewStyles = adminConstants.VIEW_STYLES;
        this.appConstants = appConstants;
        this.isBlocsCtrl = false;


        $rootScope.$on(appValues.EVENTS.LOAD_TEACHERS, loadTeachers);
        $rootScope.$on(appValues.EVENTS.INDEX_LOADED,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }




    });
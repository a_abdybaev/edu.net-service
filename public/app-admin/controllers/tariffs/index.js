angular
    .module("EduNet")
    .controller("tariffsController", function (appHelper,adminApi,$rootScope,$scope,localStorageService,$location,$mdSidenav) {
        var loadTariffs = function () {
            adminApi.getTariffs(function (err,data) {
                if (err) return appHelper.showFailAlertGeneralError();
                self.tariff = data.tariff;
                self.tariffs = data.tariffs;
            });
        };



        // INIT
        var self = this;
        loadTariffs();


    });

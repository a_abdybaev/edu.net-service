"use strict";

angular
    .module("EduNet")
    .controller("listOfClassroomsController", function (appValues,appHelper,adminHelper,$rootScope,ngDialog,$scope,adminApi) {

        var loadClassrooms = function () {
            adminApi.getClassrooms(function (err,data) {
                if (err) return appHelper.showError(err);
                self.classrooms = data;
                self.properties = [];
            });
        };
        var showProperties = function (index) {
            this.properties[index] = true;
        };
        var hideProperties = function (index) {
            this.properties[index] = false;
        };
        var editClassroomByID = function (classroomID) {
            ngDialog.open({
                template : "/app-admin/views/classroom/alerts/edit-classroom.html",
                className : "ngdialog-theme-default",
                width : 520,
                data : {
                    classroomID : classroomID
                }
            });
        };
        var deleteClassroomByID = function (classroomID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deleteClassroomAlertController",
                data : {
                    classroomID : classroomID
                }
            });
        };
        var addClassroom = function () {
            ngDialog.open({
                template : "/app-admin/views/classroom/alerts/add-classroom.html",
                className : "ngdialog-theme-default",
                width : 520,
                controller : "addClassroomController"
            });
        };

        this.showProperties = showProperties;
        this.hideProperties = hideProperties;
        this.editClassroomByID = editClassroomByID;
        this.deleteClassroomByID = deleteClassroomByID;
        this.addClassroom = addClassroom;




        // INIT
        var self = this;
        this.classrooms = [];
        this.isEmpty = false;
        $rootScope.$on(appValues.EVENTS.LOAD_CLASSROOMS, loadClassrooms);
        $rootScope.$on(appValues.EVENTS.INDEX_LOADED,loadClassrooms);
        if ($scope.$parent.index.isLoad) {
            loadClassrooms();
        }
    });

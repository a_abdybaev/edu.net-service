"use strict";

angular
    .module("EduNet")
    .controller("addClassroomController", function (appHelper,appValidator,adminValues,appValues,adminHelper,$rootScope,$scope,adminApi) {

        var addClassRoom = function () {
            var name = $scope.name;
            if (!appValidator.isValidField($scope,'name')) return;
            $scope.closeThisDialog();
            adminApi.addClassroom({
                name : name
            }, function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.CLASSROOM_CREATED);
                $rootScope.$broadcast(appValues.EVENTS.LOAD_CLASSROOMS);
            });
        };

        $scope.addClassroom = addClassRoom;

    });

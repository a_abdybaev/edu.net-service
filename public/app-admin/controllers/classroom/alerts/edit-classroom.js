"use strict";

angular
    .module("EduNet")
    .controller("editClassroomController", function (appValues,adminValues,appValidator,appHelper,$rootScope,adminHelper,$scope,adminApi) {

        var loadClassroom = function (classroomID) {
            adminApi.getClassroomByID(classroomID, function (err,data) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                self.name = data.name;
            });
        };
        var updateClassroom = function () {
            if (!appValidator.isValidField(self,'name')) return;
            adminApi.updateClassroomByID({
                classroomID : classRoomID,
                name : self.name
            }, function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                $rootScope.$broadcast(appValues.EVENTS.LOAD_CLASSROOMS);
                appHelper.showSuccessAlert(adminValues.CLASSROOM_UPDATED);
            });
        };
        this.updateClassroom = updateClassroom;


        // INIT
        var self = this;

        var classRoomID = $scope.ngDialogData.classroomID;
        if (!classRoomID) return appHelper.closeDialogAndShowGenError($scope);


        loadClassroom(classRoomID);

    });

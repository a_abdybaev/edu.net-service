angular
    .module("EduNet")
    .controller("deleteClassroomAlertController", function (adminValues,appHelper,adminHelper,$rootScope,$scope,adminApi) {
        var deleteClassRoom = function () {
            adminApi.deleteClassroomByID(classRoomID, function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.CLASSROOM_DELETED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_CLASSROOMS);
            });
        };

        // INIT
        var modal = {
            text :  adminHelper.delMessage.classRoom.text,
            comment : adminHelper.delMessage.classRoom.text,
            del : deleteClassRoom
        };

        var classRoomID = $scope.ngDialogData.classroomID;
        if (!classRoomID) return appHelper.closeDialogAndShowGenError($scope);
        $scope.modal = modal;



    });
app.factory('scheduleBuilder', function (appHelper) {
    return {
        buildSchedule : function (lessons) {
            var self = this;
            var top = 231;
            var height = 40;
            var results = [[],[],[],[],[],[]];
            _.each(lessons,function (lesson) {
                lesson.style = {
                    'top' :  self.convertStartTimeToTopPosition(top,lesson.start) + 'px',
                    'height' : (self.convertTimeToMinutes(lesson.end) -self.convertTimeToMinutes(lesson.start)) * 5/6 + 'px',
                    'background-color' : lesson.groupID.color
                };
                results[lesson.day-1].push(lesson);
            });
            return results;
        },
        buildScheduleForTeacherInfo : function (lessons) {
            var self = this;
            var top = 696;
            var height = 40;
            var results = [[],[],[],[],[],[]];
            lessons.forEach(function (lesson) {
                lesson.style = {
                    'top' :  self.convertStartTimeToTopPosition(top,lesson.start) + 'px',
                    'height' : (self.convertTimeToMinutes(lesson.end) -self.convertTimeToMinutes(lesson.start)) * 5/6 + 'px',
                    'background-color' : lesson.groupID.color
                };
                results[lesson.day-1].push(lesson);
            });
            return results;
        },
        convertTimeToMinutes : function (time) {
            return parseInt(time.hour) * 60 + parseInt(time.minute);
        },
        convertStartTimeToTopPosition : function (top,start) {
            var difference = this.convertTimeToMinutes(start) - this.convertTimeToMinutes({
                    hour : 9,
                    minute : 0
                });
            return top + difference * 5/6;
        },
        getDayNamesWithoutSundayShort : function () {
            return ['Пн','Вт','Ср','Чт','Пт','Сб'];
        },
        buildScheduleForGroupID : function ($scope,group) {
            $scope.today = new Date();
            $scope.days = this.getDayNamesWithoutSundayShort();
            group.schedule.forEach(function (lesson) {
                var startTime = new Date();
                startTime.setHours(lesson.start.hour);
                startTime.setMinutes(lesson.start.minute);
                lesson.startTime = startTime;

                var endTime = new Date();
                endTime.setHours(lesson.end.hour);
                endTime.setMinutes(lesson.end.minute);
                lesson.endTime = endTime;
            });
            return group;
        },
        buildScheduleForEditGroupSchedule : function ($scope,group) {
            var self = this;
            if(group.schedule.length !=0 ) {
                group.schedule.forEach(function (lesson,i) {
                    var index = lesson.day-1;
                    $scope.days[index].isActive = true;
                    var startObj = self.getTimeForSelectTag(lesson.start);
                    var endObj = self.getTimeForSelectTag(lesson.end);
                    $scope.days[index].start.selectedMinute = startObj.selectedMinute;
                    $scope.days[index].start.selectedHour = startObj.selectedHour;
                    $scope.days[index].end.selectedMinute = endObj.selectedMinute;
                    $scope.days[index].end.selectedHour = endObj.selectedHour;
                    $scope.days[index].classroom = lesson.classroomID;
                });
            }
        },
        buildEditGroupSchedule : function (ctrl,lessons) {
            var self = this;
            if (_.isEmpty(lessons)) return;
            _.each(lessons,function (lesson,i) {
                var index = lesson.day-1;
                ctrl.days[index].isActive = true;
                var startObj = self.getTimeForSelectTag(lesson.start);
                var endObj = self.getTimeForSelectTag(lesson.end);
                ctrl.days[index].start.selectedMinute = startObj.selectedMinute;
                ctrl.days[index].start.selectedHour = startObj.selectedHour;
                ctrl.days[index].end.selectedMinute = endObj.selectedMinute;
                ctrl.days[index].end.selectedHour = endObj.selectedHour;
                ctrl.days[index].classroom = lesson.classroomID;
            });
        },
        getTimeForSelectTag : function (obj) {
            var hour = {};
            var minute = {};
            if (obj.hour <=9) {
                hour.value = 9;
                hour.label = "09";
            } else {
                hour.value = obj.hour;
                hour.label = obj.hour;
            }
            if (obj.minute <= 9) {
                minute.value = obj.minute;
                minute.label = "0" + obj.minute;
            } else {
                minute.value = obj.minute;
                minute.label = obj.minute;
            }
            return {
                selectedMinute : minute,
                selectedHour : hour
            };
        },
        clearErrors : function ($scope) {
            $scope.days.forEach(function (day,index) {
                $scope.days[index].classroomError = false;
                $scope.days[index].timeError = false;
            });
            $scope.lessonsNotSelectedError = false;
            $scope.padError = false;
        },
        isValidClassrooms : function ($scope) {
            var days = $scope.days;
            var result = true;
            days.forEach(function (day,i) {
                if (day.isActive == true) {
                    if (day.classroom._id == '-') {
                        result = false;
                        days[i].classroomError = true;
                    }
                }
                //if (day.classroom._id == '-' && day.isActive == true) {
                //    result = false;
                //}
            });
            return result;
        },
        isValidLessonTimes : function ($scope) {
            var days = $scope.days;
            var result = true;
            days.forEach(function (day,i) {
                if (day.isActive == true && day.start.selectedMinute.value + day.start.selectedHour.value * 60 >= day.end.selectedHour.value * 60 + day.end.selectedMinute.value) {
                    days[i].timeError = true;
                    result = false;
                }
            });
            return result;
        },
        isNotEmpty : function ($scope) {
            var result = false;
            $scope.lessonsNotSelectedError = true;
            $scope.days.forEach(function (day) {
                if (day.isActive == true) {
                    $scope.lessonsNotSelectedError = false;
                    result = true;
                }
            });
            return result;
        },
        isValidLessons : function($scope) {
            if (this.isNotEmpty($scope) && this.isValidClassrooms($scope) && this.isValidLessonTimes($scope)) {
                return true;
            } else {
                return false;
            }
        },
        processData : function ($scope) {
            var lessons = [];
            $scope.days.forEach(function (day,i) {
                if (day.isActive) {
                    lessons.push({
                        day : i+1,
                        start : {
                            hour : day.start.selectedHour.value,
                            minute : day.start.selectedMinute.value
                        },
                        end : {
                            hour : day.end.selectedHour.value,
                            minute : day.end.selectedMinute.value
                        },
                        classroomID : day.classroom._id
                    });
                }
            });
            return lessons;
        }
    }
});
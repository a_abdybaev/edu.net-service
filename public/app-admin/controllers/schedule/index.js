"use strict";

angular
    .module("EduNet")
    .controller("scheduleController", ['app','$rootScope','$scope',function (app,rootScope,scope) {
        var lessonInfo = function (lesson) {
            app.ngDialog.open({
                template : "/app-admin/views/schedule/alerts/schedule-info.html",
                className : 'ngdialog-theme-default',
                data : {
                    groupID : lesson.groupID._id,
                    teacherID : lesson.teacherID._id
                }
            });
        };
        var changeDisplayOption = function (option) {
            self.currentDisplayOption = option;
            loadSchedule();
        };
        var addSchedule = function () {
            app.location.path('add-schedule');
        };
        var loadSchedule = function () {
            if (self.noTeachers || self.noClassrooms) return;
            var data = {};
            if (self.currentDisplayOption == app.constants.SCHEDULE_DISPLAY_OPTION.SCHEDULE_DISPLAY_OPTION_CLASSROOMS) {
                data.classroomID = self.selectedClassroomID;
            } else {
                data.teacherID = self.selectedTeacherID;
            }
            app.api.getSchedule(data,function (err,lessons) {
                if (err) return app.main.helper.showError(err);
                self.lessons = app.scheduleBuilder.buildSchedule(lessons);

            });
        };
        var loadClassRooms = function (cb) {

            app.api.getClassrooms(function (err,classrooms) {
                if (err) return app.main.helper.showError(err);
                self.isLoaded = true;
                if (_.isEmpty(classrooms)) return self.noClassrooms = true;
                self.noClassrooms = false;
                self.selectedClassroomID = classrooms[0]._id;
                self.classrooms = classrooms;

                cb()
            });
        };
        var loadTeachers = function (cb) {
            app.api.getTeachers(function (err,teachers) {
                if (err) return app.main.helper.showError(err);
                if (_.isEmpty(teachers)) return self.noTeachers = true;
                self.noTeachers = false;
                self.selectedTeacherID = teachers[0]._id;
                app.main.helper.createNameForUsers(teachers);
                self.teachers = teachers;

                cb();

            });
        };
        var initController = function () {
            self.noClassrooms = false;
            self.noTeachers = false;
            self.isLoaded = false;
            self.classrooms = [];
            self.teachers = [];
            self.lessons = [];
            self.displayOptions = app.constants.SCHEDULE_DISPLAY_OPTION;
            self.currentDisplayOption = app.constants.SCHEDULE_DISPLAY_OPTION.SCHEDULE_DISPLAY_OPTION_CLASSROOMS;

            loadClassRooms(function () {
                loadTeachers(function () {
                    loadSchedule();
                });
            });

        };

        this.lessonInfo = lessonInfo;
        this.loadSchedule = loadSchedule;
        this.changeDisplayOption = changeDisplayOption;
        this.addSchedule = addSchedule;

        // INIT
        var self = this;
        this.days = app.scheduleHelper.getDayNames();


        rootScope.$on(app.main.helper.EVENTS.LOAD_SCHEDULE,initController);
        rootScope.$on(app.main.helper.EVENTS.INDEX_LOADED,initController);
        if (scope.$parent.index.isLoad) {
            initController();
        }



    }]);
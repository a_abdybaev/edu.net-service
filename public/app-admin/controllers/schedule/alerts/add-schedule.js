"use strict";

angular
    .module("EduNet")
    .controller("addLessonController", ['$rootScope','$scope','app',function (rootScope,scope,app) {

        var reloadTime = function (index) {
            var Hour = self.days[index].start.selectedHour,
                Minute = self.days[index].start.selectedMinute;


            self.days[index].end.hours = app.scheduleHelper.getHoursFrom(Hour.value);
            self.days[index].end.minutes = app.scheduleHelper.getMinutesFrom(Minute.value);

            self.days[index].end.selectedHour = self.days[index].end.hours[0];
            self.days[index].end.selectedMinute = self.days[index].end.minutes[0];
        };
        var addSchedule = function () {
            app.scheduleHelper.clearErrors(self);
            if (!app.scheduleHelper.isValidLessons(self)) return;
            var lessons = app.scheduleHelper.processData(self);
            app.api.addLesson({
                lessons : lessons,
                groupID : self.group._id,
                teacherID : self.teacherID
            }, function (err) {
                if (err && err.status != 406) return app.main.helper.showFailAlertGeneralError();
                if (err && err.status == 406) {
                    self.padError = {
                        context : err.data.context,
                        content : err.data.content
                    };
                    return self.modalObject = {
                        CLASSROOM : app.constants.CLASSROOM,
                        TEACHER : app.constants.TEACHER,
                        days : app.main.helper.getWeekDaysWithoutSundayDefault()
                    };
                }
                rootScope.$broadcast(app.main.helper.EVENTS.LOAD_SCHEDULE);
                app.main.helper.showSuccessAlert(app.values.SCHEDULE_CREATED);
            });
        };

        this.addSchedule = addSchedule;
        this.reloadTime = reloadTime;

        //     INIT

        var self = this;
        this.selection = {
            _id : app.main.dictionary.EMPTY_ID,
            name : app.main.dictionary.SELECT.SELECT
        };
        var loadClassRooms = function () {
            app.api.getClassrooms(function (err,classrooms) {
                if (err) return app.main.helper.showError(err);

                self.classrooms = [self.selection].concat(classrooms);
            });
        };
        var loadAvailableGroupsForSchedule = function (cb) {
            app.api.getAvailableGroupsForScheduling(function (err,groups) {
                if (err) return app.main.helper.showError(err);
                if (_.isEmpty(groups)) return self.noGroups = true;

                groups.unshift({
                    _id : "-",
                    teachers : [],
                    name : "Выбрать группу"
                });
                self.group = groups[0];
                self.teacher = self.group.teachers[0];
                self.groups = groups;
                cb()

            });
        };
        var initController = function () {
            loadAvailableGroupsForSchedule(function () {
                loadClassRooms();
            })
        };
        this.groupChanged = function () {
            if (!_.isEmpty(self.group.teachers)) {
                self.teacherID = self.group.teachers[0]._id;
            }
        };


        this.weekDays = app.main.helper.getWeekDaysWithoutSundayDefault();

        this.days = app.scheduleHelper.getDays();
        initController();

    }]);
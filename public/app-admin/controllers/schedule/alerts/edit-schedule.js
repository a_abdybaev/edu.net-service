"use strict";
angular
    .module("EduNet")
    .controller("editGroupScheduleController",
        ['app','$scope','$rootScope','scheduleBuilder',function (app,scope,rootScope,scheduleBuilder) {
        var loadClassRooms = function (cb) {
            app.api.getClassrooms(function (err,classrooms) {
                if (err) return app.main.helper.showError(err);
                if (_.isEmpty(classrooms)) return app.main.helper.showFailAlertGeneralError();
                self.classrooms = self.classrooms.concat(classrooms);
                cb();

            });
        };
        var loadGroupSchedule = function (groupID) {
            app.api.getScheduleByGroupID(groupID,function (err,group) {
                if (err) return appHelper.showFailAlertGeneralError();
                scheduleBuilder.buildScheduleForEditGroupSchedule(self,group);
                self.group = group;

            });
        };
        var loadLessonsByQuery = function () {
            app.api.getLessonsByQuery({
                groupID : groupID,
                teacherID : self.teacherID
            },function (err,lessons) {
                if (err) return app.helper.showError(err);
                self.lessons = lessons;
                self.days = app.scheduleHelper.getDays();
                scheduleBuilder.buildEditGroupSchedule(self,lessons);

            });

        };
        var loadGroupTeachers = function (cb) {
            app.api.getGroupTeachers({
                groupID : groupID
            },function (err,group) {
                if (err) return app.main.helper.showError(err);

                self.group = group;
                if (_.isEmpty(group.teachers)) return;
                // self.teacherID = group.teachers[0]._id;
                loadLessonsByQuery();
                cb();
            });
        };
        var reloadTime = function (index) {
            var Hour = self.days[index].start.selectedHour,
                Minute = self.days[index].start.selectedMinute;
            self.days[index].end.hours = app.scheduleHelper.getHoursFrom(Hour.value);
            self.days[index].end.minutes = app.scheduleHelper.getMinutesFrom(Minute.value);
            self.days[index].end.selectedHour = self.days[index].end.hours[0];
            self.days[index].end.selectedMinute = self.days[index].end.minutes[0];
        };
        var saveSchedule = function () {
            scheduleBuilder.clearErrors(self);
            if (scheduleBuilder.isValidLessons(self)) {
                var lessons = scheduleBuilder.processData(self);
                app.api.updateScheduleByGroupID({
                    lessons : lessons,
                    groupID : self.group._id,
                    teacherID : self.teacherID
                }, function (err) {
                    if (err && err.status != 406) return app.main.helper.showFailAlertGeneralError();
                    if (err && err.status === 406) {
                        self.padError = {
                            context : err.data.context,
                            content : err.data.content
                        };
                        return self.modalObject = {
                            CLASSROOM : app.constants.CLASSROOM,
                            TEACHER : app.constants.TEACHER,
                            days : app.main.helper.getWeekDaysWithoutSundayDefault()
                        };

                    }
                    rootScope.$broadcast(app.main.helper.EVENTS.LOAD_SCHEDULE);
                    app.main.helper.showSuccessAlert(app.values.SCHEDULE_UPDATED);
                });
            }
        };
        var cancel = function () {
            adminHelper.goToMain();
        };

        this.reloadTime = reloadTime;
        this.saveSchedule = saveSchedule;
        this.cancel = cancel;
        this.loadLessonsByQuery = loadLessonsByQuery;

        var self = this;
        // INIT


        this.selection = {
            _id : app.main.dictionary.EMPTY_ID,
            name : app.main.dictionary.SELECT.SELECT
        };
        this.classrooms = [this.selection];
        this.group = {
            name : "Загрузка",
            teachers : [{
                _id : app.main.dictionary.EMPTY_ID,
                name : app.main.dictionary.SELECT.SELECT
            }]
        };
        // this.teacherID = this.group.teachers[0]._id;
        this.days = app.scheduleHelper.getDays();


        var groupID = app.main.helper.getActGroupID(),
            teacherID = app.helper.getActTeacherID();

        this.teacherID = teacherID;

        if (!groupID || !teacherID) return app.helper.goToMain();
        loadClassRooms(function () {
            loadGroupTeachers(function () {

            });

        });






    }]);
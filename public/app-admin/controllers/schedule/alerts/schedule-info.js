"use strict";

angular
    .module("EduNet")
    .controller("lessonInfoController", ['app','$scope','$rootScope',function (app,scope,rootScope) {


        var loadGroupSchedule = function () {
            app.api.getScheduleByGroupID({
                groupID : groupID,
                teacherID : teacherID
            },function (err,group) {
                if (err) return app.main.helper.closeDialogAndShowGenError(scope);
                self.group = app.scheduleBuilder.buildScheduleForGroupID(self,group);
            });
        };
        var deleteSchedule = function () {
            scope.closeThisDialog();
            app.ngDialog.open({
                template : app.main.values.CONFIRM_DELETE_ALERT_PATH,
                controller : "deleteScheduleAlertController",
                data : {
                    groupID : groupID,
                    teacherID : teacherID
                }
            });
        };
        var editSchedule = function () {
            scope.closeThisDialog();
            app.main.helper.setActGroupID(groupID);
            app.helper.setActTeacherID(teacherID);
            app.location.path('edit-schedule');
        };

        this.deleteSchedule = deleteSchedule;
        this.editSchedule = editSchedule;

        var self = this;

        var groupID = scope.ngDialogData.groupID,
            teacherID = scope.ngDialogData.teacherID;
        if (!groupID || !teacherID) return;
        loadGroupSchedule();

    }]);
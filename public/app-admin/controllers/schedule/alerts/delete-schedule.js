"use strict";

angular
    .module("EduNet")
    .controller("deleteScheduleAlertController", ['app','$scope','$rootScope',function (app,scope,rootScope) {
        var deleteSchedule = function () {

            scope.closeThisDialog();
            app.api.deleteScheduleByGroupID({
                groupID : groupID,
                teacherID : teacherID
            }, function (err) {
                if (err) return app.main.helper.showFailAlertGeneralError();
                app.main.helper.showSuccessAlert(app.values.SCHEDULE_DELETED);
                rootScope.$broadcast(app.main.helper.EVENTS.LOAD_INDEX);
            });
        };
        var modal = {
            text :  app.helper.delMessage.schedule.text,
            comment : app.helper.delMessage.schedule.comment,
            del : deleteSchedule
        };
        scope.modal = modal;
        var groupID = scope.ngDialogData.groupID,
            teacherID = scope.ngDialogData.teacherID;


    }]);
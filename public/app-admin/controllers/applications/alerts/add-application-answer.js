angular
    .module("EduNet")
    .controller("addApplicationAnswerController", function (appValues,appValidator,adminValues,adminHelper,adminApi,$rootScope,$scope,appHelper) {
        var addApplicationAnswer = function () {
            var answer = $scope.answer;
            if (!appValidator.isValidField($scope,'answer')) return;
            $scope.closeThisDialog();

            adminApi.addApplicationAnswer({
                groupID : $scope.application.groupID._id,
                applicationID : $scope.application._id,
                content : answer
            },function (err) {
                appHelper.showSuccessAlert(adminValues.APPLICATION_ANSWER_CREATED);
                if (err) return appHelper.showError(err);
                $rootScope.$broadcast(appValues.EVENTS.LOAD_APPLICATIONS);
            });
        };
        var loadApplicationByID = function (applicationID) {
            adminApi.getApplicationByID({
                applicationID : applicationID
            },function (err,data) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                appHelper.createNameForUser(data.teacherID);
                $scope.application = data;
            });
        };




        // INIT
        var self = this;
        $scope.answerApplication = addApplicationAnswer;
        var applicationID = $scope.ngDialogData.applicationID;
        if (!applicationID) return $scope.closeThisDialog();
        loadApplicationByID(applicationID);





    });

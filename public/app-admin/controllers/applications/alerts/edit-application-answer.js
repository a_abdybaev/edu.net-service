angular
    .module("EduNet")
    .controller("editApplicationAnswerController", function (adminValues,appValues,appHelper,appValidator,adminHelper,adminApi,$rootScope,$scope) {
        var loadApplicationByID = function (applicationID) {
            adminApi.getApplicationByID({
                applicationID : applicationID
            },function (err,application) {
                if (err) return appHelper.showError(err);
                $scope.application = application;
            });

        };


        $scope.saveApplicationAnswer = function () {
            if (appValidator.isValidField($scope.application.answerID,'content')) {
                $scope.closeThisDialog();
                adminApi.updateApplicationAnswerByID({
                    applicationID : $scope.application._id,
                    content : $scope.application.answerID.content
                },function (err) {
                    if (err)  {
                        $scope.closeThisDialog();
                        return appHelper.showFailAlertGeneralError();
                    }

                    appHelper.showSuccessAlert(adminValues.APPLICATION_ANSWER_UPDATED);
                    $rootScope.$broadcast(appValues.LOAD_APPLICATIONS);
                });
            }
        };

        //     INIT
        var applicationID = $scope.ngDialogData.applicationID;
        if (!applicationID) return appHelper.closeDialogAndShowGenError($scope);
        loadApplicationByID(applicationID)

    });




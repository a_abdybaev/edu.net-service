"use strict";

angular
    .module("EduNet")
    .controller("deleteApplicationAnswerController", function (appHelper,adminValues,adminHelper,$rootScope,$scope,adminApi,appValues) {
        // INIT

        var del = function () {
            $scope.closeThisDialog();
            adminApi.deleteApplicationAnswer({
                applicationID : applicationID
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.APPLICATION_ANSWER_DELETED);
                $rootScope.$broadcast(appValues.EVENTS.LOAD_APPLICATIONS);
            });
        };
        var modal = {
            text :  adminHelper.delMessage.applicationAnswers.text,
            comment : adminHelper.delMessage.applicationAnswers.comment,
            del : del
        };

        var applicationID = $scope.ngDialogData.applicationID;
        if (!applicationID) return appHelper.closeDialogAndShowGenError($scope);
        $scope.modal = modal;


    });
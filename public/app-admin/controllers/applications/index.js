angular
    .module("EduNet")
    .controller("applicationsController", function (appValues,appDictionary,appHelper,ngDialog,adminApi,$rootScope,$scope) {
        var addApplicationAnswer = function (applicationID) {
            ngDialog.open({
                template: "applications/alerts/add-application-answer.html",
                className: 'ngdialog-theme-default',
                controller: "addApplicationAnswerController",
                data: {
                    applicationID: applicationID
                },
                width: 600
            });
        };
        var changeOption = function (option) {
            if (self[option]) return;
            self.isGroups = false;
            self.isTeachers = false;
            self[option] = true;
        };

        var loadGroups = function (cb) {
            adminApi.getListOfGroups(function (err,groups) {
                if (err) return appHelper.showError(err);
                if (_.isEmpty(groups)) return self.noGroups = true;
                self.noGroups = false;
                self.groups = [{
                    _id : appDictionary.EMPTY_ID,
                    name : appDictionary.ADMIN.ALL_GROUPS
                }].concat(groups);
                self.actGroupID = self.groups[0]._id;
                cb();
            });
        };
        var loadTeachers = function (cb) {
            adminApi.getListOfTeachers(function (err,teachers) {
                if (err) return appHelper.showError(err);
                if (_.isEmpty(teachers)) return self.noTeachers = true;
                self.noTeachers = false;
                self.teachers = teachers;
                self.actTeacherID = teachers[0]._id;
                cb();
            });
        };
        var loadApplications = function () {
            var data = {};
            if (self.isGroups && self.actGroupID != appDictionary.EMPTY_ID) data.groupID = self.actGroupID;
            if (self.isTeachers) data.teacherID = self.actTeacherID;
            adminApi.getApplications(data,function (err,applications) {
                if (err) return appHelper.showError(err);
                self.noApplications = _.isEmpty(applications);
                self.applications = applications;
            })
        };
        var initApplications = function () {
            loadGroups(function () {
                loadTeachers(function () {
                    loadApplications();
                });
            });
        };




        var editApplicationAnswer = function (applicationID) {
            ngDialog.open({
                template: "/app-admin/views/applications/alerts/edit-application-answer.html",
                className: 'ngdialog-theme-default',
                controller: "editApplicationAnswerController",
                data: {
                    applicationID: applicationID
                },
                width: 600
            });
        };
        var deleteApplicationAnswer = function (applicationID) {
            ngDialog.open({
                template: appValues.CONFIRM_DELETE_ALERT_PATH,
                className: 'ngdialog-theme-default',
                controller: "deleteApplicationAnswerController",
                data: {
                    applicationID: applicationID
                }
            });
        };

        this.deleteApplicationAnswer = deleteApplicationAnswer;
        this.editApplicationAnswer = editApplicationAnswer;
        this.changeOption = changeOption;
        this.addApplicationAnswer = addApplicationAnswer;
        this.loadApplications = loadApplications;


        // INIT
        var self = this;
        this.isTeachers = false;
        this.isGroups = true;

        $rootScope.$on(appValues.EVENTS.LOAD_APPLICATIONS, loadApplications);
        $rootScope.$on(appValues.EVENTS.INDEX_LOADED,initApplications);
        if ($scope.$parent.index.isLoad) {
            initApplications();
        }

    });

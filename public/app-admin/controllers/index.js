angular
    .module("EduNet")
    .controller("indexController", function (appConstants,localStorageService,appValidator,appHelper,ngDialog,adminHelper,adminApi,$rootScope,$scope,$location,$mdSidenav,appValues) {

        function buildToggler(componentId) {
            return function() {
                $mdSidenav(componentId).toggle();
            }
        }
        var loadUserInfo = function () {
            adminApi.getAdminInfo(function (err,data) {

                if (err) return appHelper.showFailAlertGeneralError(err);
                if (!data.admin.centerID.imageUrl) data.admin.centerID.imageUrl = appConstants.CENTER_IMAGE_DEFAULT_URL;
                self.hasBranch = (data.admin.clientID.centers).length > 1;
                self.centers = data.admin.clientID.centers;
                self.admin = data.admin;
                self.groups = data.groups;
                self.subjects = data.subjects;
                self.center = data.admin.centerID;
                self.client = data.admin.clientID;
                self.actCenter = self.center;
                appHelper.setUserInfo(data);
                self.isLoad = true;
                $rootScope.$broadcast(appValues.EVENTS.INDEX_LOADED);

            });
        };
        var goTo = function (location) {
            self.isSideNaVOpen = false;
            $location.path(location);
        };
        var logout = function () {
            adminHelper.logout();
        };
        var openUserNav = function () {
            self.userNav = true;
        };
        var hideUserNav = function(){
            self.userNav = false;
        };
        var editProfile = function () {
            ngDialog.open({
                template : "/app-admin/views/profile/alerts/edit-profile.html",
                className : 'ngdialog-theme-default',
                width : 600
            });
        };
        var changeCenter = function () {
            console.log("CHANGE CENTER");
            appHelper.removeActGroupID();
            appHelper.removeUserInfo();
            adminHelper.removeActTeacherID();
            adminHelper.removeActTestID();
            appHelper.removeActBlocID();
            if (!self.hasBranch || !self.admin.isManager) return;
            adminHelper.goToMain();
            adminApi.updateCenterID({
                centerID : self.actCenter._id
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_INDEX);
            });
        };


        this.editProfile = editProfile;
        this.hideUserNav = hideUserNav;
        this.hideUserNav = hideUserNav;
        this.openUserNav = openUserNav;
        this.logout = logout;
        this.goTo = goTo;
        this.changeCenter = changeCenter;



        // INIT
        var self = this;
        this.isSideNaVOpen = false;
        $rootScope.loader = [];
        this.toggleLeft = buildToggler('left');
        this.toggleRight = buildToggler('right');
        $rootScope.$on(appValues.EVENTS.LOAD_INDEX,loadUserInfo);



        loadUserInfo();




    });

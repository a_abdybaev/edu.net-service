angular
    .module("EduNet")
    .controller("studentInfoController", function (appDictionary,appConstants,$location,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var loadStudentByID = function (studentID) {
            adminApi.getStudentByID({
                studentID : studentID
            },function (err,student) {
                if (err) return $scope.closeThisDialog();
                self.student = student;
                if (student.status === 'ACTIVE') return initActiveStudent();
                if (student.status === 'ARCHIVE') return initArchiveStudent();
                if (student.status === 'WAITING') return initWaitingStudent();
            });
        };


        // ACTIVE STUDENT methods
        var resolveAttendance = function () {
            self.attendance = {
                presence : 0,
                absence : 0,
                absenceReason : 0
            };
            if (!_.isEmpty(self.student.appearance)) {
                _.each(self.student.appearance,function (appearance) {
                    if (appearance.groupID == self.group._id) {
                        if (appearance.value == appConstants.STUDENT_APPEARANCE_PRESENCE) self.attendance.presence++;
                        if (appearance.value == appConstants.STUDENT_APPEARANCE_ABSENCE) self.attendance.absence++;
                        if (appearance.value == appConstants.STUDENT_APPEARANCE_ABSENCE_REASON) self.attendance.absenceReason++;
                    }
                });
            }
        };
        var resolveActiveStudentPayments = function () {
            self.payments = {
                value : 0,
                hasPayments : false,
                isPricableGroup : _.isNumber(self.group.price),
                lastPayment : null
            };
            if (!_.isEmpty(self.student.payments) && self.payments.isPricableGroup) {
                _.each(self.student.payments,function (payment) {
                    if (payment.groupID == self.group._id) {
                        self.payments.value += payment.value;
                        self.payments.lastPayment = payment;
                    }
                });
            }
        };
        var resolveSchedule = function () {
            self.schedule = appHelper.getWeekDaysArrayWithoutSundayDefault();
            if (self.group.schedule.length != 0) {
                self.hasSchedule = true;
                self.group.schedule.forEach(function (lesson) {
                    self.schedule[lesson.day-1].hasLesson = true;
                    var startTime = new Date(),
                        endTime = new Date();
                    startTime.setHours(lesson.start.hour);
                    startTime.setMinutes(lesson.start.minute);
                    endTime.setHours(lesson.end.hour);
                    endTime.setMinutes(lesson.end.minute);

                    self.schedule[lesson.day-1].lesson = {
                        classroomID : lesson.classroomID,
                        startTime : startTime,
                        endTime : endTime
                    }
                });
            } else {
                self.hasSchedule = false;
            }
        };
        var selectActiveStudentGroup = function (index) {
            if (self.groupIndex == index) return;
            self.groupIndex = index;
            self.group = self.student.groups[index];
            resolveAttendance();
            resolveActiveStudentPayments();
            resolveSchedule();
        };



        // NOT ACTIVE STUDENT methods
        var resolveNotActiveStudentPayments = function () {
            self.payments = self.student.payments;
            self.totalPayments = 0;
            if (_.isEmpty(self.payments)) return;
            _.each(self.payments,function (payment) {
                self.totalPayments+=payment.value;
            });
            if (self.payments.length > 5) self.payments = [self.payments.pop(),self.payments.pop(),
                self.payments.pop(),self.payments.pop(),self.payments.pop()];


        };


        // INIT STUDENT
        var initActiveStudent = function () {
            self.selectActiveStudentGroup = selectActiveStudentGroup;
            selectActiveStudentGroup(0);
        };
        var initWaitingStudent = function () {
            self.groups = self.student.groups;
            if (self.groups.length > 5) self.groups = [self.student.groups.pop(),self.student.groups.pop(),self.student.groups.pop(),self.student.groups.pop(),self.student.groups.pop()];
            self.isWaitingStudent = true;

            if (self.student.subjectID) {
                var index = _.map(self.student.subjectID.levels,function (level) {
                    return level._id.toString();
                }).indexOf(self.student.levelID);
                if (index > -1) self.student.levelID = self.student.subjectID.levels[index];
                self.hasSubject = true;
            }

            self.student.groupType =  self.student.groupType && !_.isEmpty(self.student.groupType) ? appDictionary[self.student.groupType] : appDictionary.NOT_SELECTED;
            self.student.ageType = self.student.ageType && !_.isEmpty(self.student.ageType) ? appDictionary[self.student.ageType] : appDictionary.NOT_SELECTED;
            if (_.isEmpty(self.student.studyTimes)) self.student.studyTimes = [appDictionary.NOT_SELECTED];
            else self.student.studyTimes = _.map(self.student.studyTimes,function (time) {
                return appDictionary[time];
            });

            if (_.isEmpty(self.student.studyDays)) self.student.studyDays = [appDictionary.NOT_SELECTED];
            else self.student.studyDays = _.map(self.student.studyDays,function (day) {
                return appDictionary.days[day];
            });













            resolveNotActiveStudentPayments();
        };
        var initArchiveStudent = function () {
            if (self.student.length > 5) self.groups = [self.student.groups.pop(),self.student.groups.pop(),self.student.groups.pop(),self.student.groups.pop(),self.student.groups.pop()];
            resolveNotActiveStudentPayments();

        };


        // INIT
        var self = this;
        var studentID = $scope.ngDialogData.studentID;

        if (!studentID) return appHelper.closeDialogAndShowGenError($scope);
        loadStudentByID(studentID);
    });



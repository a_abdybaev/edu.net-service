angular
    .module("EduNet")
    .controller("deleteStudentController", function (adminValues,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {


        var deleteStudent = function () {
            $scope.closeThisDialog();
            adminApi.deleteStudentByID({
                studentID : studentID
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.STUDENT_CREATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_ARCHIVE_STUDENTS);
            });
        };
        var modal = {
            text :  adminHelper.delMessage.student.text,
            comment : adminHelper.delMessage.student.comment,
            del : deleteStudent
        };


        // INIT
        var studentID = $scope.ngDialogData.studentID;
        if (!studentID) return appHelper.closeDialogAndShowGenError($scope);

        $scope.modal = modal;



    });



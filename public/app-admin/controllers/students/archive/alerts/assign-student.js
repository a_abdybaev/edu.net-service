angular
    .module("EduNet")
    .controller("assignArchiveStudentController", function (adminValues,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var loadAvailableGroups = function (studentID) {
            adminApi.getAssignAvailableGroupsForArchiveStudent({
                studentID : studentID
            },function (err,groups) {
                if (err) return appHelper.showFailAlertGeneralError();
                if (groups.length == 0) return self.noAssignGroups = true;
                self.noAssignGroups = false;
                self.groups = groups;
                self.groupID = groups[0]._id;

            })
        };
        var assignStudentByGroupID = function () {
            adminApi.assignArchiveStudentByID({
                studentID : studentID,
                groupID : self.groupID
            },function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_ARCHIVE_STUDENTS);
                appHelper.showSuccessAlert(adminValues.STUDENT_ADDED_IN_GROUP);

            });

        };



        //     INIT
        var self = this;
        this.assignStudentByGroupID = assignStudentByGroupID;
        var studentID = $scope.ngDialogData.studentID;
        if (!studentID) return appHelper.closeDialogAndShowGenError($scope);
        loadAvailableGroups(studentID);



    });



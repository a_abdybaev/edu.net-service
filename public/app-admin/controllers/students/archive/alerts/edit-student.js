angular
    .module("EduNet")
    .controller("editArchiveStudentController", function (adminValues,appValidator,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var loadStudent = function (studentID) {
            adminApi.getArchiveStudentByID({
                studentID : studentID
            },function (err,student) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                self.student = student;
            });
        };
        var updateStudent = function () {
            if (!appValidator.isValidField(self.student,'firstName') || !appValidator.isValidField(self.student,'lastName') || !appValidator.isValidEmailField(self.student,'email')) return;
            $scope.closeThisDialog();
            adminApi.updateArchiveStudentByID({
                firstName : self.student.firstName,
                lastName : self.student.lastName,
                email : self.student.email,
                studentID : self.student._id
            },function (err) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                appHelper.showSuccessAlert(adminValues.STUDENT_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_ARCHIVE_STUDENTS);
            });
        };
        this.updateStudent = updateStudent;


        //     INIT
        var self = this;
        var studentID = $scope.ngDialogData.studentID;
        if (!studentID) return $scope.closeThisDialog();
        loadStudent(studentID);

    });



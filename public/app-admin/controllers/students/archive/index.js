angular
    .module("EduNet")
    .controller("archiveStudentsController", function (appValues,appConstants,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var assignStudent = function () {
            self.actIndex = parseInt(self.actIndex);
            if (isNaN(self.actIndex)  || !self.isSelected) return
            ngDialog.open({
                template : "/app-admin/views/students/archive/alerts/assign-student.html",
                className : "ngdialog-theme-default",
                width : 520,
                data : {
                    studentID : self.students[self.actIndex]._id
                }
            });
        };
        var restoreStudent = function () {
            if (isNaN(self.actIndex)  || !self.isSelected) return
            ngDialog.open({
                template : "/app-admin/views/students/archive/alerts/restore-student.html",
                className : "ngdialog-theme-default",
                width : 600,
                data : {
                    studentID : self.students[self.actIndex]._id
                }
            });
        };
        var loadStudents = function () {
            self.query.search = self.searchName;
            adminApi.getArchiveStudents(self.query,function (err,students) {
                if (err) return appHelper.showError(err);
                self.noMoreStudents = students.length < adminConstants.STUDENTS_PER_PAGE;
                self.students = self.isValuablePagination ? self.students.concat(students) : students;
                //DEVELOPMENT
                // restoreStudent(students[students.length-1]._id);
                // self.students[0].isActive = true;
            });
        };
        var deleteStudentByID = function (studentID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : "ngdialog-theme-default",
                width : 320,
                controller : "deleteStudentController",
                data : {
                    studentID : studentID
                }
            });
        };
        var editStudentByID = function (studentID) {
            ngDialog.open({
                template : "/app-admin/views/students/archive/alerts/edit-student.html",
                className : "ngdialog-theme-default",
                width : 600,
                controller : "editArchiveStudentController",
                data : {
                    studentID : studentID
                }
            });
        };
        var showMore = function () {
            setPagination();
            self.query.page++;
            loadStudents();
        };

        var sortByDate = function () {
            self.dateSorting = !self.dateSorting;
            self.nameSorting = false;
        };
        var sortByName = function () {
            self.nameSorting = !self.nameSorting;
            self.dateSorting = false;

        };
        var invokeSearch = function () {
            resetPagination();
            loadStudents();

        };
        var search = function () {
            if (self.searchName == self.oldSearch) return;
            self.oldSearch = self.searchName;
            if (Number.isInteger(searchTimeOut)) clearTimeout(searchTimeOut);
            searchTimeOut = setTimeout(invokeSearch,1000);


        };
        var resetPagination = function () {
            self.isValuablePagination = false;
            self.query.page = 1;
        };
        var setPagination = function () {
            self.isValuablePagination = true;
        };
        var studentInfo = function (studentID) {
            ngDialog.open({
                template : "/app-admin/views/students/alerts/student-info/index.html",
                className : "ngdialog-theme-default",
                width : 1120,
                data : {
                    studentID : studentID
                }
            });
        };

        var selectStudent = function (index) {
            console.log(index);
            // var actIndex = parseInt(self.actIndex);
            // console.log(actIndex)
            // if (isNaN(actIndex)) {
            //     self.students[index].isActive = true;
            //     self.actIndex = index;
            //     return;
            // }
            // if (index == actIndex) {
            //     self.students[index].isActive = !self.students[index].isActive;
            // } else {
            //     self.students[actIndex].isActive = false;
            //     self.students[index].isActive = true;
            // }
            // self.actIndex = index;
            self.isSelected = self.actIndex == index ? !self.isSelected : true;
            self.actIndex = index;
            console.log(self.isSelected);
            console.log(self.actIndex);

        };
        this.isSelected = false;

        this.showMore = showMore;
        this.sortByDate = sortByDate;
        this.sortByName = sortByName;
        this.assignStudent = assignStudent;
        this.restoreStudent = restoreStudent;
        this.deleteStudentByID = deleteStudentByID;
        this.editStudentByID = editStudentByID;
        this.search = search;
        this.studentInfo = studentInfo;
        this.selectStudent = selectStudent;



        // INIT
        var self = this;
        var searchTimeOut;

        this.noTitle = appConstants.NO_TITLE;
        this.isValuablePagination = true;

        var initController = function () {
            self.query = {
                page : 1
            };
            self.students = [];
            self.noMoreStudents = false;
            self.oldSearch = "";
            loadStudents();
        };

        $rootScope.$on(appHelper.EVENTS.LOAD_ARCHIVE_STUDENTS,function () {
            resetPagination();
            loadStudents();
        });
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }




    });



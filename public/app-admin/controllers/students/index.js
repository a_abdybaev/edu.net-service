angular
    .module("EduNet")
    .controller("studentsController", function ($location,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {


        var showStudents = function (title) {
            self.isWaiting = (title == self.waitingStudents);
            self.isActive = (title == self.activeStudents);
            self.isArchive = (title == self.archiveStudents);
        };
        var initController = function () {
            self.info = appHelper.getUserInfo();
            self.waitingStudentsCount = self.info.admin.centerID.students.waiting;
            self.archiveStudentsCount = self.info.admin.centerID.students.archive;
            self.activeStudentsCount = self.info.admin.centerID.students.active;
        };
        this.showStudents = showStudents;

        // INIT
        var self = this;
        this.studentsStatus = adminConstants.studentStatuses;
        this.archiveStudents = adminConstants.ARCHIVE_STUDENTS;
        this.activeStudents = adminConstants.ACTIVE_STUDENT;
        this.waitingStudents = adminConstants.WAITING_STUDENTS;


        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }
        if (appHelper.getPrev() == 'SUBJECTS'){
            appHelper.clearPrev();
            showStudents(self.waitingStudents);
        } else {
            showStudents(self.activeStudents);
        }

        if (appHelper.isDevelopment()) {
            showStudents(self.archiveStudents);
        }




        // DEVELOPMENT
        // var centerDomain = $location.host().split('.')[0];
        // if (centerDomain == 'interpress') {
        //     showStudents(self.activeStudents);
        // } else {
        //     showStudents(self.activeStudents);
        // }



    });



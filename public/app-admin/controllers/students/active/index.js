angular
    .module("EduNet")
    .controller("activeStudentsController", function ($location,appValues,appConstants,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var addStudent = function () {
            if (self.noGroups) return;
            ngDialog.open({
                template : "/app-admin/views/students/active/alerts/add-student.html",
                className : "ngdialog-theme-default",
                width : 600
            });
        };
        var editStudent = function (studentID) {
            ngDialog.open({
                template : "/app-admin/views/students/active/alerts/edit-student.html",
                className : "ngdialog-theme-default",
                width : 600,
                data : {
                    studentID : studentID
                }
            });
        };
        var assignStudentForGroup = function (studentID) {
            ngDialog.open({
                template : "/app-admin/views/students/active/alerts/assign-student.html",
                className : "ngdialog-theme-default",
                width : 520,
                data : {
                    studentID : studentID
                }
            });
        };
        var archiveStudent = function (studentID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : "ngdialog-theme-default",
                controller : "archiveActiveStudentController",
                width : 320,
                data : {
                    studentID : studentID
                }
            });
        };
        var loadStudents = function () {
            self.query.search = self.searchName;
            adminApi.getActiveStudents(self.query,function (err,students) {
                if (err) return appHelper.showError(err);
                self.noMoreStudents = students.length < adminConstants.STUDENTS_PER_PAGE;
                self.students = self.isValuablePagination ? self.students.concat(students) : students;
                if (self.queryStudents) {
                    self.noQueryStudents = _.isEmpty(self.students);
                } else {
                    self.noStudents = _.isEmpty(self.students);

                }

                self.queryStudents = false;

            });
        };
        var goSubjects = function () {
            $location.path('subjects');
        };
        var showMore = function () {
            setPagination();
            self.query.page++;
            loadStudents();
        };

        var sortByDate = function () {
            self.dateSorting = !self.dateSorting;
            self.nameSorting = false;
        };
        var sortByName = function () {
            self.nameSorting = !self.nameSorting;
            self.dateSorting = false;
        };
        var invokeSearch = function () {
            if (self.noStudents) return;
            self.queryStudents = true;
            resetPagination();
            loadStudents();
        };
        var search = function () {
        
            if (self.searchName == oldSearch) return;
            oldSearch = self.searchName;
            if (Number.isInteger(searchTimeOut)) clearTimeout(searchTimeOut);
            searchTimeOut = setTimeout(invokeSearch,1000);
        };
        var resetPagination = function () {
            self.isValuablePagination = false;
            self.query.page = 1;
        };
        var setPagination = function () {
            self.isValuablePagination = true;
        };


        var studentInfo = function (studentID) {
            ngDialog.open({
                template : "/app-admin/views/students/alerts/student-info/index.html",
                className : "ngdialog-theme-default",
                width : 1120,
                data : {
                    studentID : studentID
                }
            });
        };

        var showProperties = function (index) {
            self.properties[index] = true;
        };
        var hideProperties = function (index) {
            self.properties[index] = false;
        };

        var loadGroups = function (cb) {
            adminApi.getListOfGroups(function (err,groups) {
                if (err) return appHelper.showError(err);
                if (_.isEmpty(groups)) {
                    self.students = [];
                    self.noStudents = true;
                    self.noGroups = true;
                    return;
                }
                self.noGroups = false;
                cb();
            });
        };

        var initController = function () {
            self.properties = [];
            self.query = {
                page : 1
            };
            self.isValuablePagination = true;
            self.students = [];
            self.noMoreStudents = false;
            self.dateSorting = false;
            self.nameSorting = false;
            self.searchName = "";
            loadGroups(loadStudents);
        };
        this.goSubjects = goSubjects;
        this.showProperties = showProperties;
        this.hideProperties = hideProperties;
        this.addStudent = addStudent;
        this.editStudent = editStudent;
        this.assignStudentForGroup = assignStudentForGroup;
        this.archiveStudent = archiveStudent;
        this.sortByDate = sortByDate;
        this.sortByName = sortByName;
        this.search = search;
        this.studentInfo = studentInfo;
        this.showMore = showMore;

        // init students
        var self = this,
            searchTimeOut,
            oldSearch;

        this.noTitle = appConstants.NO_TITLE;



        $rootScope.$on(appHelper.EVENTS.LOAD_ACTIVE_STUDENTS,function () {
            resetPagination();
            loadStudents();
        });
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }


    });



angular
    .module("EduNet")
    .controller("archiveActiveStudentController", function (appValidator,appHelper,adminValues,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var archiveStudent = function () {
            $scope.closeThisDialog();
            adminApi.archiveActiveStudentByID({
                studentID : studentID
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError(err);
                appHelper.showSuccessAlert(adminValues.STUDENT_ARCHIVED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_ACTIVE_STUDENTS);

            })
        };

        var studentID = $scope.ngDialogData.studentID;
        if (!studentID) return appHelper.closeDialogAndShowGenError($scope);
        var modal = {
            text :  adminHelper.delMessage.archiveStudent.text,
            comment : adminHelper.delMessage.archiveStudent.comment,
            del : archiveStudent
        };

        $scope.modal = modal;



    });



angular
    .module("EduNet")
    .controller("assignActiveStudentController", function (adminValues,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {


        var loadAvailableGroups = function (studentID) {
            adminApi.getAssignAvailableGroupsForActiveStudent({
                studentID : studentID
            },function (err,groups) {
                if (err) return appHelper.showFailAlertGeneralError();
                if (groups.length == 0) return self.noAssignGroups = true;
                self.groups = groups;
                self.groupID = groups[0]._id;
            });
        };
        var assignStudent = function () {
            adminApi.assignActiveStudentByID({
                studentID : studentID,
                groupID : self.groupID
            },function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_ACTIVE_STUDENTS);
                appHelper.showSuccessAlert(adminValues.STUDENT_ADDED_IN_GROUP);

            });
        };
        this.assignStudent = assignStudent;



        //INIT

        var self = this;
        var studentID = $scope.ngDialogData.studentID;
        if (!studentID) return appHelper.EVENTS.LOAD_ACTIVE_STUDENTS
        loadAvailableGroups(studentID);


    });



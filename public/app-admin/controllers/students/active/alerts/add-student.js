angular
    .module("EduNet")
    .controller("addActiveStudentController", function (appValidator,appHelper,adminValues,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var loadListOfGroups = function () {
            adminApi.getListOfGroups(function (err,groups) {
                if (err) return appHelper.showError(err);
                self.groups = groups;
                if (groups.length == 0) return;
                self.groupID = groups[0]._id;
            });
        };
        var addStudent = function () {
            if (!appValidator.isValidField(self,'firstName') ||  !appValidator.isValidEmailField(self,'email') || !appValidator.isValidField(self,'lastName') || !appValidator.isValidField(self,'phone') || !appValidator.isValidField(self,'groupID')) return;
            $scope.closeThisDialog();
            var data = {
                firstName : self.firstName,
                lastName : self.lastName,
                phone : self.phone,
                email : self.email,
                groupID : self.groupID
            };
            adminApi.addActiveStudent(data,function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_ACTIVE_STUDENTS);
                return appHelper.showSuccessAlert(adminValues.STUDENT_CREATED);

            });
        };

        // INIT
        var self = this;
        this.addStudent = addStudent;
        loadListOfGroups();
    });



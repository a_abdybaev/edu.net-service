angular
    .module("EduNet")
    .controller("editActiveStudentController", function (adminValues,appValidator,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var loadStudent = function (studentID) {
            adminApi.getActiveStudentByID({
                studentID : studentID
            },function (err,student) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                self.student = student;
            });
        };
        var updateStudent = function () {
            if (!appValidator.isValidField(self.student,'firstName') || !appValidator.isValidField(self.student,'lastName') || !appValidator.isValidEmailField(self.student,'email')) return;
            adminApi.updateActiveStudentByID({
                firstName : self.student.firstName,
                lastName : self.student.lastName,
                email : self.student.email,
                studentID : self.student._id
            },function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.STUDENT_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_ACTIVE_STUDENTS);

                // MAYBE DELETE BELOW CODE(1 ROW) ABOUT LOAD STUDENTS?
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_STUDENTS);
            });
        };
        this.updateStudent = updateStudent;


        //     INIT

        var self = this;
        var studentID = $scope.ngDialogData.studentID;

        if (!studentID) return appHelper.closeDialogAndShowGenError($scope);
        loadStudent(studentID);

    });



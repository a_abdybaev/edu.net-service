angular
    .module("EduNet")
    .controller("addWaitingStudentController", function (adminValues,appConstants,appValidator,$location,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var loadSubjects = function () {
            adminApi.getSubjects(function (err,subjects) {
                if (err) return appHelper.showFailAlertGeneralError();
                if (subjects.length == 0) return self.noSubjects = true;
                self.subjects = self.subjects.concat(subjects);
            });
        };
        var createStudent = function () {
            if (!appValidator.isValidForm(self,'firstName lastName email phone')) return;

            var data = {
                firstName : self.firstName,
                lastName : self.lastName,
                phone : self.phone,
                email : self.email,
                studyTimes : [],
                studyDays : []
            };
            if (!_.isEqual(self.age.value,'-')) {
                data.ageType = self.age.value;
            }
            if (!_.isEmpty(self.GROUP_TYPE)) {
                data.groupType = self.GROUP_TYPE;
            }
            if (!_.isEqual(self.subject._id,'-') ) {
                data.subjectID = self.subject._id;
                data.levelID = self.level._id;
            }
            self.days.forEach(function (day,i) {
                if (day) data.studyDays.push(i)
            });
            for (var key in self.studyTimes) {
                if (self.studyTimes[key] == 1) {
                    data.studyTimes.push(key);
                }
            }
            adminApi.addWaitingStudent(data,function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_WAITING_STUDENTS);
                cancelAddStudent();
                return appHelper.showSuccessAlert(adminValues.STUDENT_CREATED);
            });
        };

        var updateSubject = function () {
            self.level = self.subject.levels[0];
        };
        var cancelAddStudent = function () {
            $scope.$parent.waiting.isAddStudent = false;
        };
        var setGroupType = function (value) {
            self.GROUP_TYPE = _.isEqual(value,self.GROUP_TYPE) ? null : value;
        };
        var setStudyTime = function (index) {
            self.studyTimes[index] = !self.studyTimes[index];
        };
        var setDay = function (index) {
            self.days[index] = !self.days[index];
        };
        this.createStudent = createStudent;
        this.cancelAddStudent = cancelAddStudent;
        this.setGroupType = setGroupType;
        this.setStudyTime = setStudyTime;
        this.setDay = setDay;
        this.updateSubject = updateSubject;



        // INIT
        var self = this;


        this.subject = appConstants.HELPERS.getNotSelectedObject();
        this.level = {
            _id : appConstants.HELPERS.EMPTY_ID,
            value : appConstants.HELPERS.NOT_SELECTED
        };
        this.subject.levels = [this.level];
        this.subjects = [this.subject];
        loadSubjects();
        this.individual = false;
        self.studyTimes = [];
        self.studyTimes[adminConstants.STUDENT_STUDY_TIME_MORNING] = 0;
        self.studyTimes[adminConstants.STUDENT_STUDY_TIME_EVENING] = 0;
        self.studyTimes[adminConstants.STUDENT_STUDY_TIME_LUNCH] = 0;
        self.days = [false,false,false,false,false,false,false];

        self.STUDENT_STUDY_TIME_MORNING = adminConstants.STUDENT_STUDY_TIME_MORNING;
        self.STUDENT_STUDY_TIME_EVENING = adminConstants.STUDENT_STUDY_TIME_EVENING;
        self.STUDENT_STUDY_TIME_LUNCH = adminConstants.STUDENT_STUDY_TIME_LUNCH;
        self.GROUP_TYPE_INDIVIDUAL = appConstants.GROUP_TYPE_INDIVIDUAL;
        self.GROUP_TYPE_GENERAL = appConstants.GROUP_TYPE_GENERAL;
        self.STUDENT_AGE_GROUPS = appConstants.STUDENT_AGE_GROUPS;
        self.age = appConstants.STUDENT_AGE_GROUPS[0];


    //    DEV

        // if (appHelper.isDevelopment()) createStudent();
    });



angular
    .module("EduNet")
    .controller("assignWaitingStudentsController", function (adminValues,appConstants,$location,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var loadAvailableGroupsToAssignStudents = function (students) {
            adminApi.getAssignAvailableGroupsForWaitingStudents({
                count : students.length
            },function (err,groups) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                if (groups.length == 0) return self.noAvailableGroups = true;
                self.groups = groups;
                self.groupID = groups[0]._id;
            });
        };
        var assignStudent = function () {
            $scope.closeThisDialog();
            adminApi.assignWaitingStudents({
                students : students,
                groupID : self.groupID
            },function (err) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_WAITING_STUDENTS);
                appHelper.showSuccessAlert(adminValues.STUDENT_ADDED_IN_GROUP);

            });
        };
        this.assignStudent = assignStudent;

         // INIT

        var self = this;
        var students = $scope.ngDialogData.students;
        if (_.isEmpty(students)) return appHelper.closeDialogAndShowGenError($scope);
        loadAvailableGroupsToAssignStudents(students);


    });



angular
    .module("EduNet")
    .controller("editWaitingStudentController", function (adminValues,appValidator,appConstants,appHelper,adminConstants,adminApi,$rootScope,$scope) {

        var loadStudentByID = function (studentID) {
            adminApi.getWaitingStudentByID({
                studentID : studentID
            },function (err,student) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);

                self.firstName = student.firstName;
                self.lastName = student.lastName;
                self.email = student.email;
                self.phone = student.phone;
                if (!_.isEmpty(student.subjectID) && !_.isEmpty(student.levelID)) {
                    self.subject = student.subjectID;
                    var levels = _.map(student.subjectID.levels,function (level) {
                        return level._id;
                    });
                    var index = _.indexOf(levels,student.levelID);
                    if (index < 0) return;
                    self.level = student.subjectID.levels[index];
                }
                if (!_.isEmpty(student.groupType)) {
                    self.GROUP_TYPE = student.groupType;
                }
                if (!_.isEmpty(student.ageType)) {
                    var index = _.indexOf(_.map(self.STUDENT_AGE_GROUPS,function (age) {
                        return age.value;
                    }),student.ageType);
                    if (index < 0) return;
                    self.age = self.STUDENT_AGE_GROUPS[index];
                }
                if (!_.isEmpty(student.studyTimes)) {
                    _.each(student.studyTimes,function (time) {
                        self.studyTimes[time] = 1;
                    });
                }
                if (!_.isEmpty(student.studyDays)) {
                    _.each(student.studyDays,function (day) {
                        self.days[day] = true;
                    });
                }



            });
        };
        var loadSubjectsAndStudentByID = function (studentID) {
            adminApi.getSubjects(function (err,subjects) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                if (subjects.length == 0) {
                    self.noSubjects = true;
                } else {
                    self.subjects = self.subjects.concat(subjects);
                }
                loadStudentByID(studentID);
            });
        };
        var updateStudent = function () {
            if (!appValidator.isValidForm(self,'firstName lastName email')) return;
            $scope.closeThisDialog();

            var data = {
                firstName : self.firstName,
                lastName : self.lastName,
                email : self.email,
                studyTimes : [],
                studyDays : [],
                studentID : studentID
            };
            data.subjectID = _.isEqual(self.subject._id,'-') ? null : self.subject._id;
            data.levelID = _.isEqual(self.level._id,'-') ? null : self.level._id;
            data.groupType = _.isEmpty(self.GROUP_TYPE) ? null : self.GROUP_TYPE;
            data.ageType = _.isEqual(self.age.value,'-') ? null : self.age.value;
            for (var key in self.studyTimes) {
                if (self.studyTimes[key] == 1) {
                    data.studyTimes.push(key);
                }
            }
            self.days.forEach(function (day,i) {
                if (day) data.studyDays.push(i)
            });
            adminApi.updateWaitingStudentByID(data,function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_WAITING_STUDENTS);
                return appHelper.showSuccessAlert(adminValues.STUDENT_UPDATED);
            });
        };

        var updateSubject = function () {
            self.level = self.subject.levels[0];
        };

        var setGroupType = function (value) {
            self.GROUP_TYPE = _.isEqual(value,self.GROUP_TYPE) ? null : value;
        };
        var setStudyTime = function (index) {
            self.studyTimes[index] = !self.studyTimes[index];
        };
        var setDay = function (index) {
            self.days[index] = !self.days[index];
        };
        this.updateStudent = updateStudent;
        this.setGroupType = setGroupType;
        this.setStudyTime = setStudyTime;
        this.setDay = setDay;
        this.updateSubject = updateSubject;





        // INIT
        var self = this;
        var studentID = $scope.ngDialogData.studentID;
        if (!studentID) return appHelper.closeDialogAndShowGenError($scope);
        this.subject = appConstants.HELPERS.getNotSelectedObject();
        this.level = {
            _id : appConstants.HELPERS.EMPTY_ID,
            value : appConstants.HELPERS.NOT_SELECTED
        };
        this.subject.levels = [this.level];
        this.subjects = [this.subject];


        self.studyTimes = [];
        self.studyTimes[adminConstants.STUDENT_STUDY_TIME_MORNING] = 0;
        self.studyTimes[adminConstants.STUDENT_STUDY_TIME_EVENING] = 0;
        self.studyTimes[adminConstants.STUDENT_STUDY_TIME_LUNCH] = 0;
        self.days = [false,false,false,false,false,false,false];

        self.STUDENT_STUDY_TIME_MORNING = adminConstants.STUDENT_STUDY_TIME_MORNING;
        self.STUDENT_STUDY_TIME_EVENING = adminConstants.STUDENT_STUDY_TIME_EVENING;
        self.STUDENT_STUDY_TIME_LUNCH = adminConstants.STUDENT_STUDY_TIME_LUNCH;
        self.GROUP_TYPE_INDIVIDUAL = appConstants.GROUP_TYPE_INDIVIDUAL;
        self.GROUP_TYPE_GENERAL = appConstants.GROUP_TYPE_GENERAL;
        self.STUDENT_AGE_GROUPS = appConstants.STUDENT_AGE_GROUPS;
        self.age = appConstants.STUDENT_AGE_GROUPS[0];

        loadSubjectsAndStudentByID(studentID);
    });



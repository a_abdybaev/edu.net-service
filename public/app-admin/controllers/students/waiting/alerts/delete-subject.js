angular
    .module("EduNet")
    .controller("delSubjectController", function (appValues,adminHelper,appHelper,$scope,adminApi,$rootScope) {
        var del = function () {
            $scope.closeThisDialog();
            adminApi.delSubjectByID({
                subjectID : subjectID
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminHelper.values.SUBJECT_DELETED);
                $rootScope.$broadcast(appValues.EVENTS.LOAD_SUBJECTS);
            });
        };
        var modal = {
            text :  adminHelper.delMessage.subject.text,
            comment : adminHelper.delMessage.subject.comment,
            del : del
        };

        var subjectID = $scope.ngDialogData.subjectID;
        if(!subjectID) return appHelper.closeDialogAndShowGenError($scope);

        $scope.modal = modal;
    });
angular
    .module("EduNet")
    .controller("addSubjectController", function (adminValues,adminValidator,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var addLevel = function () {
            self.levels.push("");
        };
        var addSubject = function () {
            if (!adminValidator.isValidAddSubjectForm(self)) return;
            $scope.closeThisDialog();
            var data = {
                name : self.name,
                levels : self.levels
            };
            adminApi.addSubject(data,function (err) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                appHelper.showSuccessAlert(adminValues.SUBJECT_CREATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_SUBJECTS);
            });
        };
        var removeLevel = function (index) {
            if (self.levels.length == 1) return;
            self.levels.splice(index,1);
        };
        this.addLevel = addLevel;
        this.removeLevel = removeLevel;
        this.addSubject = addSubject;



        // INIT
        var self = this;
        this.levels = [""];


    });



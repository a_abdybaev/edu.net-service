angular
    .module("EduNet")
    .controller("formGroupController", function (adminValues,appValidator,appConstants,$location,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var setColor = function (index) {
            self.activeColorIndex = index;
        };
        var addGroup = function () {
            if (!appValidator.isValidField(self,'name')) return;
            $scope.closeThisDialog();
            var data = {
                name : self.name,
                color : self.colors[self.activeColorIndex].value,
                individual : self.individual,
                students : self.students
            };
            if (!_.isEqual(self.teacher._id,'-')) {
                data.teacherID = self.teacher._id
            }
            if (!_.isEmpty(self.price)) {
                data.price = self.price;
            }
            adminApi.formGroupForWaitingStudents(data,function (err) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_WAITING_STUDENTS);
                appHelper.showSuccessAlert(adminValues.GROUP_CREATED);
            });

        };
        var loadTeachers = function () {
            adminApi.getListOfTeachers(function (err,teachers) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                appHelper.createNameForUsers(teachers);
                self.teachers = self.teachers.concat(teachers);
            });
        };
        this.setColor = setColor;
        this.addGroup = addGroup;

        // INIT
        var self = this;
        this.students = $scope.ngDialogData.students;
        if (_.isEmpty(this.students)) return appHelper.closeDialogAndShowGenError($scope);
        this.teacher = {
            name : appConstants.HELPERS.VACATION,
            _id : appConstants.HELPERS.EMPTY_ID
        };
        this.teachers = [this.teacher];
        this.colors = adminConstants.COLORS;
        this.activeColorIndex = 0;
        this.individual = 0;
        loadTeachers();
    });



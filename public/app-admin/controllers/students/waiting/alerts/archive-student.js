angular
    .module("EduNet")
    .controller("archiveWaitingStudentController", function (adminValues,appValidator,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var archiveStudent = function () {
            $scope.closeThisDialog();
            adminApi.archiveWaitingStudentByID({
                studentID : studentID
            },function (err) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                appHelper.showSuccessAlert(adminValues.STUDENT_ARCHIVED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_WAITING_STUDENTS);

            })
        };

        //     INIT
        var studentID = $scope.ngDialogData.studentID;
        if (!studentID) return appHelper.closeDialogAndShowGenError($scope);
        var modal = {
            text :  adminHelper.delMessage.archiveStudent.text,
            comment : adminHelper.delMessage.archiveStudent.comment,
            del : archiveStudent
        };
        $scope.modal = modal;

    });



angular
    .module("EduNet")
    .controller("editSubjectController", function (adminValidator,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var addLevel = function () {
            self.levels.push("");
        };
        var editSubject = function () {
            if (!adminValidator.isValidAddSubjectForm(self)) return;
            $scope.closeThisDialog();
            var data = {
                name : self.name,
                levels : self.levels,
                subjectID : self.subjectID
            };
            adminApi.editSubjectByID(data,function (err) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                appHelper.showGeneralSuccessAlert();
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_SUBJECTS);
            });
        };
        var loadSubject = function (subjectID) {
            adminApi.getSubjectByID({
                subjectID : subjectID
            },function (err,subject) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                self.subject = subject;
                self.name = subject.name;
                self.levels = _.map(subject.levels,function (level) {
                    return level.value;
                });
            });
        };
        var removeLevel = function (index) {
            if (self.levels.length == 1) return;
            self.levels.splice(index,1);
        };
        this.addLevel = addLevel;
        this.removeLevel = removeLevel;
        this.editSubject = editSubject;



        // INIT
        var self = this;
        this.levels = [""];
        this.subjectID = $scope.ngDialogData.subjectID;
        if (!this.subjectID) return appHelper.closeDialogAndShowGenError($scope);
        loadSubject(this.subjectID);


    });



angular
    .module("EduNet")
    .controller("subjectsController", function (appHelper,ngDialog,adminApi,$rootScope,$scope,appValues,$location) {
        var addSubject = function () {
            ngDialog.open({
                template : "/app-admin/views/students/waiting/alerts/add-subject.html",
                className : "ngdialog-theme-default",
                width : 660
            });
        };
        var loadSubjects = function () {
            adminApi.getSubjects(function (err,subjects) {
                if (err) return appHelper.showFailAlertGeneralError();
                self.subjects = subjects;
                self.noSubjects = _.isEmpty(subjects);
                // editSubject(subjects[2]._id);
            });
        };
        var deleteSubject = function (subjectID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                controller : "delSubjectController",
                className : "ngdialog-theme-default",
                width : 320,
                data : {
                    subjectID : subjectID
                }
            });
        };
        var editSubject = function (subjectID) {
            ngDialog.open({
                template : "/app-admin/views/students/waiting/alerts/edit-subject.html",
                className : "ngdialog-theme-default",
                width : 660,
                data : {
                    subjectID : subjectID
                }
            });
        };
        var goBack = function() {
            appHelper.setPrev('SUBJECTS');
            $location.path('students');
        };
        var initController = function () {
            loadSubjects();
        };



        this.addSubject = addSubject;
        this.deleteSubject = deleteSubject;
        this.editSubject = editSubject;
        this.goBack = goBack;

        // INIT
        var self = this;

        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,initController);
        $rootScope.$on(appHelper.EVENTS.LOAD_SUBJECTS,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }


    });



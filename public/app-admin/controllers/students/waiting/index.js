angular
    .module("EduNet")
    .controller("waitingStudentsController", function (appValues,appConstants,$location,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var loadStudents = function (query) {
            adminApi.getWaitingStudents(query,function (err,students) {
                if (err) return appHelper.showError(err);
                self.noMoreStudents = students.length < adminConstants.STUDENTS_PER_PAGE;
                self.students = self.isValuablePagination ? self.students.concat(students) : students;
                if (_.isEmpty(self.students)) {
                    self.noQueryStudents = self.queryStudents;
                    self.noStudents = !self.queryStudents;
                } else {
                    self.noStudents = false;
                    self.noQueryStudents = false;
                }
            });
        };
        var resetPagination = function () {
            self.isValuablePagination = false;
            self.page = 1;
        };
        var setPagination = function () {
            self.isValuablePagination = true;
        };
        var getSelectedStudents = function () {
            var results = [];
            _.each(self.students,function (student,index) {
                if (student.isActive == true) results.push(student._id);
            });
            return results;
        };

        var selectStudent = function (index) {
            if (index > self.students.length) return;
            if (!self.students[index].isActive == null) {
                self.students[index].isActive = true;
            } else {
                self.students[index].isActive = !self.students[index].isActive;
            }
            self.isStudentsSelection = !_.isEmpty(getSelectedStudents());
        };

        var addSubject = function () {
            ngDialog.open({
                template : "/app-admin/views/students/waiting/alerts/add-subject.html",
                className : "ngdialog-theme-default",
                width : 520
            });
        };
        var editStudentByID = function (studentID) {
            ngDialog.open({
                template : "/app-admin/views/students/waiting/alerts/edit-student.html",
                className : "ngdialog-theme-default",
                width : 1000,
                data : {
                    studentID : studentID
                }
            });
        };
        var assignStudents = function () {
            ngDialog.open({
                template : "/app-admin/views/students/waiting/alerts/assign-students.html",
                className : "ngdialog-theme-default",
                width : 520,
                data : {
                    students : getSelectedStudents()
                }
            });
        };
        var formGroup = function () {
            ngDialog.open({
                template : "/app-admin/views/students/waiting/alerts/form-group.html",
                className : "ngdialog-theme-default",
                width : 800,
                data : {
                    students : getSelectedStudents()
                }
            });
        };




        var goToFilterSettings = function () {
            $location.path('/filter-settings');
        };
        var addStudent = function () {
            self.isAddStudent = true;
        };
        var loadSubjects = function (cb) {
            console.log("LOAD");
            adminApi.getSubjects(function (err,subjects) {
                if (err) return appHelper.showFailAlertGeneralError();
                self.subjects = self.subjects.concat(subjects);
                self.noSubjects = _.isEmpty(subjects);
                cb();
            });
        };

        var getStudentsByQuery = function () {
            self.queryStudents = false;
            query = {
                groupType : self.GROUP_TYPE,
                ageType : self.ageType,
                studyDays : [],
                studyTimes : [],
                page : self.page
            };
            if (!_.isEmpty(self.searchName)) {
                query.search = self.searchName;
                self.queryStudents = true;
            }
            if (!_.isEqual(self.subject._id,'-') ) {
                self.queryStudents = true;
                query.subjectID = self.subject._id;
                query.levelID = self.level._id;
            }
            for (var key in self.search.days) {
                if (self.search.days[key] == true) {
                    self.queryStudents = true;
                    query.studyDays.push(key);
                }
            }
            for (var key in self.search.studyTimes) {
                if (self.search.studyTimes[key] == true) {
                    self.queryStudents = true;
                    query.studyTimes.push(key);
                }
            }
            if (query.ageType || query.groupType) self.queryStudents = true;
            loadStudents(query);

        };
        var showMore = function () {
            self.page++;
            setPagination();
            getStudentsByQuery();

        };


        var updateSubject = function () {
            self.level = self.subject.levels[0];
            resetPagination();
        };
        var cancelAddStudent = function () {
            self.isAddStudent = false;
        };
        var setGroupType = function (value) {
            self.GROUP_TYPE = _.isEqual(value,self.GROUP_TYPE) ? null : value;
            resetPagination();
        };
        var setStudyTime = function (index) {
            self.search.studyTimes[index] = !self.search.studyTimes[index];
            resetPagination();
        };
        var setDay = function (index) {
            self.search.days[index] = !self.search.days[index];
            resetPagination();
        };

        var clear = function () {
            self.GROUP_TYPE = null;
            self.subject = appConstants.HELPERS.getNotSelectedObject();
            self.level = {
                _id : appConstants.HELPERS.EMPTY_ID,
                value : appConstants.HELPERS.NOT_SELECTED
            };
            self.subject.levels = [self.level];
            self.ageType = appConstants.EMPTY_ID;
            self.search.days = [false,false,false,false,false,false,false];
            self.search.studyTimes[appConstants.STUDENT_STUDY_TIME_MORNING] = false;
            self.search.studyTimes[appConstants.STUDENT_STUDY_TIME_LUNCH] =  false;
            self.search.studyTimes[appConstants.STUDENT_STUDY_TIME_EVENING] = false;
            getStudentsByQuery();
            resetPagination();

        };
        var archiveStudentByID = function (studentID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : "ngdialog-theme-default",
                controller : "archiveWaitingStudentController",
                width : 320,
                data : {
                    studentID : studentID
                }
            });
        };
        var updateAge = function () {
            resetPagination();
        };
        var studentInfo = function (studentID) {
            ngDialog.open({
                template : "/app-admin/views/students/alerts/student-info/index.html",
                className : "ngdialog-theme-default",
                width : 1120,
                data : {
                    studentID : studentID
                }
            });
        };
        var searchByName = function () {
            if (_.isEmpty(self.students)) return;
            if (self.searchName == self.oldSearch) return;
            self.oldSearch = self.searchName;
            if (Number.isInteger(self.searchTimeOut)) clearTimeout(self.searchTimeOut);
            self.searchTimeOut = setTimeout(invokeSearch,1000);
        };
        var invokeSearch = function () {
            resetPagination();
            getStudentsByQuery();
        };
        var initController = function () {
            self.isAddStudent = false;
            self.oldSearch = "";


            query = null;
            self.searchName = "";

            self.GROUP_TYPE = null;

            self.subject = {
                _id : appConstants.HELPERS.EMPTY_ID,
                name : "Выбрать предмет"
            };
            self.level = {
                _id : appConstants.HELPERS.EMPTY_ID,
                value : "Выбрать уровень"
            };
            self.subject.levels = [self.level];
            self.subjects = [self.subject];
            self.search = {
                days : [false,false,false,false,false,false,false],
                ageTypes : []
            };
            self.search.studyTimes = [];
            self.search.studyTimes[appConstants.STUDENT_STUDY_TIME_MORNING] = false;
            self.search.studyTimes[appConstants.STUDENT_STUDY_TIME_LUNCH] =  false;
            self.search.studyTimes[appConstants.STUDENT_STUDY_TIME_EVENING] = false;


            self.search.ageTypes[appConstants.HELPERS.NOT_SELECTED] = 'Выбрать категорию';
            self.search.ageTypes[appConstants.STUDENT_AGE_GROUP_CHILDREN] = 'Детская ';
            self.search.ageTypes[appConstants.STUDENT_AGE_GROUP_TEENAGE] = 'Подростоковая ';
            self.search.ageTypes[appConstants.STUDENT_AGE_GROUP_YOUTH] = 'Молодежная ';
            self.search.ageTypes[appConstants.STUDENT_AGE_GROUP_ADULT] = 'Взрослая ';

            // LIST
            self.days = [];
            self.days[1] = "Пн";
            self.days[2] = "Вт";
            self.days[3] = "Ср";
            self.days[4] = "Чт";
            self.days[5] = "Пт";
            self.days[6] = "Сб";

            self.studyTimes = [];
            self.studyTimes[appConstants.STUDENT_STUDY_TIME_MORNING] = "Утренее";
            self.studyTimes[appConstants.STUDENT_STUDY_TIME_LUNCH] =  "Обеденное";
            self.studyTimes[appConstants.STUDENT_STUDY_TIME_EVENING] = "Вечернее";

            self.STUDENT_STUDY_TIME_MORNING = appConstants.STUDENT_STUDY_TIME_MORNING;
            self.STUDENT_STUDY_TIME_LUNCH = appConstants.STUDENT_STUDY_TIME_LUNCH;
            self.STUDENT_STUDY_TIME_EVENING = appConstants.STUDENT_STUDY_TIME_EVENING;


            self.groupTypes = [];
            self.groupTypes[appConstants.GROUP_TYPE_GENERAL] = "Общая";
            self.groupTypes[appConstants.GROUP_TYPE_INDIVIDUAL] = "Инд.";



            self.ageTypes = [];
            self.ageTypes[appConstants.HELPERS.EMPTY_ID] = appConstants.HELPERS.NOT_SELECTED;
            self.ageTypes[appConstants.STUDENT_AGE_GROUP_CHILDREN] = 'Детская ';
            self.ageTypes[appConstants.STUDENT_AGE_GROUP_TEENAGE] = 'Подростоковая ';
            self.ageTypes[appConstants.STUDENT_AGE_GROUP_YOUTH] = 'Молодежная ';
            self.ageTypes[appConstants.STUDENT_AGE_GROUP_ADULT] = 'Взрослая ';

            self.STUDENT_AGE_GROUP_CHILDREN = appConstants.STUDENT_AGE_GROUP_CHILDREN;
            self.STUDENT_AGE_GROUP_TEENAGE = appConstants.STUDENT_AGE_GROUP_TEENAGE;
            self.STUDENT_AGE_GROUP_YOUTH = appConstants.STUDENT_AGE_GROUP_YOUTH;
            self.STUDENT_AGE_GROUP_ADULT = appConstants.STUDENT_AGE_GROUP_ADULT;

            self.NOT_SELECTED = appConstants.HELPERS.NOT_SELECTED;
            self.EMPTY_ID = appConstants.EMPTY_ID;
            self.ageType = appConstants.EMPTY_ID;

            self.GROUP_TYPE_INDIVIDUAL = appConstants.GROUP_TYPE_INDIVIDUAL;
            self.GROUP_TYPE_GENERAL = appConstants.GROUP_TYPE_GENERAL;
            self.isStudentsSelection = false;
            self.isValuablePagination = true;
            self.page = 1;
            self.students = [];
            self.noMoreStudents = false;


            loadSubjects(function () {
                getStudentsByQuery();
            });
        };
        var initEvent = function () {
            self.isStudentsSelection = false;
            resetPagination();
            getStudentsByQuery();

        };
        var goSubjects = function () {
            $location.path('subjects');
        };
        this.goSubjects = goSubjects;
        this.addStudent = addStudent;
        this.updateSubject = updateSubject;
        this.studentInfo = studentInfo;
        this.cancelAddStudent = cancelAddStudent;
        this.addSubject = addSubject;
        this.goToFilterSettings = goToFilterSettings;
        this.setGroupType = setGroupType;
        this.setStudyTime = setStudyTime;
        this.setDay = setDay;
        this.editStudentByID = editStudentByID;
        this.assignStudents = assignStudents;
        this.formGroup = formGroup;
        this.getStudentsByQuery = getStudentsByQuery;
        this.clear = clear;
        this.selectStudent = selectStudent;
        this.archiveStudentByID = archiveStudentByID;
        this.showMore = showMore;
        this.updateAge = updateAge;
        this.searchByName = searchByName;


        // INIT

        var self = this,
            query;
        this.noTitle = appConstants.NO_TITLE;
        $rootScope.$on(appHelper.EVENTS.LOAD_WAITING_STUDENTS,initEvent);
    //     INIT LIST AND SEARCH
        $rootScope.$on(appHelper.EVENTS.LOAD_SUBJECTS,initEvent);
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }


    });
angular
    .module("EduNet")
    .controller("errorsController", function (adminApi,$rootScope,$scope,$location) {
        $scope.goToMain = function () {
            $location.path('/');
        };
    });

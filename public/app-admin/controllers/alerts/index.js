angular
    .module("EduNet")
    .controller("alertController", function ($scope,appModalHelper,adminModalHelper) {
        // INIT


        var self = this;
        this.isSuccess = !!$scope.ngDialogData.isSuccess;
        this.message = $scope.ngDialogData.message;
        if (!this.message) return $scope.closeThisDialog();
        this.messages = this.isSuccess ? appModalHelper.successMessages.concat(adminModalHelper.successMessages) : appModalHelper.errorMessages.concat(adminModalHelper.errorMessages);

        var index = _.map(this.messages,function (message) {
            return message.value
        }).indexOf(self.message);
        if (index < 0) return $scope.closeThisDialog();
        this.text = this.messages[index].message;


    });

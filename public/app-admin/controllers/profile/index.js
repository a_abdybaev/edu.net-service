angular
    .module("EduNet")
    .controller("profileController", function (appValues,appConstants,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {

        var editProfile = function () {
            ngDialog.open({
                template : "/app-admin/views/profile/alerts/edit-profile.html",
                className : 'ngdialog-theme-default',
                width : 600

            });
        };
        var editPassword = function () {
            ngDialog.open({
                template : "/app-admin/views/profile/alerts/edit-password.html",
                className : 'ngdialog-theme-default',
                width : 600

            });
        };
        var loadProfile = function () {
            adminApi.getAdminInfo(function (err,info) {
                if (err) return appHelper.showError(err);
                if (!info.admin.centerID.imageUrl) info.admin.centerID.imageUrl = appConstants.CENTER_IMAGE_DEFAULT_URL;
                self.admin = info.admin;
                self.groups = info.groups;
                self.client = info.admin.clientID;


            });
        };
        //AVATAR .. start
        var showAvatarView = function () {
            angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
            self.myImage = '';
            self.myCroppedImage = '';
            self.imageUrl = null;
            self.isSettingAvatar = true;
        };
        var closeAvatarView = function () {
            self.isSettingAvatar = false;
        };
        var cancelAvatarView = function () {
            angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
            self.isSettingAvatar = false;
            self.myImage = undefined;
            self.myCroppedImage = undefined;
            self.imageUrl = undefined;
        };
        var handleFileSelect=function(evt) {
            var file=evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function($scope){
                    self.myImage=evt.target.result;
                });
            };
            reader.readAsDataURL(file);
        };
        var updateImage = function () {
            var myCroppedImage = self.myCroppedImage;
            if (!myCroppedImage) return;
            adminApi.updateImage({
                image : myCroppedImage
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(appValues.AVATAR_UPDATED);
                closeAvatarView();
                loadProfile();
            });
        };
        var initController = function () {
            loadProfile();
        };

        var self = this;
        this.myImage='';
        this.myCroppedImage='';
        angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
        this.cancelAvatar = function () {
            self.myImage = '';
            self.myCroppedImage = '';
            self.closeAvatarView();
            self.imageUrl = null;
        };
        this.showAvatarView = showAvatarView;
        this.closeAvatarView = closeAvatarView;
        this.cancelAvatarView = cancelAvatarView;
        this.updateImage = updateImage;
        this.isSettingAvatar = false;

        // avatar end

        this.editProfile = editProfile;
        this.editPassword = editPassword;


        $rootScope.$on(appHelper.EVENTS.LOAD_INDEX,initController);
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }
    });



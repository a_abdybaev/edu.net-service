angular
    .module("EduNet")
    .controller("editProfileController", function (appValues,adminValidator,adminApi,$rootScope,$scope,appHelper) {
        var self = this;
        var loadProfile = function () {
            adminApi.getAdminInfo(function (err,info) {
                if (err) return appHelper.showError(err);
                self.email = info.admin.email;
                self.name = info.admin.centerID.name;
            });
        };
        var updateProfile = function () {
            if(!adminValidator.isValidEditProfile(self)) return;
            $scope.closeThisDialog();
            adminApi.updateProfile({
                email : self.email,
                name : self.name
            },function (err) {
                if (err) return console.log(err);
                appHelper.showSuccessAlert(appValues.PROFILE_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_INDEX);
            })
        };
        this.updateProfile = updateProfile;
        loadProfile();
    });

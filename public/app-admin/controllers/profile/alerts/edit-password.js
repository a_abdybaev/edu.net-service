angular
    .module("EduNet")
    .controller("editPasswordController", function (appValues,adminHelper,adminValidator,appHelper,adminApi,$rootScope,$scope) {
        var updatePassword = function () {
            if (!adminValidator.isValidEditPassword(self)) return;
            $scope.closeThisDialog();
            adminApi.updatePassword({
                oldPassword : self.oldPassword,
                newPassword : self.newPassword
            },function (err) {
                if (err && err.status == 404) return appHelper.showFailAlert(appValues.PASSWORDS_NOT_MATCHES);
                if (err) return appHelper.showError(err);
                appHelper.showSuccessAlert(appValues.PASSWORD_UPDATED);
            });
        };


        this.updatePassword = updatePassword;




        // INIT
        var self = this;
    });

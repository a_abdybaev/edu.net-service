angular
    .module("EduNet")
    .controller("editSectionAnswersController", function (appValues,appConstants,testValidator,$location,appHelper,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        console.log("CALLED");
        var loadTestByID = function (testID) {
            adminApi.getTestByID({
                testID : testID
            },function (err,test) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                var answers = test.sections[sectionIndex].answers;
                _.each(answers,function (answer) {
                    answer.value = "";
                    _.each(answer.values,function (value,i) {
                        answer.value +=value;
                        if (i < answer.values.length-1) {
                            answer.value+= "\r\n";
                        }
                    });
                });
                if (questionsCount) {
                    if (questionsCount > answers.length) {
                        self.alert = true;
                        self.oldAnswersCount = answers.length + 1;
                        for (var i = answers.length; i < questionsCount;i++) {
                            answers.push({
                                value : ""
                            })
                        }
                    }
                    if (questionsCount < answers.length) {
                        answers.splice(questionsCount,answers.length-questionsCount);
                    }
                }
                self.answers = answers;

            });
        };
        var updateTestSectionByIndex = function () {
            if(!testValidator.isValidUpdateTestSectionAnswers(self)) return;

            $scope.closeThisDialog();

            var answers = _.each(self.answers,function (answer) {
                return answer.value;
            });

            adminApi.updateTestSectionByIndex({
                answers : answers,
                testID : testID,
                sectionIndex : sectionIndex,
                duration : duration
            },function (err) {
                console.log("WTF");
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(appValues.TEST_SECTION_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_TEST);
            });
        };

    //     INIT
        this.updateTestSectionByIndex = updateTestSectionByIndex;
        var self = this,
            testID = $scope.ngDialogData.testID,
            sectionIndex = $scope.ngDialogData.sectionIndex,
            duration = $scope.ngDialogData.duration,
            questionsCount = parseInt($scope.ngDialogData.questionsCount);
        if (!testID) return  appHelper.closeDialogAndShowGenError($scope);
        loadTestByID(testID);


    });






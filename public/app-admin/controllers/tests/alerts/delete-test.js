"use strict";

angular
    .module("EduNet")
    .controller("deleteTestController", function (appHelper,appValues,appConstants,appModalHelper,$rootScope,adminHelper,$scope,$location,adminApi) {
        var deleteTest = function () {
            adminApi.deleteTestByID({
                testID : testID
            },function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError(err);
                appHelper.showSuccessAlert(appValues.TEST_DELETED);
                $location.path('/tests');
            });
        };
        
        //     INIT
        var modal = {
            text :  appModalHelper.deleteMessages.test.text,
            comment : appModalHelper.deleteMessages.test.comment,
            del : deleteTest
        };
        var testID = $scope.ngDialogData.testID;
        if (!testID) return appHelper.closeDialogAndShowGenError($scope);
        $scope.modal = modal;
    });


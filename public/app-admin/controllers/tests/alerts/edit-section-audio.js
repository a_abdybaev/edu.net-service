angular
    .module("EduNet")
    .controller("editSectionAudioController", function (appValues,testValidator,$location,appHelper,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var updateAudio = function () {
            var data = {
                audioFile : self.audioFile,
                testID : testID,
                sectionIndex : sectionIndex
            };
            if (!data.audioFile) return;
            $scope.closeThisDialog();
            adminApi.updateTestSectionByIndex(data,function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(appValues.TEST_SECTION_UPDATED);
            });
        };
        // INIT
        this.updateAudio = updateAudio;
        var self = this,
            testID = $scope.ngDialogData.testID,
            sectionIndex = $scope.ngDialogData.sectionIndex;
        if (!testID || isNaN(sectionIndex)) return appHelper.closeDialogAndShowGenError($scope);
    });






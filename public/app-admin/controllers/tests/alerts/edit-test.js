angular
    .module("EduNet")
    .controller("editTestController", function (appValidator,appValues,$location,appHelper,adminApi,$rootScope,$scope) {
        var editTest = function () {
            var data = {
                name : self.test.name,
                testID : testID
            };
            if (!appValidator.isValidField(self.test,'name')) return;
            $scope.closeThisDialog();

            adminApi.updateTestByID(data,function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(appValues.TEST_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_TEST);
            });
        };
        var loadTestByID = function (testID) {
            adminApi.getTestByID({
                testID : testID
            },function (err,test) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                self.test = test;
            });
        };


        // INIT
        var self = this;
        var testID = $scope.ngDialogData.testID;
        if (!testID) return $scope.closeThisDialog();
        this.editTest = editTest;
        loadTestByID(testID);
    });






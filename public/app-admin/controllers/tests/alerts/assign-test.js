angular
    .module("EduNet")
    .controller("assignTest", function (appValues,appConstants,$location,appHelper,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
       var loadAvailableGroupsForTest = function (testID) {
           adminApi.getAssignAvailableGroupsForTest({
               testID : testID
           },function (err,groups) {
               if (err) return appHelper.closeDialogAndShowGenError($scope);
               if (groups.length == 0) return self.noGroups = true;
               self.noGroups = false;
               self.groups = groups;
               self.groupID = groups[0]._id;
           });
       };
       var assignTest = function () {
           $scope.closeThisDialog();
           adminApi.assignTest({
               groupID : self.groupID,
               testID : testID
           },function (err) {
               if (err) return appHelper.showFailAlertGeneralError();
               appHelper.showSuccessAlert(appValues.TEST_ASSIGNED);
           });
       };

       // INIT
       var self = this;
       this.assignTest = assignTest;
       var testID = $scope.ngDialogData.testID;
       if (!testID) return appHelper.closeDialogAndShowGenError($scope);
       loadAvailableGroupsForTest(testID);


    });






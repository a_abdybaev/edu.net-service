angular
    .module("EduNet")
    .controller("editSectionController", function (appValues,appConstants,$location,appHelper,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var loadTestByID = function (testID) {
            adminApi.getTestByID({
                testID : testID
            },function (err,test) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                self.duration = test.sections[sectionIndex].duration / 60;
                self.questionsCount = test.sections[sectionIndex].questionsCount;
                self.test = test;

            });
        };
        var updateSection = function () {
            var data = {
                duration : self.duration,
                testID : testID,
                sectionIndex : sectionIndex
            };
            if (!data.duration || isNaN(parseInt(data.duration)) || !self.questionsCount || isNaN(parseInt(self.questionsCount))) return;
            $scope.closeThisDialog();
            if (parseInt(self.questionsCount) == self.test.sections[sectionIndex].questionsCount) {
                adminApi.updateTestSectionByIndex(data,function (err) {
                    if (err) return appHelper.showFailAlertGeneralError();
                    appHelper.showSuccessAlert(appValues.TEST_SECTION_UPDATED);
                    $rootScope.$broadcast(appHelper.EVENTS.LOAD_TEST);
                });
            } else {
                data.questionsCount = self.questionsCount;
                ngDialog.open({
                    template : "/app-admin/views/tests/alerts/edit-section-answers.html",
                    className : 'ngdialog-theme-default',
                    width : 900,
                    data : data
                });
            }
        };

        //     INIT
        var self = this,
            testID = $scope.ngDialogData.testID,
            sectionIndex = $scope.ngDialogData.sectionIndex;
        this.udpateSection = updateSection;

        if (!testID) return appHelper.closeDialogAndShowGenError($scope);
        loadTestByID(testID);
    });






"use strict";

angular
    .module("EduNet")
    .controller("deleteTestSectionController", function (appValues,appHelper,$rootScope,adminHelper,$scope,$location,adminApi) {
        var deleteTestSection = function () {
            $scope.closeThisDialog();
            adminApi.deleteTestSectionByIndex({
                sectionIndex : sectionIndex,
                testID : testID
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError(err);
                appHelper.showSuccessAlert(appValues.TEST_SECTION_DELETED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_TEST);

            })
        };
        //     deleteTestSection

        var modal = {
            text :  adminHelper.delMessage.testSection.text,
            comment : adminHelper.delMessage.testSection.text,
            del : deleteTestSection
        };

        var sectionIndex = $scope.ngDialogData.sectionIndex,
            testID = $scope.ngDialogData.testID;

        if (!_.isNumber(sectionIndex) || !testID) return appHelper.closeDialogAndShowGenError($scope);
        $scope.modal = modal;
    });
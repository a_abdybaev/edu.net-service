angular
    .module("EduNet")
    .controller("editSectionFileController", function (appValues,$location,appHelper,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var updateFile = function () {
            if (!self.file) return;
            var data = {
                file : self.file,
                testID : testID,
                sectionIndex : sectionIndex,
                pagesFrom : parseInt(self.pagesFrom),
                pagesTo : parseInt(self.pagesTo)
            };
            if (!data.file || isNaN(data.pagesFrom) || isNaN(data.pagesTo)) return;
            $scope.closeThisDialog();

            adminApi.updateTestSectionByIndex(data,function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(appValues.TEST_SECTION_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_TEST);
            });
        };



        var self = this;
        var testID = $scope.ngDialogData.testID,
            sectionIndex = $scope.ngDialogData.sectionIndex;
        if (!testID || sectionIndex == null)return appHelper.closeDialogAndShowGenError($scope);
        this.updateFile = updateFile;

    });






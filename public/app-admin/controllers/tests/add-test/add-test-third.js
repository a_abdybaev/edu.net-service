angular
    .module("EduNet")
    .controller("addTestThirdStepController", function ($location,testValidator,appValues,appHelper,$scope,adminApi) {
        var cancel = function () {
            ctrl.isFirstStep = false;
            ctrl.isThirdStep = false;
            ctrl.isSecondStep = true;
        };
        var draw = function () {
        };

        var addTest = function () {
            if (!testValidator.isValidThirdStep(ctrl)) return;
            adminApi.addTest({
                sections : ctrl.sections,
                name : ctrl.name
            },function (err) {
                if (err) return appHelper.showError(err);
                appHelper.showSuccessAlert(appValues.TEST_CREATED);
                $location.path('tests');

            });

        };



        //    INIT
        var self = this;
        var ctrl = $scope.$parent.ctrl;
        _.each(ctrl.sections,function (section) {
            var questionsCount = parseInt(section.questionsCount);
            section.answers = new Array(questionsCount);
            section.answersValidation = new Array(questionsCount);
        });

        // ctrl.sections[0].answers = ["a","B","c","D","e"];
        this.cancel = cancel;
        this.addTest = addTest;
        this.draw = draw;




    });



angular
    .module("EduNet")
    .controller("addTestSecondStepController", function (testValidator,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var cancel = function () {
            ctrl.isFirstStep = true;
            ctrl.isThirdStep = false;
            ctrl.isSecondStep = false;
        };
        var next = function () {
            if(!testValidator.isValidSecondStep(ctrl)) return;
            ctrl.isFirstStep = false;
            ctrl.isThirdStep = true;
            ctrl.isSecondStep = false;
        };

        // init
        var self = this;
        var ctrl = $scope.$parent.ctrl;


        this.next = next;
        this.cancel = cancel;
        // next();



    });



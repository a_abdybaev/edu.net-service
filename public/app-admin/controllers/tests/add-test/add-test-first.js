angular
    .module("EduNet")
    .controller("addTestFirstStepController", function (testValidator,$location,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var increaseSections = function () {
            ctrl.sections.push({
                pages : 'ALL'
            });
        };
        var decreaseSections = function () {
            if (ctrl.sections.length == 1) return;
            ctrl.sections.pop();
        };
        var cancel = function () {
            $location.path('tests');
        };
        var next = function () {
            if (!testValidator.isValidFirstStep(ctrl)) return;
            ctrl.isFirstStep = false;
            ctrl.isThirdStep = false;
            ctrl.isSecondStep = true;
        };

        // init
        var self = this;
        var ctrl = $scope.$parent.ctrl;


        this.increaseSections = increaseSections;
        this.decreaseSections = decreaseSections;
        this.next = next;
        this.cancel = cancel;


        //    DEVELOOPMENt
        // ctrl.name = "IELTS LISTENING";
        // ctrl.sections = [{
        //     pages : 'INTERVAL',
        //     minutesCount : 60,
        //     questionsCount : 5,
        //     pagesFrom : 1,
        //     pagesTo : 4
        //     // questionsCount : 3
        // }];

        // next();


    });



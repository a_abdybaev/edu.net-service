angular
    .module("EduNet")
    .controller("testsController", function ($location,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var loadTests = function () {
            adminApi.getTests(function (err,tests) {
                if (err) return appHelper.showFailAlertGeneralError();
                if (_.isEmpty(tests)) return self.noTests = true;
                self.noTests = false;
            //    compute total duration and questions count
                _.each(tests,function (test) {
                    test.duration = 0;
                    test.questionsCount = 0;
                    _.each(test.sections,function (section) {
                        test.duration +=section.duration;
                        test.questionsCount +=section.questionsCount;
                    });
                });
                self.tests = tests;
            })
        };
        var assignTest = function (testID) {
            ngDialog.open({
                template : "/app-admin/views/tests/alerts/assign-test.html",
                className : 'ngdialog-theme-default',
                width : 520,
                data : {
                    testID : testID
                }
            });
        };
        var addTest = function () {
            $location.path('add-test');
        };
        var goToTestInfo = function (testID) {
            adminHelper.setActTestID(testID);
            $location.path('test-info');
        };
        var initController = function () {
            loadTests();
        };
        this.addTest = addTest;
        this.assignTest = assignTest;
        this.goToTestInfo = goToTestInfo;

        var self = this;


        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }
    });



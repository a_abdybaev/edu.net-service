angular
    .module("EduNet")
    .controller("testInfoController", function (appValues,$window,$location,appHelper,adminConstants,adminHelper,ngDialog,adminApi,$rootScope,$scope) {
        var loadTestByID = function () {
            adminApi.getTestByID({
                testID : testID
            },function (err,test) {
                if (err) return appHelper.showError(err);
                test.questionsCount = 0;
                test.duration = 0;
                _.each(test.sections,function (section) {
                    test.questionsCount += section.questionsCount;
                    test.duration += section.duration;
                });
                self.test = test;
                setActiveSection(0);
            });
        };

        var setActiveSection = function (index) {
            self.sectionIndex = index;
            self.section = self.test.sections[index];
            self.imageIndex = 0;
            // updateAudio();

        };

        var nextPage = function () {
            self.imageIndex = self.imageIndex < self.section.images.length - 1 ? self.imageIndex + 1 : self.imageIndex;
        };
        var previousPage = function () {
            self.imageIndex = self.imageIndex > 0 ? self.imageIndex - 1 : self.imageIndex;
        };

        var editTest = function () {
            ngDialog.open({
                template : "/app-admin/views/tests/alerts/edit-test.html",
                className : 'ngdialog-theme-default',
                width : 600,
                data : {
                    testID : testID
                }
            });
        };
        var editSection = function () {
            ngDialog.open({
                template : "/app-admin/views/tests/alerts/edit-section.html",
                className : 'ngdialog-theme-default',
                width : 520,
                data : {
                    testID : testID,
                    sectionIndex : self.sectionIndex
                }
            });
        };
        var editSectionFile = function () {
            ngDialog.open({
                template : "/app-admin/views/tests/alerts/edit-section-file.html",
                className : 'ngdialog-theme-default',
                width : 520,
                data : {
                    testID : testID,
                    sectionIndex : self.sectionIndex
                }
            });
        };
        var editSectionAnswers = function () {
            ngDialog.open({
                template : "/app-admin/views/tests/alerts/edit-section-answers.html",
                className : 'ngdialog-theme-default',
                width : 900,
                data : {
                    testID : testID,
                    sectionIndex : self.sectionIndex
                }
            });
        };
        var deleteTest = function () {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                controller : "deleteTestController",
                width : 320,
                data : {
                    testID : testID
                }
            });
        };
        var deleteSection = function () {
            if (self.test.sections.length <= 1) return deleteTest();
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                controller : "deleteTestSectionController",
                width : 520,
                data : {
                    sectionIndex : self.sectionIndex,
                    testID : testID
                }
            });
        };
        var updateAudio = function () {
            ngDialog.open({
                template : "/app-admin/views/tests/alerts/edit-section-audio.html",
                width : 520,
                data : {
                    sectionIndex : self.sectionIndex,
                    testID : testID
                }
            });
        };
        var initController = function () {
            testID = adminHelper.getActTestID();
            if (!testID) return $location.path('tests');
            loadTestByID();
        };
        this.editTest = editTest;
        this.setActiveSection = setActiveSection;
        this.editSection = editSection;
        this.deleteSection = deleteSection;
        this.nextPage = nextPage;
        this.previousPage = previousPage;
        this.editSectionFile = editSectionFile;
        this.editSectionAnswers = editSectionAnswers;
        this.deleteTest = deleteTest;
        this.deleteSection = deleteSection;
        this.updateAudio = updateAudio;



        // init
        var self = this,
            testID;

        $rootScope.$on(appHelper.EVENTS.LOAD_TEST,loadTestByID);
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }


    });



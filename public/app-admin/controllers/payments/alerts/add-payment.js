angular
    .module("EduNet")
    .controller("addPaymentController", function (adminValues,appValidator,adminConstants,appHelper,adminApi,$rootScope,$scope) {
        var loadStudents = function (groupID) {
            adminApi.getStudentsByGroupID({
                groupID : groupID
            },function (err,students) {
                if (err) return appHelper.showError(err);
                if (_.isEmpty(students)) return $scope.closeThisDialog();
                appHelper.createNameForUsers(students);
                self.students = students;
                self.student = students[0];
            });
        };
        var addPayment = function () {
            if (!appValidator.isValidField(self,'value')) return;
            var data = {
                value : self.value,
                comment : self.comment,
                studentID : self.student._id,
                groupID : groupID
            };
            $scope.closeThisDialog();
            adminApi.addPayment(data,function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.PAYMENT_CREATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_PAYMENTS);
            });

        };

        this.addPayment = addPayment;

        // INIT
        var self = this;
        var groupID = $scope.ngDialogData.groupID;
        if (!groupID) return appHelper.closeDialogAndShowGenError($scope);
        loadStudents(groupID);

    });

angular
    .module("EduNet")
    .controller("editPaymentController", function (adminValues,appValidator,appHelper,adminConstants,adminHelper,adminApi,$rootScope,$scope) {
        var loadPaymentByID = function (paymentID) {
            adminApi.getPaymentByID({
                paymentID : paymentID
            },function (err,payment) {
                if (err) {
                    appHelper.showFailAlertGeneralError();
                    return $scope.closeThisDialog();
                }
                appHelper.createNameForUser(payment.studentID);
                self.name = payment.studentID.name;
                self.students = [payment.studentID];
                self.student = payment.studentID;
                self.value = payment.value;
                self.comment = payment.comment;
            });
        };
        var updatePayment = function () {
            if (!appValidator.isValidField(self,'value')) return;

            $scope.closeThisDialog();
            adminApi.updatePaymentByID({
                paymentID : paymentID,
                value : self.value,
                comment : self.comment
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.PAYMENT_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_PAYMENTS);
            });
        };

        this.updatePayment = updatePayment;

        // INIT
        var self = this;
        var paymentID = $scope.ngDialogData.paymentID;
        if (!paymentID) return appHelper.closeDialogAndShowGenError($scope);
        loadPaymentByID(paymentID);

    });

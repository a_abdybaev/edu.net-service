angular
    .module("EduNet")
    .controller("deletePaymentController", function (adminValues,appHelper,adminConstants,adminHelper,adminApi,$rootScope,$scope) {
        var deletePayment = function () {
            $scope.closeThisDialog();
            adminApi.deletePaymentByID({
                paymentID : paymentID
            },function (err) {
                if (err) return console.log(err);
                appHelper.showSuccessAlert(adminValues.PAYMENT_DELETED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_STUDENTS);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_PAYMENTS);
            });
        };


        // INIT
        var paymentID = $scope.ngDialogData.paymentID;
        if (!paymentID) return appHelper.closeDialogAndShowGenError($scope);
        var modal = {
            text :  adminHelper.delMessage.payment.text,
            comment : adminHelper.delMessage.payment.comment,
            del : deletePayment
        };
        $scope.modal = modal;

    });

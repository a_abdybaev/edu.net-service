angular
    .module("EduNet")
    .controller("paymentsController", function (appValues,ngDialog,appHelper,adminHelper,adminApi,$rootScope,$scope) {

        var addPayment = function () {
            ngDialog.open({
                template : "/app-admin/views/payments/alerts/add-payment.html",
                className : 'ngdialog-theme-default',
                width : 600,
                data : {
                    groupID : self.activeGroup._id
                }
            });
        };
        var editPaymentByID = function (paymentID) {
            ngDialog.open({
                template : "/app-admin/views/payments/alerts/edit-payment.html",
                className : 'ngdialog-theme-default',
                width : 600,
                data : {
                    paymentID : paymentID
                }
            });
        };
        var deletePaymentByID = function (paymentID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deletePaymentController",
                data : {
                    paymentID : paymentID
                }
            });
        };
        var initController = function () {

            adminApi.getListOfGroups(function (err,groups) {
                if (err) return appHelper.showError(err);
                if (groups.length == 0) return self.noGroups = true;
                self.noGroups = false;
                if (!self.isLoaded) {
                    self.activeGroup = groups[0];
                    self.activeGroupID = self.activeGroup._id;
                }
                self.groups = groups;
                loadStudentsByGroup();
            });
        };
        var changeActiveStudentIndex = function (index) {
            self.studentIndex = index;
            self.student = self.students[index];
        };
        var loadStudentsByGroup = function () {
            var index = self.groups.map(function (group) {
                return group._id;
            }).indexOf(self.activeGroupID);
            if (index < 0) return;
            self.activeGroup = self.groups[index];
            if (!self.activeGroup.price || self.activeGroup.price == 0) return self.noPrice = true;
            self.noPrice = false;
            adminApi.getStudentsByGroupID({
                groupID : self.activeGroupID
            },function (err,students) {
                if (_.isEmpty(students)) return self.noStudents = true;
                self.noStudents = false;
                appHelper.computeTotalPaymentForStudents(students);
                self.students = students;
                if (!self.isLoaded) {
                    self.isLoaded = true;
                    changeActiveStudentIndex(0);
                }  else {
                    changeActiveStudentIndex(self.studentIndex);
                }

            });
        };



        this.loadStudentsByGroup = loadStudentsByGroup;
        this.changeActiveStudentIndex = changeActiveStudentIndex;
        this.addPayment = addPayment;
        this.editPaymentByID = editPaymentByID;
        this.deletePaymentByID = deletePaymentByID;

        //INIT



        var self = this;
        this.isLoaded = false;


        $rootScope.$on(appHelper.EVENTS.LOAD_PAYMENTS,loadStudentsByGroup);
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }



    });

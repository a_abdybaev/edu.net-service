"use strict";

angular
    .module("EduNet")
    .controller("editGroupController", ['app','$scope','$rootScope',function (app,scope,root) {
        var loadTeachers = function () {
            app.api.getTeachers(function (err,teachers) {
                if (err) return app.main.helper.showError(err);
                self.teachers = self.teachers.concat(teachers);
                loadGroupByID(groupID);
            });
        };
        var loadGroupByID = function (cb) {
            app.api.getGroupByID(groupID, function (err,group) {
                if (err) return app.main.helper.showError(err);
                self.name = group.name;
                self.price = group.price;
                self.colors = app.constants.COLORS;
                var index = self.colors.map(function (color) {
                    return color.value;
                }).indexOf(group.color);
                if (index > -1) self.activeColorIndex = index;
                _.each(group.teachers,function (teacher) {
                    teacher.isChoosed = true;
                });

                self.group = group;
                cb()
            });
        };
        var setColor = function (index) {
            self.activeColorIndex = index;
        };
        var updateGroup = function () {
            console.log("upda");
            if(!app.validator.isValidEditGroup(self)) return;
            console.log("valid");
            var color = self.colors[self.activeColorIndex].value,
                name = self.name,
                data = {
                    name : name,
                    color : color,
                    price : self.price,
                    groupID : groupID,
                    teachers : {
                        plus : [],
                        minus : []
                    }
                };
            _.each(self.group.teachers,function (teacher) {
                if (!teacher.isChoosed) data.teachers.minus.push(teacher._id);
            });
            _.each(self.teachers,function (teacher) {
                if (teacher.isChoosed) data.teachers.plus.push(teacher._id);
            });
            console.log(data);
            // return;
            app.api.updateGroupByID(data, function (err) {
                if (err) return app.main.helper.showFailAlertGeneralError();
                app.main.helper.showSuccessAlert(app.values.GROUP_UPDATED);
                app.helper.goToMain();
            });
        };
        var cancel = function () {
            app.helper.goToMain();
        };
        var initController = function () {
            loadGroupByID(function () {
                getAvailableTeachersByGroup();
            });

        };
        var fixChoosing = function (index) {
            self.group.teachers[index].isChoosed = !self.group.teachers[index].isChoosed;
        };
        var getAvailableTeachersByGroup = function () {
            app.api.getAvailableTeachersByGroupID({
                groupID : groupID
            },function (err,teachers) {
                if (err) return app.helper.showError(err);

                self.isAvailableTeachers = !_.isEmpty(teachers);
                self.additionAvailability = self.isAvailableTeachers;
                self.teachers = [self.teacherObj].concat(teachers);

            });
        };
        var cancelSelection = function (index) {
            self.teachers[index].isChoosed = false;
            fixAddition();
        };
        var applyStep = function (step) {
            if (step == 1) {
                self.teacherStep = 2;
            }
            if (step == 2) {
                // if (self.selectedIndex == 0) return console.log("NEED SELECt");
                var index = _.map(self.teachers,function (teacher) {
                    return teacher._id;
                }).indexOf(self.teacherID);
                if (index <=0) return console.log("NEED SELECT");

                self.teachers[index].isChoosed = true;
                return fixAddition();
            }
            if (step == 3) return fixAddition();
        };
        var fixAddition = function () {
            self.additionAvailability = _.some(self.teachers,function (teacher) {
                return !teacher.isChoosed && teacher._id != app.main.dictionary.EMPTY_ID
            });
            self.teacherStep = 1;
        };
        this.cancelSelection = cancelSelection;
        this.fixChoosing = fixChoosing;
        this.setColor = setColor;
        this.updateGroup = updateGroup;
        this.cancel = cancel;
        this.applyStep = applyStep;

        // INIT
        var self = this;

        var groupID = app.main.helper.getActGroupID();
        if (!groupID) return app.helper.goToMain();
        this.teacherObj = {
            _id : app.main.dictionary.EMPTY_ID,
            firstName : "Выбрать учителя",
            lastName : ""
        };


        this.vacationObj = {
            _id : app.main.dictionary.EMPTY_ID,
            firstName : app.main.dictionary.VACATION,
            lastName : ''
        };

        this.students = [];
        this.teachers = [this.vacationObj];
        this.teacherStep = 1;
        this.selectedIndex = 0;
        root.$on(app.main.values.EVENTS.INDEX_LOADED,initController);
        if (scope.$parent.index.isLoad) {
            initController();
        }
    }]);
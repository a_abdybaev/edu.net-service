"use strict";

angular
    .module("EduNet")
    .controller("deleteGroupController", function (appHelper,adminValues,$rootScope,adminHelper,$scope,$location,adminApi) {

        var del = function () {
            $scope.closeThisDialog();
            adminApi.deleteGroupByID(groupID, function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.GROUP_DELETED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_GROUPS);
            });
        };



        // INIT
        var groupID = $scope.ngDialogData.groupID;
        if (!groupID) return appHelper.closeDialogAndShowGenError($scope);
        var modal = {
            text :  adminHelper.delMessage.group.text,
            comment : adminHelper.delMessage.group.comment,
            del : del
        };
        console.log(modal);
        $scope.modal = modal;


    });
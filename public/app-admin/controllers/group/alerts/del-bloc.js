angular
    .module("EduNet")
    .controller("delBlocController", function (adminValues,appDictionary,appValidator,adminHelper,appHelper,$scope,adminApi,$rootScope) {
        var del = function () {
            $scope.closeThisDialog();
            adminApi.delBlocByID({
                blocID : blocID
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.BLOC_DELETED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_GROUPS);
            });
        };
        var modal = {
            text :  adminHelper.delMessage.bloc.text,
            comment : adminHelper.delMessage.bloc.comment,
            del : del
        };

        var blocID = $scope.ngDialogData.blocID;
        if(!blocID) return appHelper.closeDialogAndShowGenError($scope);

        $scope.modal = modal;
    });
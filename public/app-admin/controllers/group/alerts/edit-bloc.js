angular
    .module("EduNet")
    .controller("editBlocController", function (adminValues,appDictionary,appValidator,adminHelper,appHelper,$scope,adminApi,$rootScope) {
        var loadBlocByID = function (blocID) {
            adminApi.getBlocByID({
                blocID : blocID
            },function (err,bloc) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                self.bloc = bloc;
                var index = _.map(self.colors,function (color) {
                    return color.value
                }).indexOf(self.bloc.color);
                index < 0  ? setColor(0) : setColor(index);



                if (_.isEmpty(bloc.groups)) return self.noGroups = true;
            });
        };
        var editBlocByID = function () {
            var data = {
                name : self.bloc.name,
                groups : [],
                blocID : self.bloc._id,
                color : self.colors[self.actColor].value
            };
            if (!data.name) return;
            _.each(self.bloc.groups,function (group) {
                if (group.isBloc) return;
                data.groups.push({
                    groupID : group._id,
                    isBloc : group.isBloc
                });
            });
            $scope.closeThisDialog();
            adminApi.editBlocByID(data,function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.BLOC_UPDATED);
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_GROUPS);
            })
        };
        var assign = function (index) {
            self.bloc.groups[index].isBloc =  !self.bloc.groups[index].isBloc
        };
        var setColor = function (index) {
            self.actColor = index;
        };


        var initController = function () {
            self.blocID = $scope.ngDialogData.blocID;
            if (!self.blocID) return appHelper.closeDialogAndShowGenError($scope);
            loadBlocByID(self.blocID);
        };

        this.editBlocByID= editBlocByID;
        this.assign = assign;
        this.setColor = setColor;
        this.colors = adminValues.COLORS;
        this.actColor = 0;
        var self = this;
        initController();



    });
angular
    .module("EduNet")
    .controller("assignGroupForBlocController", function ($rootScope,appDictionary,appValidator,adminHelper,appHelper,$scope,adminApi) {
        var loadBlocs = function () {
            adminApi.getBlocs(function (err,blocs) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                if (_.isEmpty(blocs)) return appHelper.closeDialogAndShowGenError($scope);
                self.blocs = blocs;
                self.bloc = blocs[0];

            })
        };
        var assignGroup = function () {
            $scope.closeThisDialog();
            adminApi.assignGroupByBlocID({
                blocID : self.bloc._id,
                groupID : self.groupID
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showGeneralSuccessAlert();
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_GROUPS);
            })
        };
        var initController = function () {
            self.groupID = $scope.ngDialogData.groupID;
            if (!self.groupID) appHelper.closeDialogAndShowGenError($scope);
            loadBlocs();
        };

        this.assignGroup = assignGroup;


        var self = this;



        initController();


    });
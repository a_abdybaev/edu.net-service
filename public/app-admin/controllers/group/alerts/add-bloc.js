angular
    .module("EduNet")
    .controller("addGroupBlocController", function (appValues,adminValues,$rootScope,appDictionary,appValidator,appHelper,$scope,adminApi) {

        var addBloc = function () {
            if (!appValidator.isValidField(self,'name')) return;
            var data = {
                name : self.name,
                groups : [],
                color : self.colors[self.actColor].value
            };
            if (!self.noGroups) _.each(self.groups,function (group) {
                if (group.isSelected) data.groups.push(group._id);
            });
            $scope.closeThisDialog();
            adminApi.addBloc(data,function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(adminValues.BLOC_CREATED);

                $rootScope.$broadcast(appHelper.EVENTS.LOAD_GROUPS);
            });
            

        };
        var setColor = function (index) {
            self.actColor = index;
        };

        var initController = function () {
            adminApi.getGroups({
                isBloc : false
            },function (err,groups) {
                if (err) return appHelper.closeDialogAndShowGenError($scope);
                self.groups = groups;

                self.noGroups = _.isEmpty(groups);
            });
        };
        this.addBloc = addBloc;
        this.setColor = setColor;
        

        // INIT
        var self = this,
            vacation = appDictionary.VACATION;

        this.colors = adminValues.COLORS;
        this.actColor = 0;

        initController();






    });
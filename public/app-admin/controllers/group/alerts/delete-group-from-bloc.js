angular
    .module("EduNet")
    .controller("delGroupFromBlocController", function (appDictionary,appValidator,adminHelper,appHelper,$scope,adminApi,$rootScope) {
        var del = function () {
            $scope.closeThisDialog();
            adminApi.delGroupFromBloc({
                blocID : blocID,
                groupID :groupID
            },function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showGeneralSuccessAlert();
                $rootScope.$broadcast(appHelper.EVENTS.LOAD_GROUPS);
            });
        };
        var modal = {
            text :  adminHelper.delMessage.delGroupFromBloc.text,
            comment : adminHelper.delMessage.delGroupFromBloc.comment,
            del : del
        };

        var blocID = $scope.ngDialogData.blocID,
            groupID = $scope.ngDialogData.groupID;


        if(!blocID || !groupID) return appHelper.closeDialogAndShowGenError($scope);

        $scope.modal = modal;
    });
angular
    .module("EduNet")
    .controller("blocGroupsController", function (appValues,appDictionary,adminHelper,appHelper,adminConstants,ngDialog,$rootScope,$scope,$location,adminApi) {
        var loadBlocByID = function (blocID) {
            adminApi.getBlocByID({
                blocID : blocID
            },function (err,bloc) {
                if (err) return appHelper.showFailAlertGeneralError();
                self.bloc = bloc;
                if (_.isEmpty(bloc.groups)) return self.noGroups = true;
                self.noGroups = false;
                self.groups = self.bloc.groups;
                _.each(self.groups,function (group) {
                    if (!group.teacherID) group.teacherID = {
                        name : appDictionary.VACATION
                    }
                });
            });
        };
        var changeViewStyle = function (viewStyle) {
            self.viewStyle = viewStyle;
        };
        var back = function () {
            $location.path('list-of-groups');
        };

        var addGroup = function () {
            $location.path('/add-group');
        };
        var editGroupByID = function (groupID) {
            appHelper.setActGroupID(groupID);
            $location.path('/edit-group');
        };
        var deleteGroupByID = function (groupID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deleteGroupController",
                data : {
                    groupID : groupID
                }
            });
        };
        var addGroupBlock = function () {
            ngDialog.open({
                template : "/app-admin/views/group/alerts/add-group-block.html",
                className : 'ngdialog-theme-default',
                width : 720
            });
        };
        var showProperties = function (index) {
            self.properties[index] = true;
        };
        var hideProperties = function (index) {
            self.properties[index] = false;
        };
        var goToGroupInfo = function (groupID) {
            appHelper.setActGroupID(groupID);
            $location.path('/group-info');
        };
        var initController = function () {
            self.blocID = appHelper.getActBlocID();
            if (!self.blocID) return adminHelper.goToMain();
            loadBlocByID(self.blocID);
        };
        var delGroupFromBloc = function (groupID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "delGroupFromBlocController",
                data : {
                    groupID : groupID,
                    blocID : self.blocID
                }
            });
        };
        this.changeViewStyle = changeViewStyle;
        this.addGroup = addGroup;
        this.editGroupByID = editGroupByID;
        this.deleteGroupByID = deleteGroupByID;
        this.showProperties = showProperties;
        this.hideProperties = hideProperties;
        this.goToGroupInfo = goToGroupInfo;
        this.addGroupBlock = addGroupBlock;
        this.back = back;
        this.delGroupFromBloc = delGroupFromBloc;

        // INIT
        var self = this;
        this.properties = [];
        this.viewStyle = adminConstants.VIEW_STYLES.VIEW_STYLE_BLOCKS;
        this.viewStyles = adminConstants.VIEW_STYLES;
        this.isBlocsCtrl = true;


        $rootScope.$on(appHelper.EVENTS.LOAD_GROUPS, initController);
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }




    });
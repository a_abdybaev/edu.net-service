"use strict";

angular
    .module("EduNet")
    .controller("addGroupController", ['$scope','$rootScope','app',function (scope,rootScope,app) {
        var setColor = function (index) {
            self.activeColorIndex = index;
        };
        var initController = function () {
            loadTeachers();
        };
        var loadTeachers = function () {
            app.api.getTeachers(function (err,teachers) {
                if (err) return appHelper.showError(err);
                app.main.helper.createNameForUsers(teachers);



                self.teachers = teachers;

                self.isLoaded = true;
                self.hasTeachers = !_.isEmpty(teachers);

                self.availableTeachers = [[vacation].concat(teachers)];
                self.selectedTeachers = [vacation];
            });
        };
        var addGroup = function () {
            if (!app.validator.isValidAddGroup(self)) return;
            var name = self.name,
                price = self.price;

            var color = self.colors[self.activeColorIndex].value;
            var obj = {
                name : name,
                color : color,
                individual : self.individual
            };
            if (!_.isEmpty(self.selectedTeachers)) {
                obj.teachers = _.compact(_.uniq(_.map(self.selectedTeachers,function (teacher) {
                    if (teacher._id != "-") {
                        return teacher._id
                    }
                })));
            }

            if (price != null) {
                obj.price = price;
            }
            app.api.addGroup(obj, function (err) {
                if (err) return app.main.helper.showFailAlertGeneralError();
                app.main.helper.showSuccessAlert(app.values.GROUP_CREATED);
                app.helper.goToMain();
            });
        };
        var cancel = function () {
            app.helper.goToMain();
        };
        var addTeacher = function () {

            if (!self.isLoaded || !self.hasTeachers) return;
            self.availableTeachers.push(self.teachers);
            self.selectedTeachers.push(self.teachers[0]);

        };
        var removeTeacher = function (index) {
            if (index == 0) return;
            self.availableTeachers.splice(index,1);
            self.selectedTeachers.splice(index,1);
        };

        this.setColor = setColor;
        this.addGroup = addGroup;
        this.cancel = cancel;
        this.addTeacher = addTeacher;
        this.removeTeacher = removeTeacher;
    //     INIT
        var self = this;

        var vacation = {
            name : app.main.dictionary.SELECT.TEACHER,
            _id : "-"
        };
        this.teachers = [];
        this.activeTeacher = vacation;
        this.colors = app.constants.COLORS;
        this.activeColorIndex = 0;
        this.individual = 0;

        this.avilableTeachers = [];
        this.selectedTeachers = [];


        this.isLoaded = false;

        rootScope.$on(app.main.helper.EVENTS.INDEX_LOADED,initController);
        if (scope.$parent.index.isLoad) {
            initController();
        }




    }]);
angular
    .module("EduNet")
    .controller("listOfGroupsController", function (appValues,appDictionary,adminHelper,appHelper,adminConstants,ngDialog,$rootScope,$scope,$location,adminApi) {

        var changeViewStyle = function (viewStyle) {
            self.viewStyle = viewStyle;
        };
        var loadGroups = function () {
            adminApi.getGroups({
                isBloc : false
            },function (err,groups) {
                if (err) return appHelper.showError(err);
                self.hasGroups = !_.isEmpty(groups);
                self.groups = groups;
                loadBlocs();
            });
        };

        var addGroup = function () {
            $location.path('/add-group');
        };
        var editGroupByID = function (groupID) {
            appHelper.setActGroupID(groupID);
            $location.path('/edit-group');
        };
        var deleteGroupByID = function (groupID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                className : 'ngdialog-theme-default',
                controller : "deleteGroupController",
                data : {
                    groupID : groupID
                }
            });
        };
        var addGroupBlock = function () {
            ngDialog.open({
                template : "group/alerts/add-bloc.html",
                className : 'ngdialog-theme-default',
                width : 720
            });
        };
        var showProperties = function (index) {
            self.properties[index] = true;
        };
        var hideProperties = function (index) {
            self.properties[index] = false;
        };



        var goToGroupInfo = function (groupID) {
            appHelper.setActGroupID(groupID);
            $location.path('/group-info');
        };
        var initController = function () {
            appHelper.removeActBlocID();
            self.searchPattern = '';
            self.properties = [];
            self.blocProp = [];
            loadGroups();

        };

        var GoToBlocByID = function (blocID) {
            appHelper.setActBlocID(blocID);
            $location.path('/bloc-groups');
        };
        var loadBlocs = function () {
            adminApi.getBlocs(function (err,blocs) {
                if (err) return appHelper.showFailAlertGeneralError();
                self.hasGroupsInBlocs = false;
                self.hasBlocs = true;
                if (_.isEmpty(blocs)) return self.hasBlocs = false;
                self.blocs = blocs;
                _.each(blocs,function (bloc) {

                    if (!_.isEmpty(bloc.groups)) {
                        self.hasGroupsInBlocs = true;
                        self.groups= self.groups.concat(bloc.groups);
                    }
                });
                self.isLoaded = true;

            });
        };
        var assignGroup = function (groupID) {
            ngDialog.open({
                template : "/app-admin/views/group/alerts/assign-group.html",
                className : 'ngdialog-theme-default',
                width : 600,
                data : {
                    groupID : groupID
                }
            });
        };
        var editBloc = function (blocID) {
            ngDialog.open({
                template : "group/alerts/edit-bloc.html",
                className : 'ngdialog-theme-default',
                width : 720,
                data : {
                    blocID : blocID
                }
            });
        };
        var delBloc = function (blocID) {
            ngDialog.open({
                template : appValues.CONFIRM_DELETE_ALERT_PATH,
                controller : "delBlocController",
                width : 320,
                data : {
                    blocID : blocID
                }
            });
        };
        var blocProperties = function (value,index) {
            self.blocProp[index] = value;
        };



        this.assignGroup = assignGroup;
        this.changeViewStyle = changeViewStyle;
        this.addGroup = addGroup;
        this.editGroupByID = editGroupByID;
        this.deleteGroupByID = deleteGroupByID;
        this.showProperties = showProperties;
        this.hideProperties = hideProperties;
        this.goToGroupInfo = goToGroupInfo;
        // BLOCS
        this.addGroupBlock = addGroupBlock;
        this.GoToBlocByID = GoToBlocByID;
        this.editBloc = editBloc;
        this.blocProperties = blocProperties;
        this.delBloc = delBloc;
        // INIT
        var self = this;
        this.isLoaded = false;
        this.viewStyle = adminConstants.VIEW_STYLES.VIEW_STYLE_BLOCKS;
        this.viewStyles = adminConstants.VIEW_STYLES;
        this.isBlocsCtrl = false;


        $rootScope.$on(appValues.EVENTS.LOAD_GROUPS, initController);
        $rootScope.$on(appValues.EVENTS.INDEX_LOADED,initController);
        if ($scope.$parent.index.isLoad) {
            initController();
        }


    });
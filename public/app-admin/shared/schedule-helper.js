app.factory('adminScheduleHelper', function ($http,localStorageService,$window) {
    return {
        getHoursFrom : function (hour) {
            var hours = [];
            for (var i = hour; i <= 22; i++) {
                hours.push({
                    label : i == 9 ? '09' : i,
                    value : i
                });
            }
            return hours;
        },
        getMinutesFrom : function (minute) {
            var minutes = [];
            for (var i = minute; i<=55;i+=5) {
                minutes.push({
                    label: i < 10 ? ('0' + i) : i,
                    value : i
                });
            }
            return minutes;
        },
        getDay : function () {
            return {
                start : {
                    minutes : this.getMinutesFrom(0),
                    hours : this.getHoursFrom(9),
                    selectedHour : {
                        value : 9,
                        label : "09"
                    },
                    selectedMinute : {
                        value : 0,
                        label : "0"
                    }
                },
                end : {
                    minutes : this.getMinutesFrom(0),
                    hours : this.getHoursFrom(9),
                    selectedHour : {
                        value : 9,
                        label : "09"
                    },
                    selectedMinute : {
                        value : 0,
                        label : "0"
                    }
                },
                isActive : false,
                classroom : {
                    name : "Выбрать",
                    _id : "-"
                }
            };
        },
        getDays : function () {
            return [Object.assign({},this.getDay()),Object.assign({},this.getDay()),Object.assign({},this.getDay()),Object.assign({},this.getDay()),Object.assign({},this.getDay()),Object.assign({},this.getDay())]
        },
        isValidClassrooms : function ($scope) {
            var days = $scope.days;
            var result = true;
            days.forEach(function (day,i) {
                if (day.isActive == true) {
                    if (day.classroom._id == '-') {
                        result = false;
                        days[i].classroomError = true;
                    }
                }
                //if (day.classroom._id == '-' && day.isActive == true) {
                //    result = false;
                //}
            });
            return result;
        },
        isValidLessonTimes : function ($scope) {
            var days = $scope.days;
            var result = true;
            days.forEach(function (day,i) {
                if (day.isActive == true && day.start.selectedMinute.value + day.start.selectedHour.value * 60 >= day.end.selectedHour.value * 60 + day.end.selectedMinute.value) {
                    days[i].timeError = true;
                    result = false;
                }
            });
            return result;
        },
        isNotEmpty : function ($scope) {
            var result = false;
            $scope.lessonsNotSelectedError = true;
            $scope.days.forEach(function (day) {
                if (day.isActive == true) {
                    $scope.lessonsNotSelectedError = false;
                    result = true;
                }
            });
            return result;
        },
        isValidLessons : function($scope) {
            if (this.isNotEmpty($scope) && this.isValidClassrooms($scope) && this.isValidLessonTimes($scope)) {
                return true;
            } else {
                return false;
            }
        },
        processData : function (scope) {
            var lessons = [];
            scope.days.forEach(function (day,i) {
                if (day.isActive) {
                    lessons.push({
                        day : i+1,
                        start : {
                            hour : day.start.selectedHour.value,
                            minute : day.start.selectedMinute.value
                        },
                        end : {
                            hour : day.end.selectedHour.value,
                            minute : day.end.selectedMinute.value
                        },
                        classroomID : day.classroom._id
                    });
                }
            });
            return lessons;
        },
        clearErrors : function ($scope) {
            $scope.days.forEach(function (day,index) {
                $scope.days[index].classroomError = false;
                $scope.days[index].timeError = false;
            });
            $scope.lessonsNotSelectedError = false;
            $scope.padError = false;
        },
        convertTimeToMinutes : function (time) {
            return parseInt(time.hour) * 60 + parseInt(time.minute);
        },
        convertStartTimeToTopPosition : function (top,start) {
            var difference = this.convertTimeToMinutes(start) - this.convertTimeToMinutes({
                    hour : 9,
                    minute : 0
                });
            return top + difference * 2/3;
        },
        getLessonsForSchedule : function (lessons) {
            var self = this;
            var top = 222;
            var height = 40;
            var results = [[],[],[],[],[],[]];
            lessons.forEach(function (lesson) {
                lesson.style = {
                    'top' :  self.convertStartTimeToTopPosition(top,lesson.start) + 'px',
                    'height' : (self.convertTimeToMinutes(lesson.end) -self.convertTimeToMinutes(lesson.start)) * 2/3 + 'px',
                    'background-color' : lesson.groupID.color
                };
                results[lesson.day-1].push(lesson);
            });
            return results;
        },
        createScheduleForGroupInfo : function (group) {
            var self = this;
            var top = 363;
            var height = 40;
            var results = [[],[],[],[],[],[]];
            group.schedule.forEach(function (lesson) {
                lesson.style = {
                    'top' :  self.convertStartTimeToTopPosition(top,lesson.start) + 'px',
                    'height' : (self.convertTimeToMinutes(lesson.end) -self.convertTimeToMinutes(lesson.start)) * 2/3 + 'px',
                    'background-color' : group.color
                };
                results[lesson.day-1].push(lesson);
                var startTime = new Date();
                startTime.setHours(lesson.start.hour);
                startTime.setMinutes(lesson.start.minute);
                lesson.startTime = startTime;

                var endTime = new Date();
                endTime.setHours(lesson.end.hour);
                endTime.setMinutes(lesson.end.minute);
                lesson.endTime = endTime;
            });
            return results;
        },
        getDayNames : function () {
            return ['ПОНЕДЕЛЬНИК','ВТОРНИК','СРЕДА','ЧЕТВЕРГ','ПЯТНИЦА','СУББОТА'];
        },
        getShortDayNames : function () {
            return ['Пн','Вт','Ср','Чт','Пт','Сб'];
        },
        getDaysOfExistedSchedule : function (lessons) {
            var days = this.getDays();
            var self = this;
            if (lessons != null && lessons.length > 0) {
                lessons.forEach(function (lesson,i) {
                    var index = lesson.day-1;
                    days[index].isActive = true;
                    var startObj = self.getTimeForSelectTag(lesson.start);
                    var endObj = self.getTimeForSelectTag(lesson.end);
                    days[index].start.selectedMinute = startObj.selectedMinute;
                    days[index].start.selectedHour = startObj.selectedHour;
                    days[index].end.selectedMinute = endObj.selectedMinute;
                    days[index].end.selectedHour = endObj.selectedHour;
                    days[index].classroom = lesson.classroomID;
                });
            }
            return days;
        },
        getTimeForSelectTag : function (obj) {
            var hour = {};
            var minute = {};
            if (obj.hour <=9) {
                hour.value = 9;
                hour.label = "09";
            } else {
                hour.value = obj.hour;
                hour.label = obj.hour;
            }
            if (obj.minute <= 9) {
                minute.value = obj.minute;
                minute.label = "0" + obj.minute;
            } else {
                minute.value = obj.minute;
                minute.label = obj.minute;
            }
            return {
                selectedMinute : minute,
                selectedHour : hour
            };
        }

    }
});
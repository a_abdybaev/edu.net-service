app.factory('adminValues', function () {
    return {

        PREV_PAGE : {
            TEACHER_INFO : "TEACHER_INFO",
            LIST_OF_TEACHERS : "LIST_OF_TEACHERS"
        },



        //    SUCCESS VALUES
        GROUP_CREATED : "GROUP_CREATED",
        GROUP_UPDATED : "GROUP_UPDATED",
        GROUP_DELETED : "GROUP_DELETED",

        BLOC_CREATED : "BLOC_CREATED",
        BLOC_UPDATED : "BLOC_UPDATED",
        BLOC_DELETED : "BLOC_DELETED",
        SCHEDULE_CREATED : "SCHEDULE_CREATED",
        SCHEDULE_UPDATED : "SCHEDULE_UPDATED",
        SCHEDULE_DELETED : "SCHEDULE_DELETED",
        APPLICATION_ANSWER_CREATED : "APPLICATION_ANSWER_CREATED",
        APPLICATION_ANSWER_UPDATED : "APPLICATION_ANSWER_UPDATED",
        APPLICATION_ANSWER_DELETED : "APPLICATION_ANSWER_DELETED",
        TEACHER_CREATED : "TEACHER_CREATED",
        TEACHER_UPDATED : "TEACHER_UPDATED",
        TEACHER_DELETED : "TEACHER_DELETED",
        STUDENT_CREATED : "STUDENT_CREATED",
        STUDENT_UPDATED : "STUDENT_UPDATED",
        STUDENT_DELETED : "STUDENT_DELETED",
        NOTE_CREATED : "NOTE_CREATED",
        NOTE_UPDATED : "NOTE_UPDATED",
        NOTE_DELETED : "NOTE_DELETED",
        CLASSROOM_CREATED : "CLASSROOM_CREATED",
        CLASSROOM_UPDATED : "CLASSROOM_UPDATED",
        CLASSROOM_DELETED : "CLASSROOM_DELETED",
        PAYMENT_CREATED : "PAYMENT_CREATED",
        PAYMENT_UPDATED : "PAYMENT_UPDATED",
        PAYMENT_DELETED : "PAYMENT_DELETED",

        STUDENT_ARCHIVED : "STUDENT_ARCHIVED",
        STUDENT_ADDED_IN_GROUP : "STUDENT_ADDED_IN_GROUP",
        SUBJECT_CREATED : "SUBJECT_CREATED",
        SUBJECT_UPDATED : "SUBJECT_UPDATED",
        SUBJECT_DELETED : "SUBJECT_DELETED",
        STUDENT_RESTORED : "STUDENT_RESTORED",

    //     ERRORS VALUES
        NO_AVAILABLE_GROUPS_FOR_SCHEDULING : "NO_AVAILABLE_GROUPS_FOR_SCHEDULING",
        GENERAL_ERROR : "GENERAL_ERROR",
        PASSWORDS_NOT_MATCHES : "PASSWORDS_NOT_MATCHES",
        DUPLICATE_LOGIN_ERROR : "DUPLICATE_LOGIN_ERROR",
        NO_AVAILABLE_GROUPS_TO_ASSIGN_ACTIVE_STUDENT : "NO_AVAILABLE_GROUPS_TO_ASSIGN_ACTIVE_STUDENT",




        COLORS : [
            {
                value: "#51baf2",
                passive: "/static-files/images/lightblue-inactive.png",
                active: "/static-files/images/lightblue-active.png"
            },
            {
                value: "#fd6461",
                passive: "/static-files/images/red-inactive.png",
                active: "/static-files/images/red-active.png"
            },
            {
                value: "#f7a650",
                passive: "/static-files/images/orange-inactive.png",
                active: "/static-files/images/orange-active.png"
            },
            {
                value: "#71ca58",
                passive: "/static-files/images/green-inactive.png",
                active: "/static-files/images/green-active.png"
            },
            {
                value: "#d08ce0",
                passive: "/static-files/images/purple-inactive.png",
                active: "/static-files/images/purple-active.png"
            }]
    }
});
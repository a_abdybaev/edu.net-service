app
    .filter("listOfTeachersSearch",function () {
        return function (teachers,name,isEmptyResults) {
            var result = [];
            isEmptyResults = true;
            if (name == '' || _.isEmpty(name)) return teachers;
            name = name.toLowerCase();
            if (teachers.length == 0) return teachers;
            _.each(teachers,function (teacher) {
                var indexFirstName = teacher.firstName.toLowerCase().search(name),
                    indexLastName = teacher.lastName.toLowerCase().search(name),
                    indexName = (teacher.lastName + ' ' + teacher.firstName).toLowerCase().search(name);
                if (indexFirstName > -1 || indexLastName > -1 || indexName > -1) result.push(teacher);

            });
            return result;
        }
    })
    .filter("studentsSorting",function ($filter) {
        return function (students,isDate,isName) {
            if ((isDate || !isDate) && !isDate) return $filter('orderBy')(students,'-updatedAt');
            return _.sortBy(students,'lastName');

        }

    })
    .filter("blocMemoryFilter",function ($filter) {
        return function (bloc) {
            var value = 0;
            if (_.isEmpty(bloc.groups)) _.each(bloc.groups,function (group) {
                value+=group.memory;
            });
            return $filter('memoryUsageFilter')(value);
        }
    })





;




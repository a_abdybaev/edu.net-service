angular
    .module('EduNet')
    .factory('app',['scheduleBuilder','ngDialog','$location','main','adminApi','adminConstants','adminHelper','adminModalHelper','adminScheduleHelper','adminValidator','adminValues',
        function (scheduleBuilder,ngDialog,location,main,adminApi,adminConstants,adminHelper,adminModalHelper,adminScheduleHelper,adminValidator,adminValues) {
        var self = this;
        return {
            api : adminApi,
            location : location,
            constants : adminConstants,
            helper : adminHelper,
            modalHelper : adminModalHelper,
            scheduleHelper : adminScheduleHelper,
            validator : adminValidator,
            values : adminValues,
            main : main,
            ngDialog : ngDialog,
            scheduleBuilder : scheduleBuilder
        }}]);
app
    .factory('adminModalHelper', function (adminValues) {
        return {
            deleteMessages : {
                bloc : {
                   text : "Вы уверены что хотите удалить категорию? ",
                   comment : "Категория будет удалена безвозратно"
                },
                delGroupFromBloc : {
                   text : "Вы уверены что хотите удалить группу из текущей категории?",
                   comment : ""
                },
                subject : {
                   text : "Вы уверены, что хотите удалить предмет?",
                   comment : "Предмет будет удален безвозвратно"
                },
                applicationAnswers : {
                   text : "Вы точно хотите удалить ответ заявки?",
                   comment : "Данный ответ будет потерян навсегда"
                },
                student : {
                   text : "Вы точно хотите удалить студента?",
                   comment : "Данные студента будут потеряны навсегда"
                },
                teacher : {
                   text : "Вы точно хотите удалить учителя?",
                   comment : "Данные учителя будут потеряны навсегда"
                },
                note : {
                   text : "Вы точно хотите удлать текущую заметку?",
                   comment : "Заметка будет удалено безвозвратно"
                },
                group : {
                   text : "Вы точно хотите удалить данную группу?",
                   comment : "Данные о группе будут потеряны навсегда"
                },
                classRoom : {
                   text : "Вы точно хотите удалить аудиторию?",
                   comment : "Расписание данной аудитории будет удалено безвозвратно."
                },
                payment : {
                   text : "Вы точно хотите удалить оплату?",
                   comment : "Информация о данной оплате будет потеряна на всегда"
                },
                archiveStudent : {
                   text : "Вы точно хотите удалить студента?",
                   comment : "Студент будет удален из всех групп и отправлен в Архив студентов"
                },
                schedule : {
                    text : "Вы точно хотите удалить расписание группы?",
                    comment : "Расписание будет потеряно безвозвратно"
                },
                studentFromGroup : {
                   text : "Вы точно хотите удалить студента из группы?",
                   comment : "Студент будет удален из данной группы"
                }
           },
            successMessages : [{
                value : adminValues.GROUP_CREATED,
                message : "Группа успешно создана"
            },{
                value : adminValues.GROUP_UPDATED,
                message : "Группа успешно сохранена"
            },{
                value : adminValues.GROUP_DELETED,
                message : "Группа успешно удалена"
            },{
                value : adminValues.SCHEDULE_CREATED,
                message :"Расписание успешно добавлено"
            },{
                value : adminValues.SCHEDULE_DELETED,
                message :"Расписание группу успешно удалено"
            },{
                value : adminValues.SCHEDULE_UPDATED,
                message :"Расписание группы успешно сохранено"
            },{
                value : adminValues.APPLICATION_ANSWER_CREATED,
                message : "Ответ на заявку успешно добавлен"
            },{
                value : adminValues.APPLICATION_ANSWER_DELETED,
                message :"Ответ на заявку успешно удален"
            },{
                value : adminValues.APPLICATION_ANSWER_UPDATED,
                message :"Ответ на заявку успешно изменен"
            },{
                value : adminValues.TEACHER_CREATED,
                message :"Учитель успешно добавлен"
            },{
                value : adminValues.TEACHER_UPDATED,
                message :"Учитель успешно сохранен"
            },{
                value : adminValues.TEACHER_DELETED,
                message :"Учитель успешно удален"
            },{
                value : adminValues.STUDENT_UPDATED,
                message :"Студент успешно сохранен"
            },{
                value : adminValues.STUDENT_CREATED,
                message :"Студент успешно добавлен"
            },{
                value : adminValues.STUDENT_DELETED,
                message :"Студент успешно удален"
            },{
                value : adminValues.STUDENT_RESTORED,
                message : "Студент успешно восстановлен"
            },{
                value : adminValues.NOTE_DELETED,
                message :"Заметка успешно удалена"
            },{
                value : adminValues.NOTE_UPDATED,
                message :"Заметка успешно сохранена"
            },{
                value : adminValues.NOTE_CREATED,
                message :"Заметка успешно добавлена"
            },{
                value : adminValues.CLASSROOM_CREATED,
                message :"Аудитория успешно добавлена"
            },{
                value : adminValues.CLASSROOM_DELETED,
                message :"Аудитория успешно удалена"
            },{
                value : adminValues.CLASSROOM_UPDATED,
                message :"Аудитория успешно обновлена"
            },{
                value : adminValues.PAYMENT_CREATED,
                message :"Оплата успешно добавлена"
            },{
                value : adminValues.PAYMENT_DELETED,
                message :"Оплата успешно удалена"
            },{
                value : adminValues.PAYMENT_UPDATED,
                message :"Оплата успешно сохранена"
            },{
                value : adminValues.PROFILE_UPDATED,
                message :"Профиль успешно обновлен"
            },{
                value : adminValues.AVATAR_UPDATED,
                message :"Изображение успешно сохранено"
            },{
                value : adminValues.PASSWORD_UPDATED,
                message :"Пароль успешно обновлен"
            },{
                value : adminValues.STUDENT_ARCHIVED,
                message :"Студент успешно перемещен в архив"
            },{
                value : adminValues.STUDENT_ADDED_IN_GROUP,
                message :"Студент успешно добавлен в группу"
            },{
                value : adminValues.SUBJECT_CREATED,
                message :"Предмет успешно добавлен"
            },{
                value : adminValues.SUBJECT_DELETED,
                message : "Предмет успешно удален"
            },{
                value : adminValues.SUBJECT_UPDATED,
                message : "Предмет успешно изменен"
            },{
                value : adminValues.BLOC_CREATED,
                message :"Категория групп успешно добалена"
            },{
                value : adminValues.BLOC_UPDATED,
                message : "Категория групп успешно обновлена"
            },{
                value : adminValues.BLOC_DELETED,
                message : "Категория групп успешно удалена"
            }],


            errorMessages : [{
                value : adminValues.NO_AVAILABLE_GROUPS_FOR_SCHEDULING,
                message :"Нет групп для создания расписания"
            },{
                value : adminValues.TARIFF_GROUP_COUNT_ERROR ,
                message :"Данный тариф не позволяет создать еще одну группу"
            },{
                value : adminValues.PASSWORDS_NOT_MATCHES,
                message :"Вы ввели неверный старый пароль"
            },{
                value : adminValues.DUPLICATE_LOGIN_ERROR,
                message :"Пользователь с таким номер уже существует в системе"
            },{
                value : adminValues.NO_AVAILABLE_GROUPS_TO_ASSIGN_ACTIVE_STUDENT,
                message : "К сожалению нет групп для назначения данного студента"
            }],

        }
    });
app.factory('adminHelper', function (adminModalHelper,appValues,appHelper,$rootScope,ngDialog,$http,localStorageService,$window,$location,adminValues) {
    var self = this;
    return {
        getNamesFromUsers : function(users) {
            users.forEach(function (user) {
                user.name = user.lastName + ' ' + user.firstName;
            });
            return users;
        },
        createNameForUser : function (user) {
            user.name = user.lastName + ' ' + user.firstName;
        },
        getNameForUser : function (user) {
            return user.lastName + ' ' + user.firstName;
        },
        createNameForUsers : function (users) {
            users.forEach(function (user) {
                this.createNameForUser(user);
            });
        },
        showError : function (err) {
            localStorageService.set('errorData',err);
            $location.path('/error500');
        },
        logout : function () {
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.ACT_TEACHER_ID);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.ACT_GROUP_ID);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.IS_DEV);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.USER_TYPE);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.USER_INFO);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.TOKEN);

            $window.location.href = "/";
        },
        goToMain : function () {
            $location.path('list-of-groups');
        },
        setActTeacherID : function (teacherID) {
            return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.ACT_TEACHER_ID,teacherID);
        },
        getActTeacherID : function () {
            return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.ACT_TEACHER_ID);
        },
        setActTestID : function (testID) {
            return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.ACT_TEST_ID,testID)
        },
        getActTestID : function () {
            return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.ACT_TEST_ID);
        },
        removeActTeacherID : function () {
            return localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.ACT_TEACHER_ID);
        },
        removeActTestID : function () {
            return localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.ACT_TEST_ID);
        },
        EVENTS : adminValues.EVENTS,
        LOCAL : adminValues.LOCAL_STORAGE_KEYS,
        delMessage : adminModalHelper.deleteMessages,
        successMessages : adminModalHelper.successMessages,
        errorMessages : adminModalHelper.errorMessages,
        values : adminValues,
    }
});
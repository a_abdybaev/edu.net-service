app.factory('adminValidator', function (appValidator) {
    return {
        isEmpty : function (field) {
            return appValidator.isEmpty(field);
        },
        // groupTab : groupTabValidator,


        isValidAddGroup : function (ctrl) {
            var self = this;
            return appValidator.isValidField(ctrl,'name');
        },
        isValidEditGroup : function (ctrl) {
            return this.isValidAddGroup(ctrl);
        },
        isValidAddTeacher : function (ctrl) {
            return this.isValidStudent(ctrl);
        },
        isValidEditTeacher : function (ctrl) {
            return this.isValidExistedStudent(ctrl);
        },





        isValidStudents : function (students) {
            var self = this;
            if (students.length == 0) return true;
            var result = true;
            students.forEach(function (student) {
                if (!self.isValidStudent(student)) result = false;
            });
            return result;
        },
        isValidStudent : function (student) {
            appValidator.isValidField(student,'firstName');
            appValidator.isValidField(student,'lastName');
            appValidator.isValidField(student,'phone');
            this.isValidEmailField(student,student.email);
            return (appValidator.isValidField(student,'firstName') && appValidator.isValidField(student,'lastName') && appValidator.isValidField(student,'phone') && this.isValidEmailField(student,student.email));
        },
        // EDIG GROUP
        isValidExistedStudent : function (student) {
            appValidator.isValidField(student,'firstName');
            appValidator.isValidField(student,'lastName');
            this.isValidEmailField(student,student.email);
            return appValidator.isValidField(student,'firstName') && appValidator.isValidField(student,'lastName') && this.isValidEmailField(student,student.email);
        },
        isValidExistedStudents : function (students) {
            var self = this;
            if (students.length == 0) return true;
            var result = true;
            students.forEach(function (student) {
                if (!self.isValidExistedStudent(student)) result = false;
            });
            return result;
        },

        // EMAIL
        isValidEmail : function (email) {
            if (appValidator.isEmpty(email)) return true;
            return validator.isEmail(email);
        },
        isValidEmailField : function (ctrl,email) {
            if (this.isValidEmail(email)) {
                ctrl['emailError'] = false;
                return true;
            } else {
                ctrl['emailError'] = true;
                return false;
            }
        },
        isValidEditPassword : function (ctrl) {
            ctrl['oldPasswordError'] = false;
            ctrl['newPasswordError'] = false;
            ctrl['newPasswordConfirmError'] = false;
            ctrl['newPasswordConfirmInvalidError'] = false;
            ctrl['newPasswordInvalidError'] = false;

            if (!appValidator.isValidField(ctrl,'oldPassword') || !appValidator.isValidField(ctrl,'newPassword') || !appValidator.isValidField(ctrl,'newPasswordConfirm')) return false;
            if (ctrl['newPassword'] != ctrl['newPasswordConfirm']) {
                ctrl['newPasswordConfirmInvalidError'] = true;
                return false;
            }
            ctrl['newPasswordConfirmInvalidError'] = true;
            if (ctrl['newPassword'].length < 6) {
                ctrl['newPasswordInvalidError'] = true;
                return false;
            }
            ctrl['newPasswordInvalidError'] = true;
            return true;
        },
        isValidEditProfile : function (ctrl) {
            appValidator.isValidField(ctrl,'name');
            this.isValidEmailField(ctrl,ctrl.email);
            return appValidator.isValidField(ctrl,'name') && this.isValidEmailField(ctrl,ctrl.email);
        },
        isValidAddSubjectForm : function (ctrl) {
            var result = true;
            ctrl.levelErrors = [];
            _.each(ctrl.levels,(function (level,index) {
                if (appValidator.isEmpty(level)) {
                    ctrl.levelErrors[index] = true;
                    result = false;
                } else {
                    ctrl.levelErrors[index] = false;
                }
            }));

            if (appValidator.isEmpty(ctrl.name)) {
                result = false;
                ctrl.nameError = true;
            } else {
                ctrl.nameError = false;
            }
            return result;
        }

    }
});
angular.module("EduNet")
    .config(function($routeProvider,$locationProvider) {
        $routeProvider
            .when("/list-of-groups", {
                templateUrl: "group/index.html"
            })
            .when("/add-group", {
                templateUrl: "group/add-group.html"
            })
            .when("/edit-group", {
                templateUrl: "group/edit-group.html"
            })
            .when("/group-info", {
                templateUrl: "group-info/index.html"
            })
            .when("/schedule", {
                templateUrl: "schedule/index.html"
            })
            .when("/add-schedule", {
                templateUrl: "schedule/add-schedule.html"
            })

            .when("/edit-schedule", {
                templateUrl: "schedule/edit-schedule.html"
            })

            .when("/list-of-classrooms", {
                templateUrl: "classroom/index.html"
            })
            .when("/add-teacher", {
                templateUrl: "teacher/add-teacher.html",
                controller: "addTeacherController"
            })
            .when("/list-of-teachers", {
                templateUrl: "teacher/index.html"
            })
            .when("/edit-teacher", {
                templateUrl: "teacher/edit-teacher.html",
                controller: "editTeacherController"
            })
            .when("/teacher-info", {
                templateUrl: "teacher/teacher-info.html",
                controller: "teacherInfoController"
            })
            .when("/error404", {
                templateUrl: "errors/404.html",
                controller: "errorsController"
            })
            .when("/error500", {
                templateUrl: "errors/500.html",
                controller: "errorsController"
            })
            .when("/list-of-applications", {
                templateUrl: "applications/index.html"
            })
            .when("/payments", {
                templateUrl: "payments/index.html"
            })
            .when("/tariffs", {
                templateUrl: "tariffs/index.html"
            })
            .when("/notifications", {
                templateUrl: "notifications/index.html"
            })
            .when("/profile", {
                templateUrl: "profile/index.html"
            })
            .when("/students", {
                templateUrl: "students/index.html"
            })
            .when("/add-student", {
                templateUrl: "students/add-student.html"
            })
            .when("/tests", {
                templateUrl: "tests/index.html"
            })
            .when("/add-test", {
                templateUrl: "tests/add-test/index.html"
            })
            .when("/test-info", {
                templateUrl: "tests/test-info.html"
            })
            .when("/search", {
                templateUrl: "students-search/index.html"
            })
            .when("/analytics", {
                templateUrl: "analytics/index.html"
            })
            .when("/bloc-groups", {
                templateUrl: "group/bloc-info.html"
            })
            .when("/subjects", {
                templateUrl: "students/waiting/subjects.html"
            })
            .when('/example',{
                templateUrl : 'templateID.html'
            })
            .otherwise({
                redirectTo: '/list-of-groups'
            })

    });

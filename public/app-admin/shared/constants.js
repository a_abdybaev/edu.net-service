app.factory('adminConstants', function (appConstants) {

    var VIEW_STYLE_BLOCKS = "BLOCKS",
        VIEW_STYLE_LIST = "LIST",
        ALL_GROUPS = "Все группы",
        SCHEDULE_DISPLAY_OPTION_TEACHERS = "TEACHERS",
        SCHEDULE_DISPLAY_OPTION_CLASSROOMS = "CLASSROOMS",
        APPLICATIONS_DISPLAY_OPTION_TEACHERS = SCHEDULE_DISPLAY_OPTION_TEACHERS,
        APPLICATIONS_DISPLAY_OPTION_GROUPS = "GROUPS",

        // STUDY_TIME
        STUDENT_STUDY_TIME_MORNING = "MORNING_TIME",
        STUDENT_STUDY_TIME_LUNCH = "LUNCH_TIME",
        STUDENT_STUDY_TIME_EVENING = "EVENING_TIME"
        ;
    var TEACHER = "TEACHER",
        CLASSROOM = "CLASSROOM";



    var ARCHIVE_STUDENTS = "ARCHIVE_STUDENTS",
        ACTIVE_STUDENT = "ACTIVE_STUDENTS",
        WAITING_STUDENTS = "WAITING_STUDENTS",
        studentStatuses = [ARCHIVE_STUDENTS,ACTIVE_STUDENT,WAITING_STUDENTS];


    return {
        VIEW_STYLES : {
            VIEW_STYLE_BLOCKS : VIEW_STYLE_BLOCKS,
            VIEW_STYLE_LIST : VIEW_STYLE_LIST
        },
        SCHEDULE_DISPLAY_OPTION : {
            SCHEDULE_DISPLAY_OPTION_TEACHERS :SCHEDULE_DISPLAY_OPTION_TEACHERS,
            SCHEDULE_DISPLAY_OPTION_CLASSROOMS : SCHEDULE_DISPLAY_OPTION_CLASSROOMS
        },
        APPLICATIONS_DISPLAY_OPTION : {
            APPLICATIONS_DISPLAY_OPTION_TEACHERS : APPLICATIONS_DISPLAY_OPTION_TEACHERS,
            APPLICATIONS_DISPLAY_OPTION_GROUPS : APPLICATIONS_DISPLAY_OPTION_GROUPS
        },
        COLORS : [{
                value: "#51baf2",
                nonActiveUrl: "/static-files/images/lightblue-inactive.png",
                activeUrl: "/static-files/images/lightblue-active.png"
            },
            {
                value: "#fd6461",
                nonActiveUrl: "/static-files/images/red-inactive.png",
                activeUrl: "/static-files/images/red-active.png"
            },
            {
                value: "#f7a650",
                nonActiveUrl: "/static-files/images/orange-inactive.png",
                activeUrl: "/static-files/images/orange-active.png"
            },
            {
                value: "#71ca58",
                nonActiveUrl: "/static-files/images/green-inactive.png",
                activeUrl: "/static-files/images/green-active.png"
            },
            {
                value: "#d08ce0",
                nonActiveUrl: "/static-files/images/purple-inactive.png",
                activeUrl: "/static-files/images/purple-active.png"
            }],
        ALL_GROUPS_SELECT : {
            _id : '-',
            name : ALL_GROUPS
        },
        CLASSROOM : CLASSROOM,
        TEACHER : TEACHER,
        ALL_GROUPS : ALL_GROUPS,
        // STUDENTS
        ARCHIVE_STUDENTS : ARCHIVE_STUDENTS,
        ACTIVE_STUDENT : ACTIVE_STUDENT,
        WAITING_STUDENTS : WAITING_STUDENTS,
        studentStatuses : studentStatuses,
        STUDENT_STUDY_TIME_MORNING : STUDENT_STUDY_TIME_MORNING,
        STUDENT_STUDY_TIME_LUNCH : STUDENT_STUDY_TIME_LUNCH,
        STUDENT_STUDY_TIME_EVENING : STUDENT_STUDY_TIME_EVENING,
        STUDENTS_PER_PAGE : 20


    }
});
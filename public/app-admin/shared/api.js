var ADMIN_URL = "/api/admins",
    STUDENTS_URL = ADMIN_URL + '/students',
    ACTIVE_STUDENTS_URL = STUDENTS_URL + '/active',
    ARCHIVE_STUDENTS_URL = STUDENTS_URL + '/archive',
    WAITING_STUDENTS_URL = STUDENTS_URL + '/waiting',
    GROUPS_URL = ADMIN_URL + '/groups',
    SCHEDULE_URL = ADMIN_URL + '/schedule',
    PAYMENTS_URL = ADMIN_URL + '/payments',
    TESTS_URL = ADMIN_URL + '/tests',
    APPLICATION_URL = ADMIN_URL + '/applications',
    NOTES_URL = ADMIN_URL + '/notes',
    INTERVIEW_URL = ADMIN_URL + '/interview',
    SELF_URL = ADMIN_URL + '/self',
    CLASSROOM_URL = ADMIN_URL + '/classrooms',
    TEACHERS_URL = ADMIN_URL + '/teachers',
    LIST_URL = ADMIN_URL + '/list',
    LIST_TEACHERS_URL = LIST_URL + '/teachers',
    SUBJECTS_URL = ADMIN_URL + '/subjects',
    LIST_GROUPS_URL = LIST_URL + '/groups',
    SCHEDULE_QUERY_URL = SCHEDULE_URL + '/query',
    BLOC_GROUPS = ADMIN_URL + '/blocs';



var createResource= function (response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 201) return cb(null);
    return cb(response);
};
var updateResource = function (response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null);
    return cb(response);
};
var deleteResource = function(response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null);
    return cb(response);
};
var getResource = function(response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null,response.data);
    return cb(response);
};
var errorCallback = function(response,cb) {
    var appHelper = angular.element(document.body).injector().get('appHelper'),
        appValues = angular.element(document.body).injector().get('appValues');
    appHelper.removeLoader();
    if (response.status == 403) {
        angular.element(document.querySelector('[ng-controller]')).injector().get('localStorageService').remove(appValues.LOCAL_STORAGE_KEYS.TOKEN);
        angular.element(document.querySelector('[ng-controller]')).injector().get('$window').location.href = "/";
        return;
    }
    return cb(response);
};
var successCallback = function (response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null);
    return cb(response);
};


app.factory('adminApi', function (appValues,appHelper,$http,localStorageService,$window,Upload) {
    return {
        setToken : function (token) {
            return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.TOKEN,token);
        },
        getToken : function () {
            return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.TOKEN)
        },
        checkAuthorization : function () {
            var token = this.getToken();
            if (!token) {
                $window.location.href = "/";
            }
        },
        removeToken : function () {
            return localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.TOKEN);
        },
        getAdminInfo : function (cb) {
            appHelper.addLoader();
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .get(SELF_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });


        },
        addGroup : function (data,cb) {
            appHelper.addLoader();
            var self = this;
            this.checkAuthorization();
            var token = this.getToken();
            return $http
                .post(GROUPS_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                   return createResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getGroups : function(data,cb) {
            appHelper.addLoader();

            this.checkAuthorization();
            var token = this.getToken();
            return $http
                .get(GROUPS_URL,{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getGroupByID : function (groupID,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(GROUPS_URL + '/' + groupID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        deleteStudentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .delete(ARCHIVE_STUDENTS_URL + '/' + data.studentID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });

        },
        deleteGroupByID : function (groupID,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .delete(GROUPS_URL + '/' + groupID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateGroupByID : function(data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .put(GROUPS_URL + '/' + data.groupID,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        addLesson : function (obj,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .post(SCHEDULE_URL,obj,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getEmptyScheduleGroups : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(SCHEDULE_URL + '/available-groups',{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getSchedule : function (query,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(SCHEDULE_URL,{
                    headers : {
                        token : token
                    },
                    params : query
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });

        },
        getScheduleByClassroomID : function (classroomID,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(CLASSROOM_URL + '/' + classroomID + '/schedule',{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateScheduleByGroupID : function (obj,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .put(GROUPS_URL + '/' + obj.groupID + '/schedule',obj,{
                    headers : {
                        token :token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        deleteScheduleByGroupID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .delete(GROUPS_URL + '/' + data.groupID + '/schedule',{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getClassrooms : function (cb) {
            var self = this;
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(CLASSROOM_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });

        },
        deleteClassroomByID : function (classroomID,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .delete(CLASSROOM_URL + '/' + classroomID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        addClassroom : function (obj,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .post(CLASSROOM_URL,obj,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateClassroomByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .put(CLASSROOM_URL + '/' + data.classroomID,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getClassroomByID : function (classroomID,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(CLASSROOM_URL + '/' + classroomID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        addTeacher : function (obj,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .post(TEACHERS_URL,obj,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getTeachers : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(TEACHERS_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        deleteTeacherByID : function (teacherID,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .delete(TEACHERS_URL + '/' + teacherID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getTeacherByID : function (teacherID,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(TEACHERS_URL + '/' + teacherID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                        return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateTeacherByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .put(TEACHERS_URL + '/' + data.teacherID,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getScheduleByTeacherID : function (teacherID,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(TEACHERS_URL + '/' + teacherID + '/schedule',{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getScheduleByGroupID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(GROUPS_URL + '/' + data.groupID  + '/schedule',{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getScheduleByQuery : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(SCHEDULE_URL + '/query',{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getListOfTeachers : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(LIST_TEACHERS_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getListOfGroups : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(LIST_GROUPS_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                   return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getApplications : function (query,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(APPLICATION_URL,{
                    headers : {
                        token : token
                    },
                    params : query
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });

        },
        addApplicationAnswer : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .post(APPLICATION_URL + '/' + data.applicationID + '/answer',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                })
        },
        deleteApplicationAnswer : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .delete(APPLICATION_URL + '/' + data.applicationID + '/answer',{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);

                });
        },
        getApplicationByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(APPLICATION_URL + '/' + data.applicationID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateApplicationAnswerByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .put(APPLICATION_URL + '/' + data.applicationID + '/answer',data ,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getStudentsByGroupID : function (data,cb) {
            var self = this;
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(GROUPS_URL + '/' + data.groupID + '/students',{
                    headers : {
                        token : token
                    },
                    params : {
                        groupID : data.groupID
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateStudentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .put(STUDENTS_URL + '/' + data.studentID,data,{
                    headers : {
                        token  :token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getStudentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(STUDENTS_URL + '/list/'+  data.studentID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getAttendance : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(ADMIN_URL + '/attendance',{
                    headers : {
                        token : token
                    },
                    params : {
                        groupID : data.groupID
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        addStudentByGroupID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .post(STUDENTS_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        addNoteByGroupID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .post(NOTES_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb)
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getNoteByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(NOTES_URL + '/' + data.noteID,{
                    headers : {
                        token : token
                    },
                    params : {
                        groupID : data.groupID
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateNoteByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .put(NOTES_URL + '/' + data.noteID,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getNotesByGroupID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(NOTES_URL,{
                    headers : {
                        token : token
                    },
                    params : {
                        groupID : data.groupID
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        deleteNoteByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .delete(NOTES_URL + '/' + data.noteID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        addPayment : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .post(PAYMENTS_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                },function (repsonse) {
                    return errorCallback(repsonse,cb);
                });
        },
        getPaymentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(PAYMENTS_URL + '/' + data.paymentID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        deletePaymentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .delete(PAYMENTS_URL + '/' + data.paymentID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updatePaymentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .put(PAYMENTS_URL + '/' + data.paymentID,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateImage : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            return $http
                .put(SELF_URL + '/image',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updatePassword : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .put(SELF_URL + '/password',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateProfile : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .put(SELF_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getActiveStudents : function (query,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(ACTIVE_STUDENTS_URL,{
                    headers : {
                        token : token
                    },
                    params : query
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        addActiveStudent : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .post(ACTIVE_STUDENTS_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getAssignAvailableGroupsForActiveStudent : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(ACTIVE_STUDENTS_URL + '/' + data.studentID +  '/assign',{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getActiveStudentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(ACTIVE_STUDENTS_URL + '/' + data.studentID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateActiveStudentByID  : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .put(ACTIVE_STUDENTS_URL + '/' + data.studentID,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        assignActiveStudentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .post(ACTIVE_STUDENTS_URL + '/' + data.studentID + '/assign',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        archiveActiveStudentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .delete(ACTIVE_STUDENTS_URL + '/' + data.studentID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        deleteStudentFromGroup : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .delete(GROUPS_URL + '/' + data.groupID + '/students/' + data.studentID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getArchiveStudents : function (query,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(ARCHIVE_STUDENTS_URL,{
                    headers : {
                        token : token
                    },
                    params : query
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateArchiveStudentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .put(ARCHIVE_STUDENTS_URL + '/' + data.studentID,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });

        },

        getArchiveStudentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(ARCHIVE_STUDENTS_URL + '/' + data.studentID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        assignArchiveStudentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .post(ARCHIVE_STUDENTS_URL + '/' + data.studentID + '/assign',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getAssignAvailableGroupsForArchiveStudent : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(ARCHIVE_STUDENTS_URL + '/' + data.studentID + '/assign',{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        addSubject : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .post(SUBJECTS_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getSubjects : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(SUBJECTS_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        delSubjectByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .delete(SUBJECTS_URL + '/' + data.subjectID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        editSubjectByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .put(SUBJECTS_URL + '/' + data.subjectID,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getSubjectByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(SUBJECTS_URL + '/' + data.subjectID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getPaymentsByGroupID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(PAYMENTS_URL,{
                    headers : {
                        token : token
                    },
                    params : {
                        groupID : data.groupID
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        addWaitingStudent : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .post(WAITING_STUDENTS_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getWaitingStudents : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(WAITING_STUDENTS_URL,{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getAssignAvailableGroupsForWaitingStudents : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(WAITING_STUDENTS_URL + '/groups/assign',{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        assignWaitingStudents : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .put(WAITING_STUDENTS_URL + '/groups/assign',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        formGroupForWaitingStudents : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .post(WAITING_STUDENTS_URL + '/groups/assign',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getWaitingStudentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(WAITING_STUDENTS_URL + '/' + data.studentID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateWaitingStudentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .put(WAITING_STUDENTS_URL + '/' + data.studentID,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        archiveWaitingStudentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .delete(WAITING_STUDENTS_URL + '/' + data.studentID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        restoreArchiveStudentByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .post(ARCHIVE_STUDENTS_URL + '/' + data.studentID + '/restore',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        //TESTS
        addTest : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            Upload
                .upload({
                    url : TESTS_URL,
                    headers : {
                        token : token
                    },
                    data : data,
                    method : 'post'
                })
                .then(function (resp) {
                    return createResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        getTests : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(TESTS_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getTestByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .get(TESTS_URL + '/' + data.testID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return getResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        deleteTestByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();

            return $http
                .delete(TESTS_URL + '/' + data.testID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        deleteTestSectionByIndex : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();

            appHelper.addLoader();
            return $http
                .delete(TESTS_URL + '/' + data.testID + '/sections/' + data.sectionIndex,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return deleteResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        assignTest : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .post(TESTS_URL + '/' + data.testID + '/assign',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getAssignAvailableGroupsForTest : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(TESTS_URL + '/' + data.testID + '/assign',{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateTestByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .put(TESTS_URL + '/' + data.testID ,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateTestSectionByIndex : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            Upload
                .upload({
                    url : TESTS_URL + '/' + data.testID + '/sections/' + data.sectionIndex,
                    headers : {
                        token : token
                    },
                    data : data,
                    method : 'put'
                })
                .then(function (resp) {
                    return updateResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        getTariffs : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(ADMIN_URL + '/tariffs' ,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        startInterview : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .post(INTERVIEW_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },

    //     END OF TESTS


        updateCenterID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .put(SELF_URL + '/' + 'center',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },

    //  GROUP-BLOC
        addBloc : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .post(BLOC_GROUPS,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getBlocs : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(BLOC_GROUPS,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getBlocByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(BLOC_GROUPS + '/' + data.blocID ,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        assignGroupByBlocID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .put(BLOC_GROUPS + '/' + data.blocID + '/assign',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        editBlocByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .put(BLOC_GROUPS + '/' + data.blocID,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        delBlocByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .delete(BLOC_GROUPS + '/' + data.blocID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        delGroupFromBloc : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .delete(BLOC_GROUPS + '/' + data.blocID + '/assign',{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getAvailableGroupsForScheduling : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(SCHEDULE_URL + '/available-groups',{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getLessonsByQuery : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(SCHEDULE_QUERY_URL,{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getGroupTeachers : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(GROUPS_URL + '/' + data.groupID + '/teachers',{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getAvailableTeachersByGroupID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(GROUPS_URL + '/' + data.groupID + '/teachers/available',{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        }
    }
});
var gulp = require('gulp'),
    ADMIN = require('./gulp/admin'),
    TEACHER = require('./gulp/teacher'),
    AUTH = require('./gulp/auth'),
    STUDENT = require('./gulp/student'),
    MANUAL = require('./gulp/manual');




gulp.task('watch',[ADMIN.tasks.watch,AUTH.tasks.watch,STUDENT.tasks.watch,TEACHER.tasks.watch,MANUAL.tasks.watch]);
gulp.task('build',[ADMIN.tasks.build.start,AUTH.tasks.build.start,STUDENT.tasks.build.start,TEACHER.tasks.build.start,MANUAL.tasks.build.start]);
gulp.task('default',['watch']);



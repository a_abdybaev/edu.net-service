angular
    .module("EduNet")
    .controller("clientsController", function ($location,localStorageService,$scope,$window,managerApi,managerHelper) {

        var loadClients = function () {
            managerApi.getClients(function (err,clients) {
                if (err) return console.log(err);
                self.clients = clients;
                console.log(clients);
            });
        };
        var loadTariffs = function () {
            managerApi.getTariffs(function (err,tariffs) {
                if (err) return alert("ERRORS");
                self.tariffs = tariffs;
                if (_.isEmpty(tariffs)) return;
                self.tariffID = tariffs[0]._id;
            });
        };
        var makeAdd = function () {
            this.isAdd = !this.isAdd;
        };
        var addClient = function () {
            var data = {
                name : self.name,
                link : self.link,
                tariffID : self.tariffID
            };
            if (!data.name || !data.link || !data.tariffID ) return;
            managerApi.addClient(data,function (err) {
                if (err) {
                    console.log(err);
                    alert("ERRORS");
                    return;
                }
                makeAdd();
                loadClients();
                alert("SUCCESS");
            });
        };
        var del = function (clientID) {
            var isYes = confirm("Точно удалить???");
            if (isYes) return managerApi.delClientByID({
                clientID : clientID
            },function (err) {
                if (err) {
                    console.log(err);
                    alert("ERRORS");
                }
                loadClients();
                alert("SUCCESS");

            });
        };

        var info = function (clientID) {
            managerHelper.setActClientID(clientID);
            $location.path('/client-info');
        };
        var edit = function (clientID) {
            managerHelper.setActClientID(clientID);
            $location.path('/edit-client');
        };

        this.info = info;
        this.edit = edit;
        this.makeAdd = makeAdd;
        this.addClient = addClient;
        this.del = del;


        // INIT
        managerApi.checkAuthorization();
        var self = this;



        loadClients();
        loadTariffs();
    })

    .controller('editClientController',function ($location,localStorageService,$scope,$window,managerApi,managerHelper) {
        var loadClientByID = function (clientID) {
            managerApi.getClientByID({
                clientID : clientID
            },function (err,client) {
                if (err) return console.log(err);
                self.client = client;
                self.tariff = client.tariffID;
                console.log(client);
                loadTariffs();
            });
        };
        var updateClient = function () {
            var data = {
                tariffID : self.tariff._id,
                isPaid : self.client.isPaid,
                clientID : self.client._id,
                name : self.client.name
            };
            if (!data.tariffID || data.isPaid == null) return;
            managerApi.updateClientByID(data,function (err) {
                if (err) return alert("ERROR");
                alert("SUCCESS")
            });

        };
        var loadTariffs = function () {
            managerApi.getTariffs(function (err,tariffs) {
                if (err) return console.log(err);
                self.tariffs = tariffs;
            })
        };
        this.updateClient = updateClient;


        // INIT
        var self = this;
        var clientID = managerHelper.getActClientID();
        if (!clientID) return alert("ERROR");
        loadClientByID(clientID);

    })

    .controller("clientInfoController", function ($location,localStorageService,$scope,$window,managerApi,managerHelper) {
        var self = this;


    })
    .controller("addClientController", function ($location,localStorageService,$scope,$window,managerApi,managerHelper) {
        var self = this;

    });



angular
    .module("EduNet")
    .controller("centersController", function ($location,localStorageService,$scope,$window,managerApi,managerHelper) {
        var loadClients = function () {
            managerApi.getClients(function (err,clients) {
                if (err) {
                    alert("ERRORS");
                    return console.log(err);
                }
                self.clients = clients;
                if (_.isEmpty(clients)) return;
                self.clientID = clients[0]._id;
            });
        };
        var loadCenters = function () {
            managerApi.getCenters(function (err,centers) {
                if (err) return console.log(err);
                self.centers = centers;
            });
        };

        var centerInfo = function (centerID) {
            managerHelper.setActiveCenterID(centerID);
            $location.path('/center-info');
        };
        var del = function (centerID) {
            var isYes = confirm("Точно удалить???");
            if (isYes) managerApi.delCenterByID({
                centerID : centerID
            },function (err) {
                if (err) {
                    console.log(err);
                    alert("ERRORS");
                }
                loadCenters();
                alert("SUCCESS.");

            });
        };
        var edit = function (centerID) {
            managerHelper.setActiveCenterID(centerID);
            $location.path('/edit-center');
        };
        var makeAdd = function () {
            self.isAdd = !self.isAdd;
        };
        var addCenter = function () {
            var data = {
                name : self.name,
                clientID : self.clientID
            };
            if (!data.name) return;
            managerApi.addCenter(data,function (err) {
                if (err) {
                    alert("Ошибка");
                    console.log(err);
                    return;
                }
                makeAdd();
                loadCenters();
                alert("SUCCESS.");

            });
        };
        this.del = del;
        this.addCenter = addCenter;
        this.centerInfo = centerInfo;
        this.edit = edit;
        this.makeAdd = makeAdd;



        // INIT
        managerApi.checkAuthorization();
        var self = this;
        // makeAdd();



        loadCenters();
        loadClients();
    })

    .controller('editCenterController',function ($location,localStorageService,$scope,$window,managerApi,managerHelper) {
        var loadCenterByID = function (centerID) {
            managerApi.getCenterByID({
                centerID : centerID
            },function (err,data) {
                if (err) return console.log(err);
                self.center = data.center;
                loadTariffs();
            });
        };
        var updateCenter = function () {
            var data = {
                tariffID : self.center.tariffID,
                isPaid : self.center.isPaid,
                centerID : self.center._id
            };
            if (!data.tariffID || data.isPaid == null) return;
            managerApi.updateCenterByID(data,function (err) {
                if (err) return alert("ERROR");
                alert("SUCCESS")
            });

        };
        var loadTariffs = function () {
            managerApi.getTariffs(function (err,tariffs) {
                if (err) return console.log(err);
                self.tariffs = tariffs;
            })
        };
        this.updateCenter = updateCenter;


        // INIT
        var self = this;
        var centerID = managerHelper.getActiveCenterID();
        if (!centerID) return console.log('err');
        loadCenterByID(centerID);

    })

    .controller("centerInfoController", function ($location,localStorageService,$scope,$window,managerApi,managerHelper) {
        var self = this;
        var centerID = managerHelper.getActiveCenterID();
        if (!centerID) return $location.path('/centers');
        var loadCenterInfo = function () {
            managerApi.getCenterByID({
                centerID : centerID
            },function (err,data) {
                if (err) return console.log(err);
                self.center = data.center;
                self.teachers = data.teachers;
                self.students = data.students;
                self.groups = data.groups;
            });
        };
        loadCenterInfo();

    })

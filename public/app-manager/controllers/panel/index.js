angular
    .module("EduNet")
    .controller("panelController", function ($location,localStorageService,$scope,$window,managerApi,managerHelper) {
        managerApi.checkAuthorization();
        var self = this;
        var pullOrigin = function () {
            managerApi.pullOrigin(function (err) {
                if (err) return console.log(err);
                console.log("PULLED");
            });
        };
        var restartApp = function () {
            managerApi.restartApp(function (err) {
                if (err) return console.log(err);
                console.log("RESTARTED");
            });
        };
        var merge = function () {
            managerApi.mergeFrontEnd(function (err) {
                if (err) return console.log(err);
                console.log("MERGERD");
            });
        };
        var pushOrigin = function () {
            managerApi.pushOrigin(function (err) {
                if (err) return console.log(err);
                console.log("PUSHED");
            });
        };
        var lookLogs = function () {
            managerApi.logs(function (err,data) {
                if (err) return console.log(err);
                self.logs = data;
            });
        };
        this.pullOrigin = pullOrigin;
        this.restartApp = restartApp;
        this.pushOrigin = pushOrigin;
        this.merge = merge;
        this.lookLogs = lookLogs;
        this.host = $location.host();

    });


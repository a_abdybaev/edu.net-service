angular
    .module("EduNet")
    .controller("authController", function ($location,localStorageService,$scope,$window,managerApi) {
        var self = this;

        var authorize = function () {
            var data = {
                login : self.login,
                password : self.password
            };
            if (!data.login || !data.password) return;
            managerApi.authManager(data,function (err,token) {
                if (err) return console.log(err);
                managerApi.setToken(token);
            });
        };

        self.authorize = authorize;
    });

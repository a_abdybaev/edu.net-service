angular
    .module("EduNet")
    .controller("adminsController", function ($location,localStorageService,$scope,$window,managerApi) {
        var loadAdmins = function () {
            managerApi.getAdmins(function (err,admins) {
                if (err) return console.log(err);
                self.admins = admins;

            });
        };
        var loadClients = function () {
            managerApi.getClients(function (err,clients) {
                if (err) return console.log(err);
                self.clients = clients;
                if (_.isEmpty(clients)) return console.log("ERROR");
                self.client = clients[0];

                if (_.isEmpty(self.client.centers)) return;
                self.center = self.client.centers[0];

            });
        };
        var makeAdd = function () {
            self.isAdd = !self.isAdd;
        };
        var updateCenters = function () {
            if (self.client.centers.length != 0) self.center = self.client.centers[0];
        };

        var addAdmin = function () {
            var data = {
                phone : self.phone,
                clientID : self.client._id,
                isManager : self.isManager
            };
            if (!data.isManager) data.centerID = self.center._id;
            if (!data.phone || !data.clientID) return;
            managerApi.addAdmin(data,function (err) {
                if (err) {
                    console.log(err);
                    return alert("ERROR");
                }
                makeAdd();
                loadAdmins();
                alert("SUCCESS");
            });
        };
        var delByID = function (adminID) {
            var isDel = confirm("Точно?");
            if (!isDel) return;
            managerApi.delAdminByID({
                adminID : adminID
            },function (err) {
                if (err) {
                    console.log("ERROR");
                    return alert(err);
                }
                loadAdmins();
                alert("SUCCESS");
            });
        };
        this.addAdmin = addAdmin;
        this.makeAdd = makeAdd;
        this.updateCenters =updateCenters;
        this.delByID = delByID;


        // INIT
        managerApi.checkAuthorization();
        var self = this;

        loadAdmins();
        loadClients();


        //DEV
        // makeAdd();



    })
    .controller("adminInfoController", function ($location,localStorageService,$scope,$window,managerApi,managerHelper) {

    })
;




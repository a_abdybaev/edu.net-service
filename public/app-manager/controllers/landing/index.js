angular
    .module("EduNet")
    .controller("requestsController", function ($location,localStorageService,$scope,$window,managerApi,managerHelper) {
        managerApi.checkAuthorization();

        var self = this;

        var loadRequests = function () {
            managerApi.getRequests(function (err,requests) {
                if (err) return console.log(err);
                self.requests = requests;
            });
        };

        loadRequests();
    });



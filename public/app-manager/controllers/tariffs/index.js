angular
    .module("EduNet")
    .controller("tariffsController", function ($location,localStorageService,$scope,$window,managerApi) {

        var loadTariffs = function () {
            managerApi.getTariffs(function (err,tariffs ) {
                if (err) return console.log(err);
                self.tariffs = tariffs;
            });
        };
        var addTariff = function () {
            self.isAddTariff = !self.isAddTariff;
        };
        var deleteTariff = function (tariffID) {
            var result = confirm("Вы уверены что хотите удалить данный тариф?");
            if (!result) return;
            managerApi.deleteTariffByID({
                tariffID : tariffID
            },function (err) {
                if (err) return console.log("ERROOR");
                loadTariffs();
                alert("SUCCESS");
            });
        };





        // INIT
        var self = this;
        managerApi.checkAuthorization();

        this.addTariff = addTariff;
        this.isAddTariff = false;
        this.deleteTariff = deleteTariff;
        loadTariffs();

    })
    .controller("addTariffController", function ($location,localStorageService,$scope,$window,managerApi,managerHelper) {
        var addTariff = function () {
            var data = {
                name : self.name,
                size : self.size,
                students : self.students,
                price : self.price
            };
            if (!data.name || !data.size || !data.students || !data.price) return;
            managerApi.addTariff(data,function (err) {
                if (err) return console.log(err);
                alert("SUCCESS");
            });
        };

        var self = this;
        managerApi.checkAuthorization();
        this.addTariff = addTariff;

        // dev
        this.name = "TestTariff";
        this.size = 1500;
        this.students = 99;
        this.price = 10000;

    });





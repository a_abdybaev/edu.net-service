var BASE_URL = "/api/managers",
    CENTERS_URL = BASE_URL + '/centers',
    ADMINS_URL  = BASE_URL + '/admins',
    TARIFFS_URL = BASE_URL + '/tariffs',
    CLIENTS_URL = BASE_URL + '/clients';

var createResource= function (response,cb) {
    if (response.status === 201) return cb(null);
    return cb(response);
};
var updateResource = function (response,cb) {
    if (response.status === 200) return cb(null);
    return cb(response);
};
var deleteResource = function(response,cb) {
    if (response.status === 200) return cb(null);
    return cb(response);
};
var getResource = function(response,cb) {
    if (response.status === 200) return cb(null,response.data);
    return cb(response);
};
var errorCallback = function(response,cb) {

    if (response.status == 403) {
        angular.element(document.querySelector('[ng-controller]')).injector().get('localStorageService').remove("token");
        angular.element(document.querySelector('[ng-controller]')).injector().get('$window').location.href = "/";
        return;
    }

    return cb(response);
};
var successCallback = function (response,cb) {
    if (response.status === 200) return cb(null);
    return cb(response);
};


app.factory('managerApi', function ($http,localStorageService,$location) {
    return {
        getToken : function () {
            return localStorageService.get('token');
        },
        checkAuthorization : function () {
            var token = localStorageService.get('token');
            if (!token) return $location.path('/auth').replace();
        },
        authManager : function (data,cb) {
            $http
                .post('/api/auth/login-manager',data)
                .then(function (resp) {
                    if (resp.status === 200) return getResource(resp,cb);
                    return errorCallback(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        setToken : function (token) {
            return localStorageService.set('token',token);
        },
        getCenters : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .get(CENTERS_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return getResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        updateCenterByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .put(CENTERS_URL +'/' + data.centerID,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return updateResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        getAdmins : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .get(ADMINS_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return getResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        getTariffs : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .get(TARIFFS_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return getResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        addCenter : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .post(CENTERS_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return createResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        addAdmin : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .post(ADMINS_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return createResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        getCenterByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .get(CENTERS_URL + '/' + data.centerID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return getResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        pullOrigin : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .get(BASE_URL +'/panel/pull',{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return successCallback(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        restartApp : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .get(BASE_URL +'/panel/restart',{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return successCallback(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        pushOrigin : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .get(BASE_URL +'/panel/push',{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return successCallback(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        mergeFrontEnd : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .get(BASE_URL +'/panel/merge',{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return successCallback(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        logs : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .get(BASE_URL +'/panel/logs',{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return getResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        getRequests : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .get(BASE_URL +'/landing/requests',{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return getResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        addTariff : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .post(TARIFFS_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return createResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        deleteTariffByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .delete(TARIFFS_URL +'/' + data.tariffID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return deleteResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        addClient : function(data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .post(CLIENTS_URL,data,{
                    headers : {
                        token  :token
                    }
                })
                .then(function (resp) {
                    return createResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        getClients : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .get(CLIENTS_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return getResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        getClientByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .get(CLIENTS_URL + '/' + data.clientID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return getResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        delCenterByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .delete(CENTERS_URL +'/' + data.centerID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return deleteResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        delClientByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .delete(CLIENTS_URL +'/' + data.clientID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return deleteResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        delAdminByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .delete(ADMINS_URL +'/' + data.adminID,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return deleteResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        },
        updateClientByID : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            $http
                .put(CLIENTS_URL +'/' + data.clientID,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (resp) {
                    return updateResource(resp,cb);
                },function (resp) {
                    return errorCallback(resp,cb);
                });
        }

    };
});

app.factory('managerHelper', function ($http,localStorageService,$location) {
    return {
        setActiveCenterID : function (centerID) {
            return localStorageService.set('activeCenterID',centerID)
        },
        getActiveCenterID : function () {
            return localStorageService.get('activeCenterID');
        },
        getActClientID : function () {
            return localStorageService.get('actClientID');
        },
        setActClientID : function (clientID) {
            return localStorageService.set('actClientID',clientID);
        }

    };
});




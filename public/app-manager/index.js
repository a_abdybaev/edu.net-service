'use strict';
var app = angular.module("EduNet", ['ngRoute','LocalStorageModule'])
    .config(function (localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('EduNet')
            .setStorageType('sessionStorage')
            .setNotify(true, true)
    })
        .config(function($routeProvider,$locationProvider){
            $routeProvider
                .when("/auth",{
                    templateUrl : "/app-manager/views/auth/index.html"
                })
                .when("/centers",{
                    templateUrl : "/app-manager/views/centers/index.html"
                })
                .when("/center-info",{
                    templateUrl : "/app-manager/views/centers/center-info.html"
                })
                .when("/edit-client",{
                    templateUrl : "/app-manager/views/clients/edit-client.html"
                })
                .when("/admins",{
                    templateUrl : "/app-manager/views/admins/index.html"
                })
                .when("/admin-info",{
                    templateUrl : "/app-manager/views/admins/admin-info.html"
                })
                .when("/add-admin",{
                    templateUrl : "/app-manager/views/admins/add-admin.html"
                })
                .when("/panel",{
                    templateUrl : "/app-manager/views/panel/index.html"
                })
                .when("/landing",{
                    templateUrl : "/app-manager/views/landing/index.html"
                })
                .when("/tariffs",{
                    templateUrl : "/app-manager/views/tariffs/index.html"
                })
                .when('/clients',{
                    templateUrl : "/app-manager/views/clients/index.html"
                })
                .otherwise({
                    redirectTo : '/centers'
                })
            ;

        })
    ;


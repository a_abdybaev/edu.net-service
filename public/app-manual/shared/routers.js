angular
    .module("EduNet")
    .config(function($routeProvider){
        $routeProvider
            .when("/",{
                templateUrl : "template/index.html"
            })
            .when("/template",{
                templateUrl : "template/index.html"
            })
            .when("/teacher-groups",{
                templateUrl : "teacher-groups/index.html"
            })
            .otherwise({
                redirectTo : '/template'
            })
    });
angular
    .module("EduNet")
    .controller("indexController",['$scope','$location', function (scope,location) {
        var self = this;

        this.goTo = function (url) {
            location.path(url);
        };
    }]);

var SHORTLIMIT = 200;
var LONGLIMIT = 10000;

app.factory('testingHelper', function ($location,localStorageService,$window) {
    return {
        logout : function () {
            localStorageService.remove("token");
            $window.location.href = "/";
        },
        EVENTS : {
            END_SECTION : "END_SECTION"
        }
    }
});
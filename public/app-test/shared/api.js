var TESTING_URL = "/api/testing/";

var createResource= function (response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 201) return cb(null);
    return cb(response);
};
var updateResource = function (response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null);
    return cb(response);
};
var deleteResource = function(response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null);
    return cb(response);
};
var getResource = function(response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null,response.data);
    return cb(response);
};
var errorCallback = function(response,cb) {
    var appHelper = angular.element(document.body).injector().get('appHelper'),
        appValues = angular.element(document.body).injector().get('appValues');

    appHelper.removeLoader();
    if (response.status == 403) {
        angular.element(document.querySelector('[ng-controller]')).injector().get('localStorageService').remove(appValues.LOCAL_STORAGE_KEYS.TOKEN);
        angular.element(document.querySelector('[ng-controller]')).injector().get('$window').location.href = "/";
        return;
    }

    return cb(response);
};
var successCallback = function (response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null);
    return cb(response);
};

app.factory('testingApi', function (appHelper,$http,localStorageService,$window,appValues) {
    return {
        getToken : function () {
            return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.TOKEN)
        },
        checkAuthorization : function () {
            var token = this.getToken();
            if (!token) {
                $window.location.href = "/";
            }
        },
        removeToken : function () {
            return localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.TOKEN);
        },
        resolveTesting : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(TESTING_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        startSection : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .post(TESTING_URL + 'sections',{},{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        endSection : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .put(TESTING_URL + 'sections',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        cancelTest : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .delete(TESTING_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        }

    }
});
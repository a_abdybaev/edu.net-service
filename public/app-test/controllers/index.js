app
    .controller("indexController", function ($window,appConstants,appHelper,testingApi,testingHelper,localStorageService,$rootScope,$scope,$location) {
        var resolveTest = function () {
            testingApi.resolveTesting(function (err,data) {
                if (err) return appHelper.showError(err);
                self.inInitialization = false;
                self.testID = data.student.testID;
                if (data.isNotTesting) return $window.location.href = "/student";
                if (data.isNotStarted) {
                    self.isStarting = true;
                }
                if (data.isContinueTesting) {
                    self.section = data.section;
                    self.isSection = true;
                    self.sectionNumber = data.sectionNumber;
                }
            });
        };
        var startSection = function (cb) {
            testingApi.startSection(cb);
        };
        var lookResults = function () {
            self.isSection = false;
            self.isResults = true;
        };
        this.startSection = startSection;
        this.lookResults = lookResults;
        this.resolveTest = resolveTest;


        // INIT
        var self = this,
            token = testingApi.getToken();

        $rootScope.loader = [];
        self.isInitialization = true;
        if (!token) return testingHelper.logout();
        resolveTest();




    });

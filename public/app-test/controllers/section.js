app
    .controller("sectionController", function (ngDialog,$window,appConstants,appHelper,testingApi,testingHelper,localStorageService,$rootScope,$scope,$location) {
        var initSection = function () {
            self.pages = ctrl.section.images;
            self.pageIndex = 0;
            self.answers = new Array(parseInt(ctrl.section.questionsCount));
            self.hasTime = true;
            var now = new Date(),
                startedAt = new Date(ctrl.section.startedAt),
                difference = (now - startedAt) / 1000;
            if (difference >= ctrl.section.duration) return endSection();
            self.remainingTime = (ctrl.section.duration - difference);
            this.timeOut = setTimeout(closeSectionByRemainingTime,self.remainingTime * 1000);
            if (ctrl.section.hasAudio) resolveAudio();
            ctrl.isLastSection = ctrl.sectionNumber + 1 == ctrl.testID.sections.length;


        };
        var resolveAudio = function () {
            self.sound = new Howl({
                src: [ctrl.section.audio]
            });
            self.sound.once('load',function(){
                var duration = self.sound.duration();
                var now = new Date(),
                    startedAt = new Date(ctrl.section.startedAt),
                    difference = (now - startedAt) / 1000;
                if (difference > duration) return;
                self.sound.seek(difference);
                self.sound.play();
                self.isPlaying = true;
            });
            self.sound.once('stop',function () {
                self.isPlaying = false;
            });
        };
        var closeSectionByRemainingTime = function () {
            self.hasTime = false;
            $scope.$apply();
        };
        var previousPage = function () {
            if (self.pageIndex == 0) return;
            self.pageIndex--;
        };
        var nextPage = function () {
            if (self.pageIndex + 1 == self.pages.length) return;
            self.pageIndex++;
        };


        var endSection = function () {
            if (this.timeOut != null) clearTimeout(this.timeOut);
            testingApi.endSection({
                answers : self.answers
            },function (err,data) {
                if (err) return console.log(err);
                if (data.isEnded) {
                    ctrl.results = data.results;
                    return self.isCompleted = true;
                }
                ctrl.startSection(function (err,data) {
                    if (err) return console.log(err);
                    ctrl.section = data.section;
                    ctrl.sectionNumber = data.sectionNumber;
                    initSection();
                });
            });
        };
        var nextSection = function () {
            ngDialog.open({
                template: "/app-test/views/alerts/next-section.html",
                className: 'ngdialog-theme-default',
                width: 600,
                data : {
                    isLastSection : ctrl.isLastSection,
                    timeOut : this.timeOut
                }
            });
        };
        $rootScope.$on(testingHelper.EVENTS.END_SECTION,function () {
            endSection();
        });
        // INIT

        var self = this;
        var ctrl = $scope.$parent.index;
        this.previousPage = previousPage;
        this.nextPage = nextPage;
        this.endSection = endSection;
        this.nextSection = nextSection;
        //DEVELOPMENT


        initSection();

    });

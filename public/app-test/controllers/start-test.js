app
    .controller("startTestController", function ($window,appConstants,appHelper,testingApi,testingHelper,localStorageService,$rootScope,$scope,$location) {
        var startTest = function () {
            ctrl.startSection(function (err,data) {
                if (err) return console.log(err);
                ctrl.section = data.section;
                ctrl.sectionNumber = 0;
                ctrl.isSection = true;
                ctrl.isStartSection = true;
                ctrl.isStarting = false;
            });
        };
        var cancelTest = function () {
            var result = confirm("Вы уверены что хотите отменить тестирование?");
            testingApi.cancelTest(function (err) {
                if (err) return console.log("ERROR");
                ctrl.resolveTest();
            });
        };

        // INIT
        var self = this;
        this.startTest = startTest;
        this.cancelTest = cancelTest;
        var ctrl = $scope.$parent.index,
            testID = ctrl.testID,
            questions = 0,
            duration = 0;

        this.sections = testID.sections.length;
        _.each(testID.sections,function (section) {
            questions += section.answers.length;
            duration += section.duration;
        });
        this.duration = duration;
        this.questions = questions;




    });

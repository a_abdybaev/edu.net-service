app
    .factory('studentModalHelper', function (studentValues) {
        return {
            deleteMessages : {
               
            },
            successMessages : [{
                value : studentValues.COMMENT_CREATED,
                message : "Комментарий успешно добавлен"
            }],
            errorMessages : []
        }
    });
app.factory('studentHelper', function (appValues,appHelper,studentConstants,$location,localStorageService,$window,ngDialog,appConstants) {
    return {
        getProfileSchedule : function (schedule) {
            var results = this.getWeekDaysArrayWithoutSundayDefault();
            if (schedule.length != 0) {
                schedule.forEach(function (lesson) {
                    results[lesson.day-1].hasLesson = true;
                    var startTime = new Date(),
                        endTime = new Date();
                    startTime.setHours(lesson.start.hour);
                    startTime.setMinutes(lesson.start.minute);
                    endTime.setHours(lesson.end.hour);
                    endTime.setMinutes(lesson.end.minute);

                    results[lesson.day-1].lesson = {
                        classroomID : lesson.classroomID,
                        startTime : startTime,
                        endTime : endTime
                    }
                });
            }
            return results;
        },
        getWeekDaysArrayWithoutSundayDefault : function () {
            var results = [];
            var weekDays = appHelper.getWeekDaysWithoutSundayDefault();
            weekDays.forEach(function (day) {
                results.push({
                    name : day,
                    hasLesson : false
                });
            });
            return results;
        },
        getCalendar : function (lessons,homeworks) {
            var today = new Date(),
                currentDayStyle = ' teacher-calendar-today-item';

            var notThisMonthStyle = "teacher-calendar-item",
                thisMonthStyle = "teacher-calendar-active-item",
                lessonDayStyle = "teacher-calendar-lesson-item";
            var currentMonth;
            var calendar =[];
            var monthNames = appHelper.getMonthNamesLowerCase();
            var lessonDays = this.getLessonDaysFromSchedule(lessons);
            for(var i = 0; i < 12;i++) {
                var date = new Date(2017,i,1);
                var days = [];
                var dayOfWeek = date.getDay();
                if (dayOfWeek == 0) {
                    date.setDate(date.getDate() - 6);
                } else {
                    date.setDate(date.getDate() - dayOfWeek + 1);
                }
                calendar.push({
                    monthName : monthNames[i],
                    days : []
                });
                for (var k=0;k<6;k++) {
                    calendar[i].days.push([]);
                    for (var j = 0; j < 7;j++){
                        var isLessonDay = false;
                        var style = notThisMonthStyle;
                        if (date.getMonth() == i) {
                            style = thisMonthStyle;
                            currentMonth = true;
                            style = thisMonthStyle;
                        } else {
                            currentMonth = false;
                        }
                        var index = lessonDays.indexOf(j);
                        if (index > -1 && currentMonth === true) {
                            isLessonDay = true;
                            style = lessonDayStyle;
                        }
                        var dayCSS = "calendar-day-number";
                        if (today.getDate() == date.getDate() && today.getMonth() == date.getMonth()) {
                            dayCSS = "calendar-day-number calendar-day-number-active";
                        }
                        calendar[i].days[k].push({
                            hasHomework : false,
                            style : style,
                            day : date.getDate(),
                            dayCSS : dayCSS,
                            currentMonth : currentMonth,
                            isLessonDay : isLessonDay
                        });
                        date.setDate(date.getDate()+1);
                    }
                }

            }
            this.assignHomeworksForCalendar(calendar,homeworks);
            return calendar;
        },
        assignHomeworksForCalendar : function (calendar,homeworks) {
            if (homeworks.length == 0) return;
            homeworks.forEach(function (homework) {
                var month = homework.date.month,
                    day = homework.date.day;
                var days = calendar[month].days;
                for (var weekNumber=0;weekNumber<6;weekNumber++) {
                    for (var weekDay = 0;weekDay<7;weekDay++) {
                        var currentDay = days[weekNumber][weekDay];
                        if (currentDay.currentMonth === true && currentDay.day === homework.date.day) {
                            calendar[month].days[weekNumber][weekDay].hasHomework = true;
                            calendar[month].days[weekNumber][weekDay].homework = homework;

                        }
                    }
                }
            });
        },
        getLessonDaysFromSchedule : function (schedule) {
            var results = [];
            if (schedule != undefined && schedule.length != 0) {
                schedule.forEach(function (lesson) {
                    results.push(lesson.day-1);
                });
            }
            return results;
        },
        getName : function (student) {
            return student.lastName + ' ' + student.firstName;
        },
        getAttendance : function (days) {
            var results =[];
            var monthNames = appHelper.getMonthNamesWithYearLowerCase();
            monthNames.forEach(function (monthName) {
                results.push({
                    name : monthName,
                    attendance : []
                });
            });
            days.forEach(function (day) {
                results[day.month].attendance.push({
                    day : day.day,
                    value : day.value
                })
            });
            return this.deleteEmptyMonthsFromAttendance(results);
        },
        buildAttendance : function (appearance) {
            var getAllDaysArray = function () {
                var months = appHelper.getMonthNamesWithYearLowerCase();
                var results = [];
                var year = 2017;
                months.forEach(function (month,i) {
                    var num = 0;
                    results.push({
                        name : month,
                        days : [],
                        hasAttendance : false
                    });
                    var daysInArray = new Date(year,i+1,0).getDate();
                    for (var j = 1; j <= daysInArray;j++) {
                        results[i].days[j] = appConstants.STUDENT_APPEARANCE_EMPTY_DAY;
                    }
                });
                return results;
            };
            var days = getAllDaysArray();
            appearance.forEach(function (attendance) {
                var day = attendance.day,
                    month = attendance.month,
                    value = attendance.value;
                days[month].days[day] = value;
                days[month].hasAttendance = true;
            });
            return days;

        },
        deleteEmptyMonthsFromAttendance : function (months) {
            var results = [];
            months.forEach(function (month) {
                if (month.attendance.length != 0) {
                    results.push(month);
                }
            });
            return results;
        },
        setActiveGroupIndex : function (index) {
            return localStorageService.set(this.LOCAL.GROUP_INDEX,index);
        },
        getActiveGroupIndex : function () {
            return localStorageService.get(this.LOCAL.GROUP_INDEX);
        },
        getActiveGroup : function () {
            return localStorageService.get(this.LOCAL.GROUP);
        },
        setActiveGroup : function (group) {
            return localStorageService.set(this.LOCAL.GROUP,group);
        },
        getTeacherName : function () {
            var info = appHelper.getGeneralInfo();
            if (!info) {
                return "Вакансия"
            } else {
                return appHelper.getNameForUser(info.groupID.teacherID);
            }
        },
        // LOCAL STORAGE
        LOCAL : {
            GROUP_INDEX : "GROUP_INDEX",
            GROUP : "GROUP"
        },
        buildChannelForPosts : function (posts) {
            var limits = [],
                moreButtons = [],
                self = this;

            posts.forEach(function (post) {
                limits.push(appConstants.POST_TEXT_SHORT_LIMIT);
                self.resolveComments(post);
                if (post.homeworkID == null) {
                    if (post.text.length > appConstants.POST_TEXT_SHORT_LIMIT) {
                        moreButtons.push(true);
                    } else {
                        moreButtons.push(false);
                    }
                    if (post.images.length == 0) {
                        post.hasImages = false;
                        post.hasSingleImage = false;
                    } else if(post.images.length == 1) {
                        post.hasImages = true;
                        post.hasSingleImage = true;
                        post.images[0].original_url = post.images[0].url;
                    } else {
                        post.hasImages = true;
                        post.hasSingleImage = false;
                        post.gridImages = post.images.map(function (image) {
                            return {
                                original_url : image.url
                            }
                        });
                    }
                } else {
                    moreButtons.push(false);
                }

            });
            return {
                posts : posts,
                moreButtons : moreButtons,
                limits : limits
            };
        },
        resolveComments : function (post) {

        },
        logout : function () {

            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.GROUP);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.ACT_GROUP_ID);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.GROUP_INDEX);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.USER_INFO);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.IS_DEV);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.TOKEN);
            localStorageService.remove(appValues.LOCAL_STORAGE_KEYS.USER_TYPE);
            $window.location.href = "/";
        },
        getLang : function () {
            var en = 'en',
                ru = 'ru',
                lang = localStorageService.get('LANG');
            if (!lang) this.setLang(ru);
            return localStorageService.get('LANG')


        },
        setLang : function (lang) {
            return localStorageService.set('LANG',lang);
        }
    }
});
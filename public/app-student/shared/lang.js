var EN_LANG = {
    //МЕНЮ
    CHANNEL : {
        TITLE : "Channel",
        EMPTY_TITLE : "NO POSTS",
        EMPTY_TEXT : "Unfortunately, there are no posts on this section yet.",
        TABS : {
            ALL : "All posts",
            HOMEWORK : "Homework",
            DOCS : "Documents",
            IMPORTANT : "Important",
            OTHER : "Other posts"
        }
    },
    PAYMENTS : {
        TITLE : "Payments",
        TITLE2 : "My payments",
        TITLE3 : "Payment history",
        REMAINDER : "Remainder to pay",
        LAST : "Last payment",
        INSERTION : "Paid amount",
        PAID : "PAID AMOUNT",
        TOTAL : "TOTAL COST",
        TITLE4 : "PAID"
    },
    DOCUMENTS : {
        TITLE : "Documents",
        SEARCH : "Search for documants"
    },
    ONLINE_TESTS : {
        TITLE : "Online tests",
        INFO : {
            SECTIONS : "Sections:",
            QUESTIONS : "Questions:",
            DURATION : "Duration:",
            MIN : "minutes",
            RESULT : "RESULT"
        },
        START : "START TEST"
    },
    RESULTS : {
        TITLE : "Results",
        TITLE2 : "My results",
        DATE : "Test date:",
        RESULT : "Test result:",
        SECTION : "Section"
    },
    ATTENDANCE : {
        TITLE : "Attendance",
        EMPTY_TITLE : "NO ATTENDANCE",
        EMPTY_TEXT : "Unfortunately, attendance has not been added yet. You may contact the Teacher.",
        TABS : {
            PRESENT : "Attended lesson",
            ABSENT : "Missed lesson",
            EXCUSE : "Valid reason"
        }
    },
    MY_PAGE : {
        TITLE : "My page",
        CHANGE_PASSWORD : "Change password",
        EDIT_PROFILE : "Edit profile",
        HOMEWORK : {
            TITLE : "Homework",
            EMPTY : "No homework"
        },
        SCHEDULE : {
            TITLE : "Schedule",
            EMPTY_TITLE : "NO SCHEDULE",
            EMPTY_TEXT : "Unfortunately, schedule has not been formed yet. You may contact the Administration."
        },
        TEST : {
            TITLE : "New test",
            EMPTY : "No test available"
        },
        RESULT : {
            TITLE : "Latest result",
            EMPTY : "No results"
        }
    },
    CALENDAR : {
        TITLE : "Calendar",
        DESC : "Lesson days and homework",
        DAYS : {
            MON : "Mon",
            TUE : "Tue",
            WED : "Wed",
            THU : "Thu",
            FRI : "Fri",
            SAT : "Sat",
            SUN : "Sun"
        }
    }


};

var RU_LANG = {

    CHANNEL : {
        TITLE : "Канал",
        EMPTY_TITLE : "ПОСТОВ НЕТ",
        EMPTY_TEXT : "К сожалению, по данному разделу еще нет постов.",
        TABS : {
            ALL : "Все посты",
            HOMEWORK : "Домашние задания",
            DOCS : "Документы",
            IMPORTANT : "Важное",
            OTHER : "Прочие посты"
        }
    },
    PAYMENTS : {
        TITLE : "Оплаты",
        TITLE2 : "Мои оплаты",
        TITLE3 : "История оплат",
        REMAINDER : "Остаток к оплате",
        LAST : "Последняя оплата",
        INSERTION : "Внесенная сумма",
        PAID : "ВНЕСЕННАЯ СУММА",
        TOTAL : "ПОЛНАЯ СТОИМОСТЬ",
        TITLE4 : "ОПЛАЧЕНО"
    },
    DOCUMENTS : {
        TITLE : "Документы",
        SEARCH : "Поиск по документам"
    },
    ONLINE_TESTS : {
        TITLE : "Онлайн тесты",
        INFO : {
            SECTIONS : "Разделов:",
            QUESTIONS : "Вопросов:",
            DURATION : "Длительность:",
            MIN : "минут",
            RESULT : "РЕЗУЛЬТАТ"
        },
        START : "НАЧАТЬ ТЕСТ"
    },
    RESULTS : {
        TITLE : "Результаты",
        TITLE2 : "Мои результаты",
        DATE : "Дата прохождения:",
        RESULT : "Результат тестирования:",
        SECTION : "Раздел"
    },
    ATTENDANCE : {
        TITLE : "Посещаемость",
        EMPTY_TITLE : "ПОСЕЩАЕМОСТИ НЕТ",
        EMPTY_TEXT : "К сожалению, на данный момент, посещаемость не выставлена. Необходимо обратиться к Учителю.",
        TABS : {
            PRESENT : "Был на уроке",
            ABSENT : "Не был на уроке",
            EXCUSE : "Уважительная причина"
        }
    },
    MY_PAGE : {
        TITLE : "Моя страница",
        CHANGE_PASSWORD : "Сменить пароль",
        EDIT_PROFILE : "Редактировать",
        HOMEWORK : {
            TITLE : "Домашнее задание",
            EMPTY : "Домашнего задания нет"
        },
        SCHEDULE : {
            TITLE : "Расписание",
            EMPTY_TITLE : "РАСПИСАНИЯ НЕТ",
            EMPTY_TEXT : "К сожалению, расписание еще не сформировано. Мы можете обратиться в Администрацию центра."
        },
        TEST : {
            TITLE : "Новый тест",
            EMPTY : "Нет доступных фильмов"
        },
        RESULT : {
            TITLE : "Последний результат",
            EMPTY : "Результатов пока нет"
        }
    },
    CALENDAR : {
        TITLE : "Календарь",
        DESC : "Дни занятий и домашние задания",
        DAYS : {
            MON : "Пн",
            TUE : "Вт",
            WED : "Ср",
            THU : "Чт",
            FRI : "Пт",
            SAT : "Сб",
            SUN : "Вс"
        }
    }



};
angular
    .module("EduNet")
    .config(function($translateProvider) {
        $translateProvider.translations('en',EN_LANG);
        $translateProvider.translations('ru',RU_LANG);
        // $translateProvider.preferredLanguage('en');

});


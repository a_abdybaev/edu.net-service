angular
    .module("EduNet")
    .config(function($routeProvider){
        $routeProvider
            .when("/channel",{
                templateUrl : "channel/index.html"
            })
            .when("/files",{
                templateUrl : "files/index.html"
            })
            .when("/payments",{
                templateUrl : "payments/index.html"
            })
            .when("/profile",{
                templateUrl : "profile/index.html"
            })
            .when("/results",{
                templateUrl : "results/index.html"
            })
            .when("/calendar",{
                templateUrl : "calendar/index.html",
                controller : "calendarController"
            })
            .when("/attendance",{
                templateUrl : "attendance/index.html"
            })
            .when("/error404",{
                templateUrl : "errors/404.html",
                controller : "errorController"
            })
            .when("/error500",{
                templateUrl : "errors/500.html",
                controller : "errorController"
            })
            .when("/group",{
                templateUrl : "group/index.html"
            })
            .when("/tests",{
                templateUrl : "tests/index.html"
            })
            .when("/interview",{
                templateUrl : "interview/index.html"
            })
            .otherwise({
                redirectTo : '/profile'
            })
    });
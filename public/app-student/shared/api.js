var STUDENTS_URL = "/api/students/";
var BASE_URL = "/api/students",
    PROFILE_URL = BASE_URL + '/profile',
    SCHEDULE_URL = BASE_URL + '/schedule',
    HOMEWORK_URL = BASE_URL + '/homeworks',
    ATTENDANCE_URL = BASE_URL + '/attendance',
    FILES_URL = BASE_URL + '/files',
    PAYMENTS_URL = BASE_URL + '/payments',
    GROUP_URL = BASE_URL + '/group',
    CHANNEL_URL = BASE_URL + '/channel',
    SELF_URL = BASE_URL + '/self',
    INTERVIEW_URL = BASE_URL + '/interview',
    TESTS_URL = BASE_URL + '/tests',
    RESULTS_URL = BASE_URL + '/results';



var createResource= function (response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 201) return cb(null);
    return cb(response);
};
var updateResource = function (response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null);
    return cb(response);
};
var deleteResource = function(response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null);
    return cb(response);
};
var getResource = function(response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null,response.data);
    return cb(response);
};
var errorCallback = function(response,cb) {
    var appHelper = angular.element(document.body).injector().get('appHelper'),
        appValues = angular.element(document.body).injector().get('appValues');
    appHelper.removeLoader();
    if (response.status == 403) {
        angular.element(document.querySelector('[ng-controller]')).injector().get('localStorageService').remove(appValues.LOCAL_STORAGE_KEYS.TOKEN);
        angular.element(document.querySelector('[ng-controller]')).injector().get('$window').location.href = "/";
        return;
    }

    return cb(response);
};
var successCallback = function (response,cb) {
    angular.element(document.body).injector().get('appHelper').removeLoader();
    if (response.status === 200) return cb(null);
    return cb(response);
};

app.factory('studentApi', function (Upload,$http,localStorageService,$window,appHelper,appValues) {
    return {
        setToken : function (token) {
            return localStorageService.set(appValues.LOCAL_STORAGE_KEYS.TOKEN,token);
        },
        getToken : function () {
            return localStorageService.get(appValues.LOCAL_STORAGE_KEYS.TOKEN)
        },
        checkAuthorization : function () {
            var token = this.getToken();
            if (!token) {
                $window.location.href = "/";
            }
        },
        removeToken : function () {
            return localStorageService.remove(appHelper.LOCAL.TOKEN);
        },
        getProfile : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(PROFILE_URL,{
                    headers : {
                        token : token
                    },
                    params : {
                        groupID : data.groupID
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getSchedule : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(SCHEDULE_URL,{
                    headers : {
                        token : token
                    },
                    params : {
                        groupID : data.groupID
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getHomeworks : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(HOMEWORK_URL,{
                    headers : {
                        token : token
                    },
                    params : {
                        groupID : data.groupID
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getAttendance : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(ATTENDANCE_URL,{
                    headers : {
                        token : token
                    },
                    params : {
                        groupID : data.groupID
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getFiles : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(FILES_URL,{
                    headers : {
                        token : token
                    },
                    params : {
                        groupID : data.groupID
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getPayments : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(PAYMENTS_URL,{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getGroupInfo : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            return $http
                .get(GROUP_URL,{
                    headers : {
                        token : token
                    },
                    params : {
                        groupID : data.groupID
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getPosts : function (obj,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            $http
                .get(CHANNEL_URL,{
                    headers : {
                        token : token
                    },
                    params : {
                        page : obj.page,
                        homework : obj.homework,
                        important : obj.important,
                        onlyFiles : obj.onlyFiles,
                        groupID : obj.groupID
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                }, function (response) {
                    return errorCallback(response,cb);
                });
        },
        getUserInfo : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            $http
                .get(SELF_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        addComment : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            $http
                .post(CHANNEL_URL + '/comments',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getGroup : function (cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            $http
                .get(GROUP_URL,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updateProfile : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            $http
                .put(SELF_URL ,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        updatePassword : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            $http
                .put(SELF_URL + '/password',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return updateResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getTests : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            $http
                .get(TESTS_URL,{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        assignTest : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            $http
                .post('/api/testing',data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return successCallback(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        resolveInterview : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            $http
                .get(INTERVIEW_URL,{
                    headers : {
                        token : token
                    },
                    params : data
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        addInterview : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            $http
                .post(INTERVIEW_URL,data,{
                    headers : {
                        token : token
                    }
                })
                .then(function (response) {
                    return createResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        },
        getResults : function (data,cb) {
            this.checkAuthorization();
            var token = this.getToken();
            appHelper.addLoader();
            $http
                .get(RESULTS_URL,{
                    headers : {
                        token : token
                    },
                    params :data
                })
                .then(function (response) {
                    return getResource(response,cb);
                },function (response) {
                    return errorCallback(response,cb);
                });
        }
    }
});
angular
    .module("EduNet")
    .filter("filesByKind",function () {
        return function (files,value) {
            if (value === '') return files;
            var results = [];
            files.forEach(function (file) {
                if (file.kind === value) results.push(file);

            });
            return results;
        }
    });
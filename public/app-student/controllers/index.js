angular
    .module("EduNet")
    .controller("mainController", function ($window,ngDialog,$rootScope,appHelper,$scope,$location,studentApi,studentHelper,appConstants,$translate) {
        var changeGroup = function (index) {
            $scope.index.activeGroupIndex = index;
            var group = appHelper.getUserInfo().groups[index];
            studentHelper.setActiveGroup(group);
            studentHelper.setActiveGroupIndex(index);
            appHelper.setActGroupID(group._id);
            $rootScope.$broadcast(appHelper.EVENTS.GROUP_CHANGED);
        };
        $rootScope.loader = [];
        var self = this;
        var loadUserInfo = function () {
            studentApi.getUserInfo(function (err,info) {
                if (err) return appHelper.showError(err);
                if (info.isTesting) return $window.location.href = "/test";
                var activeGroupIndex = studentHelper.getActiveGroupIndex();
                if (!activeGroupIndex || activeGroupIndex >= info.groups.length) {
                    activeGroupIndex = 0;
                    studentHelper.setActiveGroupIndex(0);
                }
                appHelper.setActGroupID(info.groups[activeGroupIndex]._id);
                studentHelper.setActiveGroup(info.groups[activeGroupIndex]);
                // var activeGroupID = appHelper.getActGroupID();
                // if (!activeGroupID) {
                //     activeGroupID = info.groups[0]._id;
                //     appHelper.setActGroupID(activeGroupID);
                // }
                // studentHelper.setActiveGroup(info.groups[activeGroupIndex]);
                // console.log(info);
                // console.log(activeGroupIndex);
                appHelper.resolveGroupTeacher(info.groups[activeGroupIndex]);
                $scope.name = info.firstName;
                $scope.imageUrl = info.imageUrl;
                $rootScope.name = info.firstName;
                $rootScope.info = info;
                appHelper.setUserInfo(info);
                appHelper.setUserType(appConstants.STUDENT);
                $scope.index = {
                    groups : info.groups,
                    activeGroupIndex : activeGroupIndex,
                    changeGroup : changeGroup
                };
                $rootScope.$broadcast(appHelper.EVENTS.INDEX_LOADED);
                $rootScope.indexLoaded = true;
            });
        };
        loadUserInfo();
        $rootScope.$on(appHelper.EVENTS.LOAD_INDEX,loadUserInfo);

        $scope.goTo = function (url) {
            $location.path(url);
        };
        $scope.editProfile = function () {
            ngDialog.open({
                template : "/app-student/views/profile/alerts/edit-profile.html",
                className : 'ngdialog-theme-default',
                width : 600
            });
        };
        $scope.openUserNav = function(){
            $scope.userNav = true;
        };
        $scope.hideUserNav = function(){
            $scope.userNav = false;
        };
        $scope.logout = function () {
            studentHelper.logout();
        };
        $scope.changeLang = function (lang) {

            $translate.use(lang);
            studentHelper.setLang(lang);
            $scope.langRu = studentHelper.getLang() == 'ru';
            $scope.langEn = studentHelper.getLang() == 'en';

        };
        var fixLang = function () {

            $scope.changeLang(studentHelper.getLang());


        };
        fixLang();

    });
angular
    .module("EduNet")
    .controller("testsController", function (studentConstants,$scope,studentApi,studentHelper,appHelper,ngDialog) {
        var loadTests = function () {
            studentApi.getTests({
                groupID : appHelper.getActGroupID()
            },function (err,data) {
                if (err) return appHelper.showError(err);
                self.active = data.active;
                self.ended = data.ended;
                appHelper.computeTestsDuration(self.active);
                appHelper.computeTestsDuration(self.ended);

            });
        };
        var starTest = function (testID) {
            ngDialog.open({
                template : "/app-student/views/tests/alerts/start-test.html",
                data : {
                    testID : testID
                },
                width : 500
            });
        };

        this.startTest = starTest;

        // INIT

        var self = this;




        loadTests();


    });

angular
    .module("EduNet")
    .controller("startTestController", function ($scope,studentApi,appHelper,$window) {
        var startTest = function () {
           studentApi.assignTest({
               testID : testID
           },function (err,data) {
               $scope.closeThisDialog();
               if (err) return appHelper.showFailAlertGeneralError();
               $window.location.href = "/test";
           });
        };

    //     INIT
        var self = this;
        var testID = $scope.ngDialogData.testID;
        if (!testID) return $scope.closeThisDialog();
        this.startTest = startTest;
    });

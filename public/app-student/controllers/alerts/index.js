angular
    .module("EduNet")
    .controller("alertController", function ($scope,appModalHelper,studentHelper,studentModalHelper) {
        // INIT
        var self = this;
        this.isSuccess = !!$scope.ngDialogData.isSuccess;
        this.message = $scope.ngDialogData.message;
        if (!this.message) return $scope.closeThisDialog();

        this.messages = this.isSuccess ? appModalHelper.successMessages.concat(studentModalHelper.successMessages) : appModalHelper.errorMessages.concat(studentModalHelper.errorMessages);

        var index = _.map(this.messages,function (message) {
            return message.value
        }).indexOf(self.message);
        if (index < 0) return $scope.closeThisDialog();
        this.text = this.messages[index].message;

    });

angular
    .module("EduNet")
    .controller("attendanceController", function ($rootScope,appHelper,$scope,studentApi,studentHelper,appConstants) {
        var loadAttendance = function () {
            studentApi.getAttendance({
                groupID : appHelper.getActGroupID()
            },function (err,appearance) {
                if (err) return appHelper.showFailAlertGeneralError();
                if (appearance.length != 0) {
                    self.hasAttendance = true;
                    self.attendances = studentHelper.buildAttendance(appearance);
                } else {
                    self.hasAttendance = false;
                }
            });
        };
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,function () {
            loadAttendance();
        });
        $rootScope.$on(appHelper.EVENTS.GROUP_CHANGED,function () {
            loadAttendance();
        });


        // INIT
        var self = this;
        this.STUDENT_APPEARANCE_PRESENCE = appConstants.STUDENT_APPEARANCE_PRESENCE;
        this.STUDENT_APPEARANCE_ABSENCE = appConstants.STUDENT_APPEARANCE_ABSENCE;
        this.STUDENT_APPEARANCE_ABSENCE_REASON = appConstants.STUDENT_APPEARANCE_ABSENCE_REASON;
        this.STUDENT_APPEARANCE_EMPTY_DAY = appConstants.STUDENT_APPEARANCE_EMPTY_DAY;
        if ($rootScope.indexLoaded == true) {
            loadAttendance();
        }

    });

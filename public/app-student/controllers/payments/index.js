angular
    .module("EduNet")
    .controller("paymentsController", function ($rootScope,$scope,studentApi,appHelper,studentHelper) {
        var loadPayments = function () {
            self.group = studentHelper.getActiveGroup();
            if (!self.group.price) return self.noAvailablePayments = true;
            self.noAvailablePayments = false;
            self.totalPayment = 0;
            studentApi.getPayments({
                groupID : self.group._id
            },function (err,payments) {
                if (err) return appHelper.showError(err);
                self.payments = payments;
                if (payments.length == 0) return self.noPayments = true;
                self.noPayments = false;
                _.each(payments,function (payment) {
                    self.lastPayment = payment;
                    self.totalPayment+=payment.value;
                });



            });
        };

        // INIT
        var self = this;
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,function () {
            loadPayments();
        });
        $rootScope.$on(appHelper.EVENTS.GROUP_CHANGED,function () {
            loadPayments();
        });
        if ($rootScope.indexLoaded == true) {
            loadPayments();
        }


    });

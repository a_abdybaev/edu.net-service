angular
    .module("EduNet")
    .controller("profileController", function ($rootScope,ngDialog,$scope,studentApi,studentHelper,appHelper,appConstants) {
        var editProfile = function () {
            ngDialog.open({
                template : "/app-student/views/profile/alerts/edit-profile.html",
                className : 'ngdialog-theme-default',
                width : 600
            });
        };
        var editPassword = function () {
            ngDialog.open({
                template : "/app-student/views/profile/alerts/edit-password.html",
                className : 'ngdialog-theme-default',
                width : 600
            });
        };
        var loadProfile = function () {
            studentApi.getProfile({
                groupID : appHelper.getActGroupID()
            },function (err,student) {
                if (err) return appHelper.showFailAlertGeneralError();
                self.imageUrl = appConstants.STUDENT_AVATAR_DEFAULT_URL_BIG;
                self.group  = student.groups[0];
                if (self.group.homeworks.length > 0) {
                    self.hasHomeworks = true;
                    self.homework = self.group.homeworks[0];
                } else {
                    self.hasHomeworks = false;
                }
                self.schedule = studentHelper.getProfileSchedule(self.group.schedule);
                self.student = student;
                if (self.group.tests.length == 0) self.hasTest = false;
                else {
                    self.test = null;
                    _.each(self.group.tests,function (test) {
                        var isAvailable = true;
                        _.each(self.student.answers,function (answer) {
                            if (_.isEqual(answer.testID,test._id)) isAvailable = false;
                        });
                        if (isAvailable) self.test = test;
                    });
                    self.hasTest = self.test ? true : false;
                }
                


            });
        };
        // INIT
        var self = this;
        this.isLoaded = false;
        this.editProfile = editProfile;
        this.editPassword = editPassword;
        this.info = appHelper.getUserInfo();
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,function () {
            loadProfile();
        });
        $rootScope.$on(appHelper.EVENTS.GROUP_CHANGED,function () {
            loadProfile();
        });
        if ($rootScope.indexLoaded == true) {
            loadProfile();
        }


        // development
    });

angular
    .module("EduNet")
    .controller("editProfileController", function (appValues,appConstants,studentConstants,appHelper,$rootScope,$scope,studentApi,studentHelper,appValidator) {
        var updateProfile = function () {
            if (!appValidator.isValidField(self,'firstName') || !appValidator.isValidField(self,'lastName') ||
            !appValidator.isValidEmailField(self,'email')
            ) return;
            var data = {
                firstName : self.firstName,
                lastName : self.lastName,
                email : self.email
            };
            studentApi.updateProfile(data,function (err) {
                $scope.closeThisDialog();
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(appValues.PROFILE_UPDATED);
                $rootScope.$broadcast(appValues.EVENTS.LOAD_INDEX);

            })
        };
        var self = this;
        var info = appHelper.getUserInfo();
        this.firstName = info.firstName;
        this.lastName = info.lastName;
        this.phone = info.phone;
        this.email = info.email;
        this.updateProfile = updateProfile;

    });

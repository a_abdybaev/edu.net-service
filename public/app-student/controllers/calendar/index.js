angular
    .module("EduNet")
    .controller("calendarController", function ($rootScope,appHelper,$scope,studentApi,studentHelper,ngDialog) {
        var loadSchedule = function () {
            studentApi.getSchedule({
                groupID : appHelper.getActGroupID()
            },function (err,schedule) {
                if (err) return appHelper.showError(err);
                $scope.activeMonth = new Date().getMonth();
                if (schedule.length === 0) {
                    $scope.hasSchedule = false;
                    $scope.calendar = studentHelper.getCalendar([],[]);
                } else {
                    $scope.hasSchedule = true;
                    studentApi.getHomeworks({
                        groupID : appHelper.getActGroupID()
                    },function (err,homeworks) {
                        if (err) return appHelper.showError(err);

                        $scope.calendar = studentHelper.getCalendar(schedule,homeworks);
                    });
                }
            });
        };
        $scope.showHomework = function (month,week,day) {
            var lesson = $scope.calendar[month].days[week][day];
            if (!lesson.hasHomework) return;
            ngDialog.open({
                template : "/app-student/views/calendar/alerts/info-homework.html",
                className : 'ngdialog-theme-default',
                controller : "homeworkInfoAlertController",
                data : {
                    homework : lesson.homework
                },
                width : 500
            });
        };
        $scope.nextMonth = function () {
            if ($scope.activeMonth < 11) {
                $scope.activeMonth = $scope.activeMonth+1;
            }
        };
        $scope.previousMonth = function () {
            if ($scope.activeMonth > 0) {
                $scope.activeMonth =  $scope.activeMonth-1;
            }
        };
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,function () {
            loadSchedule();
        });
        $rootScope.$on(appHelper.EVENTS.GROUP_CHANGED,function () {
            loadSchedule();
        });

        if ($rootScope.indexLoaded == true) {
            loadSchedule();
        }
    });

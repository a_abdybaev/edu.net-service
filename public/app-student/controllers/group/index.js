angular
    .module("EduNet")
    .controller("groupController", function ($scope,studentApi,appHelper,studentHelper,$rootScope) {
        var loadGroup = function () {
            studentApi.getGroupInfo({
                groupID : appHelper.getActGroupID()
            },function (err,group) {
                if (err) return appHelper.showError(err);
                self.group = group;
            });
        };

        // INIT
        var self = this;

        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,function () {
            loadGroup();
        });
        $rootScope.$on(appHelper.EVENTS.GROUP_CHANGED,function () {
            loadGroup();
        });
        if ($rootScope.indexLoaded == true) {
            loadGroup();
        }

    });

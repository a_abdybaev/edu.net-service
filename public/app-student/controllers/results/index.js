angular
    .module("EduNet")
    .controller("resultsController", function ($rootScope,$scope,studentApi,appHelper) {

        var initResults = function () {
            studentApi.getResults({
                groupID : appHelper.getActGroupID()
            },function (err,tests) {
                if (err) return appHelper.showError(err);
                if (_.isEmpty(tests)) return self.noTests = true;
                self.tests = tests;
            })
        };


        // INIT
        var self = this;
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,function () {
            initResults();
        });
        $rootScope.$on(appHelper.EVENTS.GROUP_CHANGED,function () {
            initResults();
        });
        if ($rootScope.indexLoaded == true) {
            initResults();
        }


    });

angular
    .module("EduNet")
    .controller("interviewController", function ($rootScope,$scope,studentApi,appHelper,studentHelper,appConstants) {
        var resolveInterview = function () {
            studentApi.resolveInterview({
                groupID : appHelper.getActGroupID()
            },function (err,data) {
                if (err) return appHelper.showError(err);
                self.isExist = data.isExist;
                self.interview = data.interview;
                if (!self.isExist) return;
                //  DEV
                _.each(data.interview.questions,function (question) {
                    question.value = 1;
                });

            });
        };
        var send = function () {
            if (!isValidInterview()) return;
            var results = _.map(self.interview.questions,function (question) {
                return question.value
            });
            studentApi.addInterview({
                groupID : appHelper.getActGroupID(),
                results : results
            },function (err) {
                if (err) return appHelper.showError(err);
                appHelper.showGeneralSuccessAlert();
                resolveInterview();
            });
        };
        var isValidInterview = function () {
            var result = true;
            _.each(self.interview.questions,function (question) {
                if (!question.value) result = false;
            });
            return result;
        };
    //     INIT
        this.send = send;
        var self = this;
        this.OUT_OF_TWO = appConstants.INTERVIEW.OUT_OF_TWO;
        this.OUT_OF_FIVE = appConstants.INTERVIEW.OUT_OF_FIVE;
        this.CUSTOM = appConstants.CUSTOM;




        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,function () {
            resolveInterview();
        });
        $rootScope.$on(appHelper.EVENTS.GROUP_CHANGED,function () {
            resolveInterview();
        });
        if ($rootScope.indexLoaded == true) {
            resolveInterview();
        }





    });

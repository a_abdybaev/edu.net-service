angular
    .module("EduNet")
    .controller("channelController", function (appHelper,$rootScope,ngDialog,$scope,studentApi,studentHelper,studentConstants,appConstants) {
        var loadPosts = function () {
            studentApi.getPosts(self.query,function (err,posts) {
                if (err) return appHelper.showError(err);
                if (posts.length < appConstants.POSTS_PER_PAGE) self.noAvailablePosts = true;
                if (posts.length == 0) return;
                self.noAvailablePosts = false;
                var data = studentHelper.buildChannelForPosts(posts);
                self.posts = self.posts.concat(data.posts);
                self.moreButtons = self.moreButtons.concat(data.moreButtons);
                self.limits = self.limits.concat(data.limits);

            });
        };
        var addComment = function (index) {
            var data = {
                content : self.posts[index].comment,
                postID : self.posts[index]._id,
                groupID : appHelper.getActGroupID()
            };
            if (!data.content || _.isEmpty(data.content)) return;
            data.isReply = !!self.posts[index].isReply;

            if (data.isReply) data.replyCommentID = self.posts[index].replyCommentID;
            studentApi.addComment(data,function (err) {
                if (err) return appHelper.showFailAlertGeneralError();
                appHelper.showSuccessAlert(appValues.COMMENT_ADDED);
                self.posts[index].comment = null;
                initChannel();
            });
        };
        var answerComment = function (postIndex,commentIndex) {
            var comment = self.posts[postIndex].comment;
            self.posts[postIndex].isReply = true;
            self.posts[postIndex].replyCommentID = self.posts[postIndex].comments[commentIndex]._id;
            if (!_.isEmpty(comment)) return;
            self.posts[postIndex].comment = self.posts[postIndex].comments[commentIndex].user.value.firstName + ", ";
        };
        var showComments = function (index) {
            self.posts[index].showComments = true;

        };
        var hideComments = function (index) {
            self.posts[index].showComments = false;
        };
        var loadPostsByType = function (index) {
            if (self.postsTypeIndex == index) return;
            self.postTypes[index].style = style;
            self.postTypes[self.postsTypeIndex].style = null;
            self.query = Object.assign({},self.postTypes[index].query);
            self.query.page = 0;
            self.query.groupID = appHelper.getActGroupID();
            self.posts = [];
            self.postsTypeIndex = index;
            self.noAvailablePosts = false;
            loadPosts();
        };
        var readMore = function (index){
            self.limits[index] = appConstants.POST_TEXT_LONG_LIMIT;
            self.moreButtons[index] = false;
        };
        var loadMorePosts = function () {
            self.query.page++;
            loadPosts();
        };
        var showPostImageDetailed = function (image) {
            ngDialog.open({
                template : "/app-teacher/views/group-info/channel/alerts/post-image-view.html",
                className : 'ngdialog-theme-default',
                appendClassName : 'post-image-view-modal',
                controller : "postImageDetailedAlertController",
                width : 800,
                data : {
                    image : image
                }
            });
        };

        var style = {
            'background-color' : '#f5f9fc',
            'border-left' : '2px solid #2196F3'
        };

        var initChannel = function () {

            self.group = studentHelper.getActiveGroup();
            self.postTypes[0].style = style;
            self.postsTypeIndex = 0;
            self.query = {
                page : 0,
                groupID : appHelper.getActGroupID()
            };
            self.posts = [];
            self.moreButtons = [];
            self.limits = [];
            loadPosts();
        };



        this.addComment = addComment;
        this.showComments = showComments;
        this.hideComments = hideComments;
        this.readMore = readMore;
        this.loadPostsByType = loadPostsByType;
        this.loadMorePosts = loadMorePosts;
        this.showPostImageDetailed = showPostImageDetailed;
        this.answerComment = answerComment;

        //     INIT


        var self = this;
        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,function () {
            initChannel();
        });
        $rootScope.$on(appHelper.EVENTS.GROUP_CHANGED,function () {
            initChannel();
        });




        this.postTypes = Object.assign({},appConstants.getPostTypesWithQueryObject);
        this.studentID = appHelper.getUserInfo()._id;
        this.gridOptions = {
            onClicked : function (image) {
                self.showPostImageDetailed(image);
            }
        };
        this.STUDENT = appConstants.STUDENT;
        this.TEACHER = appConstants.TEACHER;
        this.STUDENT_AVATAR_DEFAULT_URL = appConstants.STUDENT_AVATAR_DEFAULT_URL;
        this.TEACHER_AVATAR_DEFAULT_URL = appConstants.TEACHER_AVATAR_DEFAULT_URL;

        if ($rootScope.indexLoaded) {
            initChannel();
        }


    });

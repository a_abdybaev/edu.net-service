angular
    .module("EduNet")
    .controller("filesController", function ($rootScope,appHelper,$scope,studentApi,studentHelper) {
        var loadFiles = function () {
            studentApi.getFiles({
                groupID : appHelper.getActGroupID()
            },function (err,files) {
                if (err) return appHelper.showError(err);
                if (files.length == 0) return self.noFiles = true;
                self.noFiles = false;
                self.files = files;

            });
        };
        // INIT
        var self = this;
        this.files = [];


        $rootScope.$on(appHelper.EVENTS.INDEX_LOADED,function () {
            loadFiles();
        });
        $rootScope.$on(appHelper.EVENTS.GROUP_CHANGED,function () {
            loadFiles();
        });
        if ($rootScope.indexLoaded == true) {
            loadFiles();
        }



    });
var selfRouter = require('express').Router();
var authenticationController = require('../controllers/authentication');
selfRouter
    .route('/login-manager')
        .post(authenticationController.loginManager);

selfRouter
    .route('/login-user')
        .post(authenticationController.checkAuthBody,authenticationController.loginStudent,authenticationController.loginTeacher,authenticationController.loginAdmin);
module.exports = selfRouter;
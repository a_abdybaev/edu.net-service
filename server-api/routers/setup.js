"use strict";

var selfRouter = require('express').Router();
var setupController = require('../controllers/setup');
var tariffsController = require('../controllers/manager/tariffs');
selfRouter
    .route('/managers')
        .post(setupController.addManager)
        .get(setupController.getManagers)
        .delete(setupController.deleteManagers);

selfRouter
    .route('/tariffs')
        .post(setupController.createTariffs)
        .get(tariffsController.getTariffs);
selfRouter
    .route('/check-connection')
        .get(setupController.checkConnection);
module.exports = selfRouter;
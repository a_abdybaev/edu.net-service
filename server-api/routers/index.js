"use strict";

var selfRouter = require('express').Router()
    ,authRouter = require('./authentication')
    ,adminRouter = require('./admin')
    ,setupRouter = require('./setup')
    ,managerRouter = require('./manager')
    ,teacherRouter = require('./teacher')
    ,studentRouter = require('./student')
    ,developmentRouter = require('./development')
    ,landingRouter = require('./landing')
    ,testingRouter = require('./testing');

selfRouter.use('/auth',authRouter);
selfRouter.use('/admins',adminRouter);
selfRouter.use('/teachers',teacherRouter);
selfRouter.use('/managers',managerRouter);
selfRouter.use('/setup',setupRouter);
selfRouter.use('/students',studentRouter);
selfRouter.use('/landing',landingRouter);
selfRouter.use('/development',developmentRouter);
selfRouter.use('/testing',testingRouter);


module.exports = selfRouter;
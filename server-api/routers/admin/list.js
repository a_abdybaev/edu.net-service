var selfRouter = require('express').Router();
var authenticationController = require('../../controllers/authentication');
var listController = require('../../controllers/admin/list');


selfRouter
    .route('/groups')
    .get(authenticationController.authAdmin,listController.getListOfGroups);
selfRouter
    .route('/teachers')
    .get(authenticationController.authAdmin,listController.getListOfTeachers);

module.exports = selfRouter;

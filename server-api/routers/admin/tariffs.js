var selfRouter = require('express').Router();
var tariffsController = require('../../controllers/admin/tariffs');
var authentication = require('../../controllers/authentication').authAdmin;

selfRouter
    .route('')
    .get(authentication,tariffsController.getTariffs);


module.exports = selfRouter;




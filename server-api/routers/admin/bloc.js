var selfRouter = require('express').Router(),
    Authentication = require('../../controllers/authentication').authAdmin,
    GroupsController = require('../../controllers/admin/group');
selfRouter
    .route('')
    .post(Authentication,GroupsController.addBloc)
    .get(Authentication,GroupsController.getBlocs);

selfRouter
    .route('/:blocID')
    .get(Authentication,GroupsController.getBlocByID)
    .put(Authentication,GroupsController.editBlocByID)
    .delete(Authentication,GroupsController.delBlocByID);

selfRouter
    .route('/:blocID/assign')
    .delete(Authentication,GroupsController.delGroupFromBloc)
    .put(Authentication,GroupsController.assignGroupByBlocID);


module.exports = selfRouter;





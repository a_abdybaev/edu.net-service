var selfRouter = require('express').Router(),
    archiveStudentsController = require('../../../controllers/admin/students').archiveStudents,
    adminAuthentication = require('../../../controllers/authentication').authAdmin;
selfRouter
    .route('')
    .get(adminAuthentication,archiveStudentsController.getStudents);
selfRouter
    .route('/:studentID')
    .put(adminAuthentication,archiveStudentsController.updateStudentByID)
    .get(adminAuthentication,archiveStudentsController.getStudentByID)
    .delete(adminAuthentication,archiveStudentsController.deleteStudentByID);
selfRouter
    .route('/:studentID/assign')
    .get(adminAuthentication,archiveStudentsController.getAvailableGroupsToAssignStudent)
    .post(adminAuthentication,archiveStudentsController.assignStudentByID);
selfRouter
    .route('/:studentID/restore')
    .post(adminAuthentication,archiveStudentsController.restoreStudentByID);



module.exports = selfRouter;

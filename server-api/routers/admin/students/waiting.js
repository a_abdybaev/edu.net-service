var selfRouter = require('express').Router(),
    waitingStudentsController = require('../../../controllers/admin/students').waitingStudents,
    adminAuthentication = require('../../../controllers/authentication').authAdmin;
selfRouter
    .route('')
    .post(adminAuthentication,waitingStudentsController.addStudent)
    .get(adminAuthentication,waitingStudentsController.getStudents);

selfRouter
    .route('/groups/assign')
    .put(adminAuthentication,waitingStudentsController.assignStudents)
    .get(adminAuthentication,waitingStudentsController.getAvailableGroupToAssignStudents)
    .post(adminAuthentication,waitingStudentsController.formGroup);

selfRouter
    .route('/:studentID')
    .get(adminAuthentication,waitingStudentsController.getStudentByID)
    .put(adminAuthentication,waitingStudentsController.updateStudentByID)
    .delete(adminAuthentication,waitingStudentsController.archiveStudentByID);

module.exports = selfRouter;




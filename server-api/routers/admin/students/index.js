var selfRouter = require('express').Router();

selfRouter.use('/waiting',require('./waiting'));
selfRouter.use('/archive',require('./archive'));
selfRouter.use('/active',require('./active'));
selfRouter.use('/list',require('./list'));

module.exports = selfRouter;

var selfRouter = require('express').Router(),
    listStudentsController = require('../../../controllers/admin/students').listStudents,
    adminAuthentication = require('../../../controllers/authentication').authAdmin;

selfRouter
    .route('/:studentID')
    .get(adminAuthentication,listStudentsController.getStudentByID);



module.exports = selfRouter;

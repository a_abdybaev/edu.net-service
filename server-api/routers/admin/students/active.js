var selfRouter = require('express').Router(),
    activeStudentsController = require('../../../controllers/admin/students').activeStudents,
    adminAuthentication = require('../../../controllers/authentication').authAdmin;

selfRouter
    .route('')
    .post(adminAuthentication,activeStudentsController.addStudents)
    .get(adminAuthentication,activeStudentsController.getStudents);

selfRouter
    .route('/:studentID')
    .delete(adminAuthentication,activeStudentsController.archiveStudentByID)
    .put(adminAuthentication,activeStudentsController.updateStudentByID)
    .get(adminAuthentication,activeStudentsController.getStudentByID);
selfRouter
    .route('/:studentID/assign')
    .post(adminAuthentication,activeStudentsController.assignStudentForGroup)
    .get(adminAuthentication,activeStudentsController.getAvailableGroupsToAssignStudent);

module.exports = selfRouter;

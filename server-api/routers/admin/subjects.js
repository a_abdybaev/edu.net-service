
var selfRouter = require('express').Router();
var subjectsController = require('../../controllers/admin/subjects');
var authenticationController = require('../../controllers/authentication');
selfRouter
    .route('')
    .post(authenticationController.authAdmin,subjectsController.addSubject)
    .get(authenticationController.authAdmin,subjectsController.getSubjects);
selfRouter
    .route('/:subjectID')
    .get(authenticationController.authAdmin,subjectsController.getSubjectByID)
    .delete(authenticationController.authAdmin,subjectsController.deleteSubjectByID)
    .put(authenticationController.authAdmin,subjectsController.updateSubjectByID);


module.exports = selfRouter;




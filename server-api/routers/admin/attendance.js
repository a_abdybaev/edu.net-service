var selfRouter = require('express').Router();
var authenticationController = require('../../controllers/authentication');
var attendanceController = require('../../controllers/admin/attendance');

selfRouter
    .route('')
    .get(authenticationController.authAdmin,attendanceController.getAttendance);

module.exports = selfRouter;
var SelfRouter = require('express').Router();

var ClassroomRouter = require('./classroom'),
    GroupsRouter = require('./group'),
    TeachersRouter = require('./teacher'),
    StudentsRouter = require('./students'),
    ScheduleRouter = require('./schedule'),
    AdminSelfRouter = require('./self'),
    ListRouter = require('./list'),
    ApplicationsRouter = require('./application'),
    NotesRouter = require('./notes'),
    PaymentsRouter = require('./payments'),
    SubjectsRouter = require('./subjects'),
    TestsRouter = require('./tests'),
    AttendanceRouter = require('./attendance'),
    TariffsRouter = require('./tariffs'),
    InterviewRouter = require('./interview'),
    GroupBlocRouter = require('./bloc');

SelfRouter.use('/classrooms',ClassroomRouter);
SelfRouter.use('/groups',GroupsRouter);
SelfRouter.use('/teachers',TeachersRouter);
SelfRouter.use('/schedule',ScheduleRouter);
SelfRouter.use('/students',StudentsRouter);
SelfRouter.use('/self',AdminSelfRouter);
SelfRouter.use('/list',ListRouter);
SelfRouter.use('/applications',ApplicationsRouter);
SelfRouter.use('/attendance',AttendanceRouter);
SelfRouter.use('/notes',NotesRouter);
SelfRouter.use('/payments',PaymentsRouter);
SelfRouter.use('/subjects',SubjectsRouter);
SelfRouter.use('/tests',TestsRouter);
SelfRouter.use('/tariffs',TariffsRouter);
SelfRouter.use('/interview',InterviewRouter);
SelfRouter.use('/blocs',GroupBlocRouter);

module.exports = SelfRouter;
var Authentication = require('../../controllers/authentication').authAdmin;
var Controller = require('../../controllers/admin/teacher');

var SelfRouter = require('express').Router();
SelfRouter
    .route('')
        .post(Authentication,Controller.addTeacher)
        .get(Authentication,Controller.getTeachers);
SelfRouter
    .route('/:teacherID')
        .get(Authentication,Controller.getTeacherByID)
        .put(Authentication,Controller.updateTeacherByID)
        .delete(Authentication,Controller.deleteTeacherByID);
SelfRouter
    .route('/:teacherID/schedule')
        .get(Authentication,Controller.getScheduleByTeacherID);

module.exports = SelfRouter;
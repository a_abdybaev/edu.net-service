var selfRouter = require('express').Router();
var authenticationController = require('../../controllers/authentication');
var paymentsController = require('../../controllers/admin/payments');

selfRouter
    .route('')
    .post(authenticationController.authAdmin,paymentsController.addPayment)
    .get(authenticationController.authAdmin,paymentsController.getPayments);

selfRouter
    .route('/:paymentID')
    .put(authenticationController.authAdmin,paymentsController.updatePaymentByID)
    .get(authenticationController.authAdmin,paymentsController.getPaymentByID)
    .delete(authenticationController.authAdmin,paymentsController.deletePaymentByID);

module.exports = selfRouter;

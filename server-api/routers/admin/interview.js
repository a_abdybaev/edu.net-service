var selfRouter = require('express').Router();
var adminAuthentication = require('../../controllers/authentication').authAdmin;
var interviewController = require('../../controllers/admin/interview');

selfRouter
    .route('/')
    .post(adminAuthentication,interviewController.startInterview);

module.exports = selfRouter;

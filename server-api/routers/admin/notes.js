var selfRouter = require('express').Router();
var authenticationController = require('../../controllers/authentication');
var notesController = require('../../controllers/admin/notes');

selfRouter
    .route('/:noteID')
    .put(authenticationController.authAdmin,notesController.updateNoteByID)
    .get(authenticationController.authAdmin,notesController.getNoteByID)
    .delete(authenticationController.authAdmin,notesController.deleteNoteByID);

selfRouter
    .route('')
    .get(authenticationController.authAdmin,notesController.getNotesByGroupID)
    .post(authenticationController.authAdmin,notesController.addNoteByGroupID);

module.exports = selfRouter;

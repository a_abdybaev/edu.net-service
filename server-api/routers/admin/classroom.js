var selfRouter = require('express').Router();
var authenticationController = require('../../controllers/authentication');
var classroomForAdminController = require('../../controllers/admin/classroom');

selfRouter
    .route('')
    .post(authenticationController.authAdmin,classroomForAdminController.addClassroom)
    .get(authenticationController.authAdmin,classroomForAdminController.getClassrooms);
selfRouter
    .route('/:classroomID')
    .get(authenticationController.authAdmin,classroomForAdminController.getClassroomByID)
    .delete(authenticationController.authAdmin,classroomForAdminController.deleteClassroomByID)
    .put(authenticationController.authAdmin,classroomForAdminController.updateClassroomByID);

//selfRouter
//    .route('/:classroomID/schedule')
//        .get(authenticationController.authAdmin,classroomForAdminController.getScheduleByClassroomID);

module.exports = selfRouter;
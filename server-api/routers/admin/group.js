var SelfRouter = require('express').Router(),
    _Authentication = require('../../controllers/authentication').authAdmin,
    Controller = require('../../controllers/admin/group');

SelfRouter
    .route('')
    .post(_Authentication,Controller.addGroup)
    .get(_Authentication,Controller.getGroups);

SelfRouter
    .route('/:groupID')
    .put(_Authentication,Controller.updateGroupByID)
    .get(_Authentication,Controller.getGroupByID)
    .delete(_Authentication,Controller.deleteGroupByID);

SelfRouter
    .route('/:groupID/schedule')
    .get(_Authentication,Controller.getScheduleByGroupID)
    .put(_Authentication,Controller.updateScheduleByGroupID)
    .delete(_Authentication,Controller.deleteScheduleByGroupID);

SelfRouter
    .route('/:groupID/teachers')
    .get(_Authentication,Controller.getGroupTeachers);

SelfRouter
    .route('/:groupID/teachers/available')
    .get(_Authentication,Controller.getAvailableTeachersByGroupID);

SelfRouter
    .route('/:groupID/students/:studentID')
    .delete(_Authentication,Controller.deleteStudentFromGroup);
SelfRouter
    .route('/:groupID/students')
    .get(_Authentication,Controller.getStudentsByGroupID);

module.exports = SelfRouter;

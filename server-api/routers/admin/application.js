var selfRouter = require('express').Router();
var authenticationController = require('../../controllers/authentication');
var applicationsController = require('../../controllers/admin/application');

selfRouter
    .route('')
        .get(authenticationController.authAdmin,applicationsController.getApplications);
selfRouter
    .route('/:applicationID')
        .get(authenticationController.authAdmin,applicationsController.getApplicationByID);

selfRouter
    .route('/:applicationID/answer')
        .post(authenticationController.authAdmin,applicationsController.addApplicationAnswer)
        .put(authenticationController.authAdmin,applicationsController.updateApplicationAnswer)
        .delete(authenticationController.authAdmin,applicationsController.deleteApplicationAnswer);
module.exports = selfRouter;
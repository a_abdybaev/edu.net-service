var SelfRouter = require('express').Router(),
    Authentication = require('../../controllers/authentication').authAdmin,
    Controller = require('../../controllers/admin/schedule');

SelfRouter
    .route('')
    .post(Authentication,Controller.addSchedule)
    .get(Authentication,Controller.getSchedule);

SelfRouter
    .route('/query')
    .get(Authentication,Controller.getByQuery);
SelfRouter
    .route('/available-groups')
    .get(Authentication,Controller.getAvailableGroupsBySchedule);

module.exports = SelfRouter;

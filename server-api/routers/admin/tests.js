var selfRouter = require('express').Router();
var adminAuthentication = require('../../controllers/authentication').authAdmin;
    path = require('path'),
    multipart = require('connect-multiparty'),
    temporaryFilesDirectory = path.join(__publicPath,'storage/temporary-files'),
    filesMiddleware = multipart({
    uploadDir : temporaryFilesDirectory
});


var testsController = require('../../controllers/admin/tests');

selfRouter
    .route('')
    .post(adminAuthentication,filesMiddleware,testsController.addTest)
    .get(adminAuthentication,testsController.getTests);
selfRouter
    .route('/:testID')
    .get(adminAuthentication,testsController.getTestByID)
    .put(adminAuthentication,testsController.updateTestByID)
    .delete(adminAuthentication,testsController.deleteTestByID);
selfRouter
    .route('/:testID/sections/:sectionIndex')
    .put(adminAuthentication,filesMiddleware,testsController.updateSectionByIndex)
    .delete(adminAuthentication,testsController.deleteSectionByIndex);




selfRouter
    .route('/:testID/assign')
    .post(adminAuthentication,testsController.assignTest)
    .get(adminAuthentication,testsController.getAvailableGroupsForTest);


module.exports = selfRouter;





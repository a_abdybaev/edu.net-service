var selfRouter = require('express').Router(),
    adminAuthentication = require('../../controllers/authentication').authAdmin,
    selfController = require('../../controllers/admin/self');

selfRouter
    .route('')
    .get(adminAuthentication,selfController.info)
    .put(adminAuthentication,selfController.updateProfile);

selfRouter
    .route('/image')
    .put(adminAuthentication,selfController.updateImage);

selfRouter
    .route('/password')
    .post(selfController.recoverPassword)
    .put(adminAuthentication,selfController.updatePassword);

selfRouter
    .route('/recover-password')
    .post(selfController.requestRecoverPassword)
    .put(selfController.recoverPassword);
selfRouter
    .route('/recover-password-code')
    .post(selfController.isValidCode);
selfRouter
    .route('/center')
    .put(adminAuthentication,selfController.updateCenterID)
    .get(adminAuthentication,selfController.getCenterInfo);

module.exports = selfRouter;

var selfRouter = require('express').Router(),
    authentication = require('../../controllers/authentication').authManager,
    interviewController = require('../../controllers/manager/interview');

selfRouter
    .route('/')
    .post(authentication,interviewController.addInterview);

module.exports = selfRouter;


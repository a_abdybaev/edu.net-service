var selfRouter = require('express').Router();
var authentication = require('../../controllers/authentication').authManager;
var centerController = require('../../controllers/manager/center');
selfRouter
    .route('')
        .post(authentication,centerController.addCenter)
        .get(authentication,centerController.getCenters);

selfRouter
    .route('/:centerID')
        .delete(authentication,centerController.deleteCenterByID)
        .put(authentication,centerController.updateByID)
        .get(authentication,centerController.getCenterByID);
selfRouter
    .route('/:centerID/payment')
        .delete(authentication,centerController.setPaymentByCenterID);

module.exports = selfRouter;
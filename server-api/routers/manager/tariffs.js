var selfRouter = require('express').Router();
var authManager = require('../../controllers/authentication').authManager;
var tariffsController = require('../../controllers/manager/tariffs');

selfRouter
    .route('')
    .post(authManager,tariffsController.addTariff)
    .get(authManager,tariffsController.getTariffs);

selfRouter
    .route('/:tariffID')
    .delete(authManager,tariffsController.deleteTariffByID);
module.exports = selfRouter;
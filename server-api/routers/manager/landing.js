var selfRouter = require('express').Router();
var managerAuthentication = require('../../controllers/authentication').authManager;
var landingController = require('../../controllers/manager/landing');
selfRouter
    .route('/requests')
    .get(managerAuthentication,landingController.getRequests);


module.exports = selfRouter;
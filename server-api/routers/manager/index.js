var selfRouter = require('express').Router();

var AdminRouter = require('./admin'),
    CenterRouter= require('./center'),
    PanelRouter = require('./panel'),
    TariffRouter = require('./tariffs'),
    LandingRouter = require('./landing'),
    ClientsRouter = require('./client'),
    InterviewRouter = require('./interview');

selfRouter.use('/admins',AdminRouter);
selfRouter.use('/centers',CenterRouter);
selfRouter.use('/tariffs',TariffRouter);
selfRouter.use('/panel',PanelRouter);
selfRouter.use('/landing',LandingRouter);
selfRouter.use('/interview',InterviewRouter);
selfRouter.use('/clients',ClientsRouter);
module.exports = selfRouter;
var selfRouter = require('express').Router();
var managerAuthentication = require('../../controllers/authentication').authManager;
var panelController = require('../../controllers/manager/panel');
selfRouter
    .route('/pull')
    .get(managerAuthentication,panelController.pullGit);
selfRouter
    .route('/merge')
    .get(managerAuthentication,panelController.mergeGit);
selfRouter
    .route('/push')
    .get(managerAuthentication,panelController.pushGit);
selfRouter
    .route('/restart')
    .get(managerAuthentication,panelController.restart);

selfRouter
    .route('/logs')
    .get(managerAuthentication,panelController.logs);
module.exports = selfRouter;
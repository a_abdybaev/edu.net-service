var selfRouter = require('express').Router(),
    authentication = require('../../controllers/authentication').authManager,
    clientsController = require('../../controllers/manager/clients');

selfRouter
    .route('')
    .post(authentication,clientsController.add)
    .get(authentication,clientsController.get);

selfRouter
    .route('/:clientID')
    .delete(authentication,clientsController.delByID)
    .put(authentication,clientsController.editByID)
    .get(authentication,clientsController.getByID);
selfRouter
    .route('/:clientID/payment')
    .delete(authentication,clientsController.setPaymentByID);


module.exports = selfRouter;
var selfRouter = require('express').Router(),
    Authentication = require('../../controllers/authentication').authManager,
    AdminModule = require('../../controllers/manager/admin');

selfRouter
    .route('')
        .post(Authentication,AdminModule.add)
        .get(Authentication,AdminModule.get)
        .delete(Authentication,AdminModule.delete);

selfRouter
    .route('/:adminID')
    .delete(Authentication,AdminModule.delByID);

module.exports = selfRouter;

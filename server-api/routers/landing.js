"use strict";

var selfRouter = require('express').Router()
    , landingController = require('../controllers/landing')
    , managerAuthentication = require('../controllers/authentication').authManager;
selfRouter
    .route('/requests')
    .post(landingController.addRequest);


module.exports = selfRouter;
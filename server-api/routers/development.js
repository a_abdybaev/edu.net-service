"use strict";

var selfRouter = require('express').Router();
var developmentController = require('../controllers/development');

selfRouter
    .route('/create-waiting-student')
    .get(developmentController.createWaitingStudent);
selfRouter
    .route('/delete-tests')
    .get(developmentController.deleteAnswerSheets);
selfRouter
    .route('/create-development-tariff')
    .get(developmentController.addTariff);
selfRouter
    .route('/get-interview')
    .get(developmentController.getInterview);

module.exports = selfRouter;
var selfRouter = require('express').Router(),
    authenticationController = require('../../controllers/authentication'),
    filesController = require('../../controllers/student/files');

selfRouter
    .route('')
    .get(authenticationController.authStudent,filesController.getFiles);


module.exports = selfRouter;
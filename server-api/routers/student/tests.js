var selfRouter = require('express').Router(),
    studentAuthentication = require('../../controllers/authentication').authStudent,
    testsController = require('../../controllers/student/tests');

selfRouter
    .route('')
    .get(studentAuthentication,testsController.getTests);

module.exports = selfRouter;
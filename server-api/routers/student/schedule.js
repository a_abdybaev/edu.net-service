var selfRouter = require('express').Router(),
    authenticationController = require('../../controllers/authentication'),
    scheduleController = require('../../controllers/student/schedule');

selfRouter
    .route('')
    .get(authenticationController.authStudent,scheduleController.getSchedule);


module.exports = selfRouter;
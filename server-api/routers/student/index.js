var selfRouter = require('express').Router();

var profileRouter = require('./profile'),
    scheduleRouter = require('./schedule'),
    homeworkRouter = require('./homework'),
    attendanceRouter = require('./attendance'),
    filesRouter = require('./files'),
    channelRouter = require('./channel'),
    groupRouter = require('./group'),
    paymentsRouter = require('./payments'),
    studentSelfRouter = require('./self'),
    testsRouter = require('./tests'),
    resultsRouter = require('./results'),
    interviewRouter = require('./interview');


selfRouter.use('/profile',profileRouter);
selfRouter.use('/schedule',scheduleRouter);
selfRouter.use('/homeworks',homeworkRouter);
selfRouter.use('/attendance',attendanceRouter);
selfRouter.use('/files',filesRouter);
selfRouter.use('/channel',channelRouter);
selfRouter.use('/group',groupRouter);
selfRouter.use('/payments',paymentsRouter);
selfRouter.use('/self',studentSelfRouter);
selfRouter.use('/tests',testsRouter);
selfRouter.use('/interview',interviewRouter);
selfRouter.use('/results',resultsRouter);

module.exports = selfRouter;

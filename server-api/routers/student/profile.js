var selfRouter = require('express').Router(),
    authenticationController = require('../../controllers/authentication'),
    profileController = require('../../controllers/student/profile');

selfRouter
    .route('')
    .get(authenticationController.authStudent,profileController.getProfile);


module.exports = selfRouter;
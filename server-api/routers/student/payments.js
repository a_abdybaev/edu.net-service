var selfRouter = require('express').Router(),
    authenticationController = require('../../controllers/authentication'),
    paymentsController = require('../../controllers/student/payments');

selfRouter
    .route('')
    .get(authenticationController.authStudent,paymentsController.getPayments);


module.exports = selfRouter;
var selfRouter = require('express').Router(),
    studentAuthentication = require('../../controllers/authentication').authStudent,
    resultsController = require('../../controllers/student/results');

selfRouter
    .route('')
    .get(studentAuthentication,resultsController.getResults);

module.exports = selfRouter;
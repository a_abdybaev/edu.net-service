var selfRouter = require('express').Router(),
    authentication = require('../../controllers/authentication').authStudent,
    interviewController = require('../../controllers/student/interview');

selfRouter
    .route('')
    .get(authentication,interviewController.resolve)
    .post(authentication,interviewController.addAnswer);


module.exports = selfRouter;
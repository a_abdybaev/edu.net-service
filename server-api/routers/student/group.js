var selfRouter = require('express').Router(),
    authenticationController = require('../../controllers/authentication'),
    groupController = require('../../controllers/student/group');

selfRouter
    .route('')
    .get(authenticationController.authStudent,groupController.getGroupInfo);


module.exports = selfRouter;
var selfRouter = require('express').Router(),
    authenticationController = require('../../controllers/authentication'),
    attendanceController = require('../../controllers/student/attendance');

selfRouter
    .route('')
    .get(authenticationController.authStudent,attendanceController.getAttendance);


module.exports = selfRouter;
var selfRouter = require('express').Router(),
    authentication = require('../../controllers/authentication').authStudent,
    channelController = require('../../controllers/student/channel');

selfRouter
    .route('')
    .get(authentication,channelController.getPosts);
selfRouter
    .route('/:postID/comments')
    .post(authentication,channelController.replyComment);
selfRouter
    .route('/comments')
    .post(authentication,channelController.addComment);


module.exports = selfRouter;
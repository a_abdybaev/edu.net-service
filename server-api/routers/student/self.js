var selfRouter = require('express').Router(),
    authentication = require('../../controllers/authentication').authStudent,
    selfController = require('../../controllers/student/self');

selfRouter
    .route('/image')
    .put(authentication,selfController.updateImage);
selfRouter
    .route('/password')
    .put(authentication,selfController.updatePassword);
selfRouter
    .route('')
    .get(authentication,selfController.getUserInfo)
    .put(authentication,selfController.updateUserInfo);

module.exports = selfRouter;
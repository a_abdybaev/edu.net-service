var selfRouter = require('express').Router(),
    authenticationController = require('../../controllers/authentication'),
    homeworkController = require('../../controllers/student/homework');

selfRouter
    .route('')
    .get(authenticationController.authStudent,homeworkController.getHomeworks);


module.exports = selfRouter;
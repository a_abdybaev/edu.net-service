var selfRouter = require('express').Router(),
    selfController = require('../../controllers/teacher/self'),
    authenticationController = require('../../controllers/authentication');

selfRouter
    .route('')
        .get(authenticationController.authTeacher,selfController.info)
        .put(authenticationController.authTeacher,selfController.updateProfile);

selfRouter
    .route('/image')
    .post(authenticationController.authTeacher,selfController.updateImage);
selfRouter
    .route('/password')
    .post(selfController.recoverPassword)
    .put(authenticationController.authTeacher,selfController.updatePassword);

module.exports = selfRouter;

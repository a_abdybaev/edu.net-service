var selfRouter = require('express').Router(),
    authenticationController = require('../../controllers/authentication'),
    attendanceController = require('../../controllers/teacher/attendance');

selfRouter
    .route('')
    .post(authenticationController.authTeacher,attendanceController.addAttendance);

module.exports = selfRouter;
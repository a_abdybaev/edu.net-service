var selfRouter = require('express').Router(),
    Authentication = require('../../controllers/authentication').authTeacher,
    Controller = require('../../controllers/teacher/application');

selfRouter
    .route('')
    .get(Authentication,Controller.getApplications)
    .post(Authentication,Controller.addApplication);


selfRouter
    .route('/groups/:groupID')
    .get(Authentication,Controller.getApplicationsByGroupID);

selfRouter
    .route('/:applicationID')
    .put(Authentication,Controller.updateApplicationByID)
    .delete(Authentication,Controller.deleteApplicationByID)
    .get(Authentication,Controller.getApplicationByID);


module.exports = selfRouter;

var selfRouter = require('express').Router(),
    authentication = require('../../controllers/authentication').authTeacher,
    filesMiddleWare = require('system/helper').filesMiddleWare,
    filesController = require('../../controllers/teacher/files');


selfRouter
    .route('')
    .get(authentication,filesController.getFiles)
    .post(authentication,filesMiddleWare,filesController.addFile);

selfRouter
    .route('/:fileID')
    .delete(authentication,filesController.deleteFileByID);
selfRouter
    .route('/groups/:groupID')
    .get(authentication,filesController.getFilesByGroupID);

module.exports = selfRouter;
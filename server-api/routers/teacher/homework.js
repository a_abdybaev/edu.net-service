var selfRouter = require('express').Router(),
    Authentication = require('../../controllers/authentication').authTeacher,
    HomeworkController = require('../../controllers/teacher/homework');

selfRouter
    .route('')
    .get(Authentication,HomeworkController.get)
    .post(Authentication,HomeworkController.add);


selfRouter
    .route('/:homeworkID')
        .delete(Authentication,HomeworkController.delByID)
        .put(Authentication,HomeworkController.editByID)
        .get(Authentication,HomeworkController.getByID);


module.exports = selfRouter;
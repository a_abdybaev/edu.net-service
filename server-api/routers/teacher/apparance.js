var selfRouter = require('express').Router(),
    teacherAuthentication = require('../../controllers/authentication').authTeacher,
    appearanceController = require('../../controllers/teacher/appearance');

selfRouter
    .route('')
    .post(teacherAuthentication,appearanceController.addAppearanceByGroupID)
    .get(teacherAuthentication,appearanceController.getAppearanceByGroupID);

module.exports = selfRouter;
var selfRouter = require('express').Router(),
    authenticationController = require('../../controllers/authentication'),
    channelController = require('../../controllers/teacher/channel'),
    filesMiddleWare = require('system/helper').filesMiddleWare;

selfRouter
    .route('')
    .post(authenticationController.authTeacher,filesMiddleWare,channelController.addPost)
    .get(authenticationController.authTeacher,channelController.getPosts);

selfRouter
    .route('/:postID')
    .put(authenticationController.authTeacher,filesMiddleWare,channelController.updatePostByID)
    .delete(authenticationController.authTeacher,channelController.deletePostByID);

selfRouter
    .route('/:postID/comments')
    .post(authenticationController.authTeacher,channelController.addComment);


module.exports = selfRouter;

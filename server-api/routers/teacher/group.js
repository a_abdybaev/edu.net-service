var selfRouter = require('express').Router(),
    authentication = require('../../controllers/authentication').authTeacher,
    groupController = require('../../controllers/teacher/group');

selfRouter
    .route('')
    .get(authentication,groupController.getGroups);

selfRouter
    .route('/:groupID')
        .get(authentication,groupController.getGroupByID);
selfRouter
    .route('/:groupID/tests/results')
        .get(authentication,groupController.getTestResultsByGroupID);
selfRouter
    .route('/:groupID/tests/students/results')
    .get(authentication,groupController.getStudentsResultsByGroupID);

module.exports = selfRouter;
var selfRouter = require('express').Router(),
    authenticationController = require('../../controllers/authentication'),
    scheduleController =require('../../controllers/teacher/schedule');

selfRouter
    .route('')
        .get(authenticationController.authTeacher,scheduleController.getSchedule);


module.exports = selfRouter;
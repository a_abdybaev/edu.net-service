var selfRouter = require('express').Router(),
    GroupsRouter = require('./group'),
    HomeworkRouter = require('./homework'),
    ChannelRouter = require('./channel'),
    ApplicationsRouter = require('./application'),
    FilesRouter = require('./files'),
    SelfRouter = require('./self'),
    ListRouter = require('./list'),
    AttendanceRouter = require('./attendance'),
    ScheduleRouter = require('./schedule'),
    AppearanceRouter = require('./apparance'),
    TestsRouter = require('./tests');




selfRouter.use('/groups',GroupsRouter);
selfRouter.use('/homeworks',HomeworkRouter);
selfRouter.use("/channel",ChannelRouter);
selfRouter.use('/applications',ApplicationsRouter);
selfRouter.use('/files',FilesRouter);
selfRouter.use('/self',SelfRouter);
selfRouter.use('/lists',ListRouter);
selfRouter.use('/attendance',AttendanceRouter);
selfRouter.use('/schedule',ScheduleRouter);
selfRouter.use('/appearance',AppearanceRouter);
selfRouter.use('/tests',TestsRouter);




module.exports = selfRouter;


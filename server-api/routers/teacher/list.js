var selfRouter = require('express').Router(),
    authenticationController = require('../../controllers/authentication'),
    teacherListController =require('../../controllers/teacher/list');

selfRouter
    .route('/groups')
    .get(authenticationController.authTeacher,teacherListController.getListOfGroups);
selfRouter
    .route('/students')
    .post(authenticationController.authTeacher,teacherListController.getListOfStudentsByGroupID);


module.exports = selfRouter;
var selfRouter = require('express').Router();
var Authentication = require('../../controllers/authentication').authTeacher,
    filesMiddleWare = require('system/helper').filesMiddleWare,
    Controller = require('../../controllers/teacher/tests');

selfRouter
    .route('')
    .post(Authentication,filesMiddleWare,Controller.addTest)
    .get(Authentication,Controller.getTests);
selfRouter
    .route('/:testID')
    .get(Authentication,Controller.getTestByID)
    .put(Authentication,Controller.updateTestByID)
    .delete(Authentication,Controller.deleteTestByID);
selfRouter
    .route('/:testID/sections/:sectionIndex')
    .put(Authentication,filesMiddleWare,Controller.updateSectionByIndex)
    .delete(Authentication,Controller.deleteSectionByIndex);
selfRouter
    .route('/:testID/assign')
    .post(Authentication,Controller.assignTest)
    .delete(Authentication,Controller.removeTestFromGroup)
    .get(Authentication,Controller.getAvailableGroupsForTest);
selfRouter
    .route('/:testID/results')
    .get(Authentication,Controller.getGroupResultsByTestID);

module.exports = selfRouter;





var selfRouter = require('express').Router(),
    testingController = require('../../controllers/testing'),
    studentAuthentication = require('../../controllers/authentication').authStudent;



selfRouter
    .route('/')
        .post(studentAuthentication,testingController.assignTest)
        .delete(studentAuthentication,testingController.cancelTest)
        .get(studentAuthentication,testingController.resolveTesting);


selfRouter
    .route('/sections')
        .post(studentAuthentication,testingController.beginSection)
        .put(studentAuthentication,testingController.endSection);
selfRouter
    .route('/dev')
    .get(testingController.dev);




module.exports = selfRouter;

module.exports.createWaitingStudent = function (req,res,next) {
    request({
        method : 'post',
        body : {
            firstName : faker.name.firstName(),
            lastName : faker.name.lastName(),
            phone : faker.random.number({
                min : 1000000000,
                max : 9999999999
            })
        },
        json : true,
        headers : {
            token : "828c2b3d0762dcf6232fddcea1ad21c09cc5fa42438268624bede9c5ca8bed87ac2fcf9a1ccbb18b2f525aafcc09d113184d36ab9ab3bfe625a98c5f1999cbaa6f7eba940a90012ff48dd4e61ca9c3d3d68e6cbe770554426d5d8f077e6f49c68d695808"
        },
        url : SITE_URL + '/admins/students/waiting'
    },function (err,resp,body) {
        if (err) return next(err);
        res.send('ok');
    });
};
module.exports.getInterview = function (req,res,next) {
    Interview
        .find({})
        .exec(function (err,interviews) {
            if (err) return next(err);
            res.status(200).json(interviews);
        });
};

module.exports.deleteAnswerSheets = function (req,res,next) {
    Student
        .find({})
        .exec(function (err,students) {
            _.each(students,function (student) {
                student.answers = [];
                student.testID = undefined;
                student.isTesting = faker;
                student.save();
            })
        });
    AnswerSheet
        .find({})
        .remove()
        .exec();
    TestResult
        .find({})
        .remove()
        .exec();
    res.end('ok');
};
module.exports.addTariff = function (req,res,next) {
     var interpressTariff = new Tariff({
         name : "DevelopmentLight",
         size : "20000000",
         students : 50,
         price : 5000
     });
     interpressTariff.save(function (err) {
         if (err) return next(err);
         res.send('ok');
     })
};
var faker = require('faker'),
    request = require('request'),
    _ = require('underscore'),
    Student = require(__modelsPath + 'student'),
    AnswerSheet = require(__modelsPath + 'tests/answer-sheet'),
    Tariff =  require(__modelsPath + 'tariff'),
    TestResult = require(__modelsPath + 'tests/test-result'),
    Interview = require(__modelsPath + 'interview');



var SITE_URL = "http://localhost:8000/api";


var GroupModule = require('group');

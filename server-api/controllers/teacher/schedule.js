module.exports.getSchedule = function (req,res,next) {
    var data = {
        teacherID : req.teacher._id,
        centerID : req.teacher.centerID
    };
    if (!data.teacherID || !data.centerID) return next(errors.invalidBodyError);
    ScheduleModule.getSchedule(data,function (err,lessons) {
        if (err) return next(err);
        res.status(200).json(lessons);
    });
};

var ScheduleModule = require('teacher/schedule'),
    errors = require('system/errors');

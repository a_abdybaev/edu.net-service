module.exports.getListOfGroups = function (req,res,next) {
    var data = {
        teacherID : req.teacher._id,
        centerID : req.teacher.centerID
    };
    if (!data.teacherID || !data.centerID) next(errors.invalidBodyError);
    ListModule.getGroupsList(data,function (err,list) {
        if (err) return next(err);
        res.status(200).json(list);
    });
};

module.exports.getListOfStudentsByGroupID = function (req,res,next) {
    var data = {
        teacherID : req.teacher._id,
        centerID : req.teacher.centerID,
        groupID : req.body.groupID
    };
    if (!data.teacherID || !data.centerID || !data.groupID) return next(errors.invalidBodyError);
    ListModule.getStudentsListByGroupID(data,function (err,list) {
        if (err) return next(err);
        res.status(200).json(list);
    });
};
var ListModule = require('teacher/list'),
    ErrorsModule = require('system/errors');
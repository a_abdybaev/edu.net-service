module.exports.getFiles = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        teacherID : req.teacher._id,
        clientID : req.teacher.clientID
    };
    if (!data.centerID || !data.teacherID) return next(errors.invalidBodyError);
    FileModule.getFilesByTeacherID(data, function (err,result) {
        if (err) return next(err);
        res.status(200).json(result);
    });
};
module.exports.addFile = function (req,res,next) {
    var data = {
        groupID : req.body.groupID,
        teacherID : req.teacher._id,
        centerID : req.teacher.centerID,
        file : req.files.file,
        clientID : req.teacher.clientID
    };
    if (!data.groupID || !data.teacherID || !data.centerID || !data.file) return next(errors.invalidBodyError);
    FileModule.addFileForGroup(data, function (err) {
        if (err) return next(err);
        res.status(201).json(responseStatus.createdMessage);
    });
};

module.exports.deleteFileByID = function (req,res,next) {
    var data = {
        fileID : req.params.fileID,
        teacherID : req.teacher._id,
        centerID : req.teacher.centerID,
        clientID : req.teacher.clientID
    };
    if (!data.fileID || !data.teacherID || !data.centerID) return next(errors.invalidBodyError);
    FileModule.deleteFileByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.deletedMessage);
    });
};
module.exports.getFilesByGroupID = function (req,res,next) {
    var data = {
        groupID : req.params.groupID,
        teacherID : req.teacher._id,
        centerID : req.teacher.centerID,
        clientID : req.teacher.clientID
    };
    if (!data.groupID || !data.teacherID || !data.centerID) return next(errors.notFoundError);
    FileModule.getFilesByGroupID(data,function (err,files) {
        if (err) return next(err);
        res.status(200).json(files);
    });
};

var File = require(__modelsPath + 'file'),
    errors = require('system/errors'),
    responseStatus = require('system/resp'),
    FileModule = require('teacher/file');
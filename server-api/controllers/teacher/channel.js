module.exports.addPost = function (req,res,next) {
    var obj = {
        centerID : req.teacher.centerID,
        groupID : req.body.groupID,
        text : req.body.text,
        files : req.files.files || [],
        images : req.files.images || [],
        important : req.body.important,
        clientID : req.teacher.clientID
    };
    if (!obj.centerID || !obj.groupID || !obj.text) return next(errors.invalidBodyError);
    ChannelModule.addPost(obj, function (err) {
        if (err) return next(err);
        res.status(201).json(responseStatus.createdMessage);
    });
};
module.exports.updatePostByID = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        postID : req.params.postID,
        teacherID : req.teacher._id,
        text : req.body.text,
        images : req.files.images || [],
        files : req.files.files || [],
        important : req.body.important,
        deletingFiles : req.body.deletingFiles,
        deletingImages : req.body.deletingImages,
        clientID : req.teacher.clientID

    };
    if (!data.centerID || !data.postID || !data.teacherID || !data.text || !data.important) return next(errors.invalidBodyError);
    ChannelModule.updatePost(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.successMessage);
    });
};
module.exports.deletePostByID = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        postID : req.params.postID,
        teacherID : req.teacher._id
    };
    if (!data.centerID || !data.postID || !data.teacherID) return next(errors.invalidBodyError);
    ChannelModule.deletePost(data, function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.deletedMessage);
    });
};

module.exports.getPosts = function (req,res,next) {
    var data = {
        teacherID : req.teacher._id,
        groupID : req.query.groupID,
        centerID : req.teacher.centerID,
        page : parseInt(req.query.page),
        homework : req.query.homework,
        important : req.query.important,
        onlyFiles : req.query.onlyFiles,
        another : req.query.another
    };
    if (!data.teacherID || !data.groupID || !data.centerID || _.isNaN(data.page)) return next(errors.invalidBodyError);
    ChannelModule.getPosts(data,function (err,posts) {
        if (err) return next(err);
        res.status(200).json(posts);
    });

};
module.exports.addComment = function (req,res,next) {
    var data = {
        postID : req.params.postID,
        content : req.body.content,
        teacherID : req.teacher._id,
        centerID : req.teacher.centerID,
        isReply : req.body.isReply || false,
        replyCommentID : req.body.replyCommentID
    };
    if (!data.postID || !data.content || !data.teacherID || !data.centerID) return next(errors.invalidBodyError);
    ChannelModule.addComment(data,function (err) {
        if (err) return next(err);
        res.status(201).json(responseStatus.createdMessage);
    });
};

var errors = require('system/errors'),
    responseStatus = require('system/resp'),
    _ = require('underscore'),
    ChannelModule = require('teacher/channel');

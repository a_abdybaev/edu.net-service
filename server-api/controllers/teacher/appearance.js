module.exports.addAppearanceByGroupID = function (req,res,next) {
    var data = {
        teacherID : req.teacher._id,
        centerID : req.teacher.centerID,
        attendance : req.body.appearance,
        day : req.body.day,
        month : req.body.month,
        groupID : req.body.groupID
    };
    if (!data.teacherID || !data.centerID || !data.attendance || !data.day || !data.month || !data.groupID || data.attendance.length == 0) return next(ErrorsModule.invalidBodyError);
    AppearanceModule.addAppearanceByGroupID(data,function (err) {
        if (err) return next(err);
        res.status(201).json(ResponseModule.createdMessage);
    });

};
module.exports.getAppearanceByGroupID = function (req,res,next) {
    var data = {
        teacherID : req.teacher._id,
        groupID : req.query.groupID,
        centerID : req.teacher.centerID
    };
    if (!data.teacherID || !data.groupID || !data.centerID) return next(ErrorsModule.invalidBodyError);
    AppearanceModule.getAppearanceByGroupID(data,function (err,students) {
        if (err) return cb(err);
        res.status(200).json(students);
    });

};

var ErrorsModule = require('system/errors'),
    AppearanceModule = require('teacher/appearance'),
    ResponseModule = require('system/resp');



module.exports.addAttendance = function (req,res,next) {
    var data = {
        teacherID : req.teacher._id,
        centerID : req.teacher.centerID,
        appearance : req.body.appearance,
        day : req.body.day,
        month : req.body.month,
        groupID : req.body.groupID
    };
    if (!data.teacherID || !data.centerID || !data.appearance || !data.day || !data.month) return next(errors.invalidBodyError);
    AttendanceModule.addAttendance(data,function (err) {
        if (err) return next(err);
        res.status(201).json(responseStatus.createdMessage);
    });
};
var AttendanceModule = require('teacher/attendance'),
    ErrorsModule = require('system/errors'),
    responseStatus = require('system/resp');
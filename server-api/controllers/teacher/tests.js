module.exports.addTest = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        name : req.body.name,
        sections : req.body.sections,
        files : req.files.sections,
        ownerID : {
            kind : ConstantsModule.TEACHER_MODEL,
            value : req.teacher._id,
            userType : ConstantsModule.TEACHER
        },
        clientID : req.teacher.clientID
    };
    // console.log(str.replace(/\s+/g,' '));  // udalite nenujnye probely
    if (!TestModule.isValidTest(data)) return next(ErrorsModule.invalidBodyError);
    TestModule.addTest(data,function (err) {
        if (err) return next(err);
        Response.created(res);
    });
};
module.exports.getGroupResultsByTestID = function (req,res,next) {
    var data = {
        centerID :req.teacher.centerID,
        testID : req.params.testID,
        groupID : req.query.groupID,
        clientID : req.teacher.clientID
    };
    if (!data.centerID || !data.testID || !data.groupID) return next(ErrorsModule.invalidBodyError);
    TeacherTestModule.getGroupResultsByTestID(data,function (err,tests) {
        if (err) return next(err);
        Response.get(res,tests);
    });
};
module.exports.getTestResultsByGroupID = function (req,res,next) {
    var data = {
        centerID :req.teacher.centerID,
        groupID : req.query.groupID,
        clientID : req.teacher.clientID
    };
    if (!data.centerID || !data.groupID) return next(ErrorsModule.invalidBodyError);
    TeacherTestModule.getTestResultsByGroupID(data,function (err,tests) {
        if (err) return next(err);
        Response.get(res,tests);
    });
};
module.exports.getTests = function (req,res,next) {
    var data = {
        centerID :req.teacher.centerID,
        clientID : req.teacher.clientID
    };
    if (!data.centerID) return next(ErrorsModule.invalidBodyError);
    TestModule.getTests(data,function (err,tests) {
        if (err) return next(err);
        Response.get(res,tests);
    });

};
module.exports.getTestByID = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        testID : req.params.testID,
        clientID : req.teacher.clientID
    };
    if (!data.centerID || !data.testID) return next(ErrorsModule.invalidBodyError());
    TestModule.getTestByID(data,function (err,test) {
        if (err) return next(err);
        Response.get(res,test);
    });
};
module.exports.updateTestByID = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        testID : req.params.testID,
        name : req.body.name,
        clientID : req.teacher.clientID
    };
    if (!data.centerID || !data.testID || _.isEmpty(data.name)) return next(ErrorsModule.invalidBodyError);
    TestModule.updateTestByID(data,function (err) {
        if (err) return next(err);
        Response.updated(res);
    });
};

module.exports.deleteTestByID = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        testID : req.params.testID,
        clientID : req.teacher.clientID
    };
    if (!data.centerID || !data.testID) return next(ErrorsModule.invalidBodyError());
    TestModule.deleteTestByID(data,function (err) {
        if (err) return next(err);
        Response.deleted(res);
    });
};
module.exports.deleteSectionByIndex = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        testID : req.params.testID,
        sectionIndex : req.params.sectionIndex,
        clientID : req.teacher.clientID
    };
    if (!data.centerID || !data.testID || !data.sectionIndex) return next(ErrorsModule.invalidBodyError());
    TestModule.deleteSectionByIndex(data,function (err) {
        if (err) return next(err);
        Response.deleted(res);
    });
};


// ASSIGN
module.exports.assignTest = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        testID : req.params.testID,
        groupID : req.body.groupID,
        clientID : req.teacher.clientID
    };
    if (_.isEmpty(data.centerID) || _.isEmpty(data.testID) || _.isEmpty(data.groupID)) return next(ErrorsModule.invalidBodyError);
    TestModule.assignTest(data,function (err) {
        if (err) return next(err);
        Response.success(res);
    });
};
module.exports.getAvailableGroupsForTest = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        testID : req.params.testID,
        clientID : req.teacher.clientID
    };
    if (!data.centerID || !data.testID) return next(ErrorsModule.invalidBodyError());
    TestModule.getAvailableGroupsForTest(data,function (err,data) {
        if (err) return next(err);
        Response.get(res,data);
    });
};


// ABOUT SECTION
module.exports.updateTestSectionFile = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        testID : req.params.testID,
        file : req.files.file,
        clientID : req.teacher.clientID
    };
    if (!data.centerID || !data.testID  || !data.file) return next(ErrorsModule.invalidBodyError());
    TestModule.updateTestSectionFile(data,function (err) {
        if (err) return next(err);
        Response.updated(res);
    });
};

module.exports.updateAnswersBySectionIndex = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        testID : req.params.testID,
        answers : req.body.answers,
        sectionIndex : parseInt(req.params.sectionIndex),
        duration : parseInt(req.body.duration),
        clientID : req.teacher.clientID
    };
    if (!_.isArray(data.answers)) return next(ErrorsModule.invalidBodyError);
    var validation = true;
    _.each(data.answers,function (answer) {
        if (_.isEmpty(answer)) return validation = false;
    });

    if (!data.centerID || !data.testID || !validation) return next(ErrorsModule.invalidBody);
    TestModule.updateAnswersBySectionIndex(data,function (err) {
        if (err) return next(err);
        Response.updated(res);
    });
};
module.exports.updateSectionByIndex = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        testID : req.params.testID,
        duration : req.body.duration,
        answers : req.body.answers,
        sectionIndex : req.params.sectionIndex,
        file : req.files.file,
        pagesFrom : req.body.pagesFrom,
        pagesTo : req.body.pagesTo,
        clientID : req.teacher.clientID
    };
    if (!data.centerID || !data.testID || !data.sectionIndex) return next(ErrorsModule.invalidBodyError);
    TestModule.updateSectionByIndex(data,function (err) {
        if (err) return next(err);
        Response.updated(res);
    });
};
module.exports.removeTestFromGroup = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        testID : req.params.testID,
        groupID : req.query.groupID,
        clientID : req.teacher.clientID
    };
    if (!data.clientID || !data.centerID || !data.testID || !data.groupID) return next(ErrorsModule.invalidBody);
    TestModule.removeTestFromGroup(data,function (err) {
        if (err) return next(err);
        Response.deleted(res);
    });
};
var Response = require('system/response'),
    ErrorsModule = require('system/errors'),
    _ = require('underscore'),
    ConstantsModule = require('system/constants'),
    TestModule = require('test'),
    TeacherTestModule = require('teacher/tests');
module.exports.add = function (req,res,next) {
    var obj = {
        groupID : req.body.groupID,
        centerID : req.teacher.centerID,
        content : req.body.content,
        comment : req.body.comment,
        day : req.body.day,
        month : req.body.month
    };
    if (!obj.groupID || !obj.centerID || !obj.content || obj.day == null || obj.month == null) return next(errors.invalidBodyError);
    HomeWorkModule.add(obj, function (err) {
        if (err) return next(err);
        Response.created(res);
    });
};
module.exports.getByID = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        homeworkID : req.params.homeworkID
    };
    if (!data.centerID || !data.homeworkID) return next(Errors.invalidBody);
    HomeWorkModule.getByID(data,function (err,homework) {
        if (err) return next(err);
        Response.get(res,homework);
    });
};
module.exports.delByID = function (req,res,next) {
    var data = {
        centerID: req.teacher.centerID,
        homeworkID : req.params.homeworkID
    };
    if (!data.centerID || !data.homeworkID) return next(Errors.invalidBodyError);
    HomeWorkModule.delByID(data,function (err) {
        if (err) return next(err);
        Response.deleted(res);
    });
};
module.exports.editByID = function (req,res,next) {
    var obj = {
        centerID: req.teacher.centerID,
        homeworkID : req.params.homeworkID,
        content : req.body.content,
        comment : req.body.comment
    };
    if (!obj.centerID || !obj.homeworkID || !obj.content) return next(Errors.invalidBodyError);
    HomeWorkModule.editByID(obj,function (err) {
        if (err) return next(err);
        Response.updated(res);
    });
};
module.exports.get = function (req,res,next) {
    var data = {
        groupID : req.query.groupID,
        centerID : req.teacher.centerID
    };
    if (!data.groupID || !data.centerID) return next(errors.invalidBodyError);
    HomeWorkModule.get(data,function (err,homeworks) {
        if (err) return next(err);
        Response.get(res,homeworks);
    });
};


var Response = require('system/response'),
    Errors = require('system/errors'),
    HomeWorkModule = require('teacher/homework');

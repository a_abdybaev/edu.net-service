module.exports.getGroups = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        teacherID : req.teacher.id
    };
    if (!data.centerID || !data.teacherID) return next(ErrorsModule.invalidBodyError);
    GroupModule.getGroups(data, function (err,groups) {
        if (err) return next(err);
        res.status(200).json(groups);
    });
};
module.exports.getGroupByID = function (req,res,next) {
  var data = {
      centerID : req.teacher.centerID,
      groupID : req.params.groupID,
      teacherID : req.teacher._id
  };
  if (!data.groupID || !data.teacherID || !data.centerID) return next(errors.invalidBodyError);
    GroupModule.getGroupByID(data, function (err,group) {
        if (err) return next(err);
        res.status(200).json(group);
    });
};
module.exports.getTestResultsByGroupID = function (req,res,next) {
    var data = {
        centerID :req.teacher.centerID,
        groupID : req.params.groupID
    };
    if (!data.centerID || !data.groupID) return next(ErrorsModule.invalidBodyError);
    TeacherTestModule.getTestResultsByGroupID(data,function (err,tests) {
        if (err) return next(err);
        res.status(200).json(tests);
    });
};
module.exports.getStudentsResultsByGroupID = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        groupID : req.params.groupID
    };
    if (!data.groupID || !data.centerID) return next(ErrorsModule.invalidBodyError);
    TeacherTestModule.getStudentsResultsByGroupID(data,function (err,students) {
        if (err) return next(err);
        res.status(200).json(students);
    });
};

var ErrorsModule = require('system/errors'),
    responseStatus = require('system/resp'),
    TeacherTestModule = require('teacher/tests'),
    GroupModule = require('teacher/group');

module.exports.addApplication = function (req,res,next) {
    var obj = {
        groupID : req.body.groupID,
        centerID : req.teacher.centerID,
        teacherID : req.teacher._id,
        content : req.body.content
    };
    if (!obj.groupID || !obj.centerID || !obj.teacherID || !obj.content) next(Errors.invalidBodyError);
    ApplicationModule.createApplication(obj, function (err) {
        if (err) return next(err);
        Response.created(res);
    });

};
module.exports.getApplicationByID = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        applicationID : req.params.applicationID,
        teacherID : req.teacher._id
    };
    if (!data.centerID || !data.applicationID) return next(errors.invalidBodyError);
    ApplicationModule.getApplicationByID(data,function (err,application) {
        if (err) return next(err);
        res.status(200).json(application);
    });

};
module.exports.getApplications = function (req,res,next) {
    var obj = {
        centerID : req.teacher.centerID,
        teacherID : req.teacher._id
    };
    if (!obj.centerID || !obj.teacherID) return next(Errors.invalidBodyError);
    ApplicationModule.getApplications(obj,function (err,applications) {
        if (err) return next(err);
        Response.get(res,applications);
    });
};
module.exports.getApplicationsByGroupID = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        teacherID : req.teacher._id,
        groupID : req.params.groupID
    };
    if (!data.centerID || !data.teacherID || !data.groupID) return next(Errors.invalidBodyError);
    ApplicationModule.getApplicationsByGroupID(data,function (err,applications) {
        if (err) return next(err);
        Response.get(res,applications);
    });
};
module.exports.updateApplicationByID = function (req,res,next) {
    var obj = {
        centerID : req.teacher.centerID,
        teacherID : req.teacher._id,
        applicationID : req.params.applicationID,
        content : req.body.content
    };
    if (!obj.centerID || !obj.teacherID || !obj.content || !obj.applicationID) return next(Errors.invalidBodyError);
    ApplicationModule.updateApplicationByID (obj, function (err) {
        if (err) return next(err);
        Response.updated(res);
    });
};
module.exports.deleteApplicationByID = function (req,res,next) {
    var obj = {
        centerID : req.teacher.centerID,
        teacherID : req.teacher._id,
        applicationID : req.params.applicationID
    };
    if (!obj.centerID || !obj.teacherID || !obj.applicationID) return next(Errors.invalidBodyError);
    ApplicationModule.deleteApplicationByID(obj, function (err) {
        if (err) return next(err);
        Response.deleted(res);
    });
};



var Errors = require('system/errors'),
    Response = require('system/response'),
    ApplicationModule = require('teacher/application'),
    Application = require(__modelsPath + 'application');
module.exports.info = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        teacherID : req.teacher._id
    };

    SelfModule.getTeacherInfoByID(data,function (err,info) {
        if (err) return next(err);
        res.status(200).json(info);
    })
};
module.exports.updatePassword = function (req,res,next) {
    var data = {
        teacherID : req.teacher._id,
        centerID : req.teacher.centerID,
        oldPass : req.body.oldPassword,
        newPass : req.body.newPassword
    };
    SelfModule.updatePassword(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.updatedMessage);
    });
};
module.exports.recoverPassword = function (req,res,next) {
    var data = {
        centerDomain : req.body.centerDomain,
        phone : req.body.phone
    };
    if (!data.centerDomain || !data.phone) return next(errors.invalidBodyError);
    SelfModule.recoverPassword(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.successMessages);
    });
};

module.exports.updateImage = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        image : req.body.image,
        teacherID : req.teacher._id
    };
    if (!data.centerID || !data.image || !data.teacherID) return next(errors.invalidBodyError);
    SelfModule.updateImage(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.createdMessage);
    });
};
module.exports.updateProfile = function (req,res,next) {
    var data = {
        centerID : req.teacher.centerID,
        teacherID : req.teacher._id,
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        email : req.body.email
    };
    if (!data.centerID || !data.teacherID || !data.firstName || !data.lastName) return next(errors.invalidBodyError);
    SelfModule.updateProfile(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.udpatedMessage);
    });
};

var SelfModule = require('teacher/self'),
    responseStatus = require('system/resp'),
    errors = require('system/errors');
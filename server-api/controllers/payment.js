"use strict";

var Center = require('../models/center');
var errors = require('system/errors');

module.exports.isPaidCenter = function (centerID,cb) {
    Center
        .findOne({
            _id : centerID
        })
        .exec(function (err,center) {
            if (err) {
                cb(err);
            } else if (!center) {
                cb(errors.notFoundError);
            } else {
                cb(null,center.isPaid);
            }
        });
};
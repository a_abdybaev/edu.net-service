module.exports.resolve = function (req,res,next) {
    var data = {
        studentID : req.student._id,
        centerID : req.student.centerID,
        groupID : req.query.groupID
    };
    if (!data.studentID || !data.centerID || !data.groupID) return next(ErrorsModule.invalidBodyError);
    InterviewModule.resolve(data,function (err,result) {
        if (err) return next(err);
        res.status(200).json(result);
    });
};
module.exports.addAnswer = function (req,res,next) {
    var data = {
        studentID : req.student._id,
        centerID : req.student.centerID,
        groupID : req.body.groupID,
        results : req.body.results
    };
    if (!exports.isValidInterview(data)) return next(ErrorsModule.invalidBodyError);
    InterviewModule.addAnswer(data,function (err) {
        if (err) return next(err);
        res.status(201).json(ResponseStatus.createdMessage);

    });
};

module.exports.isValidInterview = function (data) {
    if (!data.studentID || !data.centerID || !data.groupID || !data.results || _.isEmpty(data.results)) return false;
    var isValid = true;
    _.each(data.results,function (result) {
        if (!result) isValid = false;
    });
    return isValid;
};
var InterviewModule = require('student/interview'),
    _ = require('underscore'),
    ResponseStatus = require('system/resp'),
    ErrorsModule = require('system/errors');
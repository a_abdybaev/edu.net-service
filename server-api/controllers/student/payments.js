module.exports.getPayments = function (req,res,next) {
    var data = {
        studentID : req.student._id,
        centerID : req.student.centerID,
        groupID : req.query.groupID
    };
    if (_.isEmpty(data.studentID) || _.isEmpty(data.centerID) || _.isEmpty(data.groupID)) return next(ErrorsModule.invalidBodyError);
    PaymentsModule.getPayments(data,function (err,payments) {
        if (err) return next(err);
        res.status(200).json(payments);
    });
};


var PaymentsModule = require('student/payments'),
    ErrorsModule = require('system/errors'),
    _ = require('underscore');

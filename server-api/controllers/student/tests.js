module.exports.getTests = function (req,res,next) {
    var data = {
        centerID : req.student.centerID,
        groupID : req.query.groupID,
        studentID : req.student._id
    };

    if (!data.centerID || !data.groupID || !data.studentID) return next(ErrorsModule.invalidBodyError);
    TestsModule.getTests(data,function (err,data) {
        if (err) return next(err);
        res.status(200).json(data);
    });
};


var TestsModule = require('student/tests'),
    ErrorsModule = require('system/errors'),
    ResponseModule = require('system/resp'),
    _ = require('underscore');
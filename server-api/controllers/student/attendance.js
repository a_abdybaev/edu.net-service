module.exports.getAttendance = function (req,res,next) {
    var data = {
        studentID : req.student._id,
        centerID : req.student.centerID,
        groupID : req.query.groupID
    };
    if (!data.studentID || !data.centerID || !data.groupID) return next(errors.indalivBodyError);
    AttendanceModule.getAttendance(data,function (err,attendance) {
        if (err) return next(err);
        res.status(200).json(attendance);
    });
};


var AttendanceModule = require('student/attendance'),
    errors = require('system/errors');
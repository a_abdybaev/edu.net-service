module.exports.getProfile = function (req,res,next) {
    var data = {
        studentID : req.student._id,
        centerID : req.student.centerID,
        groupID : req.query.groupID
    };
    if (!data.studentID || !data.centerID || !data.groupID) return next(ErrorsModule.invalidBodyError);
    ProfileModule.getProfile(data,function (err,student) {
        if (err) return next(err);
        res.status(200).json(student);
    });
};

var ProfileModule = require('student/profile'),
    ResponseModule = require('system/resp'),
    ErrorsModule = require('system/errors');
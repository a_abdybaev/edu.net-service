module.exports.updateImage = function (req,res,next) {
    var data = {
        centerID : req.student.centerID,
        image : req.body.image,
        studentID : req.student._id
    };
    if (!data.centerID || !data.image || !data.studentID) return next(ErrorsModule.invalidBodyError);
    SelfModule.updateImage(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.updatedMessage);
    });
};
module.exports.updateUserInfo = function (req,res,next) {
    var data = {
        studentID : req.student._id,
        centerID : req.student.centerID,
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        email : req.body.email
    };
    if (_.isEmpty(data.studentID) || _.isEmpty(data.centerID) || _.isEmpty(data.firstName) ||
        _.isEmpty(data.lastName)) return next(ErrorsModule.invalidBodyError);
    SelfModule.updateUserInfo(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.updatedMessage);
    });

};
module.exports.updatePassword = function (req,res,next) {
    var data = {
        studentID : req.student._id,
        centerID : req.student.centerID,
        oldPass : req.body.oldPassword,
        newPass : req.body.newPassword,
    };
    SelfModule.updatePassword(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.updatedMessage);
    });
};
module.exports.getUserInfo = function (req,res,next) {
    var data = {
        studentID : req.student._id,
        centerID : req.student.centerID
    };
    if (!data.studentID || !data.centerID) return  next(ErrorsModule.invalidBodyError);
    SelfModule.getUserInfo(data,function (err,info) {
        if (err) return next(err);
        res.status(200).json(info);
    });
};

var SelfModule = require('student/self'),
    ErrorsModule = require('system/errors'),
    ResponseModule = require('system/resp'),
    _ = require('underscore');
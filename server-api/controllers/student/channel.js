module.exports.getPosts = function (req,res,next) {
    var data = {
        groupID : req.query.groupID,
        centerID : req.student.centerID,
        page : parseInt(req.query.page),
        homework : req.query.homework,
        important : req.query.important,
        onlyFiles : req.query.onlyFiles,
        another : req.query.another
    };
    if (!data.groupID || !data.centerID || !_.isNumber(data.page)) return next(errors.invalidBodyError);
    ChannelModule.getPosts(data,function (err,posts) {
        if (err) return next(err);
        res.status(200).json(posts);
    });
};
module.exports.addComment = function (req,res,next) {
    var data = {
        studentID : req.student._id,
        centerID : req.student.centerID,
        content : req.body.content,
        postID : req.body.postID,
        groupID : req.body.groupID,
        replyCommentID : req.body.replyCommentID,
        isReply : req.body.isReply || false
    };
    if (!data.studentID || !data.centerID || !data.content || !data.postID || !data.groupID) return next(errors.invalidBodyError);
    ChannelModule.addComment(data,function (err) {
        if (err) return next(err);
        return res.status(201).json(responseStatus.createdMessage);
    });
};
module.exports.replyComment = function (req,res,next) {

};
var ChannelModule = require('student/channel'),
    responseStatus = require('system/resp'),
    _ = require('underscore'),
    errors = require('system/resp');




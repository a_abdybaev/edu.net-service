module.exports.getHomeworks = function (req,res,next) {
    var data = {
        studentID : req.student._id,
        centerID : req.student.centerID,
        groupID : req.query.groupID
    };
    if (!data.studentID || !data.centerID || !data.groupID) return next(errors.invalidBodyError);
    HomeworkModule.getHomeworks(data,function (err,homeworks) {
        if (err) return next(err);
        res.status(200).json(homeworks);
    });
};

var HomeworkModule = require('student/homework'),
    errors = require('system/errors');
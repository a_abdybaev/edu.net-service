module.exports.getSchedule = function (req,res,next) {

    var data = {
        studentID : req.student._id,
        centerID : req.student.centerID,
        groupID : req.query.groupID
    };
    if (!data.studentID || !data.centerID || !data.groupID) return next(errors.invalidBodyError);
    ScheduleModule.getSchedule(data,function (err,schedule) {
        if (err) return next(err);
        res.status(200).json(schedule);
    });
};



var ScheduleModule = require('student/schedule'),
    errors = require('system/errors');
module.exports.getFiles = function (req,res,next) {
    var data = {
        centerID : req.student.centerID,
        studentID : req.student._id,
        groupID : req.query.groupID
    };
    if (!data.centerID || !data.studentID || !data.groupID) return next(errors.invalidBodyError);
    FilesModule.getFiles(data,function (err,files) {
        if (err) return next(err);
        res.status(200).json(files);
    });
};
var FilesModule = require('student/files'),
    errors = require('system/errors');
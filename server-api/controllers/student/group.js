module.exports.getGroupInfo = function (req,res,next) {
    var data = {
        centerID : req.student.centerID,
        groupID : req.query.groupID,
        studentID : req.student._id
    };
    if (!data.centerID || !data.groupID || !data.studentID) return next(errors.invalidBodyError);
    GroupModule.getGroupInfo(data,function (err,group) {
        if (err) return next(err);
        res.status(200).json(group);
    });
};


var GroupModule = require('student/group'),
    errors = require('system/errors');
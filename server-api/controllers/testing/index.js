module.exports.resolveTesting = function (req,res,next) {
    var data = {
        centerID : req.student.centerID,
        studentID : req.student._id
    };
    if (!data.studentID || !data.centerID) return next(ErrorsModule.invalidBodyError);
    TestingModule.resolveTesting(data,function (err,data) {
        if (err) return next(err);
        res.status(200).json(data);
    });
};
module.exports.startSection = function (req,res,next) {
    var data = {
        centerID : req.student.centerID,
        studentID : req.student._id
    };
    if (!data.studentID || !data.centerID) return next(ErrorsModule.invalidBodyError);
    TestingModule.startSection(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.successMessage);
    });
};
module.exports.endSection = function (req,res,next) {
    var data = {
        centerID : req.student.centerID,
        studentID : req.student._id,
        answers : req.body.answers || []
    };
    if (!data.studentID || !data.centerID) return next(ErrorsModule.invalidBodyError);
    TestingModule.endSection(data,function (err,obj) {
        if (err) return next(err);
        res.status(200).json(obj);
    });
};

module.exports.beginSection = function (req,res,next) {
    var data = {
        centerID : req.student.centerID,
        studentID : req.student._id
    };
    if (!data.studentID || !data.centerID) return next(ErrorsModule.invalidBodyError);
    TestingModule.beginSection(data,function (err,data) {
        if (err) return next(err);
        res.status(200).json(data);
    });
};
module.exports.assignTest = function (req,res,next) {
    var data = {
        centerID : req.student.centerID,
        studentID : req.student._id,
        testID : req.body.testID
    };
    if (!data.studentID || !data.centerID || !data.testID) return next(ErrorsModule.invalidBodyError);
    TestingModule.assignTest(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.successMessage);
    });
};
module.exports.cancelTest = function (req,res,next) {
    var data = {
        centerID : req.student.centerID,
        studentID : req.student._id
    };
    if (!data.centerID || !data.studentID) return next(ErrorsModule.invalidBodyError);
    TestingModule.cancelTest(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.successMessage);
    })
};

module.exports.dev = function (req,res,next) {

    TestingModule.dev(function (err,data) {
        if (err) return next(err);
        res.status(200).json(data);
    })
};



var ErrorsModule = require('system/errors'),
    TestingModule = require('testing'),
    ResponseModule = require('system/resp'),
    _ = require('underscore');
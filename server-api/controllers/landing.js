module.exports.addRequest = function (req,res,next) {
    var data = {
        phone : req.body.phone,
        name : req.body.name,
        centerName : req.body.centerName
    };
    if (!data.phone) return next(ErrorsModule.invalidBodyError);
    LandingModule.addRequest(data,function (err) {
        if (err) return next(err);
        res.status(201).json(ResponseModule.createdMessage);
    });
};
module.exports.getRequests = function (req,res,next) {
    LandingModule.getRequests(function (err,requests) {
        if (err) return next(err);
        res.status(200).json(requests);
    });
};

var LandingModule = require('landing'),
    ResponseModule = require('system/resp'),
    ErrorsModule = require('system/errors');
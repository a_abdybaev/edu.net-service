module.exports.addManager = function (req,res,next) {
    Manager
        .find({})
        .exec(function (err,managers) {
            if (err) return next(err);
            if (managers.length !=0) return next(errors.badRequestError);
            var manager = new Manager({
                login : "cyrax"
            });
            manager.setPassword("123");
            manager.save(function (err,manager) {
                if (err) return next(err);
                res.status(201).json(responseStatus.createdMessage);
            });
        });

};
module.exports.getManagers = function (req,res,next) {
    Manager
        .find({})
        .exec(function (err,managers) {
            if (err) return next(err);
            res.status(200).json(managers);
        });
};
module.exports.deleteManagers = function (req,res,next) {
    Manager
        .find({})
        .remove({})
        .exec(function (err) {
            if (err) return next(err);
            res.status(200).json(responseStatus.deleteMessage);
        })
};
module.exports.addGroupColors = function (req,res,next) {
    var color1 = new Color({
        rgb : "",
        addGroupActiveColorLink : "/static-files/images/",
        addGroupInActiveColorLink : "/static-files/images/"
    });
    var color2 = new Color({
        rgb : "",
        addGroupActiveColorLink : "/static-files/images/",
        addGroupInActiveColorLink : "/static-files/images/"
    });
    var color3 = new Color({
        rgb : "",
        addGroupActiveColorLink : "/static-files/images/",
        addGroupInActiveColorLink : "/static-files/images/"
    });
    var color4 = new Color({
        rgb : "",
        addGroupActiveColorLink : "/static-files/images/",
        addGroupInActiveColorLink : "/static-files/images/"
    });
    var color5 = new Color({
        rgb : "",
        addGroupActiveColorLink : "/static-files/images/",
        addGroupInActiveColorLink : "/static-files/images/"
    });
    var color6 = new Color({
        rgb : "",
        addGroupActiveColorLink : "/static-files/images/",
        addGroupInActiveColorLink : "/static-files/images/"
    });
    var colors = [color1,color2,color3,color4,color5,color6];
    var results = [];
    colors.forEach(function (color,i) {
        results[i] = function (cb) {
            color.save(function(err,color){
                if (err) {
                    cb(err)
                } else {
                    cb(null,i);
                }
            });
        }
    });
    async.series(results, function (err,data) {
        if (err) {
            next(err);
        } else {
            res.status(201).json({
                message : "COLORS ADDED"
            });
        }
    });
};
module.exports.createTariffs = function (req,res,next) {
    Tariff
        .find({})
        .exec(function (err,tariffs) {
            if (err) return next(err);
            if (tariffs.length != 0) return next(errors.badRequestError);
            var light = new Tariff({
                name : "Light",
                groupCount : 3
            });
            var premium = new Tariff({
                name : "Premium",
                groupCount : 6
            });
            var silver = new Tariff({
                name : "Silver",
                groupCount : 10
            });
            var gold = new Tariff({
                name : "Gold",
                groupCount : 15
            });
            var platinum = new Tariff({
                name : "Platinum",
                groupCount : 20
            });
            var results = [];
            var tariffs = [light,premium,silver,gold,platinum];
            tariffs.forEach(function (tariff,i) {
                results[i] = function (callback) {
                    tariff.save(function (err) {
                        if (err) {
                            callback(err);
                        } else {
                            callback(null,null);
                        }
                    });
                }
            });
            async.series(results, function (err,data) {
                if (err) {
                    next(err);
                } else {
                    res.status(201).json(responseStatus.createdMessage);
                }
            });
        });

};

module.exports.createFileTypes = function () {

};
module.exports.checkConnection = function (req,res,next) {
    res.send("successful connection");
};

var errors = require('system/errors'),
    Manager = require('../models/manager'),
    Color = require('../models/color'),
    async = require('async'),
    Tariff = require('../models/tariff'),
    responseStatus = require('system/errors');
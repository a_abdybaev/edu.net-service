module.exports.addSchedule = function (req,res,next) {
    var data = {
        lessons : req.body.lessons,
        centerID : req.admin.centerID,
        groupID : req.body.groupID,
        clientID : req.admin.clientID,
        teacherID : req.body.teacherID
    };
    if (_Module.validateAddScheduleBody(data)) return next(_Module.validateAddScheduleBody(data));
    _Module.addSchedule(data,function (err) {
        if (err) return next(err);
        Response.created(res);
    });
};
module.exports.getSchedule = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        teacherID : req.query.teacherID,
        classroomID : req.query.classroomID,
        clientID : req.admin.clientID
    };
    if (!data.centerID) return next(Errors.invalidBodyError);
    _Module.getSchedule(data,function (err,lessons) {
        if (err) return next(err);
        Response.get(res,lessons);
    });
};
module.exports.getByQuery = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        teacherID : req.query.teacherID,
        groupID : req.query.groupID
    };
    if (!data.centerID || !data.teacherID || !data.groupID) return next(Errors.invalidBody);
    _Module.getByQuery(data,function (err,lessons) {
        if (err) return next(err);
        Response.get(res,lessons);
    });
};

module.exports.getAvailableGroupsBySchedule = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID
    };
    _Module.getAvailableGroupsForSchedule(data, function (err,groups) {
        if (err) return next(err);
        Response.get(res,groups);
    });
};


var _Module = require('admin/schedule'),
    Errors = require('system/errors'),
    Response = require('system/response');
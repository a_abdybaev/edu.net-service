module.exports.addClassroom = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        name : req.body.name
    };

    if (!data.name || !data.centerID) return next(ErrorsModule.invalidBodyError);
    ClassRoomModule.addClassroom(data, function (err) {
        if (err) return next(err);
        res.status(201).json(ResponseStatus.createdMessage);
    });
};
module.exports.getClassrooms = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID
    };
    if (!data.centerID) return next(ErrorsModule.invalidBodyError);
    ClassRoomModule.getClassrooms(data, function (err,classrooms) {
        if (err) return next(err);
        res.status(200).json(classrooms);
    });
};

module.exports.getClassroomByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        classroomID : req.params.classroomID
    };
    if (!data.classroomID || !data.centerID) return next(ErrorsModule.invalidBodyError);
    ClassRoomModule.getClassroomByID(data, function (err,classroom) {
        if (err) return next(err);
        res.status(200).json(classroom);
    });
};

module.exports.updateClassroomByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        classroomID : req.params.classroomID,
        name : req.body.name
    };
    if (!data.classroomID || !data.centerID || !data.name) return next(ErrorsModule.invalidBodyError);
    ClassRoomModule.updateClassroomByID(data, function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseStatus.successMessage);
    });
};
module.exports.deleteClassroomByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        classroomID : req.params.classroomID
    };
    if (!data.classroomID || !data.centerID) return next(ErrorsModule.invalidBodyError);
    ClassRoomModule.deleteClassroomByID(data, function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseStatus.deletedMessage);
    });
};
var ErrorsModule = require('system/errors'),
    ResponseStatus = require('system/resp'),
    ClassRoomModule = require('admin/classroom');
module.exports.getApplications = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        groupID : req.query.groupID,
        teacherID : req.query.teacherID
    };
    if (!data.centerID) return cb(errors.invalidBodyError);
    ApplicationModule.getApplications(data,function (err,applications) {
        if(err) return next(err);
        res.status(200).json(applications);
    });
};
module.exports.addApplicationAnswer = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        applicationID : req.params.applicationID,
        content : req.body.content
    };
    if (!data.centerID || !data.applicationID || !data.content) return next(errors.invalidBodyError);
    ApplicationModule.addAnswer(data,function (err) {
        if (err) return next(err);
        res.status(201).json(responseStatus.createdMessage);
    });
};
module.exports.deleteApplicationAnswer = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        applicationID : req.params.applicationID
    };
    if (!data.centerID || !data.applicationID) return next(errors.invalidBodyError);
    ApplicationModule.deleteApplicationAnswer(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.deletedMessage);
    });
};
module.exports.getApplicationByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        applicationID : req.params.applicationID
    };
    if (!data.centerID || !data.applicationID) return next(errors.invalidBodyError);
    ApplicationModule.getApplicationByID(data,function (err,application) {
        if (err) return next(err);
        res.status(200).json(application);
    });
};
module.exports.updateApplicationAnswer = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        applicationID : req.params.applicationID,
        content : req.body.content
    };
    if (!data.centerID || !data.applicationID || !data.content) return next(errors.invalidBodyError);
    ApplicationModule.updateApplicationAnswerByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.successMessage);
    });
};


var ApplicationModule = require('admin/application'),
    errors = require('system/errors'),
    responseStatus = require('system/resp');

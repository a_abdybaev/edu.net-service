module.exports.addTeacher = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        phone : req.body.phone,
        email : req.body.email,
        image : req.body.image,
        clientID : req.admin.clientID,
        gender : req.body.gender
    };
    if (!data.firstName || !data.lastName || !data.phone || !data.gender) return next(errors.badRequestError);
    TeacherModule.addTeacher(data, function (err) {
        if (err) return next(err);
        res.status(201).json(responseStatus.createdMessage);
    });
};
module.exports.updateTeacherByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        teacherID : req.params.teacherID,
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        email : req.body.email,
        image : req.body.image,
        clientID : req.admin.clientID
    };
    if (!data.firstName || !data.lastName || !data.teacherID) return next(errors.badRequestError);
    TeacherModule.updateTeacherByID(data, function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.successMessage);
    });
};
module.exports.getTeachers = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID
    };
    if (!data.centerID) return next(errors.invalidBodyError);
    TeacherModule.getTeachers(data,function (err,teachers) {
        if (err) return next(err);
        res.status(200).json(teachers);
    });
};
module.exports.getTeacherByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        teacherID : req.params.teacherID
    };
    if (!data.centerID || !data.teacherID) return next(errors.invalidBodyError);
    TeacherModule.getTeacherByID(data,function (err,info) {
        if (err) return next(err);
        res.status(200).json(info);
    });
};
module.exports.getScheduleByTeacherID = function (req,res,next) {
    var data = {
        teacherID : req.params.teacherID,
        centerID : req.admin.centerID,
        groupID : req.params.groupID
    };
    if (!data.teacherID || !data.centerID) return next(errors.invalidBodyError);
    TeacherModule.getScheduleByTeacherID(data, function (err,lessons) {
        if (err) return next(err);
        res.status(200).json(lessons);
    });
};
module.exports.deleteTeacherByID = function (req,res,next) {
    var data= {
        teacherID : req.params.teacherID,
        centerID : req.admin.centerID
    };
    if (!data.teacherID || !data.centerID) return next(errors.invalidBodyError);
    TeacherModule.deleteTeacherByID(data, function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.deletedMessage);
    });
};
var Teacher = require('../../models/teacher');
var errors = require('system/errors');
var responseStatus = require('system/resp');
var TeacherModule = require('admin/teacher');
var fs = require('fs');
var path = require('path');

module.exports.updateNoteByID = function (req,res,next) {
    var data = {
        noteID : req.params.noteID,
        content : req.body.content,
        groupID : req.body.groupID,
        centerID : req.admin.centerID,
        clientID : req.admin.clientID
    };
    if (!data.noteID || !data.content || !data.centerID) return next(errors.invalidBodyError);
    NoteModule.updateNoteByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.successMessage);
    });
};
module.exports.getNoteByID = function (req,res,next) {
    var data = {
        noteID : req.params.noteID,
        centerID : req.admin.centerID,
        groupID : req.query.groupID,
        clientID : req.admin.clientID
    };
    if (!data.noteID || !data.centerID || !data.groupID) return next(errors.invalidBodyError);
    NoteModule.getNoteByID(data,function (err,note) {
        if (err) return next(err);
        res.status(200).json(note);
    });
};
module.exports.deleteNoteByID = function (req,res,next) {
    var data = {
        noteID : req.params.noteID,
        centerID : req.admin.centerID,
        clientID : req.admin.clientID
    };
    if (!data.noteID || !data.centerID) return next(errors.invalidBodyError);
    NoteModule.deleteNoteByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.deletedMessage);
    });

};
module.exports.getNotesByGroupID = function (req,res,next) {
    var data = {
        groupID : req.query.groupID,
        centerID : req.admin.centerID,
        clientID : req.admin.clientID
    };
    if (!data.groupID || !data.centerID) return next(errors.invalidBodyError);
    NoteModule.getNotesByGroupID(data,function (err,notes) {
        if (err) return next(err);
        res.status(200).json(notes);
    });
};
module.exports.addNoteByGroupID = function (req,res,next) {
    var data = {
        groupID : req.body.groupID,
        content : req.body.content,
        centerID : req.admin.centerID,
        clientID : req.admin.clientID
    };
    NoteModule.addNoteByGroupID(data,function (err) {
        if (err) return next(err);
        res.status(201).json(responseStatus.createdMessage);
    });

};


var NoteModule = require('admin/note'),
    errors = require('system/errors'),
    responseStatus = require('system/resp');
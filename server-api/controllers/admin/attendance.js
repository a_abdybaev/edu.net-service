module.exports.getAttendance = function (req,res,next) {
    var data = {
        groupID : req.query.groupID,
        clientID : req.admin.clientID,
        centerID : req.admin.centerID
    };
    if (!data.groupID || !data.centerID) return next(errors.invalidBodyError);
    AttendanceModule.getAttendance(data,function (err,attendance) {
        if (err) return next(err);
        res.status(200).json(attendance);
    });
};

var errors = require('system/errors'),
    AttendanceModule = require('admin/attendance');
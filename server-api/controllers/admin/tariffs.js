module.exports.getTariffs = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID
    };
    if (!data.centerID) return next(ErrorsModule.invalidBodyError);
    TariffModule.getTariffs(data,function (err,obj) {
        if (err) return next(err);
        res.status(200).json(obj);
    });
};

var TariffModule = require('admin/tariff'),
    ResponseModule = require('system/resp'),
    ErrorsModule = require('system/errors');
module.exports.startInterview = function (req,res,next) {
    var data = {
        groupID : req.body.groupID,
        clientID : req.admin.clientID,
        centerID : req.admin.centerID
    };
    if (!data.groupID || !data.centerID) return next(ErrorsModule.invalidBodyError);
    InterviewModule.startInterview(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseStatus.successMessage);
    });
};

var InterviewModule = require('admin/interview'),
    ResponseStatus = require('system/resp'),
    ErrorsModule = require('system/errors');

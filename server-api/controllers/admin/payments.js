module.exports.addPayment = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        studentID : req.body.studentID,
        value : req.body.value,
        comment : req.body.comment,
        groupID : req.body.groupID,
        clientID : req.admin.clientID
    };
    if (!data.centerID || !data.studentID || !data.value || !data.groupID) return next(errors.invalidBodyError);
    PaymentModule.addPayment(data,function (err) {
        if (err) return next(err);
        res.status(201).json(responseStatus.createdMessage);
    });
};
module.exports.updatePaymentByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        paymentID : req.params.paymentID,
        value : req.body.value,
        comment : req.body.comment,
        clientID : req.admin.clientID
    };
    if (!data.centerID || !data.paymentID || !data.value) return next(errors.invalidBodyError);
    PaymentModule.updatePaymentByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.updatedMessage);
    });
};
module.exports.deletePaymentByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        paymentID : req.params.paymentID,
        clientID : req.admin.clientID
    };
    if (!data.centerID || !data.paymentID) return next(errors.invalidBodyError);
    PaymentModule.deletePaymentByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.deletedMessage);
    });
};
module.exports.getPayments = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        groupID : req.query.groupID,
        clientID : req.admin.clientID
    };
    if (!data.centerID || !data.groupID) return next(errors.invalidBodyError);
    PaymentModule.getPayments(data,function (err,payments) {
        if (err) return next(err);
        res.status(200).json(payments);
    })
};
module.exports.getPaymentByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        paymentID : req.params.paymentID,
        clientID : req.admin.clientID
    };
    if (!data.centerID || !data.paymentID) return next(errors.invalidBodyError);
    PaymentModule.getPaymentByID(data,function (err,payment) {
        if (err) return next(err);
        res.status(200).json(payment);
    });
};
var errors = require('system/errors'),
    responseStatus = require('system/resp'),
    PaymentModule = require('admin/payment');
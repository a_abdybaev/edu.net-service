module.exports.addTest = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        name : req.body.name,
        clientID : req.admin.clientID,
        sections : req.body.sections,
        files : req.files.sections,
        ownerID : {
            kind : ConstantsModule.ADMIN_MODEL,
            value : req.admin._id,
            userType : ConstantsModule.ADMIN
        }
    };

    // console.log(str.replace(/\s+/g,' '));  // udalite nenujnye probely
    // console.log('--- inside ctrl');
    // _.each(data.sections,function (section) {
    //     console.log(section);
    // });
    if (!TestModule.isValidTest(data)) return next(ErrorsModule.invalidBodyError);
    TestModule.addTest(data,function (err) {
        if (err) return next(err);
        res.status(201).json(ResponseModule.createdMessage);
    });
};
module.exports.getTests = function (req,res,next) {
    var data = {
        centerID :req.admin.centerID,
        clientID : req.admin.clientID,

    };
    if (!data.centerID) return next(ErrorsModule.invalidBodyError);
    TestModule.getTests(data,function (err,tests) {
        if (err) return next(err);
        res.status(200).json(tests);
    })

};
module.exports.getTestByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,

        testID : req.params.testID
    };
    if (!data.centerID || !data.testID) return next(ErrorsModule.invalidBodyError());
    TestModule.getTestByID(data,function (err,test) {
        if (err) return next(err);
        res.status(200).json(test);
    });
};
module.exports.updateTestByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,

        testID : req.params.testID,
        name : req.body.name
    };
    if (!data.centerID || !data.testID || _.isEmpty(data.name)) return next(ErrorsModule.invalidBodyError);
    TestModule.updateTestByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.updatedMessage);
    });
};

module.exports.deleteTestByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,

        testID : req.params.testID
    };
    if (!data.centerID || !data.testID) return next(ErrorsModule.invalidBodyError());
    TestModule.deleteTestByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.deletedMessage);
    });
};
module.exports.deleteSectionByIndex = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        testID : req.params.testID,
        clientID : req.admin.clientID,

        sectionIndex : req.params.sectionIndex
    };
    if (!data.centerID || !data.testID || !data.sectionIndex) return next(ErrorsModule.invalidBodyError());
    TestModule.deleteSectionByIndex(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.deletedMessage);
    });
};


// ASSIGN
module.exports.assignTest = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        testID : req.params.testID,
        clientID : req.admin.clientID,

        groupID : req.body.groupID
    };
    if (_.isEmpty(data.centerID) || _.isEmpty(data.testID) || _.isEmpty(data.groupID)) return next(ErrorsModule.invalidBodyError);
    TestModule.assignTest(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.deletedMessage);
    });
};
module.exports.getAvailableGroupsForTest = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,

        testID : req.params.testID
    };
    if (!data.centerID || !data.testID) return next(ErrorsModule.invalidBodyError());
    TestModule.getAvailableGroupsForTest(data,function (err,data) {
        if (err) return next(err);
        res.status(200).json(data);
    });
};


// ABOUT SECTION
module.exports.updateTestSectionFile = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,

        testID : req.params.testID,
        file : req.files.file
    };
    if (!data.centerID || !data.testID  || !data.file) return next(ErrorsModule.invalidBodyError());
    TestModule.updateTestSectionFile(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.deletedMessage);
    });
};

module.exports.updateAnswersBySectionIndex = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,

        testID : req.params.testID,
        answers : req.body.answers,
        sectionIndex : parseInt(req.params.sectionIndex),
        duration : parseInt(req.body.duration)
    };
    if (!_.isArray(data.answers)) return next(ErrorsModule.invalidBodyError);
    var validation = true;
    _.each(data.answers,function (answer) {
        if (_.isEmpty(answer)) return validation = false;
    });

    if (!data.centerID || !data.testID || !validation) return next(ErrorsModule.invalidBodyError());
    TestModule.updateAnswersBySectionIndex(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.deletedMessage);
    });
};
module.exports.updateSectionByIndex = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        testID : req.params.testID,
        clientID : req.admin.clientID,

        duration : req.body.duration,
        answers : req.body.answers,
        sectionIndex : req.params.sectionIndex,
        file : req.files.file,
        pagesFrom : req.body.pagesFrom,
        pagesTo : req.body.pagesTo,
        audioFile : req.files.audioFile
    };
    if (!data.centerID || !data.testID || !data.sectionIndex) return next(ErrorsModule.invalidBodyError);
    TestModule.updateSectionByIndex(data,function (err) {
        if (err) return next(err);
        // console.log(data);
        // return res.send('ok');
        res.status(200).json(ErrorsModule.updatedMessage);
    });
};
var ResponseModule = require('system/resp'),
    ErrorsModule = require('system/errors'),
    _ = require('underscore'),
    ConstantsModule = require('system/constants'),
    TestModule = require('test');
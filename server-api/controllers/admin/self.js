module.exports.info = function (req,res,next) {
    var data = {
        adminID : req.admin._id,
        centerID : req.admin.centerID,
        clientID : req.admin.clientID
    };
    if (!data.adminID || !data.centerID) return next(Errors.invalidBodyError);
    SelfModule.info(data,function (err,admin) {
        if (err) return next(err);
        Response.get(res,admin);
    });
};
module.exports.getCenterInfo = function (req,res,next) {
    var data = {
        adminID : req.admin._id,
        centerID : req.admin.centerID,
        clientID : req.admin.clientID
    };
    if (!data.adminID || !data.centerID) return next(Errors.invalidBodyError);
    SelfModule.getCenterInfo(function (err,center) {
        if (err) return next(err);
        Response.get(res,center);
    });
};
module.exports.updateProfile = function (req,res,next) {
    var data = {
        adminID : req.admin._id,
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        email : req.body.email,
        name : req.body.name
    };
    if (!data.adminID || !data.name) return next(Errors.invalidBodyError);
    SelfModule.updateProfile(data,function (err) {
        if (err) return next(err);
        Response.updated(res);
    });
};
module.exports.updatePassword = function (req,res,next) {
    var data = {
        adminID : req.admin._id,
        centerID : req.admin.centerID,
        oldPass : req.body.oldPassword,
        newPass : req.body.newPassword,
        clientID : req.admin.clientID
    };
    SelfModule.updatePassword(data,function (err) {
        if (err) return next(err);
        Response.updated(res);
    });

};
module.exports.updateImage = function (req,res,next) {
    var data = {
        adminID : req.admin._id,
        centerID : req.admin.centerID,
        image : req.body.image,
        clientID : req.admin.clientID
    };
    if (!data.adminID || !data.centerID || !data.image) return next(Errors.invalidBodyError);
    SelfModule.updateImage(data,function (err) {
        if (err) return next(err);
        Response.updated(res)
    });
};
module.exports.requestRecoverPassword = function (req,res,next) {
    var data = {
        phone : req.body.phone,
        centerDomain : req.body.centerDomain,
        clientID : req.admin.clientID
    };
    if (!data.phone || !data.centerDomain) return next(Errors.invalidBodyError);
    SelfModule.requestRecoverPassword(data,function (err) {
        if (err) return next(err);
        Response.success(res);
    });
};
module.exports.recoverPassword = function (req,res,next) {
    var data = {
        phone: req.body.phone,
        centerDomain: req.body.centerDomain,
        code: req.body.code,
        password: req.body.password,
        clientID : req.admin.clientID
    };
    if (!data.phone || !data.centerDomain || !data.code) return next(Errors.invalidBodyError);
    SelfModule.recoverPassword(data, function (err) {
        if (err) return next(err);
    });
};
module.exports.isValidCode = function (req,res,next) {
    var data = {
        phone : req.body.phone,
        centerDomain : req.body.centerDomain,
        code : req.body.code,
        model : Constants.MODELS.ADMIN,
        clientID : req.admin.clientID
    };
    if (!data.phone || !data.centerDomain || !data.code) return next(Errors.invalidBodyError);
    SelfModule.isValidCode(data,function (err) {
        if (err) return next(err);
        Response.success(res);
    });
};
module.exports.updateCenterID = function (req,res,next) {
    var data = {
        centerID : req.body.centerID,
        adminID : req.admin._id,
        clientID : req.admin.clientID
    };
    SelfModule.updateCenterID(data,function (err) {
        if (err) return next(err);
        Response.success(res);
    });
};
var SelfModule = require('admin/self'),
    Constants = require('system/constants'),
    Response = require('system/response'),
    Errors = require('system/errors');
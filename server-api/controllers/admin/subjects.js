module.exports.addSubject = function (req,res,next) {
    var data = {
        name : req.body.name,
        levels : req.body.levels,
        centerID : req.admin.centerID,
        clientID : req.admin.clientID
    };
    if (!data.name) return next(errors.invalidBodyError);
    SubjectModule.addSubject(data,function (err) {
        if (err) return next(err);
        res.status(201).json(responseStatus.createdMessage);
    });
};
module.exports.deleteSubjectByID = function (req,res,next) {
    var data = {
        subjectID : req.params.subjectID,
        clientID : req.admin.clientID
    };
    if (!data.subjectID) return next(errors.invalidBodyError);
    SubjectModule.deleteSubjectByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.deletedMessage);
    });
};
module.exports.updateSubjectByID = function (req,res,next) {
    var data = {
        subjectID : req.params.subjectID,
        name : req.body.name,
        levels : req.body.levels,
        clientID : req.admin.clientID
    };
    if (!data.subjectID || !data.name) return next(err);
    SubjectModule.updateSubjectByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.updatedMessage);
    });
};
module.exports.getSubjects = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID
    };
    SubjectModule.getSubjects(data,function (err,subjects) {
        if (err) return next(err);
        res.status(200).json(subjects);
    });
};
module.exports.getSubjectByID = function (req,res,next) {
    var data = {
        subjectID : req.params.subjectID
    };
    if (!data.subjectID) return next(errors.invalidBodyError);
    SubjectModule.getSubjectByID(data,function (err,subject) {
        if (err) return next(err);
        res.status(200).json(subject);
    });
};

var errors = require('system/errors'),
    responseStatus = require('system/resp'),
    SubjectModule = require('admin/subject');

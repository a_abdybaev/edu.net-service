var waitingStudentsController = require('./waiting'),
    archiveStudentsController = require('./archive'),
    activeStudentsController = require('./active'),
    listStudentsController = require('./list');

module.exports.waitingStudents = waitingStudentsController;
module.exports.archiveStudents = archiveStudentsController;
module.exports.activeStudents = activeStudentsController;
module.exports.listStudents = listStudentsController;

module.exports.getStudentByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        studentID : req.params.studentID
    };
    if (_.isEmpty(data.centerID) || _.isEmpty(data.studentID)) return next(ErrorsModule.invalidBodyError);
    ListStudentsModule.getStudentByID(data,function (err,student) {
        if (err) return next(err);
        res.status(200).json(student);
    });
};

var ListStudentsModule = require('admin/student').listStudents,
    _ = require('underscore'),
    ErrorsModule = require('system/errors'),
    ResponseModule = require('system/resp');

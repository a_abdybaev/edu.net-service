module.exports.getStudents = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        subjectID : req.query.subjectID,
        levelID : req.query.levelID,
        studyTimes : req.query.studyTimes || [],
        studyDays : req.query.studyDays || [],
        ageType : req.query.ageType,
        groupType : req.query.groupType,
        page :req.query.page,
        search : req.query.search
    };
    if (!data.centerID ||
        data.subjectID && !data.levelID ||
        data.levelID && !data.subjectID || !data.page) return next(ErrorsModule.invalidBodyError);
    WaitingStudentsModule.getStudents(data,function (err,students) {
        if (err) return next(err);
        res.status(200).json(students);
    });
};
module.exports.getAvailableGroupToAssignStudents = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        count : req.query.count
    };
    if (!data.centerID || _.isEmpty(data.count)) return next(ErrorsModule.invalidBodyError);
    WaitingStudentsModule.getAvailableGroupToAssignStudents(data,function (err,groups) {
        if (err) return next(err);
        res.status(200).json(groups);
    });
};
module.exports.addStudent = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        email : req.body.email,
        phone : req.body.phone,


        subjectID : req.body.subjectID,
        levelID : req.body.levelID,

        groupType : req.body.groupType,
        studyTimes : req.body.studyTimes || [],
        studyDays : req.body.studyDays || [],
        ageType : req.body.ageType
    };
    if (!data.centerID || !data.firstName || !data.lastName ||
        !data.phone) return next(ErrorsModule.invalidBodyError);
    WaitingStudentsModule.addStudent(data,function (err,students) {
        if (err) return next(err);
        res.status(201).json(students);
    });
};
module.exports.assignStudents = function (req,res,next) {
    var data = {
        students : req.body.students,
        clientID : req.admin.clientID,
        groupID : req.body.groupID,
        centerID : req.admin.centerID
    };
    if (_.isEmpty(data.groupID) || _.isEmpty(data.students) || _.isEmpty(data.centerID)) return next(ErrorsModule.invalidBodyError);
    WaitingStudentsModule.assignStudents(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.successMessage);
    });
};
module.exports.formGroup = function (req,res,next) {
    var data = {
        name : req.body.name,
        clientID : req.admin.clientID,
        teacherID : req.body.teacherID,
        individual : req.body.individual,
        price : req.body.price,
        color : req.body.color,
        students : req.body.students,
        centerID : req.admin.centerID
    };
    if (_.isEmpty(data.name) || _.isEmpty(data.color) ||
        _.isEmpty(data.centerID) || _.isEmpty(data.students) ||
            data.individual == true && data.students.length>1) return next(ErrorsModule.invalidBodyError);

    WaitingStudentsModule.formGroup(data,function (err) {
        if (err) return next(err);
        res.status(201).json(ResponseModule.createdMessage);
    });
};
module.exports.getStudentByID = function (req,res,next) {
    var data = {
        studentID : req.params.studentID,
        clientID : req.admin.clientID,
        centerID : req.admin.centerID
    };
    if (_.isEmpty(data.studentID) || _.isEmpty(data.centerID)) return next(ErrorsModule.invalidBodyError);
    WaitingStudentsModule.getStudentByID(data,function (err,student) {
        if (err) return next(err);
        res.status(200).json(student);
    });
};
module.exports.updateStudentByID = function (req,res,next) {
    var data = {
        studentID : req.params.studentID,
        clientID : req.admin.clientID,
        centerID : req.admin.centerID,
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        email : req.body.email,
        subjectID : req.body.subjectID,
        levelID : req.body.levelID,
        groupType : req.body.groupType,
        studyTimes : req.body.studyTimes || [],
        studyDays : req.body.studyDays || [],
        ageType : req.body.ageType
    };
    if (_.isEmpty(data.centerID) || _.isEmpty(data.firstName) || _.isEmpty(data.lastName) ||
        _.isEmpty(data.studentID)) return next(ErrorsModule.invalidBodyError);
    WaitingStudentsModule.updateStudentByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.updatedMessage);
    });
};
module.exports.archiveStudentByID = function (req,res,next) {
    var data = {
        studentID : req.params.studentID,
        clientID : req.admin.clientID,
        centerID : req.admin.centerID
    };
    if (_.isEmpty(data.studentID) || _.isEmpty(data.centerID)) return next(ErrorsModule.invalidBodyError);
    WaitingStudentsModule.archiveStudentByID(data,function (err,student) {
        if (err) return next(err);
        res.status(200).json(student);
    });
};
var WaitingStudentsModule = require('admin/student').waitingStudents,
    ErrorsModule = require('system/errors'),
    ArchiveStudentsModule = require('admin/student').archiveStudents,
    ResponseModule = require('system/resp');

var _ = require('underscore');




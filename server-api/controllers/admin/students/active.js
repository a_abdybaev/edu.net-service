module.exports.addStudents = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        groupID : req.body.groupID,
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        phone : req.body.phone,
        email : req.body.email
    };
    if (!data.centerID || !data.groupID || !data.firstName || !data.lastName || !data.phone) return next(ErrorsModule.invalidBodyError);
    ActiveStudentsModule.addStudent(data,function (err) {
        if (err) return next(err);
        res.status(201).json(responseStatus.createdMessage);
    });
};
module.exports.getStudentByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        studentID : req.params.studentID,
        clientID : req.admin.clientID
    };
    if (!data.centerID || !data.studentID) return next(ErrorsModule.invalidBodyError);
    ActiveStudentsModule.getStudentByID(data,function (err,student) {
        if (err) return next(err);
        res.status(200).json(student);
    });
};
module.exports.archiveStudentByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        studentID : req.params.studentID,
        clientID : req.admin.clientID
    };
    if (!data.centerID || !data.studentID) return next(ErrorsModule.invalidBodyError);
    ActiveStudentsModule.archiveStudentByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.successMessage);
    });
};
module.exports.assignStudentForGroup = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        studentID : req.body.studentID,
        groupID : req.body.groupID
    };
    if (!data.centerID || !data.groupID || !data.studentID) return next(ErrorsModule.notFoundError);
    ActiveStudentsModule.assignStudentForGroup(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.successMessage);
    });
};
module.exports.updateStudentByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        studentID : req.params.studentID,
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        email : req.body.email
    };
    if (!data.centerID || !data.studentID || !data.firstName || !data.lastName) return next(ErrorsModule.invalidBodyError);
    ActiveStudentsModule.updateStudentByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(responseStatus.updateMessage);
    });
};
module.exports.getStudents = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        page : req.query.page,
        search : req.query.search
    };
    if (!data.centerID || !data.page) return next(ErrorsModule.invalidBodyError);
    ActiveStudentsModule.getStudents(data,function (err,students) {
        if (err) return next(err);
        res.status(200).json(students);
    });
};
module.exports.getAvailableGroupsToAssignStudent = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        studentID : req.params.studentID
    };
    if (!data.centerID || !data.studentID) return next(ErrorsModule.invalidBodyError);
    ActiveStudentsModule.getAvailableGroupsToAssignStudent(data,function (err,groups) {
        if (err) return next(err);
        res.status(200).json(groups);
    });
};
var ActiveStudentsModule = require('admin/student').activeStudents,
    ArchiveStudentsModule = require('admin/student').archiveStudents,
    ErrorsModule = require('system/errors'),
    responseStatus = require('system/resp');


module.exports.getStudents = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        page : req.query.page,
        search : req.query.search
    };
    if (!data.centerID || !data.page) return next(ErrorsModule.invalidBodyError);
    ArchiveStudentsModule.getStudents(data,function (err,students) {
        if (err) return next(err);
        res.status(200).json(students);
    });
};
module.exports.getStudentByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        studentID : req.params.studentID
    };
    if (!data.centerID || !data.studentID) return next(ErrorsModule.invalidBodyError);
    ArchiveStudentsModule.getStudentByID(data,function (err,student) {
        if (err) return next(err);
        res.status(200).json(student);
    });
};
module.exports.deleteStudentByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        studentID : req.params.studentID
    };
    if (!data.centerID || !data.studentID) return next(ErrorsModule.invalidBodyError);
    ArchiveStudentsModule.deleteStudentByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.deletedMessage);
    });
};
module.exports.updateStudentByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        studentID : req.params.studentID,
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        email : req.body.email
    };
    if (!data.centerID || !data.studentID || !data.firstName || !data.lastName) return next(ErrorsModule.invalidBodyError);
    ArchiveStudentsModule.updateStudentByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.updatedMessage);
    });
};
module.exports.getAvailableGroupsToAssignStudent = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        studentID : req.params.studentID
    };
    if (!data.centerID || !data.studentID) return next(ErrorsModule.invalidBodyError);
    ArchiveStudentsModule.getAvailableGroupsToAssignStudent(data,function (err,groups) {
        if (err) return next(err);
        res.status(200).json(groups);
    });
};
module.exports.assignStudentByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        studentID : req.params.studentID,
        groupID : req.body.groupID
    };
    if (!data.centerID || !data.groupID || !data.studentID) return next(ErrorsModule.notFoundError);
    ArchiveStudentsModule.assignStudentForGroup(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.successMessage);
    });
};
module.exports.restoreStudentByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        studentID : req.params.studentID,

        subjectID : req.body.subjectID,
        levelID : req.body.levelID,

        groupType : req.body.groupType,
        studyTimes : req.body.studyTimes || [],
        studyDays : req.body.studyDays || [],
        ageType : req.body.ageType
    };
    if (!data.centerID  || !data.studentID) return next(ErrorsModule.notFoundError);
    ArchiveStudentsModule.restoreStudentByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.successMessage);
    });
};
var ActiveStudentsModule = require('admin/student').activeStudents,
    ArchiveStudentsModule = require('admin/student').archiveStudents,
    ErrorsModule = require('system/errors'),
    ResponseModule = require('system/resp');


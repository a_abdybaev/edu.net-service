module.exports.getListOfGroups = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
    };
    if (!data.centerID) return next(errors.invalidBodyError);
    ListModule.getListOfGroups(data,function (err,groups) {
        if (err) return cb(err);
        res.status(200).json(groups);
    })
};
module.exports.getListOfTeachers = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID
    };
    if (!data.centerID) return cb(errors.invalidBodyError);
    ListModule.getListOfTeachers(data,function (err,teachers) {
        if (err) return cb(err);
        res.status(200).json(teachers);
    });
};

var ListModule = require('admin/list'),
    errors = require('system/errors'),
    responseStatus = require('system/resp');


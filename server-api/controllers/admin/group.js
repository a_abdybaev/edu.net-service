module.exports.getGroups = function (req,res,next) {
    var data = {
        clientID : req.admin.clientID,
        centerID : req.admin.centerID,
        isBloc : req.query.isBloc
    };
    _Module.getGroups(data,function (err,groups) {
        if (err) return next(err);
        _Response.get(res,groups);
    });
};
module.exports.getStudentsByGroupID = function (req,res,next) {
    var data = {
        clientID : req.admin.clientID,
        centerID : req.admin.centerID,
        groupID : req.query.groupID
    };
    if (!data.centerID || !data.groupID) return next(_Errors.notFoundError);
    _Module.getStudentsByGroupID(data,function (err,students) {
        if (err) return next(err);
        _Response.get(res,students);
    })
};
module.exports.deleteStudentFromGroup = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        groupID : req.params.groupID,
        studentID : req.params.studentID
    };
    if (!data.centerID || !data.groupID || !data.studentID) return next(_Errors.invalidBody);
    _Module.deleteStudentFromGroup(data,function (err) {
        if (err) return next(err);
        _Response.success(res);
    });
};
module.exports.addGroup = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        name : req.body.name,
        color : req.body.color,
        price : req.body.price,
        individual : req.body.individual,
        teachers : req.body.teachers
    };
    if (!data.centerID || !data.name || !data.color || !_.isArray(data.teachers)) return next(_Errors.invalidBodyError);
    _Module.addGroup(data, function (err) {
        if (err) return next(err);
        _Response.created(res);
    });
};
module.exports.updateGroupByID = function (req,res,next) {
    var obj = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        groupID : req.params.groupID,
        color : req.body.color,
        notes : req.body.notes,
        name : req.body.name,
        price : req.body.price,
        teachers : req.body.teachers
    };
    console.log(!obj.teachers);
    console.log(!obj.teachers.plus);
    console.log(!obj.teachers.minus);
    console.log(!_.isArray(obj.teachers));
    console.log(!_.isArray(obj.teachers.plus));
    console.log(!_.isArray(obj.teachers.minus));
    // console.log()
    if (!obj.centerID || !obj.groupID || !obj.color || !obj.name || !obj.teachers ||
        !obj.teachers.minus || !obj.teachers.plus ||
            !_.isArray(obj.teachers.minus) || !_.isArray(obj.teachers.plus)
    ) return next(_Errors.invalidBodyError);
    _Module.updateGroup(obj, function (err) {
        if (err) return next(err);
        _Response.updated(res);
    });
};
module.exports.getGroupByID = function (req,res,next) {
    var data = {
        groupID : req.params.groupID,
        clientID : req.admin.clientID,
        centerID : req.admin.centerID
    };
    if (!data.groupID || !data.centerID) return next(Error.invalidBodyError);
    _Module.getGroupByID(data, function (err,group) {
        if (err) return next(err);
        _Response.get(res,group);
    });
};
module.exports.getAvailableTeachersByGroupID = function (req,res,next) {
    var data = {
        groupID : req.params.groupID,
        centerID : req.admin.centerID
    };
    if (!data.groupID || !data.centerID) return next(_Errors.invalidBody);
    _Module.getAvailableTeachersByGroupID(data,function (err,teachers) {
        if (err) return next(err);
        _Response.get(res,teachers);
    });
};
module.exports.deleteGroupByID = function (req,res,next) {
    var data = {
        groupID : req.params.groupID,
        clientID : req.admin.clientID,
        centerID : req.admin.centerID,
    };
    if (!data.groupID || !data.clientID || !data.centerID) return next(_Errors.invalidBody);
    _Module.deleteGroupByID(data, function (err) {
        if (err) return next(err);
        _Response.deleted(res);
    });
};
module.exports.getScheduleByGroupID = function (req,res,next) {
    var data = {
        groupID : req.params.groupID,
        centerID : req.admin.centerID,
        teacherID : req.query.teacherID
    };
    if (!data.groupID || !data.centerID) return next(_Errors.invalidBodyError);
    ScheduleModule.getScheduleByGroupID(data,function (err,data) {
        if (err) return next(err);
        _Response.get(res,data);
    });
};
module.exports.updateScheduleByGroupID = function (req,res,next) {
    var data = {
        groupID : req.params.groupID,
        clientID : req.admin.clientID,
        centerID : req.admin.centerID,
        teacherID : req.body.teacherID,
        lessons : req.body.lessons
    };
    if (ScheduleModule.validateAddScheduleBody(data)) return next(ScheduleModule.validateAddScheduleBody(data));
    ScheduleModule.updateGroupSchedule(data,function (err) {
        if (err) return next(err);
        _Response.updated(res);
    });
};
module.exports.deleteScheduleByGroupID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        groupID : req.params.groupID,
        teacherID : req.query.teacherID
    };
    if (!data.centerID || !data.groupID || !data.teacherID) return next(_Errors.invalidBodyError);
    ScheduleModule.deleteScheduleByGroupID(data, function (err) {
        if (err) return next(err);
        _Response.deleted(res);
    });
};
module.exports.addBloc = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        clientID : req.admin.clientID,
        name : req.body.name,
        groups : req.body.groups,
        color : req.body.color
    };
    if (!data.centerID || !data.clientID || !data.name || !data.color) return next(_Errors.invalidBody);
    _Module.addBloc(data,function (err) {
        if (err) return next(err);
        _Response.created(res);
    })
};
module.exports.getBlocs = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID
    };
    if (!data.centerID) return next(_Errors.invalidBody);
    _Module.getBlocs(data,function (err,blocs) {
        if (err) return next(err);
        _Response.get(res,blocs);
    });
};
module.exports.getBlocByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        blocID : req.params.blocID
    };
    if (!data.centerID || !data.blocID) return next(_Errors.invalidBody);
    _Module.getBlocByID(data,function (err,bloc) {
        if (err) return next(err);
        _Response.get(res,bloc);
    });
};
module.exports.updateBlocByID = function (req,res,next) {

};
module.exports.assignGroupByBlocID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        blocID : req.params.blocID,
        groupID : req.body.groupID
    };
    if (!data.centerID || !data.blocID || !data.groupID) return next(_Errors.invalidBody);
    _Module.assignGroupByBlocID(data,function (err) {
        if (err) return next(err);
        _Response.success(res);
    });
};
module.exports.editBlocByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        blocID : req.params.blocID,
        groups : req.body.groups,
        isValid : true,
        name : req.body.name,
        color : req.body.color
    };
    if (!_.isArray(data.groups)) return next(_Errors.invalidBody);
    _.each(data.groups,function (Obj) {
        if (_.isUndefined(Obj.groupID) || _.isUndefined(Obj.isBloc) || !_.isBoolean(Obj.isBloc) || !data.color) data.isValid = false;
    });
    if (!data.centerID || !data.blocID || !data.isValid) return next(_Errors.invalidBody);
    _Module.editBlocByID(data,function (err) {
        if (err) return next(err);
        _Response.updated(res);
    });
};
module.exports.delBlocByID = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        blocID : req.params.blocID
    };
    if (!data.centerID || !data.blocID) return next(_Errors.invalidBody);
    _Module.delBlocByID(data,function (err) {
        if (err) return next(err);
        _Response.deleted(res);
    });
};
module.exports.delGroupFromBloc = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        groupID : req.query.groupID,
        blocID : req.params.blocID
    };
    if (!data.centerID || !data.groupID) return next(_Errors.invalidBody);
    _Module.delGroupFromBloc(data,function (err) {
        if (err) return next(err);
        _Response.success(res);
    });
};
module.exports.getGroupTeachers = function (req,res,next) {
    var data = {
        centerID : req.admin.centerID,
        groupID : req.query.groupID
    };
    if (!data.centerID || !data.groupID) return next(_Errors.invalidBody);
    _Module.getGroupTeachers(data,function (err,group) {
        if (err) return next(err);
        _Response.get(res,group);
    });
};
var ScheduleModule = require('admin/schedule'),
    _ = require('underscore'),
    _Errors = require('system/errors'),
    _Module = require('admin/group'),
    _Response = require('system/response');
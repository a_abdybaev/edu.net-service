module.exports.add = function (req,res,next) {
    var data = {
        clientID : req.body.clientID,
        centerID : req.body.centerID,
        phone : req.body.phone,
        isManager : req.body.isManager || false
    };
    if (!data.clientID || !data.phone) return next(errors.invalidBodyError);
    AdminModule.addAdmin(data,function (err) {
        if (err) return next(err);
        Response.created(res);
    });
};
module.exports.get = function (req,res,next) {
    AdminModule.getAdmins(function (err,admins) {
        if (err) return next(err);
        Response.get(res,admins);
    });
};
module.exports.delByID = function (req,res,next) {
    var data = {
        adminID : req.params.adminID
    };
    if (!data.adminID) return next(ErrorsModule.invalidBody);
    AdminModule.delByID(data,function (err) {
        if (err) return next(err);
        Response.deleted(res);
    });
};
module.exports.delete = function (req,res,next) {

};

var ErrorsModule = require('system/errors'),
    Response = require('system/response'),
    AdminModule = require('manager/admin');
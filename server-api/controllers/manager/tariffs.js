module.exports.getTariffs = function (req,res,next) {
    TariffModule.getTariffs(function (err,tariffs) {
        if (err) return next(err);
        res.status(200).json(tariffs);
    });
};
module.exports.addTariff = function (req,res,next) {
    var data = {
        name : req.body.name,
        size : parseInt(req.body.size),
        price : parseInt(req.body.price),
        students : parseInt(req.body.students),
        isMajor : req.body.isMajor || false
    };
    if (!data.name || !data.size || !data.price || !data.students || isNaN(data.size) || isNaN(data.price) || isNaN(data.students))
        return next(ErrorsModule.invalidBodyError);
    TariffModule.addTariff(data,function (err) {
        if (err) return next(err);

        res.status(201).json(ResponseModule.createdMessage);
    });
};
module.exports.deleteTariffByID = function (req,res,next) {
    var data = {
        tariffID : req.params.tariffID
    };
    if (_.isEmpty(data.tariffID)) return next(ErrorsModule.invalidBodyError);
    TariffModule.deleteTariffByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseModule.createdMessage);
    });
};
var ErrorsModule = require('system/errors'),
    ResponseModule = require('system/resp'),
    _ = require('underscore'),
    TariffModule = require('manager/tariffs');



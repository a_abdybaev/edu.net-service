var shell = require('shelljs');




module.exports.pushGit = function (req,res,next) {
    res.status(200).json('ok');
    console.log("push called");
    shell.exec("git add .");
    shell.exec("git commit -m 'auto'");
    shell.exec('git push origin',function (code,stdout,stderr) {
        console.log("START CODE");
        console.log(code);
        console.log(stdout);
        console.log(stderr);
        console.log("END CODE");
    });
};
module.exports.pullGit = function (req,res,next) {
    console.log("pull called");
    res.status(200).json('ok');
    shell.cd('/home/edu.net-service');
    shell.exec('git pull origin',function (code,stdout,stderr) {
        console.log("START CODE");
        console.log(code);
        console.log(stdout);
        console.log(stderr);
        console.log("END CODE");

    });

};
module.exports.mergeGit = function (req,res,next) {
    console.log("merge called");
    res.status(200).json('ok');
    shell.exec("git add .");
    shell.exec("git commit -m 'auto'");
    shell.exec('git pull origin front-end',function (code,stdout,stderr) {
        console.log("START CODE");
        console.log(code);
        console.log(stdout);
        console.log(stderr);
        console.log("END CODE");

    });
};
module.exports.logs = function (req,res,next) {
    shell.exec('journalctl -u edunet.service',function (code,stdout,stderr) {
        res.status(200).send(stdout);
    });
};
module.exports.restart = function (req,res,next) {
    console.log("restart called");
    res.status(200).json('ok');

    shell.exec("service edunet restart",function (code,stdout,stderr) {
        console.log("START CODE");

        console.log(code);
        console.log(stdout);
        console.log(stderr);
        console.log("END CODE");

    });
};
module.exports.add = function (req,res,next) {
    var data = {
        name : req.body.name,
        tariffID : req.body.tariffID,
        link : req.body.link
    };
    if (!data.name || !data.tariffID || !data.link) return next(ErrorsModule.badRequestError);
    ClientModule.add(data,function (err) {
        if (err) return next(err);
        ResponseModule.created(res);
    });
};
module.exports.get = function (req,res,next) {
    ClientModule.get(function (err,clients) {
        if (err) return next(err);
        res.status(200).json(clients);
    });
};
module.exports.editByID = function (req,res,next) {
    var data = {
        name : req.body.name,
        tariffID : req.body.tariffID,
        isPaid : req.body.isPaid,
        clientID : req.params.clientID
    };
    if (_.isEmpty(data.name) || !data.tariffID || !_.isBoolean(data.isPaid) || !data.clientID) return next(ErrorsModule.invalidBody);
    ClientModule.editByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseStatus.successMessage);
    });
};

module.exports.getByID = function (req,res,next) {
    var data = {
        clientID : req.params.clientID
    };
    if (!data.clientID) return next(ErrorsModule.invalidBody);
    ClientModule.getByID(data,function (err,client) {
        if (err) return next(err);
        res.status(200).json(client);
    });
};
module.exports.delByID = function (req,res,next) {
    var data = {
        clientID : req.params.clientID
    };
    if (!data.clientID) return next(ErrorsModule.invalidBody);
    ClientModule.delByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseStatus.deletedMessage);
    });
};
module.exports.setPaymentByID = function (req,res,next) {
    var data = {
        clientID : req.params.clientID
    };
    if (!data.clientID) return next(ErrorsModule.invalidBody);
    ClientModule.setPaymentByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseStatus.deletedMessage);
    });
};

var ClientModule = require('manager/client'),
    ErrorsModule = require('system/errors'),
    ResponseModule = require('system/response'),
    _ = require('underscore'),
    ResponseStatus = require('system/resp');

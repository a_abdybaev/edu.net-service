module.exports.addInterview = function (req,res,next) {
    var data = {
        isDefault : true,
        name : req.body.name,
        questions : req.body.questions
    };
    if (!InterviewModule.isValidInterview(data)) return next(ErrorsModule.invalidBodyError);
    InterviewModule.addInterview(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseStatus.createdMessage);
    });
};


var InterviewModule = require('interview'),
    ErrorsModule = require('system/errors'),
    ResponseStatus = require('system/resp');

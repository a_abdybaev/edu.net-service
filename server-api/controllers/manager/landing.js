module.exports.getRequests = function (req,res,next) {
    LandingModule.getRequests(function (err,requests) {
        if (err) return next(err);
        res.status(200).json(requests);
    });
};


var LandingModule = require('landing');
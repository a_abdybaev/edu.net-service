module.exports.addCenter = function (req,res,next) {
    var data = {
        name : req.body.name,
        clientID : req.body.clientID
    };
    if (!data.name || !data.clientID) return next(ErrorsModule.invalidBodyError);
    CenterModule.addCenter(data,function (err) {
        if (err) return next(err);
        res.status(201).json(ResponseStatus.createdMessage);
    });
};
module.exports.updateByID = function (req,res,next) {
    var data = {
        isPaid : req.body.isPaid,
        centerID : req.body.centerID
    };
    if (!data.tariffID || data.isPaid == null || !data.centerID) return next(ErrorsModule.invalidBodyError);
    CenterModule.updateByID(data,function (err) {
        if (err)return next(err);
        res.status(200).json(ResponseStatus.successMessage);
    })
};
module.exports.getCenters = function (req,res,next) {
    CenterModule.getCenters(function (err,centers) {
        if (err) return next(err);
        res.status(200).json(centers);
    });
};
module.exports.getCenterByID = function (req,res,next) {
    var data = {
        centerID : req.params.centerID
    };
    if (!data.centerID) return next(ErrorsModule.invalidBodyError);
    CenterModule.getCenterByID(data,function (err,obj) {
        if (err) return next(err);
        res.status(200).json(obj);
    });
};
module.exports.deleteCenterByID = function (req,res,next) {
    var data = {
        centerID : req.params.centerID
    };
    if (!data.centerID) return next(ErrorsModule.invalidBodyError);
    CenterModule.delByID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseStatus.deletedMessage);
    });
};
module.exports.setPaymentByCenterID = function (req,res,next) {
    var data = {
        centerID : req.params.centerID,
        isPaid : req.body.isPaid
    };

    if (!data.centerID || !data.isPaid) return next(errors.invalidBodyError);
    CenterModule.setPaymentByCenterID(data,function (err) {
        if (err) return next(err);
        res.status(200).json(ResponseStatus.successMessage);
    });

};

var CenterModule = require('manager/center'),
    ErrorsModule = require('system/errors'),
    _ = require('underscore'),
    ResponseStatus = require('system/resp');

module.exports.authManager = function (req,res,next) {
    var data = {
        token : req.get('token')
    };
    if (!data.token) return next(Errors.noTokenError);
    AuthManagerModule.auth(data,function (err,manager) {
        if (err) return next(err);
        req.user = manager;
        next();
    });
};
module.exports.authTeacher = function (req,res,next) {
    var data = {
        token : req.get('token')
    };
    if (!data.token) return next(Errors.noTokenError);
    AuthTeacherModule.auth(data,function (err,teacher) {
        if (err) return next(err);
        req.teacher = teacher;
        next();
    });


};
module.exports.authStudent = function (req,res,next) {
    var data = {
        token : req.get('token')
    };
    if (!data.token) return next(Errors.noTokenError);
    AuthStudentModule.auth(data,function (err,student) {
        if (err) return next(err);
        req.student = student;
        next();
    });
};
module.exports.authAdmin = function (req,res,next) {
    var data = {
        token : req.get('token')
    };
    if (!data.token) return next(Errors.noTokenError);
    AuthAdminModule.auth(data,function (err,admin) {
        if (err) return next(err);
        req.admin = admin;
        next();
    });

};
module.exports.loginAdmin = function (req,res,next) {
    AuthAdminModule.login(req.user,function (err,token) {
        if (err) return next(err);
        res.status(200).json({
            token : token,
            user : Constants.ADMIN
        });
    });
};
module.exports.loginManager = function (req,res,next) {
    var data = {
        login : req.body.login,
        password : req.body.password
    };
    if (!data.login || !data.password) return next(Errors.badRequestError);
    AuthManagerModule.login(data,function (err,token) {
        if (err) return next(err);
        res.status(200).json(token);
    });
};



module.exports.loginStudent = function (req,res,next) {
    AuthStudentModule.login(req.user,function (err,data) {
        if (err) return next(err);
        if (!data) return next();

        res.status(200).json({
            token : data.token,
            user : data.isTesting ? Constants.TESTING : Constants.STUDENT ,
            isTesting : data.isTesting
        });
    });
};
module.exports.loginTeacher = function (req,res,next) {
    AuthTeacherModule.login(req.user,function (err,token) {
        if (err) return next(err);
        if (!token) return next();
        res.status(200).json({
            token : token,
            user : Constants.TEACHER
        });
    });
};
module.exports.checkAuthBody = function (req,res,next) {

    var user = {
        phone : req.body.phone,
        password : req.body.password,
        link : req.body.centerDomain
    };
    if (!user.phone || !user.password || !user.link) return next(Errors.invalidBodyError);
    user.login = user.link + '.' + user.phone;
    req.user = user;
    next();
};
var Constants = require('system/constants'),
    Errors = require('system/errors'),
    AuthManagerModule = require('manager/auth'),
    AuthTeacherModule = require('teacher/auth'),
    AuthAdminModule = require('admin/auth'),
    AuthStudentModule = require('student/auth');
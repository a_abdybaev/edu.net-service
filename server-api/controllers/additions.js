module.exports.resolveMemoryUsage = function (req,res,next) {
    var documents = req.files.files || [],
        images = req.files.images || [],
        size = 0;
    var files = documents.concat(images);
    if (_.isEmpty(files)) return next();
    files.forEach(function (file) {
        size+=file.size;
    });

    Helper.isAvailableSpace(req.teacher.centerID,size,function (err,result) {
        if (err) return next(err);
        if (result) return next(Errors.paymentRequiredError);
        next();
    });
};


var Center = require(__modelsPath + 'center'),
    Helper = require('system/helper'),
    _ = require('underscore'),
    Errors = require('system/errors');
"use strict";

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Token = require('./token.schema'),
    Constants = require('system/constants');


var teacherSchema = new Schema({
    firstName : {
        type : String,
        required : true
    },
    lastName : {
        type : String,
        required : true
    },
    gender : {
        type : String,
        enum : Constants.GENDER.ARRAY,
        required : true
    },
    phone : {
        type : String,
        required : true,
        minlength : 10,
        maxlength : 10
    },
    email : {
        type : String
    },
    centerID : {
        type : Obj,
        ref : Constants.MODELS.CENTER
    },
    login : {
        type : String,
        unique : true,
        required : true
    },
    hash : {
        type : String,
        select : false,
        required : true
    },
    salt : {
        type : String,
        select : false,
        required : true
    },
    token : {
        type : Token
    },
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    updatedAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    imageUrl : {
        type : String
    },
    recoverPassword : {
        hash : {
            type : Number,
            minlength : 6,
            maxlength : 6
        },
        createdAt : {
            type : Date,
            default : Date.now
        }
    },
    lastVisit : {
        type : Date,
        required : true,
        default : Date.now
    },
    clientID : {
        type : Obj,
        ref : Constants.MODELS.CLIENT
    },
    lessons : [{
        type : Obj,
        ref : Constants.MODELS.LESSON
    }]
});

module.exports = mongoose.model(Constants.MODELS.TEACHER,teacherSchema);


var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Constants = require('system/constants'),
    Obj = Schema.Types.ObjectId,
    GroupBlocModel = new Schema({
        name : {
            type :String,
            required : true
        },
        centerID : {
            type : Obj,
            required : true,
            ref : Constants.MODELS.CENTER
        },
        createdAt : {
            type : Date,
            default : Date.now,
            required : true
        },
        updatedAt : {
            type : Date,
            default : Date.now,
            required : true
        },
        groups : [{
            type : Obj,
            ref : Constants.MODELS.GROUP
        }],
        color : {
            type : String,
            enum : Constants.GROUP_COLORS,
            required : true
        }
    });

module.exports = mongoose.model(Constants.MODELS.GROUP_BLOC,GroupBlocModel);
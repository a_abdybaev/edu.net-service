"use strict";

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Constants = require('system/constants'),
    levelSchema = new Schema({
        value : {
            type : String,
            required : true
        }
}),
    subjectSchema = new Schema({
        name : {
            type : String,
            required : true
        },
        levels : [levelSchema],
        centerID : {
            type : Obj,
            ref : Constants.MODELS.CENTER,
            required : true
        },
        createdAt : {
            type : Date,
            default : Date.now,
            required : true
        },
        updatedAt : {
            type : Date,
            default : Date.now,
            required : true
        }
});
module.exports = mongoose.model(Constants.MODELS.SUBJECT,subjectSchema);
var path = require('path'),
    folderSize = require('get-folder-size'),
    Constants = require('system/constants'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    groupModel = new Schema({
        name : {
            type : String,
            required : true
        },
        centerID : {
            type : Obj,
            ref : Constants.MODELS.CENTER,
            required : true
        },
        createdAt : {
            type : Date,
            default : Date.now,
            required : true
        },
        teacherID : {
            type : Obj,
            ref : Constants.MODELS.TEACHER
        },
        teachers : [{
            type : Obj,
            ref : Constants.MODELS.TEACHER
        }],
        students : [{
            type : Obj,
            ref : Constants.MODELS.STUDENT
        }],
        channel : [{
            type : Obj,
            ref : Constants.MODELS.POST
        }],
        schedule : [{
            type : Obj,
            ref : Constants.MODELS.LESSON
        }],
        sourceUrl : {
            type : String,
            required : true
        },
        notes : [{
            value : {
                type : String
            },
            createdAt : {
                type : Date,
                default : Date.now
            },
            updatedAt : {
                type : Date,
                default : Date.now
            }
        }],
        color : {
            type : String,
            enum : Constants.GROUP_COLORS,
            required : true

        },
        homeworks : [{
            type : Obj,
            ref : Constants.MODELS.HOMEWORK
        }],
        applications :[{
            type : Obj,
            ref : Constants.MODELS.APPLICATION
        }],
        files : [{
            type : Obj,
            ref : Constants.MODELS.FILE
        }],
        memory : {
            type : Number,
            default : 0,
            required : true
        },
        price : {
            type : Number
        },
        individual : {
            type : Boolean,
            default : false,
            required : true
        },
        tests : [{
            type : Obj,
            ref : Constants.MODELS.TEST
        }],
        interviewResultID : {
            type : Obj,
            ref : Constants.MODELS.INTERVIEW_RESULT
        },
        isBloc : {
            type : Boolean,
            required : true,
            default : false
        },
        blocID : {
            type : Obj,
            ref : Constants.MODELS.GROUP_BLOC
        },
        lessons : [{
            type : Obj,
            ref : Constants.MODELS.LESSON
        }]
});

groupModel.methods.getSourceUrl = function () {
    var publicDirectory = __publicPath;
    var self = this;
    var pathToDir = path.join(publicDirectory,self.sourceUrl);
    return pathToDir;
};


module.exports = mongoose.model(Constants.MODELS.GROUP,groupModel);







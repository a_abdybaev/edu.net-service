var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Constants = require('system/constants');

var lessonSchema = new Schema({
    day : {
        type : Number,
        required : true
    },
    start : {
        hour : {
            type : Number,
            required : true
        },
        minute : {
            type : Number,
            required : true
        },
        // value : {
        //     type : Number,
        //     // required : true
        // },
        // date : {
        //     type : Date,
        //     // required : true
        // }
    },
    end : {
        hour : {
            type : Number,
            required : true
        },
        minute : {
            type : Number,
            required : true
        },
        // value : {
        //     type : Number,
        //     // required : true
        // },
        // date : {
        //     type : Date,
        //     // required : true
        // }
    },
    classroomID : {
        type : Obj,
        ref : Constants.MODELS.CLASSROOM,
        required : true
    },
    centerID : {
        type : Obj,
        ref : Constants.MODELS.CENTER,
        required : true
    },
    groupID : {
        type : Obj,
        ref : Constants.MODELS.GROUP,
        required : true
    },
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    updatedAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    teacherID : {
        type : Obj,
        ref : Constants.MODELS.TEACHER,
        required : true
    }
});

module.exports = mongoose.model(Constants.MODELS.LESSON,lessonSchema);


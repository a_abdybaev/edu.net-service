"use strict";

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Token = require('./token.schema'),
    Constants = require('system/constants');

var managerSchema = new Schema({
    login : {
        type : String,
        required : true
    },
    hash : {
        type : String,
        required : true
    },
    salt : {
        type : String,
        select : false,
        required : true
    },
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    token : {
        type : Token
    },
    lastVisit : {
        type :Date,
        default : Date.now,
        required : true
    }
});
module.exports = mongoose.model(Constants.MODELS.MANAGER,managerSchema);


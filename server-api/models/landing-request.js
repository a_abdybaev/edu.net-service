var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Constants = require('system/constants');

var landingRequestSchema = new Schema({
    name : {
        type : String
    },
    phone : {
        type : String,
        required : true
    },
    email : {
        type : String
    },
    comment : {
        type : String
    },
    centerName : {
        type : String
    },
    createdAt : {
        type : Date,
        default : Date.now
    }
});
module.exports = mongoose.model(Constants.MODELS.LANDING_REQUEST,landingRequestSchema);



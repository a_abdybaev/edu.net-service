var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Obj = Schema.Types.ObjectId,
    ConstantsModule = require('system/constants');

var interviewSchema = new Schema({
    isDefault : {
        type : Boolean,
        default : false,
        required : true
    },
    centerID : {
        type : Obj,
        ref : "Center"
    },
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    updatedAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    name : {
        type : String
    },
    questions : [{
        text : {
            type : String
        },
        answers : [{
            text : {
                type : String
            }
        }],
        answersType : {
            type : String,
            enum : [ConstantsModule.CUSTOM,ConstantsModule.INTERVIEW_ANSWERS_OUT_OF_TWO,ConstantsModule.INTERVIEW_ANSWERS_OUT_OF_FIVE]
        }
    }]
});


module.exports = mongoose.model(ConstantsModule.MODELS.INTERVIEW,interviewSchema);


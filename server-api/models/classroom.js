"use strict";
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Obj = Schema.Types.ObjectId;
var classroomSchema = new Schema({
    name : {
        type : String,
        required : true
    },
    centerID : {
        type : Obj,
        ref : 'Center',
        required : true
    },
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    updatedAt : {
        type : Date,
        default : Date.now,
        required : true
    }
});
module.exports = mongoose.model('Classroom',classroomSchema);
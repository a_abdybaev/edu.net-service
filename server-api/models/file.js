"use strict";

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Constants = require('system/constants');

// var fileKinds  = ["TASK","BOOK","PRESENTATION","USEFUL_MATERIAL","OTHER"];
var fileSchema = new Schema({
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    updatedAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    url : {
        type : String,
        required : true
    },
    isImage : {
        type : Boolean,
        required : true
    },
    centerID : {
        type : Obj,
        ref : 'Center',
        required : true
    },
    size : {
        type : Number,
        required : true
    },
    groupID : {
        type : Obj,
        ref : 'Group',
        required : true
    },
    extension : {
        type : String,
        required : true
    },
    name : {
        type : String,
        required : true
    },
    // kind : {
    //     type : String,
    //     enums : fileKinds
    // },
    postID : {
        type : Obj,
        ref : 'Post'
    }
});
module.exports = mongoose.model(Constants.MODELS.FILE,fileSchema);

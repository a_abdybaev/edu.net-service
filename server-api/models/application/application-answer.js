"use strict";
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Constants = require('system/constants');

var applicationAnswerSchema = new Schema({
    centerID : {
        type : Obj,
        ref : Constants.MODELS.CENTER,
        required : true
    },
    applicationID : {
        type : Obj,
        ref : Constants.MODELS.APPLICATION,
        required : true
    },
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    updatedAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    content : {
        type : String,
        required : true
    }
});

module.exports = mongoose.model(Constants.MODELS.APPLICATION_ANSWERS,applicationAnswerSchema);

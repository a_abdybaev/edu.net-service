var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Constants = require('system/constants'),
    applicationSchema = new Schema({
        groupID : {
            type : Obj,
            ref : Constants.MODELS.GROUP,
            required : true
        },
        centerID : {
            type : Obj,
            ref : Constants.MODELS.CENTER,
            required : true
        },
        teacherID : {
            type : Obj,
            ref : Constants.MODELS.TEACHER,
            required : true
        },
        createdAt : {
            type : Date,
            default : Date.now,
            required : true
        },
        updatedAt : {
            type : Date,
            default : Date.now,
            required : true
        },
        content : {
            type : String,
            required : true
        },
        answerID : {
            type : Obj,
            ref : Constants.MODELS.APPLICATION_ANSWERS
        }
    });

module.exports = mongoose.model(Constants.MODELS.APPLICATION,applicationSchema);

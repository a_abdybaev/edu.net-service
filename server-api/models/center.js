var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Constants = require('system/constants');

var centerSchema = new Schema({
    name : {
        type : String,
        required : true
    },
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    isPaid : {
        type : Boolean,
        default : true
    },
    memory : {
        type : Number,
        required : true,
        default : 0
    },
    sourceUrl : {
        type : String,
        required : true
    },
    imageUrl : {
        type : String
    },
    students : {
        active : {
            type : Number,
            default : 0,
            required : true
        },
        waiting : {
            type : Number,
            default : 0,
            required : true
        },
        archive : {
            type : Number,
            default : 0,
            required : true
        }
    },
    clientID : {
        type : Obj,
        ref : Constants.MODELS.CLIENT
    },
    tariffID : {
        type : Obj,
        ref : Constants.MODELS.TARIFF
    }
});

centerSchema.methods.getSourceUrl = function (cb) {
    var self = this;
    var pathToDir = path.join(__publicPath,self.sourceUrl);
    return pathToDir;
};
module.exports = mongoose.model(Constants.MODELS.CENTER,centerSchema);

var fs = require('fs');
var path = require('path');
var ErrorsModule = require('system/errors'),
    folderSize = require('get-folder-size');

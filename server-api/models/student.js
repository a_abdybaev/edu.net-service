var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Token = require('./token.schema'),
    Constants = require('system/constants');

var levelSchema = new Schema({
    name : {
        type : String
    }
});

var studentSchema = new Schema({
    firstName : {
        type : String,
        required : true
    },
    lastName : {
        type : String,
        required : true
    },
    email : {
        type : String
    },
    phone : {
        type : String,
        required : true,
        minlength : 10,
        maxlength : 10
    },
    centerID : {
        type : Obj,
        ref : Constants.MODELS.CENTER,
        required : true
    },
    clientID : {
        type : Obj,
        ref : Constants.MODELS.CLIENT,
        required : true
    },
    hash : {
        type : String,
        select : false
    },
    salt : {
        type : String,
        select : false,
        required : true
    },
    token : {
        type : Token
    },
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    updatedAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    login : {
        type : String,
        unique : true,
        required : true
    },
    imageUrl : {
        type : String
    },
    payments : [{
        type : Obj,
        ref : Constants.MODELS.PAYMENTS
    }],
    appearance : [{
        type : Obj,
        ref : Constants.MODELS.APPEARANCE
    }],
    groups : [{
        type : Obj,
        ref : Constants.MODELS.GROUP
    }],
    // FILTER SETTINGS

    groupType : {
        type : String,
        enum : Constants.GROUP_TYPES
    },
    status : {
        type : String,
        enum : Constants.STUDENTS_STATUSES,
        required : true
    },
    subjectID : {
        type : Obj,
        ref : Constants.MODELS.SUBJECT
    },
    levelID : {
        type : Obj
    },
    ageType : {
        type : String,
        enum : Constants.STUDENT_AGE_GROUPS
    },
    studyTimes : [{
        type : String,
        enum : Constants.STUDENT_STUDY_TIMES
    }],
    studyDays : [{
        type : Number,
        enum : [1,2,3,4,5,6]
    }],
    lastVisit : {
        type : Date,
        default : Date.now,
        required : true
    },
    // TESTING
    isTesting : {
        type : Boolean,
        default : false,
        required : true
    },
    testID : {
        type : Obj,
        ref : Constants.MODELS.TEST
    },
    answers : [{
        ref : Constants.MODELS.ANSWER_SHEET,
        type : Obj
    }],
    history : {
        groups : [{
            name : {
                type : String
            },
            groupID : {
                type : Obj
            }
        }]
    }
});

module.exports = mongoose.model(Constants.MODELS.STUDENT,studentSchema);




"use strict";
var mongoose = require('mongoose'),
    Constants = require('system/constants'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    path = require('path'),
    clientModel = new Schema({
        name : {
            type : String,
            unique : true,
            required : true
        },
        createdAt : {
            type : Date,
            default : Date.now,
            required : true
        },
        updatedAt : {
            type : Date,
            default : Date.now,
            required : true
        },
        link : {  // domain name
            type : String,
            unique : true,
            required : true
        },
        tariffID : {
            type : Obj,
            ref : 'Tariff',
            required : true
        },
        isPaid : {
            type : Boolean,
            default : true
        },
        memory : {
            type : Number,
            required : true,
            default : 0
        },
        sourceUrl : {
            type : String,
            required : true
        },
        imageUrl : {
            type : String
        },
        students : {
            active : {
                type : Number,
                required : true,
                default : 0
            },
            waiting : {
                type : Number,
                required : true,
                default : 0
            },
            archive : {
                type : Number,
                required : true,
                default : 0
            }
        },
        centers : [{
            type : Obj,
            ref : Constants.MODELS.CENTER
        }],
        clientID : {
            type : Obj,
            ref : Constants.MODELS.CLIENT
        }
    });

clientModel.methods.getSourceUrl = function () {
    var self = this;

    console.log(__publicPath,self.sourceUrl);
    var pathToDir = path.join(__publicPath,self.sourceUrl);
    return pathToDir;
};
module.exports = mongoose.model(Constants.MODELS.CLIENT,clientModel);


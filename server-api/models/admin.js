"use strict";
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Token = require('./token.schema'),
    Constants = require('system/constants');

var adminSchema = new Schema({
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    updatedAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    clientID : {
        type : Obj,
        ref : Constants.MODELS.CLIENT
    },
    centerID : {
        type : Obj,
        ref : Constants.MODELS.CENTER,
        required : true
    },
    login : {
        type : String,
        required : true,
        unique : true
    },
    phone : {
        type : Number,
        required : true,
        minlength : 10,
        maxlength : 10
    },
    imageUrl : {
        type : String
    },
    hash : {
        type : String,
        select : false,
        required : true
    },
    salt : {
        type : String,
        select : false,
        required : true
    },
    token : Token,
    lastVisit : {
        type : Date,
        required : true,
        default : Date.now
    },
    isManager : {
        type : Boolean,
        default : false,
        required : true
    }
});

module.exports = mongoose.model(Constants.MODELS.ADMIN,adminSchema);



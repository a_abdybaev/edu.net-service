"use strict";


var ConstantsModule = require('system/constants'),
    _ = require('underscore'),
    path = require('path'),
    Constants = require('system/constants'),
    mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , Obj = Schema.Types.ObjectId
    , testResultSchema = new Schema({
    centerID : {
        type : Obj,
        ref : Constants.MODELS.CENTER,
        required : true
    },
    createdAt : {
        type : Date,
        required : true,
        default : Date.now
    },
    studentID : {
        type : Obj,
        ref : Constants.MODELS.STUDENT,
        required : true
    },
    testID : {
        type : Obj,
        ref : Constants.MODELS.TEST,
        required : true
    },
    updatedAt : {
        type : Date,
        required : true,
        default : Date.now
    },
    sections : [{
        sectionNumber : {
            type : Number,
            default : 0
        },
        results : [{
            number : {
                type : Number
            },
            value : {
                type : Boolean
            }
        }]
    }],
    summary : {
        right : {
            type : Number,
            required : true,
            default : 0
        },
        wrong : {
            type : Number,
            required : true,
            default : 0
        },
        total : {
            type : Number,
            required : true,
            default : 0
        }
    }
});
testResultSchema.methods.compute = function () {
    var right = 0,
        wrong = 0;
    _.each(this.sections,function (section) {
        _.each(section.results,function (answer) {
            if (answer.value) return right+=1;
            return wrong+=1;
        })
    });
    this.summary.right = right;
    this.summary.wrong = wrong;
    this.summary.total = right / (right + wrong) * 100;

};

module.exports = mongoose.model(Constants.MODELS.TEST_RESULT,testResultSchema);



var ConstantsModule = require('system/constants'),
    path = require('path'),
    mongoose = require('mongoose'),
    Constants = require('system/constants'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    testSchema = new Schema({
        centerID : {
            type : Obj,
            ref : Constants.MODELS.CENTER,
            required : true
        },
        createdAt : {
            type : Date,
            required : true,
            default : Date.now
        },
        updatedAt : {
            type : Date,
            required : true,
            default : Date.now
        },
        category : {
            type : Obj,
            ref : 'Category',
            required : true,
            default : ConstantsModule.TEST_CATEGORY_DEFAULT
        },
        name : {
            type : String
            ,required : true
        },
        ownerID : {
            kind : {
                type : String,
                enum : ['Teacher','Admin'],
                required : true
            },
            userType : {
                type : String,
                enum : [ConstantsModule.ADMIN,ConstantsModule.TEACHER]
            },
            value : {
                type : Obj,
                refPath : 'ownerID.kind',
                required : true
            }
        },
        memory : {
            type : Number,
            required : true,
            default : 0
        },
        sections : [{
            images : [{
                type : String
            }],
            answers : [{
                number : {
                    type : Number
                },
                values : [{
                    type : String
                }]
            }],
            duration : {
                type : Number,
                default : 0,
                required : true
            },
            questionsCount : {
                type : Number,
                default : 0,
                required : true
            },
            hasAudio : {
                type : Boolean,
                required : true,
                default : false
            },
            audio : {
                type : String
            }
        }],
        sourceUrl : {
            type : String,
            required : true
        }
    });

testSchema.methods.getSourceUrl = function () {
    var self = this;
    var pathToDir = path.join(__publicPath,self.sourceUrl);
    return pathToDir;
};


module.exports = mongoose.model(Constants.MODELS.TEST,testSchema);



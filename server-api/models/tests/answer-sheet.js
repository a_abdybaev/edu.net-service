"use strict";

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Constants = require('system/constants'),
    answerDefaultScheme = new Schema({
        testID : {
            type : Obj,
            ref : Constants.MODELS.TEST
        },
        studentID : {
            type : Obj,
            ref : Constants.MODELS.STUDENT
        },
        centerID : {
            type : Obj,
            ref : Constants.MODELS.CENTER
        },
        sections : [{
            sectionNumber : {
                type : Number
            },
            startedAt : {
                type : Date,
                default : Date.now,
                required : true
            },
            updatedAt : {
                type : Date,
                default : Date.now,
                required : true
            },
            answers : [{
                number : {
                    type : Number
                },
                value : {
                    type : String
                }
            }],
            isStarted : {
                type : Boolean,
                default : true
            },
            isEnded : {
                type : Boolean,
                default : false
            }
        }],
        isEnded : {
            type : Boolean,
            required : true,
            default : false
        },
        isStarted : {
            type : Boolean,
            required : true,
            default : false
        },
        createdAt : {
            type : Date,
            default : Date.now
        },
        updatedAt : {
            type : Date,
            default : Date.now
        },
        resultID : {
            type : Obj,
            ref : Constants.MODELS.TEST_RESULT
        }
});


module.exports = mongoose.model(Constants.MODELS.ANSWER_SHEET,answerDefaultScheme);

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Constants = require('system/constants');

var homeworkSchema = new Schema({
    content : {
        type : String,
        required : true
    },
    comment : {
        type  : String
    },
    date : {
        day : {
            type : Number,
            required : true
        },
        month : {
            type : Number,
            required : true
        }
    },
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    updatedAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    groupID : {
        type : Obj,
        ref : 'Group',
        required : true
    },
    centerID : {
        type : Obj,
        ref : 'Center',
        required : true
    }
});
module.exports = mongoose.model(Constants.MODELS.HOMEWORK,homeworkSchema);
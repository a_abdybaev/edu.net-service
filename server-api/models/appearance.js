var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Constants = require('system/constants');

var appearanceSchema = new Schema({
    day : {
        type : Number,
        required : true
    },
    month : {
        type : Number,
        required : true
    },
    year : {
        type : Number,
        required : true
    },
    date : {
        type : Date,
        required : true
    },
    centerID : {
        type : Obj,
        required : true,
        ref : Constants.MODELS.CENTER
    },
    groupID : {
        type : Obj,
        required : true,
        ref : Constants.MODELS.GROUP
    },
    studentID : {
        type : Obj,
        required : true,
        ref : Constants.MODELS.STUDENT
    },
    value : {
        type : String,
        enum : Constants.STUDENT_APPEARANCE,
        required : true
    }
});

module.exports = mongoose.model(Constants.MODELS.APPEARANCE,appearanceSchema);

// 0 - был на уроке
// 1 - не был на уроке
// 2 - отсутствовал по уважительной причине
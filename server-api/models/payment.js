var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Constants = require('system/constants');

var paymentSchema = new Schema({
    centerID : {
        type : Obj,
        ref : Constants.MODELS.CENTER,
        required : true
    },
    studentID : {
        type : Obj,
        ref : Constants.MODELS.STUDENT,
        required : true
    },
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    updatedAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    value : {
        type : Number,
        required : true
    },
    comment : {
        type : String
    },
    groupID : {
        type : Obj,
        ref : Constants.MODELS.GROUP
    }
});

module.exports = mongoose.model(Constants.MODELS.PAYMENTS,paymentSchema);



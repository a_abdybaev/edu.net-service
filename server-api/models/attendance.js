var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Constants = require('system/constants');

var attendanceSchema = new Schema({
    day : {
        type : Number,
        required : true
    },
    month : {
        type : Number,
        required : true
    },
    date : {
        type : Date,
        required : true
    },
    centerID : {
        type : Obj,
        ref : Constants.MODELS.CENTER
    },
    groupID : {
        type : Obj,
        ref : Constants.MODELS.GROUP
    },
    appearance : [{
        studentID : {
            type : Obj,
            ref : Constants.MODELS.STUDENT,
            required : true
        },
        value : {
            type : Number,
            min : 0,
            max : 2,
            required : true
        }
    }]
});
module.exports = mongoose.model(Constants.MODELS.ATTENDANCE,attendanceSchema);

// 0 - был на уроке
// 1 - не был на уроке
// 2 - отсутствовал по уважительной причине
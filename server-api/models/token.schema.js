var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var tokenSchema = new Schema({
    value : {
        type : String,
        required : true
    },
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    }
});

module.exports = tokenSchema;
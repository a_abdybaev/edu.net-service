var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Constants = require('system/constants'),
    tariffSchema = new Schema({
    name : {
        type : String,
        required : true
    },
    size : {
        type : Number,
        required : true
    },
    students : {
        type : Number,
        required : true
    },
    isMajor : {
        type : Boolean,
        default : false,
        required : true
    },
    individualCount : {
        type : Number
    },
    price : {
        type : Number,
        default : 0,
        required : true
    },
    isFree : {
        type : Boolean,
        default : false,
        required : true
    }

});
module.exports = mongoose.model(Constants.MODELS.TARIFF,tariffSchema);



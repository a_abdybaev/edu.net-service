"use strict";

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Obj = Schema.Types.ObjectId;
var ConstantsModule = require('system/constants');

var commentSchema = new Schema({
    postID : {
        type : Obj,
        ref : 'Post'
    },
    content : {
        type : String,
        required : true
    },
    reply : {
        isReply : {
            type : Boolean,
            default : false
        },
        isImportantName : {
            type : Boolean,
            default : true
        },
        commentID : {
            type : Obj,
            ref : 'Comment'
        },
        name : {
            type : String
        }
    },
    user : {
        model : {
            type : String,
            enum : [ConstantsModule.MODELS.TEACHER,ConstantsModule.MODELS.STUDENT]
        },
        value : {
            type : Obj,
            refPath : 'user.model'
        }
    },
    userType : {
        type : String,
        enum : [ConstantsModule.STUDENT,ConstantsModule.TEACHER]
    },
    centerID : {
        type : Obj,
        ref : 'Center'
    },
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    updatedAt : {
        type : Date,
        default : Date.now,
        required : true
    }
});
module.exports = mongoose.model('Comment',commentSchema);



"use strict";

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Obj = Schema.Types.ObjectId,
    Constants = require('system/constants');

var postSchema = new Schema({
    groupID : {
        type : Obj,
        required : true,
        ref : Constants.MODELS.GROUP
    },
    text : {
        type : String
    },
    createdAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    updatedAt : {
        type : Date,
        default : Date.now,
        required : true
    },
    comments : [{
        type : Obj,
        ref : Constants.MODELS.COMMENTS
    }],
    files : [{
        type : Obj,
        ref : Constants.MODELS.FILE
    }],
    images : [{
        type : Obj,
        ref : Constants.MODELS.FILE
    }],
    centerID : {
        type : Obj,
        ref : Constants.MODELS.CENTER,
        required : true
    },
    homeworkID : {
        type : Obj,
        ref : Constants.MODELS.HOMEWORK
    },
    important : {
        type : Boolean,
        default : false,
        required : true
    }

});
module.exports = mongoose.model(Constants.MODELS.POST,postSchema);

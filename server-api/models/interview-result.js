var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Obj = Schema.Types.ObjectId,
    ConstantsModule = require('system/constants');

var interviewResultSchema = new Schema({
    createdAt : {
        type : Boolean,
        required : true,
        default : Date.now
    },
    updatedAt : {
        type : Boolean,
        required : true,
        default : Date.now
    },
    group : {
        name : {
            type : String
        }
    },
    centerID : {
        type : Obj,
        ref : 'Center'
    },
    interviewID : {
        type : Obj,
        ref : 'Interview'
    },
    groupID : {
        type : Obj,
        ref : 'Group'
    },
    students : [{
        type : Obj,
        ref : 'Student'
    }],
    questions: [{
        analytics : [{
            type : Number,
            default : 0
        }]
    }]
});


module.exports = mongoose.model(ConstantsModule.MODELS.INTERVIEW_RESULT,interviewResultSchema);


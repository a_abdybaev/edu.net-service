var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var colorSchema = new Schema({
    rgb : {
        type : String,
        required : true
    },
    addGroupInActiveColorLink : {
        type : String
    },
    addGroupActiveColorLink : {
        type : String
    }
});
module.exports = mongoose.model('Color',colorSchema);
var express = require('express'),
    path = require('path'),
    colors = require('colors'),
    winston = require('winston'),
    mongoose = require('mongoose'),
    app = express(),
    errors = require('./libraries/system/errors'),
    bodyParser = require('body-parser');
mongoose.connect('mongodb://localhost/edunet-app');
mongoose.Promise = global.Promise;
require('app-module-path').addPath(__dirname + '/libraries');
global.__modelsPath = path.normalize(path.join(__dirname,'server-api/models/'));
global.__publicPath = path.normalize(path.join(__dirname,'public'));
global.__testingUrl = "http://edunet.kz:3000/api/testing";


app.set('port',process.env.NODE_ENV == 'development' ? 3000 : 80);
app.use(bodyParser.urlencoded({
    extended : true,
    limit : '50mb'
}));
app.use(bodyParser.json({
    limit : '50mb'
}));
app.use(function (req,res,next) {
    // FOR FINDING ERROR
    // console.log((req.method + " | " + req.hostname + req.url + " | " + JSON.stringify(req.query) + '|'  + JSON.stringify(req.body)).blue);
    // STANDART
    console.log(req.method.blue + ' | ' + req.hostname.split('.')[0].green + ' | ' + req.url.yellow);
    next();
});



var apiRouter = require('./server-api/routers');
app.use('/api',apiRouter);


app.get('/', function (req,res,next) {
    var sudomains = req.subdomains;

    if (sudomains.length == 0) {
        res.sendFile(path.join(__publicPath,'app-landing/index.html'));
    } else {
        if (sudomains[0] == "help") {
            res.sendFile(path.join(__publicPath,'dist/manual/index-' + require('./public/gulp/build').getVersions().manual_app.index_html + '.html'));
        } else {
            res.sendFile(path.join(__publicPath,'dist/auth/index-' + require('./public/gulp/build').getVersions().auth_app.index_html + '.html'));
        }
    }
});
app.get('/teacher', function (req,res,next) {
    res.sendFile(path.join(__publicPath,'dist/teacher/index-' + require('./public/gulp/build').getVersions().teacher_app.index_html + '.html'));
});
app.get('/student',function (req,res,next) {
    res.sendFile(path.join(__publicPath,'dist/student/index-' + require('./public/gulp/build').getVersions().student_app.index_html + '.html'));
});



app.get('/admin', function (req,res,next) {
    res.sendFile(path.join(__publicPath,'dist/admin/index-' + require('./public/gulp/build').getVersions().admin_app.index_html + '.html'));
});

app.get('/manager',function (req,res,next) {
    var pathToAuthFile = path.join(__publicPath,'app-manager/index.html');
    res.sendFile(pathToAuthFile);
});
app.get('/test',function (req,res,next) {
    var pathToAuthFile = path.join(__publicPath,'app-test/index.html');
    res.sendFile(pathToAuthFile);
});
app.use(function (err,req,res,next) {
    console.log("\n");
    console.log("START ERROR".red);
    console.log("%s | %s | %s%s | %s",new Date().toLocaleString() ,req.method,req.hostname ,JSON.stringify(req.url), JSON.stringify(req.body));
    if (!err.status) console.log(err);
    else {
        console.log(err.name + ' ' + err.status + err.message);
    }
    console.log("END ERROR".red);
    if (err.status !=undefined) {
        res.status(err.status).json(err);
    } else {
        err.status = 500;
        err.message = errors.serverInternal;
        res.status(500).json(err);
    }
});
var multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty({
        uploadDir : path.join(__publicPath,'storage/temporary-files')
    });

app.listen(3000, function () {
    console.log('...APPLICATION RUNNING'.yellow);

});





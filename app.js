"use strict";
var PRODUCTION = "production",
    DEVELOPMENT = "development";
require('app-module-path').addPath(__dirname + '/libraries');

var express = require('express');

var bodyParser = require('body-parser');
var path = require('path');
var winston = require('winston');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/edunet-app');
mongoose.Promise = global.Promise;
var app = express();

// "beach!"
var environment = process.env.NODE_ENV;
if (environment == DEVELOPMENT) {
    app.set('port',3000);
} else {
    app.set('port',80);
}

global.__publicPath = path.normalize(path.join(__dirname,'public'));


var errors = require('system/errors');
app.use(bodyParser.urlencoded({
    extended : true,
    limit : '50mb'
}));
app.use(bodyParser.json({
    limit : '50mb'
}));
app.use(function (req,res,next) {
    next();
});

global.__modelsPath = path.normalize(path.join(__dirname,'server-api/models/'));
global.__publicPath = path.normalize(path.join(__dirname,'public'));
var publicDirectory = __publicPath;


var apiRouter = require('./server-api/routers');
app.use('/api',apiRouter);
app.use(express.static(path.join(__dirname,'public')));


app.get('/', function (req,res,next) {
    var domain = req.subdomains;
    var AUTH_PATH = path.join(__publicPath,'dist/auth/index-' + require('./public/gulp/build').getVersions().auth_app.index_html + '.html');
    var MANUAL_PATH = path.join(__publicPath,'dist/manual/index-' + require('./public/gulp/build').getVersions().manual_app.index_html + '.html');
    var LANDING_PATH = path.join(publicDirectory,'app-landing/index.html');
    if (domain.length == 0) {
        res.sendFile(LANDING_PATH);
    } else if (domain[0] == "help") {
        console.log(MANUAL_PATH);
        res.sendFile(MANUAL_PATH);
    } else {
        res.sendFile(AUTH_PATH);
    }

});
app.get('/teacher', function (req,res,next) {
    res.sendFile(path.join(__publicPath,'dist/teacher/index-' + require('./public/gulp/build').getVersions().teacher_app.index_html + '.html'));
});
app.get('/student',function (req,res,next) {
    res.sendFile(path.join(__publicPath,'dist/student/index-' + require('./public/gulp/build').getVersions().student_app.index_html + '.html'));
});

app.get('/admin', function (req,res,next) {
    res.sendFile(path.join(__publicPath,'dist/admin/index-' + require('./public/gulp/build').getVersions().admin_app.index_html + '.html'));
});
app.get('/test',function (req,res,next) {
    var pathToAuthFile = path.join(publicDirectory,'app-test/index.html');
    res.sendFile(pathToAuthFile);
});
app.get('/manager',function (req,res,next) {
    var pathToAuthFile = path.join(publicDirectory,'app-manager/index.html');
    res.sendFile(pathToAuthFile);
});
app.use(function (err,req,res,next) {
    console.log(new Date());
    console.log(err);
    console.log(req.admin);
    console.log(req.teacher);
    console.log(req.student);
    console.log(req.token);
    console.log(req.body);
    if (err.status !=undefined) {
        res.status(err.status).json(err);
    } else {
        err.status = 500;
        err.message = errors.serverInternal;
        res.status(500).json(err);
    }
});
var multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty({
        uploadDir : path.join(publicDirectory,'temporary-files/temporary-files')
    });

app.listen(3000, function () {
    console.log('APPLICATION RUNNING...');
});







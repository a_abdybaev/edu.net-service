module.exports.sendMessageByGroupID = function (data,cb) {
    var message = data.message,
        groupID = data.groupID;
    Group
        .findOne({
            _id : data.groupID
        })
        .select('students')
        .populate({
            path : 'students',
            select : 'email'
        })
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.notFoundError);
            if (group.students.length == 0) return cb(null);
            var emails = "";
            _.each(group.students,function (student) {
                if (!_.isEmpty(student.email) && _.isEmpty(emails)) return emails+=student.email;
                if (!_.isEmpty(student.email)) return emails+=", " + student.email;
            });
            if (_.isEmpty(emails)) return cb();
            Delivery.sendMessage({
                text : data.text,
                emails : emails,
                subject : data.subject
            },cb);

        });
};
module.exports.sendHomeworkAdditionMessage = function (data,cb) {
    data.subject = "NEW HOMEWORK";
    data.text = ConstantsModule.NEW_HOMEWORK_MESSAGE;
    exports.sendMessageByGroupID(data,cb);
};

var Delivery = require('delivery'),
    ConstantsModule = require('system/constants'),
    Group = require(__modelsPath + 'group'),
    _ = require('underscore'),
    ErrorsModule = require('system/errors');

module.exports.start = function (cb) {
    exports.fixBlocsColor(cb);
};
module.exports.fixBlocsColor = function (cb) {
    console.log("FIX CLOB");
    Bloc
        .find({
            color : {
                $exists : false
            }
        })
        .exec(function (err,blocs) {
            if (err) return cb(err);
            if (_.isEmpty(blocs)) return cb();
            var res = [];
            _.each(blocs,function (bloc,index) {
                res[index] = function (callback) {
                    bloc.color = Constants.GROUP_COLORS[0];
                    bloc.save(callback);
                };
            });
            async.parallel(res,cb);
        });
};

var Bloc = require(__modelsPath + 'group-bloc'),

    async = require('async'),
    _ = require('underscore'),

    Errors = require('system/errors'),
    Constants = require('system/constants');

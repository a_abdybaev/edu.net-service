module.exports.resolve = function (cb) {
    exports.resolveFake(function (err) {
        if (err) return cb(err);
        exports.resolveLessons(function (err) {
            if (err) return cb(err);
            exports.resolveApplications(function (err) {
                if (err) return cb(err);
                exports.resolveGroups(function (err) {
                    if (err) return cb(err);
                    exports.resolveTariffs(function (err) {
                        if (err) return cb(err);
                        exports.resolveStudentsStatus(function (err) {
                            if (err) return cb(err);
                            exports.resolveStudentsGroup(function (err) {
                                if (err) {
                                    err.status = "RESOLVE STUDENTS GROUP ERR";
                                }
                                if (err) return cb(err);
                                exports.resolveCenters(function (err) {
                                    if (err) return cb(err);
                                    exports.resolveStudentsAppearanceArray(function (err) {
                                        if (err) return cb(err);
                                        exports.resolveStudentsGroupHistory(function (err) {
                                            if (err) return cb(err);
                                                exports.resolveComments(function (err) {
                                                    if (err) return cb(err);
                                                    exports.computeStudentsCount(function (err) {
                                                        if (err) return cb(err);
                                                        exports.createTestDirectory(function (err) {
                                                            if (err) return cb(err);
                                                            exports.createDefaultInterview(function (err) {
                                                                if (err) return cb(err);
                                                                exports.deleteWrongTests(function (err) {
                                                                    if (err) return cb(err);
                                                                    exports.resolveStudentsUpdatedDate(cb);
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            // });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};
module.exports.resolveFake = function (cb) {
    // AnswerSheet
    //     .findOne({
    //         studentID : "5978636f19d9d30d6a67834c",
    //         testID : "59b3e6bb52df6d02df611731"
    //     })
    //     .remove(function (err) {
    //         if (err) return cb(err);
    //         TestResult
    //             .findOne({
    //                 studentID : "5978636f19d9d30d6a67834c",
    //                 testID : "59b3e6bb52df6d02df611731"
    //             })
    //             .remove(cb);
    //     });
    cb(null);
};
module.exports.deleteWrongTests = function (cb) {
    var resolveAnswerSheets = function (cb) {
        AnswerSheet
            .find({})
            .select('testID')
            .populate({
                path : 'testID',
                select : 'name'
            })
            .exec(function (err,answerSheets) {
                if (err) return cb(err);
                if (_.isEmpty(answerSheets)) return cb();
                var testIDs = [];
                _.each(answerSheets,function (answerSheet,index) {
                    if (!answerSheet.testID) return testIDs.push(answerSheet._id);
                });
                if (_.isEmpty(testIDs)) return cb();
                AnswerSheet
                    .remove({
                        _id : {
                            '$in' : testIDs
                        }
                    })
                    .exec(cb);
            });
    };
    var resolveTestResultID = function (cb) {
        TestResult
            .find({})
            .populate({
                path : 'testID',
                select : 'name'
            })
            .exec(function (err,testResults) {
                if (err) return cb(err);
                if (_.isEmpty(testResults)) return cb();
                var testIDs = [];
                _.each(testResults,function (testResult) {
                    if (!testResult.testID) return testIDs.push(testResult._id);
                });
                if (_.isEmpty(testIDs)) return cb();
                TestResult
                    .remove({
                        _id : {
                            '$in' : testIDs
                        }
                    })
                    .exec(cb);
            });
    };
    resolveAnswerSheets(function (err) {
        if (err) return cb(err);
        resolveTestResultID(cb);
    });
};
module.exports.createDefaultInterview = function (cb) {
    Interview
        .find({})
        .exec(function (err,interviews) {
            if (err) return cb(err);
            if (interviews.length > 0) return cb();
            var interview = new Interview({
                name: "Общий опрос группы",
                questions: [{
                    text: "Оцените, на сколько Вы довольны пройденным курсом:",
                    answersType: "OUT_OF_FIVE",
                    answers:[]
                },{
                    text: "Оцените, общее качество предоставляемого материала в данном курсе (насколько он был эффективен и полезен):",
                    answersType: "OUT_OF_FIVE",
                    answers:[]
                }, {
                    text: "Оцените, насколько хорошо провел данный курс наш преподаватель:",
                    answersType: "OUT_OF_FIVE",
                    answers:[]
                }, {
                    text: "Оцените, насколько хорошо преподаватель объяснял новый материал:",
                    answersType: "OUT_OF_FIVE",
                    answers:[]
                }, {
                    text: "Оцените, насколько эффективно домашние задания помогали в понимании всего курса:",
                    answersType: "OUT_OF_FIVE",
                    answers:[]
                }, {
                    text: "Оцените, насколько учебные помещения центра оборудованы надлежащим образом:",
                    answersType: "OUT_OF_FIVE",
                    answers:[]
                }, {
                    text: "Вы бы записались на другой курс с тем же преподавателем?",
                    answersType: "OUT_OF_TWO",
                    answers:[]
                }, {
                    text: "Вы бы рекомендовали данный курс своим друзьям и знакомым?",
                    answersType: "OUT_OF_TWO",
                    answers:[]
                }
                ],
                isDefault: true
            });
            interview.save(cb);
        });
};

module.exports.createTestDirectory = function (cb) {
    Center
        .find({})
        .exec(function (err,centers) {
            if (err) return cb(err);
            if (centers.length == 0) return cb();
            var results = [];
            _.each(centers,function (center,index) {
                results[index] = function (callback) {
                    fs.stat(path.join(center.getSourceUrl(),'tests'),function (err,stats) {
                        if (err && err.code != 'ENOENT') return callback(err);
                        if (err && err.code == 'ENOENT') return fs.mkdir(path.join(center.getSourceUrl(),'tests'),callback);
                        callback();
                    });
                };
            });
            async.parallel(results,cb);
        })
};

module.exports.computeStudentsCount = function (cb) {
    Center
        .findOne({})
        .exec(function (err,centers) {
            if (err) return cb(err);
            if (!centers) return cb();
            var results = [];
            _.each(centers,function (center,index) {
                results[index] = function (callback) {
                    exports.computeActiveStudentsByCenterID({
                        centerID : center._id
                    },function (err,active) {
                        if (err) return callback(err);
                        exports.computeArchiveStudentsByCenterID({
                            centerID : center._id
                        },function (err,archive) {
                            if (err) return callback(err);
                            exports.computeWaitingStudentsByCenterID({
                                centerID : center._id
                            },function (err,waiting) {
                                if (err) return callback(err);
                                center.activeStudentsCount = active;
                                center.waitingStudentsCount = waiting;
                                center.archiveStudentsCount = archive;
                                center.save(callback);
                            });
                        });
                    });
                };
            });
            async.series(results,cb);
        });
};
module.exports.computeWaitingStudentsByCenterID = function (data,cb) {
    Student
        .count({
            centerID : data.centerID,
            status : ConstantsModule.STUDENT_STATUS_WAITING
        })
        .exec(cb)
};
module.exports.computeActiveStudentsByCenterID = function (data,cb) {
    Student
        .count({
            centerID : data.centerID,
            status : ConstantsModule.STUDENT_STATUS_ACTIVE
        })
        .exec(cb)
};
module.exports.computeArchiveStudentsByCenterID = function (data,cb) {
    Student
        .count({
            centerID : data.centerID,
            status : ConstantsModule.STUDENT_STATUS_ARCHIVE
        })
        .exec(cb)
};
module.exports.resolveAppearance = function (cb) {

};
// module.exports.createTestsFolder = function (cb) {
//     Center
//         .find({})
//         .exec(function (err,centers) {
//             if (err) return cb(err);
//             var results = [];
//             _.each(centers,function (center,i) {
//                 results[i] = function (callback) {
//                     fs.mkdir(path.join(center.getSourceUrl() + 'tests'),function (err) {
//                         if (err.code == 'EEXIST') return cb(null);
//                         cb(err);
//                     });
//                 };
//             });
//             async.series(results,cb);
//         });
// };
module.exports.resolveStudentsGroupHistory = function (cb) {
    Student
        .find({})
        .exec(function (err,students) {
            if (err) return cb(err);
            if (students.length == 0) return cb(null);
            var results = [];
            students.forEach(function (student,i) {
                results[i] = function (callback) {
                    HelperModule.refreshGroupHistory({
                        studentID : student._id
                    },callback);
                };
            });
            async.series(results,cb);
        });
};
module.exports.resolveStudentsAndGroups = function (cb) {

};



// 1kb = 1000b
// 1mb = 1000kb = 1000000
// 1gb = 1000mb = 1000000000
module.exports.resolveFreeTariff = function (cb) {
    Tariff
        .findOne({
            isFree : true,
            isMajor : true
        })
        .exec(function (err,tariff) {
            if (err) return cb(err);
            if (tariff) return cb();
            var freeTariff = new Tariff({
                name : 'Free',
                students : 25,
                isMajor : true,
                isFree : true,
                size : 300000000,
                price : 0
            });
            return  freeTariff.save(cb);
        });
};

module.exports.resolveTariffs = function (cb) {
    Tariff
        .find({})
        .exec(function (err, tariffs) {
            if (err) return cb(err);
            if (tariffs.length == 0) return cb(null);
            var results = [];
            tariffs.forEach(function (tariff, i) {
                results[i] = function (callback) {
                    if (tariff.name == 'Light') {
                        tariff.size = 2000000000;
                        tariff.students = 80;
                        tariff.isMajor = true;
                        tariff.price = 9500;
                        tariff.isFree = false;
                    }
                    if (tariff.name == 'Premium') {
                        tariff.size = 6000000000;
                        tariff.students = 300;
                        tariff.isMajor = true;
                        tariff.price = 21500;
                        tariff.isFree = false;
                    }
                    if (tariff.name == 'Silver') {
                        tariff.size = 9000000000;
                        tariff.students = 400;
                        tariff.isMajor = true;
                        tariff.price = 35500;
                        tariff.isFree = false;
                    }
                    tariff.save(callback);
                };
            });
            return async.series(results,function (err) {
                if (err) return cb(err);
                return exports.resolveFreeTariff(cb);
            });
        });
};
module.exports.resolveStudentsStatus = function (cb) {
    Student
        .find({})
        .select('status groups')
        .populate({
            path : 'groups',
            select : 'name'
        })
        .exec(function (err,students) {
            if (err) return cb(err);
            if (students.length == 0) return cb(null);
            var results = [];
            students.forEach(function (student,i) {
                results[i] = function (callback) {
                    if (student.status != null && students.status != '') return callback();
                    if (student.groups.length == 0) {
                        student.status = ConstantsModule.STUDENT_STATUS_ARCHIVE
                    } else {
                        student.status = ConstantsModule.STUDENT_STATUS_ACTIVE;
                    }
                    student.save(callback);
                };
            });
            if (results.length == 0) return cb(null);
            async.series(results,cb);
        });
};
module.exports.resolveStudentsUpdatedDate = function (cb) {
    Student
        .find({
            updatedAt : {
                $exists : false
            }
        })
        .select('updatedAt createdAt')
        .exec(function (err,students) {
            if (err) return cb(err);
            if (_.isEmpty(students)) return cb();
            var res = [];
            _.each(students,function (student,index) {
                res[index] = function (callback) {
                    student.updatedAt = student.createdAt;
                    student.save(callback);
                };
            });
            async.parallel(res,cb);
        });
};
module.exports.resolveStudentsGroup = function (cb) {
   Student
       .find({})
       .exec(function (err,students) {
           if (err) return cb(err);
           if (students.length == 0) return cb(null);
           var results = [];
           students.forEach(function (student,i) {
               if (student.groupID == null) return;
               results[i] = function (callback) {
                   student.groups.push(student.groupID);
                   student.groupID = undefined;
                   student.save(callback);
               };
           });
           if (results.length == 0) return cb(null);
           async.series(results,cb);
       });
};
module.exports.resolveStudentsAppearanceArray = function (cb) {
    Student
        .find({})
        .exec(function (err,students) {
            if (err) return cb(err);
            if (students.length == 0) return cb(null);
            var results = [];
            students.forEach(function (student,i) {
                if (student.appearance && Array.isArray(student.appearance)) return;
                results[i] = function (callback) {
                    student.appearance = [];
                    student.save(callback);
                };
            });
            if (results.length == 0) return cb(null);
            async.series(results,cb);
        });
};
module.exports.resolveLessons = function (cb) {
    Lesson
        .find({})
        .populate([{
            path : 'groupID'
        },{
            path : 'classroomID'
        }])
        .exec(function (err,lessons) {
            if (err) return cb(err);
            if (!lessons) return cb(null);
            var results = [];
            lessons.forEach(function (lesson) {
                if (lesson.classroomID == undefined) results.push(lesson._id);
                if (lesson.groupID == undefined) results.push(lesson._id);
            });
            if (results.length == 0) return cb(null);
            Lesson
                .find({
                    _id : {
                        $in : results
                    }
                })
                .remove()
                .exec(cb);
        });
};
module.exports.resolveComments = function (cb) {
    Comment
        .find({})
        .exec(function (err,comments) {
            if (err) return cb(err);
            if (_.isEmpty(comments)) return cb();
            var results = [];
            _.each(comments,function (comment,i) {
                results[i] = function (callback) {
                    if (comment.studentID) {
                        comment.user = {
                            model : ConstantsModule.MODELS.STUDENT,
                            value : comment.studentID
                        };
                        comment.userType = ConstantsModule.STUDENT;
                        comment.studentID = undefined;
                    } else if (comment.teacherID) {
                        comment.user = {
                            model : ConstantsModule.MODELS.TEACHER,
                            value : comment.teacherID
                        };
                        comment.userType = ConstantsModule.TEACHER;
                        comment.teacherID = undefined;
                    } else {
                        return callback();
                    }

                    comment.save(callback);
                };
            });
            async.parallel(results,cb);
        });
};
module.exports.resolveApplications = function (cb) {
    Application
        .find({})
        .populate({
            path : 'groupID'
        })
        .exec(function (err,applications) {
            if (err) return cb(err);
            if (applications.length == 0) return cb(null);
            var results = [];
            applications.forEach(function (application,i) {
                if (application.groupID == null) {
                    results.push(function (callback) {
                        return application.remove(callback);
                    });
                }
            });
            if (results.length == 0)return cb(null);
            async.series(results,cb);
            
        });
};
module.exports.resolveGroups = function (cb) {
    Group
        .find({})
        .select('students')
        .exec(function (err,groups) {
            if (err) return cb(err);
            if (_.isEmpty(groups)) return cb();
            var results =[];
            _.each(groups,function (group,index) {
                results[index] = function (callback) {
                    if (group.individual != null) return callback();
                    group.individual = group.students.length <= 1;
                    group.save(callback);
                };
            });
            return async.parallel(results,cb);
        });
};
module.exports.resolveCenters = function (cb) {
    cb();
    // Center
    //     .find({})
    //     .exec(function (err,centers) {
    //         if (err) return cb(err);
    //         if (centers.length == 0) return cb(null);
    //         var results = [];
    //         centers.forEach(function (center,i) {
    //             results[i] = function (callback) {
    //                 CenterModule.updateStudents.active({
    //                     centerID : center._id,
    //                     clientID : center.clientID
    //                 },callback);
    //             };
    //         });
    //         if (results.length == 0) return cb(null);
    //         async.series(results,cb);
    //     });
};

module.exports.resolveStudentsCount = function (cb) {
    Center
        .find({})
        .select('_id')
        .exec(function (err,centers) {
            if (err) return cb(err);
            if (_.isEmpty(centers)) return cb(null);
            var results = [];
            _.each(centers,function (center,index) {
                results[index] = function (callback) {
                    ConstantsModule.updateStudents.active({
                        centerID : center._id,
                        clientID : center.clientID
                    },callback);
                }
            });
            return async.series(results,cb);
        });
};
var Lesson = require(__modelsPath + 'lesson'),
    Application = require(__modelsPath + 'application'),
    ErrorsModule = require('system/errors'),
    _ = require('underscore'),
    async = require('async'),
    HelperModule = require('system/helper'),
    ConstantsModule = require('system/constants'),
    Group = require(__modelsPath + 'group'),
    Tariff = require(__modelsPath + 'tariff'),
    Center = require(__modelsPath + 'center'),
    Student = require(__modelsPath + 'student'),
    Attendance = require(__modelsPath + 'attendance'),
    Appearance = require(__modelsPath + 'appearance'),
    Comment = require(__modelsPath + 'comment'),
    TestResult = require(__modelsPath + 'tests/test-result'),
    AnswerSheet = require(__modelsPath + 'tests/answer-sheet'),
    CenterModule = require('system/center'),
    Interview = require(__modelsPath + 'interview'),
    fs = require('fs'),
    path = require('path'),
    colors = require('colors');

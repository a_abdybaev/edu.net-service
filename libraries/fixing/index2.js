
module.exports.start = function (cb) {
    exports.createClients(function (err) {
        if (err) return cb(err);
        exports.fromDomainNameToHash(function (err) {
            if (err) return cb(err);
            exports.moveCenterFilesToStorage(function (err) {
                if (err) return cb(err);
                exports.fixCenterImageUrl(function (err) {
                    if (err) return cb(err);
                    exports.fixTeacherGender(function (err) {
                        if (err) return cb(err);
                        exports.fixGroupsSourceUrl(function (err) {
                            if (err) return cb(err);
                            exports.fixAdmins(function (err) {
                                if (err) return cb(err);
                                exports.fixTests(function (err) {
                                    if (err) return cb(err);
                                    exports.fixCenterModel(function (err) {
                                        if (err) return cb(err);
                                        exports.fixStudentModel(function (err) {
                                            if (err) return cb(err);
                                            exports.fixTeacherModel(function (err) {
                                                if (err) return cb(err);
                                                exports.fixTeacherImageUrl(function (err) {
                                                    if (err) return cb(err);
                                                    exports.fixGroups(function (err) {
                                                        if (err) return cb(err);
                                                        cb();
                                                    })
                                                });
                                            });
                                        });
                                    })
                                });
                            });

                        });
                    });
                });

            });
        });
    });
};
module.exports.fixGroups = function (cb) {

    Group
        .find({
            isBloc : {
                $exists : false
            }
        })
        .select('isBloc name')
        .exec(function (err,groups) {
            console.log(groups.length);
            if (err) return cb(err);
            if (_.isEmpty(groups)) return cb();
            var results = [];
            _.each(groups,function (group,index) {
                results[index] = function (callback) {
                    group.isBloc = false;
                    return group.save(callback);
                };
            });
            async.parallel(results,cb);
        });

};
module.exports.fixTeacherModel = function (cb) {
    Teacher
        .find({})
        .select('clientID centerID')
        .populate('centerID')
        .exec(function (err,teachers) {
            if (err) return cb(err);
            if (_.isEmpty(teachers)) return cb();
            var res = [];
            _.each(teachers,function (teacher,index) {
                res[index] = function (callback) {
                    if (!teacher.centerID) return teachers.remove(callback);
                    if (teacher.clientID) return callback();
                    if (!teacher.clientID) teacher.clientID = teacher.centerID.clientID;
                    teacher.save(callback);
                };
            });
            async.parallel(res,cb);
        })
};
module.exports.fixStudentModel = function (cb) {
    Student
        .find({})
        .select('clientID centerID')
        .populate('centerID')
        .exec(function (err,students) {
            if (err) return cb(err);
            if (_.isEmpty(students)) return cb();
             var res = [];
             _.each(students,function (student,index) {
                 res[index] = function (callback) {
                     if (!student.centerID) return student.remove(callback);
                     if (student.clientID) return callback();
                     student.clientID = student.centerID.clientID;
                     student.save(callback);
                 };
             });
             async.parallel(res,cb);
        })
};
module.exports.fixCenterModel = function (cb) {
    Center
        .find({})
        .exec(function (err,centers) {
            if (err) return cb(err);
            if (_.isEmpty(centers)) return cb();
            var res = [];
            _.each(centers,function (center,index) {
                res[index] = function (callback) {
                    center.activeStudentsCount = null;
                    center.waitingStudentsCount = null;
                    center.archiveStudentsCount = null;
                    center.save(callback);
                };
            });
            async.series(res,cb);
        });
};

module.exports.fixTests = function (cb) {
    Test
        .find({})
        .populate('centerID')
        .exec(function (err,tests) {
            if (err) return cb(err);
            var results = [];
            _.each(tests,function (test,index) {
                results[index] = function (callback) {
                    var arrayLink = test.sourceUrl.split("/");
                    if (arrayLink[1] != 'server-files') return callback();
                    test.sourceUrl = path.join(test.centerID.sourceUrl,'tests',arrayLink[arrayLink.length-1]);
                    _.each(test.sections,function (section) {
                        var images = [];
                        _.each(section.images,function (image,index) {
                            var arrayLink = image.split('/');
                            images.push(path.join(test.sourceUrl,arrayLink[arrayLink.length-1]));
                        });
                        section.images = images;
                    });
                    test.save(callback)
                };
            });
            async.parallel(results,cb);
        });
};
module.exports.fixAdmins = function (cb) {
    Admin
        .find({

        })
        .populate({
            path : 'centerID'
        })
        .exec(function (err,admins) {
            if (err) return cb(err);
            if (!admins) return cb();
            var results = [];
            _.each(admins,function (admin,index) {
                results[index] = function (callback) {
                    admin.firstName = undefined;
                    admin.lastName = undefined;
                    admin.email = undefined;
                    if (!admin.centerID) return admin.remove(callback);
                    if (admin.isManager == null) {
                        admin.isManager = true;
                    }
                    if (!admin.clientID) {
                        admin.clientID = admin.centerID.clientID;
                    }
                    admin.save(callback);
                };
            });
            async.parallel(results,cb);
        });
};

module.exports.fixGroupsSourceUrl = function (cb) {
    Group
        .find({})
        .populate('centerID')
        .exec(function (err,groups) {
            if (err) return cb(err);
            if (_.isEmpty(groups)) return cb();
            var results = [];
            _.each(groups,function (group,index) {
                results[index] = function (callback) {
                    var linkArray = group.sourceUrl.split('/');
                    if (linkArray[1] != 'server-files') return callback();
                    group.sourceUrl = path.normalize(path.join(group.centerID.sourceUrl,linkArray[linkArray.length-1]));
                    group.save(callback);
                };
            });
            async.parallel(results,cb);
        })
};
module.exports.fixTeacherGender = function (cb) {
    Teacher
        .find({})
        .populate('centerID')
        .exec(function (err,teachers) {
            if (err) return cb(err);
            if (_.isEmpty(teachers)) return cb();
            var results = [];
            _.each(teachers,function (teacher,index) {
                results[index] = function (callback) {
                    if (!teacher.gender) {
                        teacher.gender = Constants.GENDER.WOMAN;
                    }
                    teacher.save(callback);
                };
            });
            async.parallel(results,cb);
        });
};

module.exports.fixTeacherImageUrl = function (cb) {
    Teacher
        .find({})
        .populate('centerID')
        .exec(function (err,teachers) {
            if (err) return cb(err);
            if (_.isEmpty(teachers)) return cb();
            var results = [];
            _.each(teachers,function (teacher,index) {
                results[index] = function (callback) {
                    if (!teacher.imageUrl) return callback();
                    var arrayLink = teacher.imageUrl.split('/');
                    teacher.imageUrl =  path.normalize(path.join(teacher.centerID.sourceUrl,arrayLink[arrayLink.length-1]));
                    teacher.save(callback);
                };
            });
            async.parallel(results,cb);
        });
};

module.exports.fixCenterImageUrl = function (cb) {
    Center
        .find({})
        .exec(function (err,centers) {
            if (err) return cb(err);
            if (_.isEmpty(centers)) return cb();
            var results = [];
            _.each(centers,function (center,index) {
                results[index] = function (callback) {
                    if (!center.imageUrl) return callback();
                    var linkArray = center.imageUrl.split('/');
                    var serverFile = linkArray[1];
                    if (serverFile != 'server-files') return callback();
                    center.imageUrl = path.normalize(path.join(center.sourceUrl,linkArray[linkArray.length-1]));
                    center.save(cb);
                };
            });
            async.parallel(results,cb);
        });
};

module.exports.moveCenterFilesToStorage = function (cb) {
    Center
        .find({})
        .populate('clientID')
        .exec(function (err,centers) {
            if (err) return cb(err);
            if (_.isEmpty(centers)) return cb();
            var results = [];
            _.each(centers,function (center,index) {
                results[index] = function (callback) {
                    var arrayPath =  center.sourceUrl.split('/');
                    var folderName =  arrayPath[0];
                    if (folderName != 'server-files') return callback();
                    var relPath = path.normalize(path.join(center.clientID.sourceUrl,arrayPath[1])),
                        absPath = path.normalize(path.join(__publicPath,relPath));
                    fsExtra.move(center.getSourceUrl(),absPath,function (err) {
                        if (err) return callback(err);
                        center.sourceUrl = relPath;
                        center.save(callback);
                    });
                };
            });
            async.parallel(results,cb);
        })
};
module.exports.fromDomainNameToHash = function (cb) {
    Center
        .find({})
        .populate('clientID')
        .exec(function (err,centers) {
            if (err) return cb(err);
            if (_.isEmpty(centers)) return cb();
            var results = [];
            _.each(centers,function (center,index) {
                results[index] = function (callback) {
                    var arrayPath =  center.sourceUrl.split('/');
                    var folderName = arrayPath[arrayPath.length-1];
                    if (folderName != center.clientID.link) return callback();
                    var relPath = path.normalize(path.join('server-files',crypto.randomBytes(10).toString('hex'))),
                        absPath = path.normalize(path.join(__publicPath,relPath));
                    fsExtra.move(center.getSourceUrl(),absPath,function (err) {
                        if (err) return callback(err);
                        center.sourceUrl = relPath;
                        center.save(callback);
                    });
                };
            });
            async.parallel(results,cb);
        });
};
module.exports.createClients = function (cb) {
    Center
        .find({})
        .exec(function (err,centers) {
            if (err) return cb(err);
            if (_.isEmpty(centers)) return cb();
            var results = [];
            _.each(centers,function (center,index) {
                results[index] = function (callback) {
                    if (center.clientID) return callback();
                    var client = new Client({
                        name : center.name,
                        link : center.subdomain,
                        tariffID : center.tariffID,
                        isPaid : center.isPaid,
                        students : {
                            active : 0,
                            waiting : 0,
                            archive : 0
                        },
                        centers : [center._id]
                    });
                    require('manager/client').createSourceDirectory(client,function (err) {
                        if (err) return callback(err);
                        client.save(function (err) {
                            if (err) return callback(err);
                            center.clientID = client._id;
                            center.save(callback);
                        });
                    });

                };
            });
            async.parallel(results,cb);
        });
};



var Center = require(__modelsPath + 'center'),
    Client = require(__modelsPath + 'client'),
    Teacher = require(__modelsPath + 'teacher'),
    Group = require(__modelsPath + 'group'),
    Admin = require(__modelsPath + 'admin'),
    ErrorsModule = require('system/errors'),
    Constants = require('system/constants'),
    Test = require(__modelsPath + 'tests'),
    Student = require(__modelsPath + 'student'),
    fsExtra = require('fs-extra'),
    _ = require('underscore'),
    crypto = require('crypto'),
    fs = require('fs'),
    path = require('path'),
    async = require('async');


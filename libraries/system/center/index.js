var updateActiveStudentsCount = function (data,cb) {
    if (!data.centerID || !data.clientID) return cb(Errors.badRequest);
    getCenterByID(data,function (err,center) {
        if (err) return cb(err);
        countStudentsByStatus({
            centerID : data.centerID,
            status : Constants.STUDENT_STATUS_ACTIVE
        },function (err,count) {
            if (err) return cb(err);
            center.students.active = count;
            center.save(function (err) {
                if (err) return cb(err);
                ClientModule.updateStudents.active(data,cb);
            });
        });
    });
};

var updateArchiveStudentsCount = function (data,cb) {
    if (!data.centerID || !data.clientID) return cb(Errors.badRequest);
    getCenterByID(data,function (err,center) {
        if (err) return cb(err);
        countStudentsByStatus({
            centerID : data.centerID,
            status : Constants.STUDENT_STATUS_ARCHIVE
        },function (err,count) {
            if (err) return cb(err);
            center.students.archive = count;
            center.save(function (err) {
                if (err) return cb(err);
                ClientModule.updateStudents.archive(data,cb);
            });
        });
    });
};
var updateWaitingStudentsCount = function (data,cb) {
    if (!data.centerID || !data.clientID) return cb(Errors.badRequest);
    getCenterByID(data,function (err,center) {
        if (err) return cb(err);
        countStudentsByStatus({
            centerID : data.centerID,
            status : Constants.STUDENT_STATUS_WAITING
        },function (err,count) {
            if (err) return cb(err);
            center.students.waiting = count;
            console.log(count);
            center.save(function (err) {
                if (err) return cb(err);
                ClientModule.updateStudents.waiting(data,cb);
            });
        });
    });
};
var getCenterByID = function (data,cb) {
    Center  
        .findOne({
            _id : data.centerID
        })
        .exec(function (err,center) {
            if (err) return cb(err);
            if (!center) return cb(Errors.objNotFound);
            cb(null,center);
        });
};
var countStudentsByStatus = function (data,cb) {
    Student
        .count({
            centerID : data.centerID,
            status : data.status
        })
        .exec(cb);
};
module.exports.updateStudents = {
    active : updateActiveStudentsCount,
    waiting : updateWaitingStudentsCount,
    archive : updateArchiveStudentsCount
};
module.exports.updateSpaceUsage = function (data,cb) {
    if (!data.centerID) return cb(Errors.badRequest);
    getCenterByID(data,function (err,center) {
        if (err) return cb(err);
        FolderSize(center.getSourceUrl(),function (err,space) {
            if (err) return cb(err);
            center.memory = space;
            center.save(function (err) {
                if (err) return cb(err);
                ClientModule.updateSpaceUsage({
                    clientID : center.clientID
                },cb);
            });
        });
    });
};
module.exports.updateSpaceUsageBG = function (data) {
    exports.updateSpaceUsage(data,function () {
        console.log("CENTER SPACE USAGE UPDATED".green);
    });
};
module.exports.getSpaceUsage = function (data,cb) {
    ClientModule.getByID(data,function (err,client) {
        if (err) return cb(err);
        FolderSize(client.getSourceUrl(),cb);
    });
};
module.exports.getFreeSpace = function (data,cb) {
    if (!data.clientID) return cb(Errors.badRequest);
    Client
        .findOne({
            _id : data.clientID
        })
        .select('tariffID memory sourceUrl')
        .populate('tariffID')
        .exec(function (err,client) {
            if (err) return cb(err);
            if (!client) return cb(Errors.objNotFound);
            FolderSize(client.getSourceUrl(),function (err,space) {
                if (err) return cb(err);
                cb(null,client.tariffID.size - space);
            });
        });
};
module.exports.getByID = getCenterByID;
module.exports.isPossibleUploadFile = function (data,cb) {
    if (!data.clientID || !data.fileSize || !data.filePath) return cb(Errors.badRequestError);
    exports.getFreeSpace(data,function (err,space) {
        if (err) return cb(err);
        console.log(space);
        if (space > data.fileSize) return cb();
        HelperModule.deleteFile(data.filePath,function (err) {
            if (err) return cb(err);
            cb(Errors.tariffStorage);
        });
    });
    // Client
    //     .findOne({
    //         _id : data.clientID
    //     })
    //     .select('tariffID memory')
    //     .populate('tariffID')
    //     .exec(function (err,client) {
    //         if (err) return cb(err);
    //         if (!client) return cb(Errors.notFoundError);
    //         if (data.fileSize + client.memory < client.tariffID.size) return cb();
    //
    //     });
};
module.exports.isPossibleUploadFileObject = function (data,cb) {
    exports.isPossibleUploadFile({
        clientID : data.clientID,
        fileSize : data.file.size,
        filePath : data.file.path
    },cb)
};
module.exports.isPossibleUploadFileObjects = function (data,cb) {
    if (!data.clientID || !_.isArray(data.files)) return cb(Errors.badRequest);
    if (_.isEmpty(data.files)) return cb();
    var fileMemory = 0;
    _.each(data.files,function (file) {
        fileMemory+= file.size;
    });
    exports.getFreeSpace(data,function (err,space) {
        if (err) return cb(err);
        if (space > fileMemory) return cb();
        var results = [];
        _.each(data.files,function (file,i) {
            results[i] = function (callback) {
                HelperModule.deleteFile(file.path,callback);
            };
        });
        async.parallel(results,function (err) {
            if (err) return cb(err);
            cb(Errors.tariffError);
        });
    });
};

var Center = require(__modelsPath + 'center'),
    Student = require(__modelsPath + 'student'),
    Constants = require('system/constants'),
    FolderSize = require('get-folder-size'),
    _ = require('underscore'),
    Client = require(__modelsPath + 'client'),
    ClientModule = require('system/client'),
    Errors = require('system/errors'),
    async = require('async'),
    HelperModule = require('system/helper');


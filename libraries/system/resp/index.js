var deleted = "Resource deleted";
var created = "Resource created";
var updated = "Resource updated";
var success = "Success message";

var deletedMessage = {
    message : deleted
};
var createdMessage = {
    message : created
};
var successMessage = {
    message : success
};
var updatedMessage = {
    message : updated
};

module.exports = {
    updatedMessage : updatedMessage,
    deletedMessage : deletedMessage,
    createdMessage : createdMessage,
    successMessage : successMessage
};

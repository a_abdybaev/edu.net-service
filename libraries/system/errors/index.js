var invalidBody = "NotValidBodyRequest",
    badRequest = "BadRequest",
    noToken = "NoTokenProvided",
    invalidToken = "InvalidToken",
    notFound = "NotFound",
    serverInternal= "ServerInternalError",
    invalidLength = "InvalidLengthError",
    duplicateLogin = "DuplicateLogin",
    paymentRequired = "PaymentRequired",
    tariff = "TariffError",
    invalidPassword = "InvalidPassword",
    notAcceptable = "NotAcceptable",
    recoverPasswordTime = "RecoverPasswordTime",
    duplicateAttendanceDay = "DuplicateAttendanceDay",
    pathNotArray = "PathNotArray",
    objectNotFound = "ObjectNotFound",
    invalidArguments = "InvalidArguments",
    modelNotFound = "modelNotFound",
    unauthorized = "UnAuthorizedError",
    conflict = "conflict";


var recoverPasswordTimeError = new Error();
recoverPasswordTimeError.status = 400;
recoverPasswordTimeError.name = recoverPasswordTime;
var notFoundError = new Error();
notFoundError.status = 404;
notFoundError.name = notFound;



var pathNotArrayError = Object.assign({},notFoundError);
pathNotArrayError.message = pathNotArray;
var objectNotFoundError = Object.assign({},notFoundError);
objectNotFoundError.message = objectNotFound;
var modelNotFoundError = Object.assign({},notFoundError);
modelNotFoundError.message = objectNotFound;

var badRequestError = new Error();
badRequestError.status = 400;
badRequestError.name = badRequest;

var invalidArgumentsError = Object.assign({},badRequestError);
invalidArgumentsError.message = invalidArguments;

var noTokenError = new Error();
noTokenError.status = 401;
noTokenError.name = noToken;

var invalidTokenError = new Error();
invalidTokenError.status = 403;
invalidTokenError.name = invalidToken;


var invalidBodyError = new Error();
invalidBodyError.status = 400;
invalidBodyError.name = invalidBody;

var serverInternalError = new Error();
serverInternalError.status = 500;
serverInternalError.name = serverInternal;

var invalidPasswordError = new Error();
invalidPasswordError.status = 404;
invalidPasswordError.name = invalidPassword;

var invalidLengthError = new Error();
invalidLengthError.status = 400;
invalidLengthError.name = invalidLength;

var notAcceptableError = new Error();
notAcceptableError.status = 406;
notAcceptableError.name = notAcceptable;

var duplicateLoginError = Object.assign({},notAcceptableError);
duplicateLoginError.message = duplicateLogin;

var duplicateAttendanceDayError = Object.assign({},notAcceptableError);
duplicateAttendanceDayError.message = duplicateAttendanceDay;

var paymentRequiredError = new Error();
paymentRequiredError.status = 402;
paymentRequiredError.name = paymentRequired;

var conflictError = new Error();
conflictError.status = 406;
conflictError.name = conflict;

var tariffError = Object.assign({},notAcceptableError);
tariffError.message = tariff;

var invalidStartAndEndTimeError = Object.assign({},notAcceptableError);
invalidStartAndEndTimeError.message = "Start lesson time more than end time";

var lessonHasPadsError = Object.assign({},notAcceptableError);
lessonHasPadsError.message = "Lesson has pads";

var unauthorizedError = Object.assign({},noTokenError);
unauthorizedError.name = unauthorized;
var objNotFound = Object.assign({},notFoundError);
objNotFound.name = "ObjectNotFoundError";



var tariffErrorStudents = Object.assign({},notAcceptableError);
tariffErrorStudents.name = tariff;
tariffErrorStudents.value = "StudentsCount";

var tariffErrorStorage = Object.assign({},notAcceptableError);
tariffErrorStorage.name = tariff;
tariffErrorStorage.value = "MemoryUsage";


var loginExist = Object.assign({},notAcceptableError);
loginExist.message = "User exist with such phone";
var sendNotAcceptable = function (data) {
    notAcceptableError.message = data;
    return notAcceptableError;
};
var sendNotFound = function (data) {

};
var sendInvalidBodyError = function (data) {
    invalidBodyError.message = data;
    return invalidBodyError;
};
var sendErrorData = function (data) {
    notFoundError.message = data;
    return notFoundError;
};


module.exports = {
    sendNotAcceptable : sendNotAcceptable,
    sendInvalidBodyError : sendInvalidBodyError,
    sendErrorData : sendErrorData,
    invalidBody: invalidBodyError,
    badRequest: badRequestError,
    noToken: noTokenError,
    invalidToken: invalidTokenError,
    notFound: notFound,
    serverInternal: serverInternal,
    invalidLength : invalidLength,
    loginExist : loginExist,
    notAcceptable : notAcceptable,
    paymentRequired : paymentRequired,
    invalidPassword : invalidPassword,
    notFoundError : notFoundError,
    unauthorized : unauthorized,
    tariffStudents : tariffErrorStudents,
    tariffStorage : tariffErrorStorage,


    paymentRequiredError : paymentRequiredError,
    invalidBodyError : invalidBodyError,
    badRequestError : badRequestError,
    noTokenError: noTokenError,
    invalidTokenError: invalidTokenError,
    invalidLengthError : invalidLengthError,
    notAcceptableError : notAcceptableError,
    duplicateLoginError : duplicateLoginError,
    tariffError : tariffError,
    invalidStartAndEndTimeError : invalidStartAndEndTimeError,
    lessonHasPadsError : lessonHasPadsError,
    invalidPasswordError : invalidPasswordError,
    duplicateAttendanceDayError : duplicateAttendanceDayError,
    objectNotFoundError : objectNotFoundError,
    pathNotArrayError : pathNotArrayError,
    invalidArgumentsError : invalidArgumentsError,
    modelNotFoundError : modelNotFoundError,
    unauthorizedError : unauthorizedError,
    conflictError : conflictError,
    objNotFound : objNotFound

};
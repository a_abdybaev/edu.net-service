// STUDENT-TYPE
var STUDENT_STATUS_WAITING = "WAITING",
    STUDENT_STATUS_ACTIVE = "ACTIVE",
    STUDENT_STATUS_ARCHIVE = "ARCHIVE";
module.exports.STUDENT_STATUS_WAITING = STUDENT_STATUS_WAITING;
module.exports.STUDENT_STATUS_ACTIVE = STUDENT_STATUS_ACTIVE;
module.exports.STUDENT_STATUS_ARCHIVE = STUDENT_STATUS_ARCHIVE;
module.exports.STUDENTS_STATUSES = [STUDENT_STATUS_ACTIVE,STUDENT_STATUS_ARCHIVE,STUDENT_STATUS_WAITING];


// GROUP-TYPE
var GROUP_TYPE_INDIVIDUAL = "INDIVIDUAL",
    GROUP_TYPE_GENERAL = "GENERAL";

module.exports.GROUP_TYPE_INDIVIDUAL = GROUP_TYPE_INDIVIDUAL;
module.exports.GROUP_TYPE_GENERAL = GROUP_TYPE_GENERAL;
module.exports.GROUP_TYPES = [GROUP_TYPE_GENERAL,GROUP_TYPE_INDIVIDUAL];
module.exports.GROUP_COLORS = ["#fd6461","#f7a650","#71ca58","#51baf2","#d08ce0"];



// ATTENDANCE
var STUDENT_APPEARANCE_PRESENCE = "PRESENCE",
    STUDENT_APPEARANCE_ABSENCE = "ABSENCE",
    STUDENT_APPEARANCE_ABSENCE_REASON = "ABSENCE_REASON";
module.exports.STUDENT_APPEARANCE_PRESENCE = STUDENT_APPEARANCE_PRESENCE;
module.exports.STUDENT_APPEARANCE_ABSENCE = STUDENT_APPEARANCE_ABSENCE;
module.exports.STUDENT_APPEARANCE_ABSENCE_REASON = STUDENT_APPEARANCE_ABSENCE_REASON;
module.exports.STUDENT_APPEARANCE = [STUDENT_APPEARANCE_PRESENCE,STUDENT_APPEARANCE_ABSENCE_REASON,STUDENT_APPEARANCE_ABSENCE];




//STUDENT AGE_GROUPS
var STUDENT_AGE_CHILDREN = "STUDENT_CHILDREN",
    STUDENT_AGE_TEENAGE = "STUDENT_TEENAGE",
    STUDENT_AGE_YOUTH = "STUDENT_YOUTH",
    STUDENT_AGE_ADULT = "STUDENT_ADULT";
module.exports.STUDENT_AGE_GROUPS = [STUDENT_AGE_ADULT,STUDENT_AGE_CHILDREN,STUDENT_AGE_TEENAGE,STUDENT_AGE_YOUTH];



var STUDENT_STUDY_TIME_MORNING = "MORNING_TIME",
    STUDENT_STUDY_TIME_LUNCH = "LUNCH_TIME",
    STUDENT_STUDY_TIME_EVENING = "EVENING_TIME";
module.exports.STUDENT_STUDY_TIMES = [STUDENT_STUDY_TIME_MORNING,STUDENT_STUDY_TIME_LUNCH,STUDENT_STUDY_TIME_EVENING];


// USER TYPES
module.exports.ADMIN = "ADMIN";
module.exports.TEACHER = "TEACHER";
module.exports.STUDENT = "STUDENT";
module.exports.TESTING = "TESTING";
module.exports.CLASSROOM = "CLASSROOM";
module.exports.NOT_TESTING = "NOT_TESTING";
module.exports.ADMIN_MODEL = "Admin";
module.exports.TEACHER_MODEL = "Teacher";
module.exports.TEST_CATEGORY_DEFAULT = "TEST_DEFAULT";
module.exports.TEST_CATEGORIES = [exports.TEST_CATEGORY_DEFAULT];
module.exports.TEST_PAGES_ALL = "ALL";
module.exports.TEST_PAGES_INTERVAL = "INTERVAL";


module.exports.MODELS = {
    ADMIN : 'Admin',
    TEACHER : "Teacher",
    STUDENT : "Student",
    CENTER : "Center",
    CLIENT : "Client",
    TARIFF : "Tariff",
    LESSON : "Lesson",
    POST : "Post",
    HOMEWORK : "Homework",
    APPLICATION : "Application",
    APPLICATION_ANSWERS : "ApplicationAnswer",
    CLASSROOM : "Classroom",
    COMMENTS : "Comment",
    TEST_RESULT : "TestResult",
    FILE : "File",
    TEST : "Test",
    GROUP : "Group",
    INTERVIEW_RESULT : "InterviewResult",
    INTERVIEW : "Interview",
    ANSWER_SHEET : "AnswerSheet",
    SUBJECT : "Subject",
    MANAGER : "Manager",
    APPEARANCE : "Appearance",
    ATTENDANCE : "Attendance",
    PAYMENTS : "Payment",
    LANDING_REQUEST : "LandingRequest",
    GROUP_BLOC : "GroupBloc"
};



module.exports.activeStudentsPerPage = 20;
module.exports.waitingStudentsPerPage = 20;
module.exports.archiveStudentsPerPage = 20;

module.exports.POSTS_PER_PAGE = 5;

module.exports.NEW_HOMEWORK_MESSAGE = "Добавлено новое домашнее задание!";
module.exports.NEW_POST_MESSAGE = "Добавлен новый пост";

module.exports.INTERVIEW_ANSWERS_OUT_OF_FIVE = "OUT_OF_FIVE";
module.exports.INTERVIEW_ANSWERS_OUT_OF_FIVE_ARRAY = [1,2,3,4,5];
module.exports.INTERVIEW_ANSWERS_OUT_OF_TWO = "OUT_OF_TWO";
module.exports.INVERVIEW_ANSWERS_OUT_OF_TWO_ARRAY = [0,1];
module.exports.ANOTHER = "ANOTHER";
module.exports.CUSTOM = "CUSTOM";


var GENDER = {
    MAN : "MAN",
    WOMAN : "WOMAN"
};
module.exports.GENDER = {
    MAN : GENDER.MAN,
    WOMAN : GENDER.WOMAN,
    ARRAY : [GENDER.WOMAN,GENDER.MAN]
};























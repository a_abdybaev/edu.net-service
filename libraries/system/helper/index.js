var path = require('path'),
    multipart = require('connect-multiparty');

module.exports.addObjectInArray = function (data,cb) {
    var subModelID = data.objectID,
        subModelField = data.objectPath,
        modelID = data.modelID,
        modelName = data.modelName;

    if (!subModelID || !subModelField || !modelID || !modelName) return cb(ErrorsModule.invalidArgumentsError);
    var Model = mongoose.model(modelName);
    Model
        .findOne({
            _id : modelID
        })
        .exec(function (err,model) {
            if (err) return cb(err);
            if (!model) return cb(ErrorsModule.modelNotFoundError);
            if (!Array.isArray(model[subModelField])) return cb(ErrorsModule.pathNotArrayError);
            model[subModelField].push(subModelID);
            model.save(cb);
        });
};
module.exports.deleteObjectFromArray = function (data,cb) {
    if (!data.modelName || !data.modelID || !data.objectID || !data.objectPath) return cb(ErrorsModule.invalidArgumentsError);
    var Query = {
        _id : data.modelID
    };
    if (data.centerID) Query.centerID = data.centerID;

    mongoose.model(data.modelName)
        .findOne(Query)
        .select(data.objectPath)
        .exec(function (err,model) {
            if (err) return cb(err);
            if (!model) return cb(ErrorsModule.modelNotFoundError);

            if (!Array.isArray(model[data.objectPath])) return cb(ErrorsModule.pathNotArrayError);
            var index = model[data.objectPath].map(function (objectID) {
                return objectID.toString();
            }).indexOf(data.objectID.toString());
            if (index < 0) {
                data.error = ErrorsModule.objectNotFoundError;
                LoggerModule.log(data);
            } else {
                model[data.objectPath].splice(index,1);
            }
            return model.save(cb);
        });

};
module.exports.refreshGroupHistory = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID
        })
        .select('groups history groupsHistory firstName')
        .populate({
            path : 'groups',
            select : 'name _id'
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.modelNotFoundError);
            if (student.groups.length == 0) return student.save(cb);
            if (_.isEmpty(student.history.groups)) {
                _.each(student.groups,function (group) {
                    student.history.groups.push({
                        groupID : group._id,
                        name : group.name
                    });
                });
                return student.save(cb);
            }
            var results = [],
                history = student.history;
            _.each(student.groups,function (group) {
                var index = _.map(student.history.groups,function (groupInHistory) {
                    return groupInHistory.groupID.toString()
                }).indexOf(group._id.toString());
                if (index > -1) return;
                results.push({
                    groupID : group._id,
                    name : group.name
                });
            });

            if (_.isEmpty(results)) return student.save(cb);
            student.history.groups.concat(results);
            return student.save(cb);
        });
};
module.exports.deleteDirectory = function (url,cb) {
    fs.stat(url,function (err,stats) {
        if (err || !stats.isDirectory()) {
            LoggerModule.log({
                message : "THE ARE NO SUCH FOLDER",
                url : url,
                err : err
            });
            return cb();

        }
        fse.remove(url,cb);
    });
};
module.exports.deleteFile = function (url,cb) {
    fs.stat(url,function (err,stats) {
        if (err || !stats.isFile()) {
            LoggerModule.log({
                message : "THE ARE NO SUCH FILE",
                url : url,
                err : err
            });
            return cb();
        }
        fs.unlink(url,cb);
    });
};
module.exports.deleteFileObject = function (data,cb) {
    return exports.deleteFile(data.file.path,cb);
};





// FROM GENERAL

module.exports.CenterModule = CenterModule;
module.exports.isAvailableSpace = function (data,cb) {
    CenterModule.updateSpaceUsage(data,function (err) {
        if (err) return cb(err);
        Center
            .findOne({
                _id : centerID
            })
            .select('tariffID memory sourceUrl')
            .populate('tariffID')
            .exec(function (err,center) {
                if (err) return cb(err);
                if (!center) return cb(ErrorsModule.objNotFound);
                cb(null,center.memory + data.size  <= center.tariffID.size);
            });
    });
};
module.exports.updateGroupSpaceUsage = function (data,cb) {
    if (!data.groupID) return cb(ErrorsModule.badRequest);
    Group
        .findOne({
            _id : data.groupID
        })
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.objNotFound);
            FolderSize(group.getSourceUrl(),function (err,space) {
                if (err) return cb(err);
                group.memory = space;
                group.save(function (err) {
                    if (err) return cb(err);
                    CenterModule.updateSpaceUsageBG({
                        centerID : group.centerID
                    },cb);
                });
            });
        });
};
module.exports.updateGroupSpaceUsageSync = function (data) {
    exports.updateGroupSpaceUsage(data,function () {
        console.log("UPDATED");
    });
};

module.exports.filesMiddleWare = multipart({uploadDir : path.join(__publicPath,'storage/temporary-files')});
// FROM GENERAL

var mongoose = require('mongoose'),
    LoggerModule = require('system/logger'),
    fse = require('fs-extra'),
    FolderSize = require('get-folder-size'),
    fs = require('fs'),
    _ = require('underscore'),
    Student = require(__modelsPath + 'student'),
    Group = require(__modelsPath + 'group'),
    Center = require(__modelsPath + 'center'),
    CenterModule = require('system/center'),
    ErrorsModule = require('system/errors');
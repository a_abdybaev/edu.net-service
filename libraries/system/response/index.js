module.exports.created = function (res) {
    res.status(201).json(ResponseStatus.createdMessage);
};
module.exports.deleted = function (res) {
    res.status(200).json(ResponseStatus.deletedMessage);
};
module.exports.updated = function (res) {
    res.status(200).json(ResponseStatus.updatedMessage);
};
module.exports.success = function (res) {
    res.status(200).json(ResponseStatus.successMessage);
};
module.exports.get = function (res,data) {
    res.status(200).json(data);
};


var ResponseStatus = require('system/resp');
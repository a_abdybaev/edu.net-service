'use strict';

var nconf = require('nconf');
var path = require('path');
nconf.argv()
    .env()
    .file({file : path.join(__dirname,'config.json')});
var publicFolder = path.join(__dirname,'../../public/');
var appFolder = path.join(__dirname, '../../');
var modelFolder = path.join(appFolder,'server-api/models/');
nconf.set('public',publicFolder);
nconf.set('appPath',appFolder);
nconf.set('modelPath',modelFolder);
module.exports = nconf;
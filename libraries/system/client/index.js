module.exports.isPaid = function (data,cb) {
    Client
        .findOne({
            _id : data.clientID
        })
        .exec(function (err,client) {
            if (err) return cb(err);
            if (!client) return cb(Errors.objNotFound);
            cb(null,client.isPaid);
        })
};

var updateActiveStudentsCount = function (data,cb) {
    getClientByID(data,function (err,client) {
        if (err) return cb(err);
        countStudentsByStatus({
            clientID : data.clientID,
            status : Constants.STUDENT_STATUS_ACTIVE
        },function (err,count) {
            if (err) return cb(err);
            client.students.active = count;
            client.save(cb);
        });
    });
};

var updateArchiveStudentsCount = function (data,cb) {
    getClientByID(data,function (err,client) {
        if (err) return cb(err);
        countStudentsByStatus({
            clientID : data.clientID,
            status : Constants.STUDENT_STATUS_ARCHIVE
        },function (err,count) {
            if (err) return cb(err);
            client.students.archive = count;
            client.save(cb);
        });
    });
};
var updateWaitingStudentsCount = function (data,cb) {
    getClientByID(data,function (err,client) {
        if (err) return cb(err);
        countStudentsByStatus({
            clientID : data.clientID,
            status : Constants.STUDENT_STATUS_WAITING
        },function (err,count) {
            if (err) return cb(err);
            client.students.waiting = count;
            client.save(cb);
        });
    });
};
var getClientByID = function (data,cb) {
    Client
        .findOne({
            _id : data.clientID
        })
        .exec(function (err,client) {
            if (err) return cb(err);
            if (!client) return cb(Errors.objNotFound);
            cb(null,client);
        });
};
var countStudentsByStatus = function (data,cb) {
    Student
        .count({
            client : data.clientID,
            status : data.status
        })
        .exec(cb);
};
module.exports.getByID = getClientByID;
module.exports.updateStudents = {
    active : updateActiveStudentsCount,
    waiting : updateWaitingStudentsCount,
    archive : updateArchiveStudentsCount
};

module.exports.updateSpaceUsage = function (data,cb) {
    if (!data.clientID) return cb(Errors.badRequest);
    getClientByID(data,function (err,client) {
        if (err) return cb(err);
        FolderSize(client.getSourceUrl(),function (err,space) {
            if (err) return cb(err);
            client.memory = space;
            client.save(cb);
        });
    });
};



var Client = require(__modelsPath + 'client'),
    Student = require(__modelsPath + 'student'),
    Constants = require('system/constants'),
    Errors = require('system/errors'),
    FolderSize = require('get-folder-size');



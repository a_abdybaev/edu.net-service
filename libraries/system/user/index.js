module.exports.isExistUser = function (data,cb) {
    Client
        .findOne({
            _id : data.user.clientID
        })
        .exec(function (err,client) {
            if (err) return cb(err);
            if (!client) return cb(Errors.objNotFound);
            var login = client.link + '.' + data.user.phone;
            Admin
                .findOne({
                    login : login
                })
                .exec(function (err,admin) {
                    if (err) return cb(err);
                    if (admin) return cb(null,true);
                    Teacher
                        .findOne({
                            login : login
                        })
                        .exec(function (err,teacher) {
                            if (err) return cb(err);
                            if (teacher) return cb(null,true);
                            Student
                                .findOne({
                                    login : login
                                })
                                .exec(function (err,student) {
                                    if (err) return cb(err);
                                    if (student) return cb(null,true);
                                    return cb(null,false);
                                });
                        });
                });
        });
};

module.exports.isValidUser = function (data,cb) {
    if (data.user.phone.toString().length != 10) return cb(Errors.invalidBody);
    exports.isExistUser(data,function (err,isExist) {
        if (err) return cb(err);
        if (isExist) return cb(Errors.loginExist);
        cb();
    });
};
module.exports.generateToken = function (data,cb) {
    data.user.token = {
        value : crypto.randomBytes(100).toString('hex'),
        createdAt : Date.now()
    };
    exports.updateVisit(data,function (err) {
        if (err) return cb(err);
        cb(null,data.user.token.value);
    });
};
module.exports.updateVisit = function (data,cb) {
    data.user.lastVisit = Date.now();
    data.user.save(cb);
};
module.exports.isValidPass = function (data) {
    var hash = crypto.pbkdf2Sync(data.password,data.user.salt,1000,64).toString('hex');
    return data.user.hash === hash;
};
module.exports.updatePass = function (data,cb) {
    data.password = data.oldPass;
    if (!this.isValidPass(data)) return cb(Errors.invalidPasswordError);
    if (data.newPass.toString().length <6 ) return cb(Errors.notAcceptable);
    data.user.hash = crypto.pbkdf2Sync(data.newPass,data.user.salt,1000,64).toString('hex');
    data.user.save(cb);
};
module.exports.initUser = function (data,cb) {
    exports.isValidUser(data,function (err) {
        if(err) return cb(err);
        var user = data.user;
        Client
            .findOne({
                _id : user.clientID
            })
            .exec(function (err,client) {
                if (err) return cb(err);
                if (!client) return cb(Errors.objNotFound);
                user.login = client.link + '.' + data.user.phone;
                var currentYear = new Date(),
                    password = client.link + currentYear.getFullYear();
                user.salt = crypto.randomBytes(16).toString('hex');
                user.hash = crypto.pbkdf2Sync(password,user.salt,1000,64).toString('hex');
                cb();
            });
    });
};


var Errors = require('system/errors'),


    Admin = require(__modelsPath + 'admin'),
    Student = require(__modelsPath + 'student'),
    Teacher = require(__modelsPath + 'teacher'),
    Client = require(__modelsPath + 'client'),
    crypto = require('crypto');
module.exports.getResults = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .select('groups')
        .populate({
            path : 'groups',
            select : 'tests',
            match : {
                _id : data.groupID
            }
        })
        .exec(function (err,student) {
            console.log(student);
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.objNotFound);
            if (_.isEmpty(student.groups)) return cb(ErrorsModule.objNotFound);
            if (_.isEmpty(student.groups[0].tests)) return cb(null,[]);
            AnswerSheet
                .find({
                    studentID : data.studentID,
                    centerID : data.centerID,
                    testID : {
                        '$in' : student.groups[0].tests
                    }
                })
                .select('resultID testID createdAt')
                .populate([{
                    path : 'testID',
                    select : 'name'
                },{
                    path : 'resultID'
                }])
                .exec(cb);
        });
};

var ErrorsModule = require('system/errors'),
    _ = require('underscore'),
    AnswerSheet = require(__modelsPath + 'tests/answer-sheet'),
    Student = require(__modelsPath  + 'student');

module.exports.getGroupInfo = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .select('groups')
        .populate({
            path : 'groups',
            select : 'teacherID students',
            populate : [{
                path : 'teacherID',
                select : 'firstName lastName imageUrl email'
            },{
                path : 'students',
                select : 'firstName lastName imageUrl email'
            }],
            match : {
                _id : data.groupID
            }
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            if (student.groups.length == 0) return cb(ErrorsModule.badRequestError);
            cb(null,student.groups[0]);
        });
};


var ErrorsModule = require('system/errors'),
    Student = require(__modelsPath + 'student'),
    Group = require(__modelsPath + 'group');



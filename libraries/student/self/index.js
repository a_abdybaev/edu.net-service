module.exports.updateImage = function (data,cb) {
    var image = data.image;
    exports.deleteImage(data,function (err) {
        if (err) return cb(err);
        Center
            .findOne({
                _id : data.centerID
            })
            .exec(function (err,center) {
                if (err) return cb(err);
                if (!center) return cb(ErrorsModule.notFoundError);
                var ext = image.split(';')[0].match(/jpeg|png|gif/)[0];
                var imageData = image.replace(/^data:image\/\w+;base64,/,'');
                var fileName = crypto.randomBytes(10).toString('hex') + '.' + ext;
                var publicImagePath = center.sourceUrl + fileName;
                var privateImagePath = center.getSourceUrl() + fileName;
                fs.writeFile(privateImagePath,imageData,{
                    encoding : 'base64'
                },function (err) {
                    if (err) return cb(err);
                    Student
                        .findOne({
                            _id : data.studentID,
                            centerID : data.centerID
                        })
                        .exec(function (err,student) {
                            if (err) return cb(err);
                            if (!student) return cb(ErrorsModule.notFoundError);
                            student.imageUrl = publicImagePath;
                            student.save(cb);
                        });
                });
            });
    });
};
module.exports.deleteImage = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            if (student.imageUrl == undefined) return cb(null);
            var pathToFile = path.normalize(path.join(__publicPath,student.imageUrl));
            fs.stat(pathToFile,function (err,stats) {
                if (err) return cb(null);
                if (stats.isFile() == false) return cb(null);
                fse.remove(pathToFile,function (err) {
                    return cb(err);
                });
            });
        });
};
module.exports.updateUserInfo = function (data,cb) {
    Student
        .findOne({
            centerID : data.centerID,
            _id : data.studentID
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            student.firstName = data.firstName;
            student.lastName = data.lastName;
            student.email = data.email;
            student.save(cb);
        });
};
module.exports.updatePassword = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .select('hash salt')
        .exec(function (err,student) {
            if(err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            data.user = student;
            UserModule.updatePass(data,cb);
        });
};
module.exports.getUserInfo = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .populate({
            path : 'groups',
            populate : {
                path : 'teacherID'
            }
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            return cb(null,student);
        });
};

var ErrorsModule = require('system/errors'),
    Center = require(__modelsPath + 'center'),
    Student = require(__modelsPath  + 'student'),
    UserModule = require('system/user'),
    fse =  require('fs-extra'),
    path = require('path'),
    crypto = require('crypto'),
    fs = require('fs');
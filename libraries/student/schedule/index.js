module.exports.getSchedule = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .select('groups')
        .populate({
            path : 'groups',
            select : 'schedule',
            populate : {
                path : 'schedule'
            },
            match : {
                _id : data.groupID
            }
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(errors.notFoundError);
            if (student.groups.length == 0) return cb(errors.badRequestError);
            return cb(null,student.groups[0].schedule);
        });
};


var errors = require('system/errors'),
    Student = require(__modelsPath + 'student');



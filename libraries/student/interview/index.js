module.exports.isValidInterview = function (data) {
    if (data.isDefault == null || _.isEmpty(data.name) || _.isEmpty(data.questions)) return false;
    var result = true;
    _.each(data.questions,function (question) {
        if (_.isEmpty(question.text)) return result = false;
        if (question.answersType == ConstantsModule.INTERVIEW_ANSWERS_OUT_OF_FIVE || question.answersType == ConstantsModule.INTERVIEW_ANSWERS_OUT_OF_TWO) return;
        if (question.answersType != ConstantsModule.CUSTOM) return result = false;
        if (_.isEmpty(question.answers) || !_.isArray(question.answers)) result = false;
        _.each(question,answers,function (answer) {
            if (_.isEmpty(answer)) return result = false;
        });
    });
    return result;
};
module.exports.resolve = function (data,cb) {
    InterviewResult
        .findOne({
            centerID : data.centerID,
            groupID : data.groupID
        })
        .select('students interviewID')
        .populate({
            path : 'interviewID',
            select : 'questions'
        })
        .exec(function (err,interviewResult) {
            if (err) return cb(err);
            if (!interviewResult) return cb(null,{
                isExist : false
            });
            var index = _.map(interviewResult.students,function (studentID) {
                return studentID.toString()
            }).indexOf(data.studentID.toString());
            if (index > -1) return cb(null,{
                isExist: false
            });
            return cb(null,{
                isExist : true,
                interview : interviewResult.interviewID
            });
        });

};
module.exports.addAnswer = function (data,cb) {
    InterviewResult
        .findOne({
            centerID : data.centerID,
            groupID : data.groupID
        })
        .populate({
            path : 'interviewID'
        })
        .exec(function (err,interviewResult) {
            if(err) return cb(err);
            if (!interviewResult) return cb(ErrorsModule.notFoundError);
            var indexOfStudent = _.each(interviewResult.students,function (studentID) {
                return studentID.toString();
            }).indexOf(data.studentID.toString());
            if (indexOfStudent > -1) return cb(ErrorsModule.notAcceptableError);
            var interview = interviewResult.interviewID,
                outOfFive = ConstantsModule.INTERVIEW_ANSWERS_OUT_OF_FIVE_ARRAY,
                outOfTwo =  ConstantsModule.INVERVIEW_ANSWERS_OUT_OF_TWO_ARRAY;

            if (data.results.length != interview.questions.length) return cb(ErrorsModule.sendInvalidBodyError("Answers count not equal to questions count"));
            var isError = false,
                index,
                analytics;

            _.each(interview.questions,function (question,questionNumber) {
                if (question.answersType == ConstantsModule.INTERVIEW_ANSWERS_OUT_OF_FIVE) {
                    index = outOfFive.indexOf(data.results[questionNumber]);
                } else if (question.answersType == ConstantsModule.INTERVIEW_ANSWERS_OUT_OF_TWO) {
                    index = outOfTwo.indexOf(data.results[questionNumber]);
                } else return isError = true;
                if (index < 0) return isError;
                analytics = interviewResult.questions[questionNumber].analytics.slice(0);
                analytics[index]++;
                interviewResult.questions[questionNumber].analytics = analytics;
            });
            if (isError) return cb(ErrorsModule.sendNotAcceptable("Questions has not acceptable answer"));
            interviewResult.students.push(data.studentID);
            interviewResult.save(cb);

        });
};



var ErrorsModule = require('system/errors'),
    InterviewResult = require(__modelsPath + 'interview-result'),
    Interview = require(__modelsPath + 'interview'),

    Group = require(__modelsPath + 'group'),
    ConstantsModule = require('system/constants'),
    _ = require('underscore');

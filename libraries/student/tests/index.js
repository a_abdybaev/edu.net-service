module.exports.getTests = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .select('groups answers')
        .populate([{
            path : 'groups',
            select : 'tests',
            match : {
                _id : data.groupID
            },
            populate : {
                path : 'tests',
                select : 'name sections'
            }
        },{
            path : 'answers',
            match : {
                isEnded : true
            },
            populate : [{
                path : 'resultID'
            },{
                path : 'testID',
                select : 'name sections'
            }]
        }])
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            if (student.groups.length == 0) return cb(ErrorsModule.badRequestError);
            var obj = {
                active : student.groups[0].tests
            };
            if (student.answers.length == 0) return cb(null,obj);
            obj.ended = _.map(student.answers,function (answer) {
                if (answer.testID == null) return;
                var index = _.map(obj.active,function (test) {
                    return test._id.toString();
                }).indexOf(answer.testID._id.toString());
                if (index > -1) obj.active.splice(index,1);
                return {
                    name : answer.testID.name,
                    sections : answer.testID.sections,
                    resultID : answer.resultID
                }
            });
            cb(null,obj);
        })
};

var Group = require(__modelsPath + 'group'),
    _ = require('underscore'),
    Student = require(__modelsPath + 'student'),
    ErrorsModule = require('system/errors');
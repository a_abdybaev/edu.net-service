module.exports.auth = function (data,cb) {
    Student
        .findOne({
            'token.value' : data.token
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(Errors.invalidTokenError);
            data.user = student;
            UserModule.updateVisit(data,function (err) {
                cb(err,student);
            });
        });
};
module.exports.login = function (data,cb) {
    Student
        .findOne({
            login : data.login
        })
        .select('hash salt centerID isTesting testID clientID')
        .populate('centerID')
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb();

            if (!student.centerID) return student.remove(function (err) {
                if (err) return (err);
                cb(Errors.objNotFound);
            });
            data.user = student;
            if (!UserModule.isValidPass(data)) return cb(Errors.notFoundError);
            console.log("CONGR!");
            ClientModule.isPaid(student,function (err,isPaid) {
                if (err) return cb(err);
                if (!isPaid) return cb(Errors.paymentRequiredError);
                UserModule.generateToken(data,function (err,token) {
                    cb(err,{
                        token : token,
                        isTesting : student.isTesting
                    });
                });
            });
        });
};

var Errors = require('system/errors'),
    ClientModule = require('system/client'),
    Student = require(__modelsPath + 'student'),
    UserModule = require('system/user');


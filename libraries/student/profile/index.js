module.exports.getProfile = function (data,cb) {
    Student
        .findOne({
            centerID : data.centerID,
            _id : data.studentID
        })
        .populate([{
            path : 'groups',
            select : 'homeworks schedule tests',
            populate : [{
                path : 'homeworks',
                options : {
                    limit : 1
                }
            },{
                path : 'schedule',
                populate : {
                    path : 'classroomID'
                }
            },{
                path : 'tests',
                select : 'name'
            }],
            match : {
                _id : data.groupID
            }
        },{
            path : 'answers',
            select : 'testID'
        }])

        .select('groups imageUrl firstName lastName phone answers')
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            if (student.groups.length == 0) return cb(ErrorsModule.badRequestError);
            return cb(null,student);
        });
};

var ErrorsModule = require('system/errors');
var Student = require(__modelsPath + 'student'),
    Group = require(__modelsPath + 'group');
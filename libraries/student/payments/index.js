module.exports.getPayments = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .select('payments')
        .populate({
            path : 'payments',
            match : {
                groupID : data.groupID
            }
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            return cb(null,student.payments);
        });
};


var ErrorsModule = require('system/errors'),
    Student = require(__modelsPath + 'student');



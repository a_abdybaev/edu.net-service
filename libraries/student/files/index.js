module.exports.getFiles = function (data,cb) {
    Student
        .findOne({
            centerID : data.centerID,
            _id : data.studentID
        })
        .select('groups')
        .populate({
            path : 'groups',
            match : {
                _id : data.groupID
            },
            select : 'files',
            populate : {
                path : 'files'
            }
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            if (student.groups.length == 0) return cb(ErrorsModule.badRequestError);
            return cb(null,student.groups[0].files);
        });
};


var ErrorsModule = require('system/errors'),
    Student = require(__modelsPath + 'student');



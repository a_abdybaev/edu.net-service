module.exports.getPosts = function (data,cb) {
    var obj = {
        centerID : data.centerID,
        groupID : data.groupID
    };
    if (data.important) {
        obj.important = data.important
    }
    var query = Post.find(obj).limit(ConstantsModule.POSTS_PER_PAGE);
    query.skip(ConstantsModule.POSTS_PER_PAGE * data.page);
    // if (data.skip != undefined) {
    //     query.skip(parseInt(data.skip));
    // }
    if (data.homework) {
        query.where('homeworkID').exists();
    }
    if (data.onlyFiles) {
        query.$where('this.files.length != 0')
    }
    if (data.another) {
        query
            .$where('this.files.length == 0')
            .where('important',false)
    }
    query
        .populate([{
            path : 'images'
        },{
            path : 'files'
        },{
            path : 'comments',
            populate : {
                path : 'user.value'
            }
        },{
            path : 'homeworkID'
        }])
        .sort({
            createdAt : -1
        })
        .exec(cb);
};
module.exports.addComment = function (data,cb) {
    var resolveReply = function (futureComment,cb) {
        if (!data.isReply) return cb();
        futureComment.reply.commentID = data.replyCommentID;
        Comment
            .findOne({
                _id : data.replyCommentID
            })
            .select('user userType')
            .populate({
                path : 'user.value',
                select : 'firstName '
            })
            .exec(function (err,comment) {
                if (err) return cb(err);
                if (!comment) return cb(ErrorsModule.notFoundError);
                var replyText = comment.user.value.firstName + ', ';
                futureComment.reply.isImportantName = false;
                // console.log(data.content.indexOf(replyText));
                if(data.content.indexOf(replyText) == 0) {
                    futureComment.reply.isImportantName = true;
                    futureComment.reply.name = comment.user.value.firstName;
                    futureComment.content = data.content.replace(replyText,'');
                }
                cb();
            });
    };
    Student
        .findOne({
            centerID : data.centerID,
            _id : data.studentID
        })
        .select('groups')
        .populate({
            path : 'groups',
            match : {
                _id : data.groupID
            },
            populate : {
                path : 'channel',
                match : {
                    _id : data.postID
                },
                select : '_id comments'
            }
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            if (student.groups.length == 0) return cb(ErrorsModule.badRequestError);
            if (student.groups[0].channel.length == 0) return cb(ErrorsModule.badRequestError);
            var comment = new Comment({
                postID : data.postID,
                studentID : student._id,
                content : data.content,
                reply : {
                    isReply : data.isReply
                },
                centerID : data.centerID,
                userType : ConstantsModule.STUDENT,
                user: {
                    model : ConstantsModule.MODELS.STUDENT,
                    value : data.studentID
                }
            });
            resolveReply(comment,function (err) {
                if (err) return cb(err);
                comment.save(function (err) {
                    if (err) return cb(err);
                    var post = student.groups[0].channel[0];
                    post.comments.push(comment._id);
                    return post.save(cb);
                });
            });
        });
};

var ErrorsModule = require('system/errors'),
    Post = require(__modelsPath + 'post'),
    ConstantsModule = require('system/constants'),
    Comment = require(__modelsPath + 'comment'),
    Student = require(__modelsPath + 'student');

module.exports.getAttendance = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .select('appearance')
        .populate({
            path : 'appearance',
            match : {
                groupID : data.groupID
            }
        })
        .exec(function (err,student) {
           if (err) return cb(err);
           if (!student) return cb(ErrorsModule.notFoundError);
           cb(null,student.appearance);
        });
};


var ErrorsModule = require('system/errors'),
    Student = require(__modelsPath + 'student');



module.exports.resolveTesting = function (data,cb) {
    var notTestingCallback = function (response) {
        response.student.isTesting = false;
        response.student.testID = undefined;
        response.isNotTesting = true;
        response.student.save(function (err) {
            return cb(err,response);
        });
    };
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .select('isTesting testID answers')
        .populate([{
            path : 'testID'
        },{
            path : 'answers'
        }])
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            var response = {
                student : student
            };
            if (!student.isTesting || !student.testID) return notTestingCallback(response);
            var index = _.map(student.answers,function (answer) {
                return answer.testID.toString();
            }).indexOf(student.testID._id.toString());

            if (index < 0 || student.answers[index].isEnded) return notTestingCallback(response);
            var answerSheet = student.answers[index],
                test = student.testID;

            // NOT STARTED
            if (!answerSheet.isStarted || answerSheet.sections.length == 0) {
                // answerSheet.isStarted = true;
                return answerSheet.save(function (err) {
                    response.isNotStarted = true;
                    cb(err,response);
                });
            }
            var testSectionsCount = test.sections.length,
                answerSheetSectionsCount = answerSheet.sections.length,
                testLastSection = test.sections[testSectionsCount-1],
                answerSheetLastSection = answerSheet.sections[answerSheetSectionsCount-1];

            if (testSectionsCount == answerSheetSectionsCount &&
                (!exports.isActiveSection(answerSheetLastSection,testLastSection) || answerSheetLastSection.isEnded)) {
                answerSheet.isEnded = true;
                return answerSheet.save(function (err) {
                    if (err) return cb(err);
                    return notTestingCallback(response);
                });
            }
            // NOT ENDED
            response.isContinueTesting = true;
            var activeSectionIndex = answerSheet.sections.length-1;
            if (answerSheet.sections[activeSectionIndex].isEnded) {
                answerSheet.sections.push({
                    sectionNumber : activeSectionIndex + 1
                });
                return answerSheet.save(function (err) {
                    response.section = student.testID.sections[activeSectionIndex+1];
                    response.createdAt = answerSheet.sections[activeSectionIndex+1].startedAt;
                    return cb(err,response);
                });
            }
            if (!exports.isActiveSection(answerSheet.sections[activeSectionIndex],student.testID.sections[activeSectionIndex])) {
                answerSheet.sections.push({
                    sectionNumber : activeSectionIndex + 1
                });
                return answerSheet.save(function (err) {
                    response.section = student.testID.sections[activeSectionIndex+1];
                    response.sectionNumber = activeSectionIndex + 1;
                    response.createdAt = answerSheet.sections[activeSectionIndex+1].startedAt;
                    return cb(err,response);
                });
            }
            response.section = {
                duration : student.testID.sections[activeSectionIndex].duration,
                images : student.testID.sections[activeSectionIndex].images,
                questionsCount : student.testID.sections[activeSectionIndex].questionsCount,
                startedAt : answerSheet.sections[activeSectionIndex].startedAt,
                audio : student.testID.sections[activeSectionIndex].audio,
                hasAudio : student.testID.sections[activeSectionIndex].hasAudio

            };
            response.sectionNumber = activeSectionIndex;
            response.test = student.testID;
            return cb(null,response);

            // response.answerSheet = answerSheet;
            // cb(null,response);
        });
};
module.exports.isActiveSection = function (answerSheetSection,testSection) {
    var now = new Date();
    var startedAt = new Date(answerSheetSection.startedAt);
    startedAt.setMinutes(startedAt.getMinutes() + testSection.duration);
    return now < startedAt;

};
module.exports.assignTest = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .select('isTesting testID answers')
        .populate({
            path : 'answers',
            match : {
                testID : data.testID
            }
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.unauthorizedError);
            if (student.answers.length != 0) return cb(ErrorsModule.notAcceptableError);
            var answerSheet = new AnswerSheet({
                testID : data.testID,
                centerID : data.centerID,
                studentID : student._id
            });
            answerSheet.save(function (err) {
                if (err) return cb(err);
                student.isTesting = true;
                student.testID = data.testID;
                student.answers.push(answerSheet._id);
                student.save(cb);
            });
        });
};
module.exports.cancelTest = function (data,cb) {
    Student
        .findOne({
            centerID : data.centerID,
            _id : data.studentID
        })
        .exec(function (err,student) {
            if (err) return cb(ErrorsModule.objectNotFoundError);
            if (!student.testID || !student.isTesting) return cb(ErrorsModule.badRequestError);
            AnswerSheet
                .findOne({
                    studentID : data.studentID,
                    testID : student.testID
                })
                .exec(function (err,answerSheet) {
                    if (err) return cb(err);
                    if (!answerSheet) return cb(ErrorsModule.badRequestError);
                    answerSheet.remove(function (err) {
                        if (err) return cb(err);
                        student.testID = undefined;
                        student.isTesting = false;
                        student.save(cb);
                    });
                });
        });
};

module.exports.beginSection = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID,
            isTesting : true
        })
        .select('testID')
        .populate('testID')
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student || !student.testID) return cb(ErrorsModule.badRequestError);
            AnswerSheet
                .findOne({
                    testID : student.testID._id,
                    studentID : data.studentID,
                    centerID : data.centerID,
                    isEnded : false
                })
                .exec(function (err,answerSheet) {
                    if (err) return cb(err);
                    if (!answerSheet) return cb(ErrorsModule.badRequestError);
                    answerSheet.isStarted = true;
                    if (answerSheet.sections.length >= student.testID.sections.length) return cb(ErrorsModule.badRequestError);
                    answerSheet.sections.push({
                        sectionNumber : answerSheet.sections.length
                    });
                    answerSheet.save(function (err) {
                        return cb(err,{
                            section : {
                                duration : student.testID.sections[answerSheet.sections.length-1].duration,
                                images : student.testID.sections[answerSheet.sections.length-1].images,
                                questionsCount : student.testID.sections[answerSheet.sections.length-1].questionsCount,
                                startedAt : answerSheet.sections[answerSheet.sections.length-1].startedAt,
                                audio : student.testID.sections[answerSheet.sections.length-1].audio,
                                hasAudio : student.testID.sections[answerSheet.sections.length-1].hasAudio
                            },
                            sectionNumber : answerSheet.sections.length-1,
                            startedAt : answerSheet.createdAt
                        });
                    });
                });
        });
};
module.exports.endSection = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID,
            isTesting : true
        })
        .select('testID')
        .populate('testID')
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student || !student.testID) return cb(ErrorsModule.badRequestError);
            AnswerSheet
                .findOne({
                    testID : student.testID._id,
                    studentID : data.studentID,
                    centerID : data.centerID,
                    isEnded : false
                })
                .exec(function (err,answerSheet) {
                    if (err) return cb(err);
                    if (!answerSheet || answerSheet.sections.length == 0) return cb(ErrorsModule.badRequestError);
                    var sectionIndex = answerSheet.sections.length-1;
                    if (sectionIndex + 1 > student.testID.sections.length) return cb(ErrorsModule.badRequest);
                    if (data.answers.length != student.testID.sections[sectionIndex].answers.length) return cb(ErrorsModule.badRequestError);
                    var section = answerSheet.sections[answerSheet.sections.length-1];
                    if (section.isEnded|| !section.isStarted) return cb(ErrorsModule.badRequestError);
                    _.each(data.answers,function (answer,i) {

                        section.answers.push({
                            value : _.isEmpty(answer) ? null : answer.replace(/\s+/g,' '),
                            number : i+1
                        });
                    });
                    section.isEnded = true;
                    if (answerSheet.sections.length == student.testID.sections.length) answerSheet.isEnded = true;
                    answerSheet.save(function (err) {
                        if (err) return cb(err);
                        if (answerSheet.isEnded) return exports.checkResults({
                            answerSheetID : answerSheet._id
                        },cb);
                        cb(null,{
                            isEnded : false
                        });
                    });

                });
        });
};
module.exports.checkResults = function (data,cb) {
    AnswerSheet
        .findOne({
            _id : data.answerSheetID
        })
        .populate([{
            path : 'testID'
        },{
            path : 'studentID'
        }])
        .exec(function (err,answerSheet) {
            if (err) return cb(err);
            if (!answerSheet) return cb(ErrorsModule.notFoundError);
            if (answerSheet.resultID) return cb(ErrorsModule.badRequestError);
            var testResult = new TestResult({
                centerID : answerSheet.centerID,
                studentID : answerSheet.studentID,
                testID : answerSheet.testID._id
            });
            var test = answerSheet.testID;
            _.each(answerSheet.sections,function (section,index) {
                testResult.sections.push({
                    sectionNumber : index,
                    results : []
                });
                _.each(section.answers,function (answer,j) {
                    // CHECK RESULT START
                    var value = false;
                    if (!_.isEmpty(answer.value)) {
                        _.each(test.sections[index].answers[j].values,function (rightValue) {
                            if (rightValue.toLowerCase() == answer.value.toLowerCase()) value = true;
                        });
                    }
                    // CHECK RESULT END

                    testResult.sections[index].results.push({
                        sectionNumber : j,
                        value : value,
                        number : answer.number
                    });
                });
            });
            testResult.compute();
            testResult.save(function (err) {
                if (err) return cb(err);
                answerSheet.resultID = testResult._id;
                answerSheet.save(function (err) {
                    if (err) return cb(err);
                    answerSheet.studentID.testID = undefined;
                    answerSheet.studentID.isTesting = false;
                    return answerSheet.save(function (err) {
                        return cb(err,{
                            isEnded : true,
                            results : testResult,
                            test : answerSheet.testID
                        });
                    });
                });
            });
        });
};
module.exports.dev = function (cb) {
    // DELETE OLD RES
    AnswerSheet
        .find({})
        .remove()
        .exec(function (err) {
            if (err) return cb(err);
            TestResult
                .find({})
                .remove()
                .exec(function (err) {
            // ASSIGN TEST
                    request({
                        headers : {
                            token : '1873497fb0195699e02f29a4e269bc2a53244471f8078dc98b88c87d4a5013c472bb2a5f077d055c1dcd2d64f6593703f0e8ba3151f7da2b264f6508f9999b3075a1ba36b4b8de3f8d9b9a39f75013fb7565735852bf1eb0cff4e22143668a789dc76e61'
                        },
                        body : {
                            "testID" :"59353fc065ec5d319ed7119c"
                        },
                        json : true,
                        uri : __testingUrl,
                        method : 'POST'
                    },function (err,res,body) {
                        return;
                        if (err) return cb(err);
                        cb();
                    });
                });
    });

};

var Student = require(__modelsPath + 'student'),
    AnswerSheet = require(__modelsPath + 'tests/answer-sheet'),
    ConstantsModule = require('system/constants'),
    ErrorsModule = require('system/errors'),
    TestResult = require(__modelsPath + 'tests/test-result'),
    request = require('request'),
    _ = require('underscore');
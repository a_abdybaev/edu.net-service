module.exports.login = function (data,cb) {
    Manager
        .findOne({
            login : data.login
        })
        .select('hash salt')
        .exec(function (err,manager) {
            if (err) return cb(err);
            if (!manager) return cb(Errors.notFoundError);
            data.user = manager;
            if (!UserModule.isValidPass(data)) return cb(Errors.notFoundError);
            UserModule.generateToken(data,cb);
        });
};
module.exports.auth = function (data,cb) {
    Manager
        .findOne({
            'token.value' : data.token
        })
        .exec(function (err,manager) {
            if (err) return cb(err);
            if (!manager) return cb(Errors.notFoundError);
            return cb(null,manager);
        });
};

var Errors = require('system/errors'),
    UserModule = require('system/user'),
    Manager = require(__modelsPath + 'manager');
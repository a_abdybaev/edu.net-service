module.exports.addAdmin = function (data,cb) {
    var admin = new Admin({
        clientID : data.clientID,
        isManager : data.isManager,
        phone : data.phone
    });
    UserModule.initUser({
        user : admin
    },function (err) {
        if (err) return cb(err);
        data.admin = admin;
        exports.resolveManager(data,function (err) {
            if (err) return cb(err);
            admin.save(cb);
        });
    });
};
module.exports.delByID = function (data,cb) {
    Admin
        .remove({
            _id : data.adminID
        },cb);
};
module.exports.resolveManager = function (data,cb) {
    if (!data.isManager){
        data.admin.centerID = data.centerID;
        return cb();
    }
    Client
        .findOne({
            _id : data.clientID
        })
        .select('centers')
        .exec(function (err,client) {
            if (err) return cb(err);
            if (!client) return cb(Errors.objNotFound);
            data.admin.centerID = client.centers[0];
            cb();
        });
};

module.exports.getAdmins = function (cb) {
    Admin
        .find({})
        .populate('clientID centerID')
        .exec(cb);
};

var Admin = require(__modelsPath + 'admin'),
    Client = require(__modelsPath + 'client'),
    Errors = require('system/errors'),


    UserModule = require('system/user');



module.exports.getTariffs = function (cb) {
    Tariff
        .find({})
        .exec(cb);
};
module.exports.addTariff = function (data,cb) {

    var tariff = new Tariff({
        name : data.name,
        students : data.students,
        size : data.size * 1000000,
        price : data.price,
        isMajor : data.isMajor
    });
    tariff.save(cb);
};
module.exports.deleteTariffByID = function (data,cb) {
    Tariff
        .findOne({
            isMajor : false,
            _id : data.tariffID
        })
        .exec(function (err,tariff) {
            if (err) return cb(err);
            if (!tariff) return cb(ErrorsModule.notFoundError);
            tariff.remove(cb);
        });
};

var Tariff = require(__modelsPath + 'tariff'),
    ErrorsModule = require('system/errors');
module.exports.initCenter = function (self,cb) {
    Helper.addObjectInArray({
        objectID : self._id,
        objectPath : 'centers',
        modelID : self.clientID,
        modelName : Constants.MODELS.CLIENT
    },function (err) {
        if (err) return cb(err);
        exports.createSourceDirectory(self,function (err) {
            if (err) return cb(err);
            exports.createTestsDirectory(self,cb);
        });
    });

};
module.exports.createSourceDirectory = function (self,cb) {
    Client
        .findOne({
            _id : self.clientID
        })
        .exec(function (err,client) {
            if (err) return cb(err);
            if (!client) return cb(ErrorsModule.objNotFound);
            var relPath = path.join(client.sourceUrl,crypto.randomBytes(10).toString('hex')),
                absPath = path.join(__publicPath,relPath);
            fs.mkdir(absPath, function (err) {
                if (err) return cb(err);
                self.sourceUrl = relPath;
                cb();
            });
        });

};
module.exports.createTestsDirectory = function (self,cb) {
    fs.mkdir(path.join(__publicPath,self.sourceUrl,'tests'),cb);
};


module.exports.addCenter = function (data,cb) {
    var center = new Center({
        name : data.name,
        clientID : data.clientID
    });
    exports.initCenter(center,function (err) {
        if (err) return cb(err);
        center.save(cb);
    });
};
module.exports.getCenters = function (cb) {
    Center
        .find({})
        .populate({
            path : 'clientID',
            populate : {
                path : 'tariffID'
            }
        })
        .exec(cb);
};
module.exports.getCenterByID = function (data,cb) {
    Center
        .findOne({
            _id : data.centerID
        })
        .exec(function (err,center) {
            if (err) return cb(err);
            if (!center) return cb(ErrorsModule.notFoundError);
            TeacherModule.getTeachersByCenterID(data,function (err,teachers) {
                if (err) return cb(err);
                StudentModule.getStudentsByCenterID(data,function (err,students) {
                    if (err) return cb(err);
                    GroupModule.getGroupsByCenterID(data,function (err,groups) {
                        if (err) return cb(err);
                        return cb(null,{
                            center : center,
                            teachers : teachers,
                            students : students,
                            groups : groups
                        });
                    });
                });
            });
        });
};
module.exports.setPaymentByCenterID = function (data,cb) {
    Center
        .findOne({
            _id : data.centerID
        })
        .exec(function (err,center) {
            if (err) {
                cb(err);
            } else if (!center) {
                cb(ErrorsModule.notFoundError);
            } else {
                center.isPaid = data.isPaid;
                center.save(function (err) {
                    cb(err);
                });
            }
        });
};
module.exports.delByID = function (data,cb) {
    if (!data.centerID) return cb(ErrorsModule.badRequest);
    var query = {
        centerID : data.centerID
    };
    var models = [Constants.MODELS.ADMIN,Constants.MODELS.ANSWER_SHEET,Constants.MODELS.APPEARANCE,
        Constants.MODELS.APPLICATION,Constants.MODELS.APPLICATION_ANSWERS,Constants.MODELS.CLASSROOM,
        Constants.MODELS.COMMENTS, Constants.MODELS.FILE,Constants.MODELS.GROUP, Constants.MODELS.HOMEWORK,
        Constants.MODELS.INTERVIEW_RESULT, Constants.MODELS.LESSON,Constants.MODELS.PAYMENTS,
        Constants.MODELS.POST,Constants.MODELS.STUDENT,Constants.MODELS.SUBJECT,Constants.MODELS.TEACHER,
        Constants.MODELS.TEST,Constants.MODELS.TEST_RESULT
    ];
    var res = [];
    _.each(models,function (model,index) {
        res[index] = function (callback) {
            if (model == Constants.MODELS.ADMIN) {
                query.isManager = false;
            } else {
                query.isManager = undefined;
            }
            mongoose
                .model(model)
                .remove(query,function (err) {
                    if (err) return callback(err);
                    console.log(model + ' DELETED!');
                    callback();
                });
        };
    });
    async.parallel(res,function (err) {
        if (err) return cb(err);
        Center
            .findOne({
                _id : data.centerID
            })
            .populate('clientID')
            .exec(function (err,center) {
                if (err) return cb(err);
                if (!center) return cb();
                var index = _.map(center.clientID.centers,function (center) {
                    return center.toString()
                }).indexOf(data.centerID.toString());

                if (index < 0) return cb(ErrorsModule.badRequest);
                console.log("WTF");
                FSExtra.remove(center.getSourceUrl(),function (err) {
                    if (err) return cb(err);
                    center.clientID.centers.splice(index,1);
                    center.clientID.save(function (err) {
                        if (err) return cb(err);
                        console.log("GOIND DEL");
                        center.remove(cb);
                    });
                });
            });
    });

};
module.exports.updateByID = function (data,cb) {
    Center
        .findOne({
            _id : data.centerID
        })
        .exec(function (err,center) {
            if (err) return cb(err);
            if (!center) return cb(ErrorsModule.notFoundError);
            center.isPaid = data.isPaid;
            center.tariffID = data.tariffID;
            center.save(cb);
        });
};


var Center = require(__modelsPath + 'center'),
    Client = require(__modelsPath + 'client'),
    mongoose = require('mongoose'),
    ErrorsModule = require('system/errors'),
    fs = require('fs'),
    FSExtra = require('fs-extra'),
    _ = require('underscore'),
    crypto = require('crypto'),
    async = require('async'),
    path = require('path'),
    StudentModule = require('manager/student'),
    TeacherModule = require('manager/teacher'),
    Helper = require('system/helper'),
    Constants = require('system/constants'),
    GroupModule = require('manager/group');
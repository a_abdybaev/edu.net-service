module.exports.getStudentsByCenterID = function (data,cb) {
    Student
        .find({
            centerID : data.centerID
        })
        .exec(cb);
};
var Student = require(__modelsPath + 'student');
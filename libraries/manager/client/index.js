module.exports.initClient = function (self,cb) {
    exports.createSourceDirectory(self,cb);
};
module.exports.createSourceDirectory = function (self,cb) {
    var relPath = path.normalize(path.join('storage',crypto.randomBytes(10).toString('hex')));
    var absPath = path.normalize(path.join(__publicPath,relPath));
    fs.mkdir(absPath,function (err) {
        if (err) return cb(err);
        self.sourceUrl = relPath;
        cb();
    });
};
module.exports.add  = function (data,cb) {
    var client = new Client({
        name : data.name,
        link : data.link,
        tariffID : data.tariffID
    });
    exports.initClient(client,function (err) {
        if (err) return cb(err);
        client.save(function (err) {
            if (err) return cb(err);
            CenterModule.addCenter({
                name : data.name,
                clientID : client._id
            },function (err,center) {
                if (err) return cb(err);
                client.save(cb);
            });
        });
    });
};
module.exports.get = function (cb) {
    Client
        .find({})
        .populate({
            path : 'centers'
        })
        .exec(cb);

};
module.exports.editByID = function (data,cb) {
    Client
        .findOne({
            _id : data.clientID
        })
        .exec(function (err,client) {
            if (err) return cb(err);
            if (!client) return cb(ErrorsModule.objNotFound);
            client.name = data.name;
            client.tariffID = data.tariffID;
            client.isPaid = data.isPaid;
            client.save(cb);
        });
};
module.exports.getByID = function (data,cb) {
    Client
        .findOne({
            _id : data.clientID
        })
        .populate('centers tariffID')
        .exec(function (err,client) {
            if (err) return cb(err);
            if (!client) return cb(ErrorsModule.objNotFound);
            cb(null,client);
        });
};
module.exports.delByID = function (data,cb) {
    Client
        .findOne({
            _id : data.clientID
        })
        .exec(function (err,client) {
            if (err) return cb(err);
            if (!client) return cb(ErrorsModule.objNotFound);
            var res = [];
            _.each(client.centers,function (center,index) {
                res[index] = function (callback) {
                    CenterModule.delByID({
                        centerID : center
                    },callback);
                };
            });
            async.parallel(res,function (err) {
                if (err) return cb(err);
                FSExtra.remove(client.getSourceUrl(),function (err) {
                    if(err) return cb(err);
                    client.remove(cb);
                });
            });
        });
};
module.exports.setPaymentByID = function (data,cb) {

};


var crypto = require('crypto'),
    fs = require('fs'),
    path = require('path'),
    _ = require('underscore'),
    FSExtra = require('fs-extra'),
    async = require('async'),
    mongoose = require('mongoose'),
    ErrorsModule = require('system/errors'),
    CenterModule = require('manager/center'),

    Client = require(__modelsPath + 'client');

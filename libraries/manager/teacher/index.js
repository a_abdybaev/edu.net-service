module.exports.getTeachersByCenterID = function (data,cb) {
    Teacher
        .find({
            centerID : data.centerID
        })
        .exec(cb);
};

var Teacher = require(__modelsPath + 'teacher');
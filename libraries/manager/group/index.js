module.exports.getGroupsByCenterID = function (data,cb) {
    Group
        .find({
            centerID : data.centerID
        })
        .populate({
            path : 'teacherID'
        })
        .exec(cb);
};
var Group = require(__modelsPath + 'group');
module.exports.addRequest = function (data,cb) {
    var landingRequest = new LandingRequest({
        phone : data.phone,
        name : data.name,
        centerName : data.centerName
    });
    return landingRequest.save(cb);
};
module.exports.getRequests = function (cb) {
    LandingRequest
        .find({})
        .sort({
            createdAt : -1
        })
        .exec(cb);
};


var LandingRequest = require(__modelsPath + 'landing-request'),
    ErrorsModule = require('system/errors');

module.exports.addTest = function (data,cb) {
    // UNCOMMENT IN FUTURE
    var files = _.map(data.files,function (file) {
        return file.file;
    });
    _.each(data.files,function (file,index) {
        if (data.sections[index].hasAudio) files.push(file.audioFile);
    });
    _.each(files,function (file) {
        // console.log(file);
    });
    CenterModule.isPossibleUploadFileObjects({
        files : files,
        centerID : data.centerID,
        clientID : data.clientID
    },function (err) {
        if (err) return cb(err);
        var test = new Test({
            name : data.name,
            ownerID : data.ownerID,
            centerID : data.centerID,
            clientID : data.clientID
        });
        exports.configure(test,function (err) {
            data.test = test;
            return exports.resolveSections(data,function (err,sections) {
                if (err) return cb(err);
                data.test.sections = sections;
                exports.deletePDF(data,function (err) {
                    if (err) return cb(err);
                    exports.computeTestMemoryUsage(data.test,function (err) {
                        if (err) return cb(err);
                        return test.save(cb);
                    });
                });
            });
        });
    });
};
module.exports.deletePDF = function (data,cb) {
    var results = [];
    _.each(data.files,function (file,i) {
        results[i] = function (callback) {
            fs.unlink(file.file.path,callback);
        };
    });
    return async.parallel(results,cb);
};
module.exports.computeTestMemoryUsage = function (self,cb) {
    folderSize(self.getSourceUrl(),function (err,size) {
        if (err) return cb(err);
        self.memory = size;
        CenterModule.updateSpaceUsage(self,cb);
    });
};
module.exports.resolveSections = function (data,cb) {
    var result = [];
    _.each(data.sections,function (section,i) {
        result[i] = function (callback) {
            data.section = section;
            data.file = data.files[i].file;
            data.audioFile = data.files[i].audioFile;
            exports.resolveSection(data,callback);
        };
    });
    return async.series(result,cb);
};

module.exports.resolveSection = function (data,cb) {
    var resolvePagesInterval = function (cb) {
        if (data.section.pages == ConstantsModule.TEST_PAGES_INTERVAL) return cb();
        if (data.section.pages != ConstantsModule.TEST_PAGES_ALL) return cb(ErrorsModule.invalidBodyError);
        var pdf = new pdfInfo(data.file.path);
        pdf.getInfo(function (err,info,params) {
            if (err) return cb(err);
            data.section.pagesFrom = 1;
            data.section.pagesTo = parseInt(info.pages);
            cb();
        });
    };

    resolvePagesInterval(function (err) {
        if (err) return cb(err);
        exports.resolveSectionPdf(data,function (err,images) {
            if (err) return cb(err);
            exports.resolveSectionAudio(data,function (err,audioFilePath) {
                if (err) return cb(err);
                exports.resolveAnswers(data,function (err,answers) {
                    if (err) return cb(err);
                    cb(null,{
                        images : images,
                        answers : answers,
                        hasAudio : data.section.hasAudio,
                        audio : audioFilePath,
                        questionsCount : answers.length,
                        duration : parseInt(data.section.minutesCount) * 60
                    });
                });
            });
        });
    });
};

module.exports.resolveAnswers = function (data,cb) {
    exports.processAnswers({
        answers : data.section.answers
    },cb);
};
module.exports.processAnswers = function (data,cb) {
    var answers = [];
    _.each(data.answers,function (answer,i) {
        answers.push({
            number : i + 1,
            values : []
        });
        answer = answer.split("\n");
        _.each(answer,function (possibleAnswer) {
            answers[i].values.push(possibleAnswer.replace(/\s+/g,' '));
        });
    });
    cb(null,answers);
};
module.exports.resolveSectionAudio = function (data,cb) {
    if (!data.section.hasAudio) return cb();
    var url = data.audioFile.path.split('/');
    var audioFileName = url[url.length-1],
        audioFileAbsName = path.join(data.test.getSourceUrl(),audioFileName),
        audioFileRelativeName = path.join(data.test.sourceUrl,audioFileName);
    fsExtra.move(data.audioFile.path,audioFileAbsName,function (err) {
        cb(err,audioFileRelativeName);
    });

};

module.exports.resolveSectionPdf = function (data,cb) {
    var pdfImage = new PDFImage(data.file.path,{
        convertOptions : {
            '-density' :150,
            '-background' : 'white'
        },
        outputDirectory : data.test.getSourceUrl()
    });
    var results = [];
    data.section.pagesFrom = parseInt(data.section.pagesFrom) -1;
    data.section.pagesTo = parseInt(data.section.pagesTo) - 1;
    var helper = [];
    for (var j=data.section.pagesFrom;j<=data.section.pagesTo;j++) {
        helper.push(j);
    }
    _.each(helper,function (item,j) {
        results[j] = function (callback) {
            pdfImage
                .convertPage(item)
                .then(function (imagePath) {
                    callback(null,imagePath)
                },function (error) {
                    callback(error);
                });
        };
    });
    async.parallel(results,function (err,pages) {
        if (err) return cb(err);
        var imageURLs = _.map(pages,function (page) {
            var url = page.split('/');
            return path.join(data.test.sourceUrl,url[url.length-1]);
        });
        cb(null,imageURLs);
    });
};
module.exports.isValidTest = function (data,cb) {
    var result = true;
    if (_.isEmpty(data.sections) || _.isEmpty(data.name) || !data.centerID) return false;
    _.each(data.sections,function (section,j) {
        section.questionsCount = parseInt(section.questionsCount);
        section.minutesCount = parseInt(section.minutesCount);
        section.hasAudio = JSON.parse(section.hasAudio);

        if (isNaN(section.questionsCount) || isNaN(section.minutesCount) || section.questionsCount <=0 || section.minutesCount<=0 || !data.files[j].file) return result= false;
        for (var i=0;i<section.questionsCount;i++) {
            if (_.isEmpty(section.answers[i])) result = false;
        }
    });
    return result;
};
module.exports.configure = function (self,cb) {
    return exports.createSourceUrl(self,cb);
};
module.exports.createSourceUrl = function (self,cb) {
    CenterModule.getByID(self,function (err,center) {
        if (err) return cb(err);
        var randomName = crypto.randomBytes(10).toString('hex'),
            testUrl = path.join(center.getSourceUrl(),'tests',randomName);
        fs.mkdir(testUrl, function (err) {
            if (err) return cb(err);
            self.sourceUrl = path.join(center.sourceUrl,'tests',randomName);
            return cb(null);
        });

    });
};
// GET TESTS
module.exports.getTests = function (data,cb) {
    Test
        .find({
            centerID : data.centerID
        })
        .select('name sections ')
        .exec(cb);
};
module.exports.getTestByID = function (data,cb) {
    Test
        .findOne({
            centerID : data.centerID,
            _id : data.testID
        })
        .select()
        .exec(function (err,test) {
            if (err) cb(err);
            if (!test) return cb(ErrorsModule.notFoundError);
            cb(null,test);
        });
};
// DELETE
module.exports.deleteSectionByIndex = function (data,cb) {
    Test
        .findOne({
            centerID : data.centerID,
            _id : data.testID
        })
        .select('sections centerID sourceUrl')
        .exec(function (err,test) {
            if (err) return cb(err);
            if (!test) return cb(ErrorsModule.notFoundError);
            if (test.sections.length == 1 && data.sectionIndex == 0) return cb(ErrorsModule.badRequestError);
            var results = [];
            if (test.sections.length < data.sectionIndex) return cb(ErrorsModule.badRequestError);
            _.each(test.sections[data.sectionIndex].images,function (imageUrl,i) {
                results[i] = function (callback) {
                    var imagePath = path.join(__publicPath,imageUrl);
                    HelperModule.deleteFile(imagePath,callback);
                };
            });
            async.series(results,function (err) {
                if (err) return cb(err);
                test.sections.splice(data.sectionIndex,1);
                exports.computeTestMemoryUsage(test,function (err) {
                    if (err) return cb(err);
                    test.save(cb)
                });
            });
        });
};
module.exports.deleteTestByID = function (data,cb) {
    Test
        .findOne({
            _id : data.testID,
            centerID : data.centerID
        })
        .exec(function (err,test) {
            if (err) return cb(err);
            if (!test) return (ErrorsModule.notFoundError);
            exports.deleteAnswerSheetsByTestID({
                testID : data.testID
            },function (err) {
                if (err) return cb(err);
                exports.deleteTestResultsByTestID({
                    testID : data.testID
                },function (err) {
                    if (err) return cb(err);
                    HelperModule.deleteDirectory(test.getSourceUrl(),function (err) {
                        if (err) return cb(err);
                        test.remove(function (err) {
                            if (err) return cb(err);
                            CenterModule.updateSpaceUsage(data,cb);
                        });
                    });
                })
            });

        });
};
module.exports.deleteAnswerSheetsByTestID = function (data,cb) {
    AnswerSheet
        .remove({
            testID : data.testID
        })
        .exec(cb);
};
module.exports.deleteTestResultsByTestID = function (data,cb) {
    TestResult
        .remove({
            testID : data.testID
        })
        .exec(cb);
};
//ASSIGN
module.exports.assignTest = function (data,cb) {
    exports.isPossibleAssignTest(data,function (err) {
        if (err) return cb(err);
        Test
            .findOne({
                _id : data.testID,
                centerID : data.centerID
            })
            .select('_id')
            .exec(function (err,test) {
                if (err) return cb(err);
                if (!test) return cb(ErrorsModule.notFoundError);
                HelperModule.addObjectInArray({
                    objectID : data.testID,
                    objectPath : 'tests',
                    modelName : "Group",
                    modelID : data.groupID,
                    centerID : data.centerID
                },cb);
            });
    });

};
module.exports.isPossibleAssignTest = function (data,cb) {
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID
        })
        .select('tests name')
        .populate({
            path : 'tests',
            match : {
                _id : data.testID
            }
        })
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.notFoundError);
            cb(group.tests.length == 0 ? null : ErrorsModule.badRequestError);
        });
};
// UN ASSIGN TEST FROM GROUP
module.exports.removeTestFromGroup = function (data,cb) {
    HelperModule.deleteObjectFromArray({
        modelName : ConstantsModule.MODELS.GROUP,
        modelID : data.groupID,
        objectPath : 'tests',
        objectID : data.testID,
        centerID : data.centerID
    },cb);
};

// UPDATED
module.exports.updateSectionByIndex = function (data,cb) {
    var resolveAnswers = function (data,cb) {
        if (!data.answers || !_.isArray(data.answers) || data.answers.length == 0) return cb(null);
        exports.processAnswers({
            answers : _.map(data.answers,function (answer) {
                return answer.value
            })
        },function (err,answers) {
            if (err) return cb(err);
            data.test.sections[data.sectionIndex].answers = answers;
            data.test.sections[data.sectionIndex].questionsCount = answers.length;
            return cb(null);
        });
    };
    var resolveFile = function (data,cb) {
        if (!data.file || !data.pagesFrom ||  isNaN(data.pagesFrom) || !data.pagesTo || isNaN(data.pagesTo)) return cb(null);
        CenterModule.isPossibleUploadFileObject(data,function (err) {
            if (err) return cb(err);
            var sourceUrl = data.test.getSourceUrl(),
                publicUrl = __publicPath;

            var pdfImage = new PDFImage(data.file.path,{
                convertOptions : {
                    '-density' :150,
                    '-background' : 'white'
                },
                outputDirectory : sourceUrl
            });
            var results = [],
                helper = [];
            for (var i = data.pagesFrom; i <= data.pagesTo; i++) {
                helper.push(i);
            }
            _.each(helper,function (item,i) {
                results[i] = function (callback) {
                    pdfImage
                        .convertPage(item)
                        .then(function (imageUrl) {
                            callback(null,imageUrl)
                        },callback);
                };
            });
            async.parallel(results,function (err,pages) {
                if (err) return cb(err);
                var imageURLs = _.map(pages,function (page) {
                    var url = page.split('/');
                    return path.join(data.test.sourceUrl,url[url.length-1]);
                });
                var deleteOldImagesTask = [];
                _.each(data.test.sections[data.sectionIndex].images,function (image,i) {
                    deleteOldImagesTask[i] = function (callback) {
                        HelperModule.deleteFile(path.join(publicUrl,image),callback);
                    };
                });
                async.parallel(deleteOldImagesTask,function (err) {
                    if (err) return cb(err);
                    data.test.sections[data.sectionIndex].images = imageURLs;
                    HelperModule.deleteFileObject(data,function (err) {
                        if (err) return cb(err);
                        folderSize(sourceUrl,function (err,size) {
                            data.test.memory = size;
                            return CenterModule.updateSpaceUsage(data,cb);
                        });

                    })
                });
            });
        });

    };
    var resolveAudioFile = function (data,cb) {
        var resolveOldFile = function (cb) {
            if (_.isEmpty(data.test.sections[data.sectionIndex].audio)) return cb();
            HelperModule.deleteFile(data.test.sections[data.sectionIndex].audio,cb);
        };

        if (!data.audioFile) return cb();
        CenterModule.isPossibleUploadFileObject({
            file : data.audioFile,
            centerID : data.centerID
        },function (err) {
            if (err) return cb(err);
            resolveOldFile(function (err) {
                if (err) return cb(err);
                var futureAudioPath = data.audioFile.path.split('/').pop();
                fs.rename(data.audioFile.path,path.join(data.test.getSourceUrl(),futureAudioPath),function (err) {
                    if (err) return cb(err);
                    data.test.sections[data.sectionIndex].audio = path.join(data.test.sourceUrl,futureAudioPath);
                    data.test.sections[data.sectionIndex].hasAudio = true;
                    data.test.save(cb);
                });
            });
        });
    };
    Test
        .findOne({
            _id : data.testID,
            centerID : data.centerID
        })
        .select('sections sourceUrl')
        .populate('sections')
        .exec(function (err,test) {
            if (err) return cb(err);
            if (!test) return cb(ErrorsModule.notFoundError);
            test.sections[data.sectionIndex].duration = data.duration && !isNaN(parseInt(data.duration)) ? data.duration * 60 : test.sections[data.sectionIndex].duration;
            data.test = test;
            resolveAnswers(data,function (err) {
                if (err) return cb(err);
                resolveFile(data,function (err) {
                    if (err) return cb(err);
                    resolveAudioFile(data,function (err) {
                        if (err) return cb(err);
                        data.test.save(cb);
                    });
                });
            });
        });
};

module.exports.updateTestByID = function (data,cb) {
    Test
        .findOne({
            _id : data.testID,
            centerID : data.centerID
        })
        .select('name')
        .exec(function (err,test) {
            if (err) return cb(err);
            if (!test) return cb(ErrorsModule.notFoundError);
            test.name = data.name;
            return test.save(cb);
        });
};
module.exports.getAvailableGroupsForTest = function (data,cb) {
    Group
        .find({
            centerID : data.centerID
        })
        .select('tests name')
        .exec(function (err,groups) {
            if (err) return cb(err);
            if (groups.length == 0) return cb(null,groups);
            var results = [];
            _.each(groups,function (group) {
                if (group.tests.length == 0) return results.push(group);
                var index =_.map(group.tests,function (test) {
                    return test.toString();
                }).indexOf(data.testID);
                if (index <0 ) return results.push(group);
            });
            cb(null,results);
        });
};


var _ = require('underscore'),
    async = require('async'),
    Test = require(__modelsPath + 'tests'),
    TestResult = require(__modelsPath + 'tests/test-result'),
    AnswerSheet = require(__modelsPath + 'tests/answer-sheet'),
    ConstantsModule = require('system/constants'),
    ErrorsModule = require('system/errors'),
    PDFImage = require("pdf-image").PDFImage,
    CenterModule = require('system/center'),
    fs = require('fs'),
    path = require('path'),
    HelperModule = require('system/helper'),
    Group = require(__modelsPath + 'group'),
    folderSize = require('get-folder-size'),
    crypto = require('crypto'),
    fsExtra = require('fs-extra'),
    pdfInfo = require('pdfinfojs');

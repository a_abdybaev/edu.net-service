module.exports.addInterview = function (data,cb) {
    var interview = new Interview({
        name : data.name,
        isDefault : data.isDefault,
        questions : data.questions
    });
    if (data.centerID) interview.centerID = data.centerID;
    interview.save(cb);
};
module.exports.getInterview = function (data,cb) {
    Interview
        .find({})
        .exec(cb);
};


var ErrorsModule = require('system/errors'),
    InterviewResult = require(__modelsPath + 'interview-result'),
    Interview = require(__modelsPath + 'interview'),

    Group = require(__modelsPath + 'group'),
    ConstantsModule = require('system/constants'),
    _ = require('underscore');




module.exports.addAttendance = function (data,cb) {
    Group
        .findOne({
            centerID : data.centerID,
            _id : data.groupID,
            teacherID : data.teacherID
        })
        .select('attendance')
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(Errors.objNotFound);
            var date = new Date();
            date.setDate(data.day);
            date.setMonth(data.month);
            date.setHours(0);
            date.setMinutes(0);
            date.setSeconds(0);
            date.setMilliseconds(0);
            data.date = date;
            exports.isValidAttendance(data,function (err,result) {
                if (err) return cb(err);
                if (!result) return cb(Errors.notAcceptable);
                var attendance = new Attendance({
                    day : data.day,
                    month : data.month,
                    date : data.date,
                    appearance : data.appearance,
                    centerID : data.centerID,
                    groupID : data.groupID
                });
                attendance.save(function (err) {
                    if (err) return cb(err);
                    Helper.addObjectInArray({
                        modelID : group._id,
                        modelName : Constants.MODELS.GROUP,
                        objectID : attendance._id,
                        objectPath : 'attendance'
                    },cb);
                });
            });
        });
};
module.exports.isValidAttendance = function (data,cb) {
    if(data.day >=0 && data.day <=31 && data.month >=0 && data.month<=11) {
        var appearance = data.appearance;
        if (appearance.length == 0) {
            return cb(null,false);
        } else {
            var result = true;
            appearance.forEach(function (context) {
                if (context.studentID == undefined || context.value == undefined ||  context.value > 2 || context.value <0) {
                    result = false;
                }
            });
            if (result == true) {
                exports.isExistSuchDay(data,function (err,result) {
                    cb(err,!result);
                });
            } else {
                cb(null,false);
            }
        }
    } else {
        cb(false);
    }
};
module.exports.isExistSuchDay = function (data,cb) {
    Attendance
        .find({
            groupID : data.groupID,
            centerID : data.centerID
        })
        .exec(function (err,attendances) {
            if (err) return cb(err);
            if (_.isEmpty(attendances)) return cb(null,false);
            var result = false;
            attendances.forEach(function (attendance) {
                if (parseInt(attendance.day) == parseInt(data.day) && parseInt(attendance.month) == parseInt(data.month)) {
                    result = true;
                }
            });
            cb(null,result);
        });
};
var Group = require(__modelsPath + 'group'),
    _ = require('underscore'),
    Attendance = require(__modelsPath + 'attendance'),
    Helper = require('system/helper'),
    Constants = require('system/constants'),
    Errors = require('system/errors');


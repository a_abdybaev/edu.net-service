module.exports.addPost = function (obj,cb) {
    CenterModule.isPossibleUploadFileObjects({
        files : obj.files.concat(obj.images),
        clientID : obj.clientID
    },function (err) {
        if (err) return cb(err);
        var groupID = obj.groupID,
            centerID = obj.centerID,
            text = obj.text,
            files = obj.files,
            important = obj.important,
            images = obj.images;
        Group
            .findOne({
                centerID : centerID,
                _id : groupID
            })
            .exec(function (err,group) {
                if (err) return cb(err);
                if (!group) return cb(ErrorsModule.notFoundError);
                var groupPath = group.getSourceUrl();
                var post = new Post({
                    centerID : centerID,
                    groupID : group._id,
                    text : text,
                    important : important
                });
                exports.savePostImages(group,images, function (err,data) {
                    if (err) return cb(err);
                    post.images = data;
                    exports.savePostFiles(group,files, function (err,data) {
                        if (err) return cb(err);
                        post.files = data;
                        group.files = group.files.concat(data);
                        exports.savePost(group,post, function (err) {
                            Helper.updateGroupSpaceUsageSync(obj);
                            cb(err);
                        });
                    });
                });
            });
    });
};

module.exports.updatePost = function (data,cb) {
    var deleteFilesAndImages = function (data,callback) {
        var results = [];
        var deletingFiles = data.deletingFiles || [];
        var deletingImages = data.deletingImages || [];
        var array = deletingFiles.concat(deletingImages);
        if (array.length == 0) return callback();
        array.forEach(function (file,i) {
            results[i] = function (callback2) {
                FileModel.deleteFile({
                    centerID : data.centerID,
                    fileID : file
                },callback2);
            }
        });
        async.series(results,callback);
    };
    CenterModule.isPossibleUploadFileObjects({
        files : data.images.concat(data.files),
        clientID : data.clientID
    },function (err) {
        if (err) return cb(err);
        Post
            .findOne({
                _id : data.postID,
                centerID : data.centerID
            })
            .populate({
                path : 'groupID'
            })
            .exec(function (err,post) {
                if (err) return cb(err);
                if (!post) return cb(ErrorsModule.notFoundError);
                post.text = data.text;
                deleteFilesAndImages(data,function (err) {
                    if (err) return cb(err);
                    exports.savePostImages(post.groupID,data.images,function (err,ids) {
                        if (err) return cb(err);
                        post.images = post.images.concat(ids);
                        exports.savePostFiles(post.groupID,data.files,function (err,ids) {
                            if (err) return cb(err);
                            post.files = post.files.concat(ids);
                            post.important = data.important;
                            post.save(function (err) {
                                Helper.updateGroupSpaceUsageSync({
                                    groupID : post.groupID
                                });
                                cb(err);
                            });
                        });
                    });
                });
            });

    });

};
module.exports.savePostImages = function (group,images,cb) {
    var groupPath = group.getSourceUrl();
    if (!images || _.isEmpty(images)) return cb(null,[]);
    var results = [];
    images.forEach(function (item,i) {
        results[i] = function (callback) {
            FileModel.addFile({
                file :item,
                centerID : group.centerID,
                groupID : group._id,
                isImage : true,
                futureAbsolutePath : groupPath,
                futureRelativePath : group.sourceUrl
            },callback);
        };
    });
    async.series(results, cb);
};
module.exports.savePostFiles = function (group,files,cb) {
    var groupPath = group.getSourceUrl();
    if (!files || _.isEmpty(files)) return cb(null,[]);
    var results = [];
    files.forEach(function (item,i) {
        results[i] = function (callback) {
            FileModel.addFile({
                file :item,
                centerID : group.centerID,
                groupID : group._id,
                isImage : false,
                futureAbsolutePath : groupPath,
                futureRelativePath : group.sourceUrl
            },callback);
        };
    });
    async.series(results, cb);
};

module.exports.savePost = function (group,post,cb) {
    group.channel.push(post._id);
    group.save(function (err) {
        if (err) return cb(err);
        post.save(cb);
    });
};
module.exports.deletePost = function (obj,cb) {
    Post
        .findOne({
            _id : obj.postID,
            centerID : obj.centerID
        })
        .exec(function (err,post) {
            if (err) return cb(err);
            if (!post) return cb(ErrorsModule.notFoundError);
            Helper.deleteObjectFromArray({
                modelName : ConstantsModule.MODELS.GROUP,
                modelID : post.groupID,
                objectID : post._id,
                objectPath : "channel"
            },function (err) {
                if (err) return cb(err);
                exports.deletePostComments(post,function (err) {
                    if (err) return cb(err);
                    exports.deletePostFiles(post,function (err) {
                        if (err) return cb(err);
                        post.remove(function (err) {
                            Helper.updateGroupSpaceUsageSync({
                                groupID : post.groupID
                            });
                            return cb(err);
                        });
                    });

                });
            });
        });
};
module.exports.deletePostComments = function (post,cb) {
    if (post.comments.length == 0) return cb(null);
    Comment
        .find({
            postID : post._id,
            centerID : post.centerID
        })
        .remove()
        .exec(cb);
};
module.exports.deletePostFiles = function (post,cb) {
    File
        .find({
            centerID : post.centerID,
            postID : post._id
        })
        .exec(function (err,files) {
            if (err) return cb(err);
            if (files.length == 0) return cb();
            var results =[];
            files.forEach(function (file,i) {
                results[i] = function (callback) {
                    var pathToFile = path.normalize(path.join(__publicPath,file.url));
                    FileModel.unlinkFile(pathToFile,function (err) {
                        if (err) return callback(err);
                        file.remove(cb);
                    });
                };
            });
        });
};
module.exports.getPosts = function (data,cb) {
    var obj = {
        centerID : data.centerID,
        groupID : data.groupID
    };
    if (data.important) {
        obj.important = data.important
    }

    var query = Post
        .find(obj)
        .limit(ConstantsModule.POSTS_PER_PAGE)
        .skip(data.page * ConstantsModule.POSTS_PER_PAGE);


    if (data.homework) {
        query.where('homeworkID').exists();
    }
    if (data.onlyFiles) {
        query.$where('this.files.length != 0')
    }

    query.sort({
        createdAt :-1
    });
    query
        .populate([{
            path : 'comments',
            populate : {
                path : 'user.value'
            }
        },{
            path : 'files'
        },{
            path : 'images'
        },{
            path : 'homeworkID'
        }]);

    query.exec(cb);
};
module.exports.addComment = function (data,cb) {
    var resolveReply = function (futureComment,cb) {
        if (!data.isReply) return cb();
        futureComment.reply.commentID = data.replyCommentID;
        Comment
            .findOne({
                _id : data.replyCommentID
            })
            .select('user userType')
            .populate({
                path : 'user.value',
                select : 'firstName '
            })
            .exec(function (err,comment) {
                if (err) return cb(err);
                if (!comment) return cb(ErrorsModule.notFoundError);
                var replyText = comment.user.value.firstName + ', ';
                futureComment.reply.isImportantName = false;
                // console.log(data.content.indexOf(replyText));
                if(data.content.indexOf(replyText) == 0) {
                    futureComment.reply.isImportantName = true;
                    futureComment.reply.name = comment.user.value.firstName;
                    futureComment.content = data.content.replace(replyText,'');
                }
                cb();
            });
    };
    Post
        .findOne({
            _id : data.postID,
            centerID : data.centerID
        })
        .populate({
            path : 'groupID',
            select : 'teacherID'
        })
        .exec(function (err,post) {
            if (err) return cb(err);
            if (!post) return cb(ErrorsModule.notFoundError);
            if (post.groupID.teacherID.toString() != data.teacherID) return cb(ErrorsModule.badRequestError);
            var comment = new Comment({
                content : data.content,
                teacherID : data.teacherID,
                postID : data.postID,
                reply : {
                    isReply : data.isReply
                },
                centerID : data.centerID,
                userType : ConstantsModule.TEACHER,
                user : {
                    model : ConstantsModule.MODELS.TEACHER,
                    value : data.teacherID

                }
            });
            resolveReply(comment,function (err) {
                if (err) return cb(err);
                Helper.addObjectInArray({
                    modelID : post._id,
                    modelName : ConstantsModule.MODELS.POST,
                    objectID : comment._id,
                    objectPath : "comments"
                },function (err) {
                    if (err) return cb(err);
                    return comment.save(cb);
                });
            });
        });
};
var Group = require(__modelsPath + 'group'),
    ErrorsModule = require('system/errors'),
    Helper = require('system/helper'),
    async = require('async'),
    _ = require('underscore'),
    fs = require('fs'),
    Post = require(__modelsPath + 'post'),
    path = require('path'),
    File = require(__modelsPath + 'file'),
    FileModel = require('teacher/file'),
    ConstantsModule = require('system/constants'),
    CenterModule = require('system/center'),
    Comment = require(__modelsPath + 'comment');



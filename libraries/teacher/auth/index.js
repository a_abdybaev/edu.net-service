module.exports.auth = function (data,cb) {
    Teacher
        .findOne({
            'token.value' : data.token
        })
        .exec(function (err,teacher) {
            if (err) return cb(err);
            if (!teacher) return cb(Errors.invalidTokenError);
            UserModule.updateVisit({
                user : teacher
            },function (err) {
                cb(err,teacher);
            });
        });
};
module.exports.login = function (data,cb) {
    Teacher
        .findOne({
            login : data.login
        })
        .select('hash salt clientID centerID')
        .populate('centerID')
        .exec(function (err,teacher) {
            if (err) return cb(err);
            if (!teacher) return cb();
            if (!teacher.centerID) return teacher.remove(function (err) {
                if (err) return (err);
                cb(Errors.objNotFound);
            });
            data.user = teacher;
            if (!UserModule.isValidPass(data)) return cb(Errors.notFoundError);
            ClientModule.isPaid(teacher,function (err,isPaid) {
                if (err) return cb(err);
                if (!isPaid) return cb(Errors.paymentRequiredError);
                UserModule.generateToken({user:teacher},cb);
            });
        });
};

var Errors = require('system/errors'),
    ClientModule = require('system/client'),
    Teacher = require(__modelsPath + 'teacher'),
    UserModule = require('system/user');



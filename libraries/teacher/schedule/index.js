module.exports.getSchedule = function (data,cb) {
    Group
        .find({
            centerID : data.centerID,
            teacherID : data.teacherID
        })
        .select('schedule')
        .populate({
            path : 'schedule',
            populate : [{
                path : 'groupID',
                select : 'name color students',
                populate : {
                    path : 'students',
                    select : '_id'
                }
            },{
                path : 'classroomID',
                select : 'name'
            }]
        })
        .exec(function (err,groups) {
            if(err) return cb(err);
            if (!groups) return (null,[]);
            var results = [];
            groups.forEach(function (group) {
                results = results.concat(group.schedule);
            });
            cb(null,results);
        });
};

var Group = require(__modelsPath + 'group'),
    errors = require('system/errors');
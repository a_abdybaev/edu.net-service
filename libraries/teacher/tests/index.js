module.exports.getGroupResultsByTestID = function (data,cb) {

    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID
        })
        .select('students tests')

        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.notFoundError);
            TestResult
                .find({
                    testID : data.testID,
                    centerID : data.centerID,
                    studentID : {
                        $in : group.students
                    }
                })
                .populate({
                    path : 'studentID',
                    select : 'firstName lastName'
                })
                .exec(cb);

        });
};
module.exports.getStudentsResultsByGroupID = function (data,cb) {
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID
        })
        .select('students tests')
        .populate({
            path : 'students',
            select : 'firstName lastName answers'
        })
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.notFoundError);
            if (_.isEmpty(group.students) || _.isEmpty(group.tests)) return cb(null,{
                noResults : true,
                students : group.students
            });
            var asyncResults = [];
            _.each(group.students,function (student,i) {
                asyncResults[i] = function (callback) {
                    if (_.isEmpty(student.answers)) return callback(null,[]);
                    TestResult
                        .find({
                            studentID : student._id,
                            testID : {
                                $in : group.tests
                            }
                        })
                        .populate({
                            path : 'testID',
                            select : 'name'
                        })
                        .select('-centerID -studentID -updatedAt -_id -__v')
                        .exec(callback);
                };
            });
            async.series(asyncResults,function (err,studentResults) {
                cb(err,{
                    results : studentResults,
                    students : group.students
                });
            });
        });
};
module.exports.getTestResultsByGroupID = function (data,cb) {
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID
        })
        .select('tests students')
        .populate({
            path : 'tests'
        })
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.notFoundError);
            var result = [];
            if (group.tests.length == 0) return cb(null,{
                tests : [],
                results : result
            });
            _.each(group.tests,function (test,i) {
                result[i] = function (callback) {
                    TestResult
                        .find({
                            testID : test._id,
                            studentID : {
                                $in : group.students
                            },
                            centerID : data.centerID
                        })
                        .populate({
                            path : 'studentID'
                        })
                        .exec(function (err,testResults) {
                            if (err) return callback(err);
                            callback(null,testResults);
                        });
                };
            });
            async.series(result,function (err,results) {
                cb(err,{
                    tests : group.tests,
                    results : results
                });
            });
        });
};

var Group = require(__modelsPath + 'group'),
    async = require('async'),
    _ = require('underscore'),
    ErrorsModule = require('system/errors'),
    TestResult = require(__modelsPath + 'tests/test-result');
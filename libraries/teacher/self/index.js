module.exports.getTeacherInfoByID = function (data,cb) {
    Teacher
        .findOne({
            _id : data.teacherID,
            centerID : data.centerID
        })
        .exec(function (err,teacher) {
            if (err) return cb(err);
            if (!teacher) return cb(Errors.objNotFound);
            ListModule.getGroupsList(data,function (err,groups) {
                console.log(groups);
                cb(err,{
                    groups : groups,
                    teacher : teacher
                });
            });
        });
};
// module.exports.recoverPassword = function (data,cb) {
//     var login = data.centerDomain + '.' + data.phone;
//     Teacher
//         .findOne({
//             login : login
//         })
//         .exec(function (err,teacher) {
//             if (err) return cb(err);
//             if (!teacher) return cb(errors.notFoundError);
//             UserModule.generateRecoverPasswordHash(teacher,cb);
//         });
// };

module.exports.updatePassword = function (data,cb) {
    Teacher
        .findOne({
            _id : data.teacherID,
            centerID : data.centerID
        })
        .select('hash salt')
        .exec(function (err,teacher) {
            if (err) return cb(err);
            if (!teacher) return cb(Errors.notFoundError);
            data.user = teacher;
            UserModule.updatePass(data,cb)
        });
};
module.exports.updateImage = function (data,cb) {
     var image = data.image;
     exports.deleteImage(data,function (err) {
     if (err) return cb(err);
     Center
         .findOne({
             _id : data.centerID
         })
         .exec(function (err,center) {
             if (err) return cb(err);
             if (!center) return cb(Errors.notFoundError);
             var ext = image.split(';')[0].match(/jpeg|png|gif/)[0];
             var imageData = image.replace(/^data:image\/\w+;base64,/,'');
             var fileName = crypto.randomBytes(10).toString('hex') + '.' + ext;
             var publicImagePath = center.sourceUrl + fileName;
             var privateImagePath = center.getSourceUrl() + fileName;
             fs.writeFile(privateImagePath,imageData,{
                 encoding : 'base64'
             },function (err) {
                 if (err) return cb(err);
                 Teacher
                     .findOne({
                         _id : data.teacherID,
                         centerID : data.centerID
                     })
                     .exec(function (err,teacher) {
                         if (err) return cb(err);
                         if (!teacher) return cb(Errors.notFoundError);
                         teacher.imageUrl = publicImagePath;
                         teacher.save(cb);
                     });
             });
         });
     });

};
module.exports.deleteImage = function (data,cb) {
    Teacher
        .findOne({
            _id : data.teacherID
        })
        .exec(function (err,teacher) {
            if (err) return cb(err);
            if (!teacher) return cb(Errors.notFoundError);
            if (teacher.imageUrl == undefined) return cb(null);
            var pathToFile = path.normalize(path.join(__publicPath,teacher.imageUrl));
            fs.stat(pathToFile,function (err,stats) {
                if (err) return cb(null);
                if (!stats.isFile()) return cb(null);
                fse.remove(pathToFile,cb);
            });
        });
};

module.exports.updateProfile = function (data,cb) {
    Teacher
        .findOne({
            _id : data.teacherID,
            centerID : data.centerID
        })
        .exec(function (err,teacher) {
            if (err) return cb(err);
            if (!teacher) return cb(Errors.notFoundError);
            teacher.firstName = data.firstName;
            teacher.lastName = data.lastName;
            teacher.email = data.email;
            return teacher.save(cb);
        });
};
var Teacher = require(__modelsPath + 'teacher'),
    Center = require(__modelsPath + 'center'),
    Group = require(__modelsPath + 'group'),

    ListModule = require('teacher/list'),
    UserModule = require('system/user'),
    Errors = require('system/errors'),
    path = require('path'),
    crypto = require('crypto'),

    fse = require('fs-extra'),
    fs = require('fs')
    ;
module.exports.getGroupsList = function (data,cb) {
    Group
        .find({
            centerID : data.centerID
        })
        .select('color name teachers')
        .populate({
            path : 'teachers',
            match : {
                _id : data.teacherID
            }
        })
        .exec(function (err,groups) {
            if (err) return cb(err);
            if (_.isEmpty(groups)) return cb(null,[]);
            cb(null,_.filter(groups,function (group) {
                return !_.isEmpty(group.teachers)
            }));
        });
};
module.exports.getStudentsListByGroupID = function (data,cb) {
    Group
        .findOne({
            teacherID : data.teacherID,
            centerID : data.centerID,
            _id : data.groupID
        })
        .select('students ')
        .populate({
            path : 'students',
            select : 'firstName lastName'
        })
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(_Errors.notFound);
            cb(null,group.students);
        });
};

var Group = require(__modelsPath + 'group'),
    _ = require('underscore'),
    _Errors = require('system/errors'),
    Teacher = require(__modelsPath + 'teacher');
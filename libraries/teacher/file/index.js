module.exports.getFilesByTeacherID = function (data,cb) {
    Group
        .find({
            centerID : data.centerID,
            teacherID : data.teacherID
        })
        .select('_id')
        .exec(function (err,groups) {
            if (err) return cb(err);
            if (_.isEmpty(groups)) return cb(null,[]);
            var groupIDs = _.map(groups,function (group) {
                return group._id;
            });
            File
                .find({
                    centerID : data.centerID,
                    groupID  : {
                        '$in' : groupIDs
                    }
                })
                .select('-centerID -updatedAt -kind -isImage -__v')
                .exec(cb);
        });
};
module.exports.addFile = function (data,cb) {
    var randomFileName = crypto.randomBytes(10).toString('hex');
    var fileExtension = data.file.name.split(".");
    if (fileExtension.length == 0) return cb(ErrorsModule.badRequestError);
    fileExtension = fileExtension.pop();
    var futureFileName = randomFileName + '.' + fileExtension;
    var futureAbsolutePath = path.normalize(path.join(data.futureAbsolutePath,futureFileName));
    var futureRelativePath = path.normalize(path.join(data.futureRelativePath,futureFileName));
    fs.rename(data.file.path,futureAbsolutePath,function (err) {
        if (err) return cb(err);
        var file = new File({
            url : futureRelativePath,
            centerID : data.centerID,
            size : data.file.size,
            groupID : data.groupID,
            isImage : data.isImage,
            extension : fileExtension,
            name : data.file.name
        });
        file.save(function (err) {
            return cb(err,file._id)
        });
    });
};
module.exports.deleteFile = function (data,cb) {
    File
        .findOne({
            centerID : data.centerID,
            _id : data.fileID
        })
        .remove()
        .exec(cb);
};

module.exports.deleteFileByID = function (data,cb) {
    var fileID = data.fileID,
        centerID = data.centerID,
        teacherID = data.teacherID;
    File
        .findOne({
            centerID : centerID,
            _id : fileID
        })
        .populate({
            path : 'groupID',
            select : 'teacherID'
        })
        .exec(function (err,file) {
            if (err) return cb(err);
            if (!file) return cb(ErrorsModule.notFoundError);
            Helper.deleteObjectFromArray({
                objectID : file._id,
                objectPath : 'files',
                modelName : Constants.MODELS.GROUP,
                modelID : file.groupID
            },function (err) {
                if (err) return cb(err);
                var pathToFile = path.normalize(path.join(__publicPath,file.url));
                exports.unlinkFile(pathToFile,function (err) {
                    if (err) return cb(err);
                    file.remove(function (err) {
                        CenterModule.updateSpaceUsageBG({
                            centerID : file.centerID
                        });
                        cb(err);
                    });
                });
            });
        });
};
module.exports.unlinkFile = function (pathToFile,cb) {
    fs.stat(pathToFile,function (err,stats) {
        if (err) return cb(err);
        if (!stats.isFile()) return cb(ErrorsModule.serverInternalError);
        fse.remove(pathToFile,cb);

    });
};
module.exports.addFileForGroup = function (data,cb) {
    CenterModule.isPossibleUploadFileObject(data,function (err) {
        if (err) return cb(err);
        Group
            .findOne({
                teacherID : data.teacherID,
                _id : data.groupID,
                centerID : data.centerID
            })
            .exec(function (err,group) {
                if (err) return cb(err);
                if (!group) return cb(ErrorsModule.notFoundError);
                exports.addFile({
                    file : data.file,
                    centerID : data.centerID,
                    groupID : group._id,
                    isImage : false,
                    futureAbsolutePath : group.getSourceUrl(),
                    futureRelativePath : group.sourceUrl
                },function (err,fileID) {
                    if (err) return cb(err);
                    group.files.push(fileID);
                    group.save(function (err) {
                        Helper.updateGroupSpaceUsageSync(data);
                        cb(err);
                    });
                });
            });
    });

};
module.exports.getFilesByGroupID = function (data,cb) {
    Group
        .findOne({
            teacherID : data.teacherID,
            centerID : data.centerID,
            _id : data.groupID
        })
        .populate({
            path : 'files'
        })
        .select('files')
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.notFoundError);
            return cb(null,group.files);
        });
};
var Teacher = require(__modelsPath + 'teacher'),
    crypto = require('crypto'),
    path = require('path'),
    fs = require('fs'),
    File = require(__modelsPath + 'file'),
    Group = require(__modelsPath + 'group'),
    Helper = require('system/helper'),
    Constants = require('system/constants'),
    fse = require('fs-extra'),
    _ = require('underscore'),
    CenterModule = require('system/center'),
    ErrorsModule = require('system/errors');
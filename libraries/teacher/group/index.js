module.exports.getGroupByID = function (data,cb) {
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID,
            teacherID : data.teacherID
        })
        .populate([{
            path : 'schedule',
            populate : {
                path : 'classroomID'
            }
        },{
            path : 'channel',
            populate : [{
                path : 'files'
            },{
                path : 'images'
            },{
                path : 'homeworkID',
                options : {
                    sort : {
                        createdAt : -1
                    }
                }
            },{
                path : 'comments',
                populate : {
                    path : 'user.value'
                }
            }],
            options : {
                limit : ConstantsModule.POSTS_PER_PAGE,
                sort : {
                    createdAt : -1
                }
            }
        },{
            path : 'homeworks',
            options : {
                sort : {
                    createdAt : -1
                },
                limit : 5
            }
        },{
            path : 'students',
            populate : {
                path : 'appearance',
                match : {
                    groupID : data.groupID
                }
            }
        },{
            path : 'files'
        },{
            path : 'applications',
            populate : {
                path : 'answerID'
            },
            options : {
                sort : {
                    createdAt : -1
                }
            }
        },{
            path : 'attendance'
        },{
            path : 'tests'
        }])
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.notFoundError);
            cb(null,group);
        });
};
module.exports.getGroups = function (data,cb) {
    Group
        .find({
            centerID : data.centerID,
            teacherID : data.teacherID
        })
        .select('memory students createdAt name color')
        .populate({
            path : 'students',
            select : 'firstName lastName'
        })
        .exec(cb);

};


var Group = require(__modelsPath + 'group'),
    ConstantsModule = require('system/constants'),
    ErrorsModule = require('system/errors');

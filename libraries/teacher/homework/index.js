module.exports.add = function (obj,cb) {
    var groupID = obj.groupID,
        centerID = obj.centerID,
        content = obj.content,
        comment = obj.comment,
        day = obj.day,
        month = obj.month;


    var homework = new Homework({
        groupID: groupID,
        date: {
            day: day,
            month: month
        },
        content: content,
        centerID: centerID
    });
    if (comment != undefined && comment != '') {
        homework.comment = comment
    }
    homework.save(function (err) {
        if (err) return cb(err);
        Helper.addObjectInArray({
            objectID: homework._id,
            objectPath: "homeworks",
            modelName: Constants.MODELS.GROUP,
            modelID: groupID
        }, function (err) {
            if (err) return cb(err);
            var post = new Post({
                groupID: groupID,
                centerID: centerID,
                homeworkID: homework._id
            });
            post.save(function (err) {
                if (err) return cb(err);
                Helper.addObjectInArray({
                    objectID: post._id,
                    objectPath: "channel",
                    modelName: Constants.MODELS.GROUP,
                    modelID: groupID
                }, cb);
            });
        });
    });
};
module.exports.delByID = function (obj,cb) {
    var deleteHomeworkAsPost = function (homework,cb) {
        Post
            .findOne({
                homeworkID : homework._id,
                centerID : homework.centerID
            })
            .exec(function (err,post) {
                if (err) return cb(err);
                if (!post) return cb();
                ChannelModule.deletePost({
                    centerID : post.centerID,
                    postID : post._id
                },cb);
            });
    };
    var centerID = obj.centerID,
        homeworkID = obj.homeworkID;
    Homework
        .findOne({
            _id : homeworkID,
            centerID : centerID
        })
        .exec(function (err,homework) {
            if (err) return cb(err);
            if (!homework) return cb(Errors.objNotFound);
            Helper.deleteObjectFromArray({
                modelName : Constants.MODELS.GROUP,
                modelID : homework.groupID,
                objectID : homework._id,
                objectPath : 'homeworks'
            },function (err) {
                 if (err) return cb(err);
                deleteHomeworkAsPost(homework,function (err) {
                    if (err) return cb(err);
                    homework.remove(cb);
                });
            });
        });
};
module.exports.get = function (data,cb) {
    Homework
        .find({
            centerID : data.centerID,
            groupID : data.groupID
        })
        .exec(cb);
};
module.exports.editByID = function (obj,cb) {
    var centerID = obj.centerID,
        content = obj.content,
        comment = obj.comment,
        homeworkID = obj.homeworkID;
    Homework
        .findOne({
            _id : homeworkID,
            centerID : centerID
        })
        .exec(function (err,homework) {
            if (err) return cb(err);
            if (!homework) return cb(Errors.objNotFound);
            homework.content  = content;
            homework.comment = comment;
            homework.save(cb);
        });
};
module.exports.getByID = function (data,cb) {
    Homework
        .findOne({
            _id : data.homeworkID,
            centerID : data.centerID
        })
        .exec(function (err,homework) {
            if (err) return cb(err);
            if (!homework) return cb(Errors.objNotFound);
            cb(null,homework);
        });
};


var Group = require(__modelsPath + 'group'),
    Homework = require(__modelsPath + 'homework'),
    Post = require(__modelsPath + 'post'),
    Errors = require('system/errors'),
    ChannelModule = require('teacher/channel'),
    Helper = require('system/helper'),
    Constants = require('system/constants');
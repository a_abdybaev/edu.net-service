module.exports.addAppearanceByGroupID = function (data,cb) {
    var results = [];
    data.attendance.forEach(function (studentAppearance,i) {
        results[i] = function (callback) {
            var appearance = new Appearance({
                groupID : data.groupID,
                day : data.day,
                month : data.month,
                year : new Date().getFullYear(),
                centerID : data.centerID,
                studentID : studentAppearance.studentID,
                value : studentAppearance.value,
                date : new Date(new Date().getFullYear(),data.month,data.day)
            });
            exports.isExistAppearance(appearance,function (err,result) {
                if (err) return callback(err);
                if (result == true) return callback(ErrorsModule.duplicateAttendanceDayError);
                appearance.save(function (err) {
                    if (err) return callback(err);
                    CustomHelper.addObjectInArray({
                        modelName : 'Student',
                        modelID : appearance.studentID,
                        objectID : appearance._id,
                        objectPath : 'appearance',
                        centerID : data.centerID
                    },callback);
                });
            });
        };
    });
    async.series(results,cb);
};
module.exports.isExistAppearance = function (appearance,cb) {
    Appearance
        .findOne({
            studentID : appearance.studentID,
            groupID : appearance.groupID,
            day : appearance.day,
            month : appearance.month,
            year : appearance.year,
            centerID : appearance.centerID
        })
        .exec(function (err,result) {
            if (err) return cb(err);
            return cb(null, result == null ? false : true);
        });
};

var Appearance = require(__modelsPath + 'appearance'),
    Student = require(__modelsPath + 'student'),
    ErrorsModule = require('system/errors'),
    CustomHelper = require('system/helper'),
    async = require('async');
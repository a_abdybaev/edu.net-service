module.exports.createApplication = function (obj,cb) {
    var groupID = obj.groupID,
        centerID = obj.centerID,
        teacherID = obj.teacherID,
        content = obj.content;
    Group
        .findOne({
            _id : groupID,
            centerID : centerID,
            teacherID : teacherID
        })
        .select('applications')
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(Errors.objNotFound);
            var application = new Application({
                groupID : groupID,
                centerID : centerID,
                teacherID : teacherID,
                content : content
            });
            group.applications.push(application._id);
            group.save(function (err) {
                if (err) return cb(err);
                application.save(cb);
            });
        });
};
module.exports.updateApplicationByID = function (obj,cb) {
    var applicationID = obj.applicationID,
        centerID = obj.centerID,
        teacherID = obj.teacherID,
        content = obj.content;
    Application
        .findOne({
            _id : applicationID,
            centerID : centerID,
            teacherID : teacherID
        })
        .exec(function (err,application) {
            if (err) return cb(err);
            if (!application) return cb(Errors.objNotFound);
            application.content = content;
            application.updatedAt = new Date();
            application.save(cb);
        });
};
module.exports.getApplicationByID = function (data,cb) {
    Application
        .findOne({
            teacherID :data.teacherID,
            centerID : data.centerID,
            _id : data.applicationID
        })
        .exec(function (err,application) {
            if (err) return cb(err);
            if (!application) return cb(Errors.notFoundError);
            return cb(null,application);
        });
};
module.exports.getApplications = function (data,cb) {
    Application
        .find({
            teacherID : data.teacherID,
            centerID : data.centerID
        })
        .populate([{
            path : 'groupID',
            select : 'name color'
        },{
            path : 'answerID'
        }])
        .exec(cb);
};

module.exports.getApplicationsByGroupID = function (data,cb) {
    Application
        .find({
            centerID : data.centerID,
            teacherID : data.teacherID,
            groupID : data.groupID
        })
        .sort({
            createdAt : -1
        })
        .populate({
            path : 'answerID'
        })
        .exec(cb);
};
module.exports.deleteApplicationByID = function (data,cb) {
    Application
        .findOne({
            centerID : data.centerID,
            _id : data.applicationID,
            teacherID : data.teacherID
        })
        .exec(function (err,application) {
            if (err) return cb(err);
            if (!application) return cb(Errors.notFoundError);
            application.remove(function (err) {
                if (err) return cb(err);
                Helper.deleteObjectFromArray({
                    modelName : Constants.MODELS.GROUP,
                    modelID : application.groupID,
                    objectID : application._id,
                    objectPath : 'applications'
                },cb);
            });
        });
};

var Application = require(__modelsPath + 'application'),
    Helper = require('system/helper'),
    Constants = require('system/constants'),
    Errors = require('system/errors'),
    Group = require(__modelsPath + 'group');
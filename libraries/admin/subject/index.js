module.exports.addSubject = function (data,cb) {
    var subject = new Subject({
        name : data.name,
        centerID : data.centerID
    });

    _.each(data.levels,function (level) {
        subject.levels.push({
            value : level
        });
    });
    return subject.save(cb);
};
module.exports.getSubjects = function (data,cb) {
    Subject
        .find({
            centerID : data.centerID
        })
        .select('-centerID -__v -updatedAt -createdAt')
        .exec(cb);
};
module.exports.deleteSubjectByID = function (data,cb) {
    Subject
        .findOne({
            _id : data.subjectID
        })
        .remove()
        .exec(cb);
};
module.exports.updateSubjectByID = function (data,cb) {
    Subject
        .findOne({
            _id : data.subjectID
        })
        .exec(function (err,subject) {
            if (err) return cb(err);
            if (!subject) return cb(errors.notFoundError);
            subject.name = data.name;
            _.each(data.levels,function (level) {
                subject.levels.push({
                    value : level
                });
            });
            subject.save(cb);
        });
};
module.exports.getSubjectByID = function (data,cb) {
    Subject
        .findOne({
            _id : data.subjectID
        })
        .exec(function (err,subject) {
            if (err) return cb(err);
            if (!subject) return cb(errors.notFoundError);
            return cb(null,subject);
        });
};

var Subject = require(__modelsPath + 'subject'),
    _ = require('underscore'),
    errors = require('system/errors');
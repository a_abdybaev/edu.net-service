module.exports.deleteLessonsByClassroomID = function (data,cb) {
    Lesson
        .find({
            centerID : data.centerID,
            classroomID : data.classroomID
        })
        .remove()
        .exec(cb);
};


var errors = require('system/errors'),
    async = require('async'),
    Lesson = require(__modelsPath + 'lesson');



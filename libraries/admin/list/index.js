module.exports.getListOfGroups = function (data,cb) {
    Group
        .find({
            centerID : data.centerID
        })
        .select('name color _id price')
        .exec(cb);
};
module.exports.getListOfTeachers = function (data,cb) {
    Teacher
        .find({
            centerID : data.centerID
        })
        .select('firstName lastName _id')
        .exec(cb);
};

var Teacher = require(__modelsPath + 'teacher'),
    Group = require(__modelsPath + 'group'),
    errors = require('system/errors');
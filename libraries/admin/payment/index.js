module.exports.addPayment = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .select('groups payments')
        .populate({
            path : 'groups',
            match : {
                _id : data.groupID
            },
            select : 'price _id'
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(Errors.notFoundError);
            if (student.groups.length == 0 || student.groups[0].price == null || student.groups[0].price == 0) return cb(Errors.badRequest);
            var payment = new Payment({
                centerID : data.centerID,
                studentID : data.studentID,
                value : data.value,
                comment : data.comment,
                groupID : data.groupID
            });
            payment.save(function (err) {
                if (err) return cb(err);
                student.payments.push(payment._id);
                return student.save(cb);
            });
        });
};
module.exports.getPayments = function (data,cb) {
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID
        })
        .select('students price')
        .populate({
            path : 'students',
            select : 'firstName lastName payments',
            populate : {
                path : 'payments',
                match : {
                    groupID : data.groupID
                }
            }
        })
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(Errors.objNotFound);
            cb(null,group.students);
        });
};
module.exports.updatePaymentByID = function (data,cb) {
    Payment
        .findOne({
            _id : data.paymentID,
            centerID : data.centerID
        })
        .exec(function (err,payment) {
            if (err) return cb(err);
            if (!payment) return cb(Errors.objNotFound);
            payment.value = data.value;
            payment.comment = data.comment;
            payment.save(cb);
        });
};
module.exports.deletePaymentByID = function (data,cb) {
    Payment
        .findOne({
            _id : data.paymentID,
            centerID : data.centerID
        })
        .exec(function (err,payment) {
            if (err) return cb(err);
            if (!payment) return cb(Errors.objNotFound);
            payment.remove(function (err) {
                if (err) return cb(err);
                Helper.deleteObjectFromArray({
                    modelName : Constants.MODELS.STUDENT,
                    modelID : payment.studentID,
                    objectPath : 'payments',
                    objectID : payment._id
                },cb);
            });
        });
};
module.exports.getPaymentByID = function (data,cb) {
    Payment
        .findOne({
            _id : data.paymentID,
            centerID : data.centerID
        })
        .populate({
            path : 'studentID',
            select : 'firstName lastName'
        })
        .exec(function (err,payment) {
            if (err) return cb(err);
            if (!payment) return cb(Errors.objNotFound);
            cb(null,payment);
        });
};

var Student = require(__modelsPath + 'student'),
    Payment = require(__modelsPath + 'payment'),
    Group = require(__modelsPath + 'group'),
    Constants = require('system/constants'),
    Helper = require('system/helper'),
    Errors = require('system/errors');
module.exports.isPossibleCreateStudentByTariff = function (data,cb) {
    Client
        .findOne({
            _id : data.clientID
        })
        .populate('tariffID')
        .exec(function (err,client) {
            if (err) return cb(err);
            if (!client) return cb(Error.objNotFound);
            Student
                .count({
                    centerID : data.centerID,
                    clientID : data.clientID,
                    status : Constants.STUDENT_STATUS_ACTIVE
                })
                .exec(function (err,count) {
                    cb(err,count < client.tariffID.students);
                });
        });
};
module.exports.isPossibleCreateStudentsByTariff = function (data,cb) {
    Client
        .findOne({
            _id : data.clientID
        })
        .populate('tariffID')
        .exec(function (err,client) {
            if (err) return cb(err);
            if (!client) return cb(Error.objNotFound);
            Student
                .count({
                    centerID : data.centerID,
                    clientID : data.clientID,
                    status : Constants.STUDENT_STATUS_ACTIVE
                })
                .exec(function (err,count) {
                    cb(err,count + data.count <= client.tariffID.students);
                });

        });
};
module.exports.getTariffs = function (data,cb) {
    Center
        .findOne({
            _id : data.centerID
        })
        .select('tariffID')
        .populate('tariffID')
        .exec(function (err,center) {
            if (err) return cb(err);
            if (!center) return cb(Errors.notFoundError);
            Tariff
                .find({
                    isMajor : true
                })
                .exec(function (err,tariffs) {
                    cb(err,{
                        tariffs : tariffs,
                        tariff : center.tariffID
                        });

                });
        });
};

var Center = require(__modelsPath + 'center'),
    Tariff = require(__modelsPath + 'tariff'),
    Student = require(__modelsPath + 'student'),
    Client = require(__modelsPath + 'client'),
    Constants = require('system/constants'),
    Errors = require('system/errors'),
    _ = require('underscore');
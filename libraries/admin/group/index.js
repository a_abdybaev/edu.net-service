module.exports.addGroup = function (obj,cb) {
    var group = new Group({
        name : obj.name,
        color : obj.color,
        centerID : obj.centerID,
        price : obj.price,
        individual : obj.individual,
        teachers : obj.teachers
    });
    exports.initGroup(group,function (err) {
        if (err) return cb(err);
        group.save(cb);
    });
};





module.exports.deleteSourceUrlForGroup = function (group,cb) {
    var groupDirectory = group.getSourceUrl();
    if (!groupDirectory) {
        cb(null);
        return LoggerModule.log(group);
    }
    fs.stat(groupDirectory, function (err,stats) {
        if (err && err.code == 'ENOENT') {
            LoggerModule.log({
                group : group,
                message : 'FOLDER NOT EXIST FOR GROUP, WHILE DELETING'
            });
            cb(null);
            return;
        }
        if (err) return cb(err);
        if (stats.isDirectory() == false) {
            LoggerModule.log({
                group : group,
                message : 'FOLDER NOT EXIST FOR GROUP, WHILE DELETING'
            });
            return cb(null);
        }
        fse.remove(groupDirectory,cb);
    });
};
module.exports.getStudentsByGroupID = function (data,cb) {
    Group
        .findOne({
            centerID : data.centerID,
            _id : data.groupID
        })
        .populate({
            path : 'students',
            populate: [{
                path : 'appearance',
                match : {
                    groupID : data.groupID
                }
            },{
                path : 'payments',
                match : {
                    groupID : data.groupID
                }
            }]
        })
        .select('students')
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(_Errors.notFoundError);
            return cb(null,group.students);
        });
};
module.exports.updateGroup = function (obj,cb) {
    var fixSchedule = function (group,cb) {
        if (_.isEmpty(obj.teachers.minus)) return cb();
        var res = [];
        _.each(obj.teachers.minus,function (teacherID) {
            res.push(function (callback) {
                _Schedule.delLessonsByQuery({
                    groupID : obj.groupID,
                    teacherID : teacherID
                },callback);
            });
        });
        async.parallel(res,cb);
    };
    var fixTeachers = function (group) {
        _.each(obj.teachers.minus,function (removeTeacherID) {
            var index = _.map(group.teachers,function (teacherID) {
                return teacherID.toString();
            }).indexOf(removeTeacherID.toString());
            if (index > -1) {
                group.teachers.splice(index,1);
            }

        });

        group.teachers = group.teachers.concat(obj.teachers.plus);
    };
    Group
        .findOne({
            _id : obj.groupID,
            centerID : obj.centerID
        })
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(_Errors.notFoundError);
            group.name = obj.name;
            group.color = obj.color;
            group.price = obj.price;
            fixSchedule(group,function (err) {
                if (err) return cb(err);
                // group.teacherID = obj.teacherID;
                fixTeachers(group);
                group.save(cb);
            });
        });
};
module.exports.deleteStudentFromGroup = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(_Errors.notFoundError);
            var index = student.groups.map(function (groupID) {
                return groupID.toString();
            }).indexOf(data.groupID);
            if (index < 0) return cb(_Errors.badRequestError);
            student.groups.splice(index,1);
            if (student.groups.length == 0) student.status = Constants.STUDENT_STATUS_ARCHIVE;
            _Helper.refreshGroupHistory({
                studentID : student._id
            },function (err) {
                if (err) return cb(err);
                student.save(function (err) {
                    if (err) return cb(err);
                    _Helper.deleteObjectFromArray({
                        modelName : 'Group',
                        modelID : data.groupID,
                        objectID : data.studentID,
                        objectPath : 'students',
                        centerID : student.centerID
                    },cb);
                });
            });
        });
};
module.exports.getGroupByID = function (data,cb) {
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID
        })
        .populate([{
            path : 'colorID'
        },{
            path : 'students',
            populate : [{
                path : 'payments',
                match : {
                    groupID : data.groupID
                }
            },{
                path : 'appearance',
                match : {
                    groupID : data.groupID
                }
            }]
        },{
            path : 'schedule',
            populate : {
                path : 'classroomID'
            }
        },{
            path : 'teachers',
            select : 'firstName lastName imageUrl gender'
        },{
            path : 'attendance'
        },{
            path : 'applications',
            populate : [{
                path : 'answerID'
            },{
                path : 'teacherID',
                select : 'firstName lastName imageUrl'
            }],
            options : {
                sort : {
                    createdAt : -1
                }
            }
        },{
            path : 'interviewResultID',
            populate : {
                path : 'interviewID'
            }
        }])
        .exec(function (err,group) {
            if (err) {
                cb(err);
            } else if (!group) {
                cb(_Errors.notFoundError);
            } else {
                cb(null,group);
            }
        });
};
module.exports.deleteGroupByID = function (data,cb) {
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID
        })
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(_Errors.notFoundError);
            exports.deleteSourceUrlForGroup(group, function (err) {
                if(err) return cb(err);
                exports.deleteStudentsFromGroup({
                    groupID : data.groupID,
                    centerID : data.centerID
                }, function (err) {
                    if(err) return cb(err);
                    _Schedule.delLessonsByQuery({
                        centerID : data.centerID,
                        groupID : data.groupID
                    },function (err) {
                        if(err) return cb(err);
                        _Application.deleteApplicationsByGroupID(data,function (err) {
                            if (err) return cb(err);
                            return group.remove(cb);
                        });
                    });
                });
            });
        });
};
module.exports.deleteStudentsFromGroup = function (data,cb) {
    Group
        .findOne({
            centerID : data.centerID,
            _id : data.groupID
        })
        .select('students')
        .populate({
            path : 'students',
            select : 'groups',
            populate : {
                path : 'groups',
                select : '_id'
            }
        })
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(_Errors.objNotFound);
            if (_.isEmpty(group.students)) return cb();
            var result = [];
            _.each(group.students,function (student,index) {
                result[index] = function (callback) {
                    if (student.groups.length == 1 && student.groups[0]._id.toString() == data.groupID.toString()) {
                        student.groups = [];
                        student.status = Constants.STUDENT_STATUS_ARCHIVE;
                        student.save(function (err) {
                            _Helper.refreshGroupHistory({
                                studentID : student._id
                            },callback);
                        });
                        return;
                    }
                    var index = _.map(student.groups,function (group) {
                        return group._id.toString();
                    }).indexOf(data.groupID.toString());
                    if (index < 0) return callback();
                    student.groups.splice(index,1);
                    student.save(function (err) {
                        if (err) return callback(err);
                        _Helper.refreshGroupHistory({
                            studentID : student._id
                        },callback);
                    });
                };
            });
            async.parallel(result,cb);
        });
};
var GetGroupsPopulateQuery = [{
    path : 'teachers',
    select : 'firstName lastName gender'
},{
    path : 'students',
    select : '_id'
}];

module.exports.getGroups = function (data,cb) {
    var Query = {
        centerID : data.centerID
    };
    if (!_.isUndefined(data.isBloc)) data.isBloc = JSON.parse(data.isBloc);
    if (_.isBoolean(data.isBloc))  Query.isBloc = data.isBloc;
    if (!_.isUndefined(data.blocID)) Query.blocID = data.blocID;
    Group
        .find(Query)
        .select('-files -centerID -tests -sourceUrl -__v -channel -applications -interview -interviewResultID -notes -price -schedule -homeworks')
        .populate(GetGroupsPopulateQuery)
        .exec(cb);
};
module.exports.isPossibleCreateStudentForGroup = function (data,cb) {
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID
        })
        .populate({
            path : 'students',
            select : '_id'
        })
        .select('students individual')
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(_Errors.notFoundError);
            group.individual ? cb(null,group.students.length == 0) : cb(null,true);
        });
};
module.exports.initGroup = function (self,cb) {
    exports.createSourceUrlForGroup(self,cb);
};
module.exports.createSourceUrlForGroup = function (group,cb) {
    var self = this;
    Center
        .findOne({
            _id : group.centerID
        })
        .exec(function (err,center) {
            if (err) return cb(err);
            if (!center) return cb(_Errors.notFoundError);
            var randomName = crypto.randomBytes(10).toString('hex'),
                relPath = path.normalize(path.join(center.sourceUrl,crypto.randomBytes(10).toString('hex'))),
                absPath = path.normalize(path.join(__publicPath,relPath));
            fs.mkdir(absPath, function (err) {
                if (err) return cb(err);
                group.sourceUrl = relPath;
                return cb();
            });
        });
};

module.exports.addBloc = function (data,cb) {
    var bloc = new Bloc({
        name : data.name,
        centerID : data.centerID,
        groups : data.groups,
        color : data.color
    });
    if (_.isEmpty(data.groups)) return bloc.save(cb);
    var res = [];
    _.each(data.groups,function (groupID,index) {
        res[index] = function (callback) {
            Group
                .findOne({
                    _id : groupID,
                    centerID : data.centerID
                })
                .select('isBloc blocID')
                .exec(function (err,group) {
                    if (err) return cb(err);
                    if (!group) return cb(_Errors.badRequest);
                    if (group.isBloc) return cb(_Errors.badRequest);
                    group.isBloc = true;
                    group.blocID = bloc._id;

                    group.save(callback);
                });
        };
    });
    async.parallel(res,function (err) {
        if (err) return cb(err);
        bloc.save(cb);
    });
};
module.exports.getBlocs = function (data,cb) {
    Bloc
        .find({
            centerID : data.centerID
        })
        .populate({
            path : 'groups',
            select : 'name color teacherID students memory blocID isBloc',
            populate : [{
                path : 'teacherID',
                select : 'firstName lastName'
            },{
                path : 'students',
                select : '_id'
            }]

        })
        .exec(cb);
};
module.exports.getBlocByID = function (data,cb) {
    Bloc
        .findOne({
            _id : data.blocID,
            centerID : data.centerID
        })

        .populate({
            path : 'groups',
            populate : GetGroupsPopulateQuery
        })
        .exec(function (err,bloc) {
            if (err) return cb(err);
            
            if (!bloc) return cb(_Errors.objNotFound);
            cb(null,bloc);
        })
};
module.exports.assignGroupByBlocID = function (data,cb) {
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID
        })
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(_Errors.objNotFound);
            if (group.isBloc) return cb(_Errors.badRequest);
            Bloc
                .findOne({
                    _id : data.blocID,
                    centerID : data.centerID
                })
                .exec(function (err,bloc) {
                    if (err) return cb(err);
                    if (!bloc) return cb(_Errors.objNotFound);
                    bloc.groups.push(group._id);
                    bloc.save(function (err) {
                        if (err) return cb(err);
                        group.isBloc = true;
                        group.blocID = bloc._id;
                        group.save(cb);
                    });
                });
        });
};

module.exports.delGroupFromBloc = function (data,cb) {
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID,
            isBloc : true,
            blocID : data.blocID
        })
        .select('blocID isBloc')
        .populate({
            path : 'blocID'
        })
        .exec(function (err,group) {
            console.log(group);
            if (err) return cb(err);
            if (!group) return cb(_Errors.objNotFound);
            if (!group.isBloc || !group.blocID) return cb(_Errors.badRequest);
            var index = _.map(group.blocID.groups,function (groupID) {
                return groupID.toString();
            }).indexOf(data.groupID);
            if (index < 0) return cb(_Errors.badRequest);
            group.blocID.groups.splice(index,1);
            group.blocID.save(function (err) {
                if (err) return cb(err);
                group.isBloc =false;
                group.blocID = undefined;
                group.save(cb);
            });
        });
};

module.exports.editBlocByID = function (data,cb) {
    Bloc
        .findOne({
            _id : data.blocID,
            centerID : data.centerID
        })
        .exec(function (err,bloc) {
            if (err) return cb(err);
            if (!bloc) return cb(_Errors.objNotFound);
            bloc.name = data.name;
            bloc.color = data.color;
            if (_.isEmpty(bloc.groups) || _.isEmpty(data.groups)) return bloc.save(cb);
            var results = [];
            _.each(data.groups,function (DataGroup,index) {
                results[index] = function (callback) {
                    Group
                        .findOne({
                            _id : DataGroup.groupID,
                            centerID : data.centerID
                        })
                        .exec(function (err,group) {
                            if (err) return callback(err);
                            if (!group) return callback(_Errors.objNotFound);

                            var index = _.map(bloc.groups,function (group) {
                                return group.toString();
                            }).indexOf(DataGroup.groupID);
                            if (index < 0) return callback(_Errors.badRequest);
                            group.isBloc = false;
                            group.blocID = undefined;
                            bloc.groups.splice(index,1);
                            group.save(callback);
                        });
                };
            });
            async.parallel(results,function (err) {
                if (err) return cb(err);
                bloc.save(cb);
            });
        });
};
module.exports.delBlocByID = function (data,cb) {

    Bloc
        .findOne({
            _id : data.blocID,
            centerID : data.centerID
        })
        .exec(function (err,bloc) {
            if (err) return cb(err);
            if (!bloc) return cb(_Errors.objNotFound);
            Group
                .find({
                    blocID : data.blocID
                })
                .select('isBloc blocID')
                .exec(function (err,groups) {
                    if (err) return cb(err);
                    if (_.isEmpty(groups)) return bloc.remove(cb);
                    var results = [];
                    _.each(groups,function (group,index) {
                        results[index] = function (callback) {
                            group.blocID = undefined;
                            group.isBloc = false;
                            group.save(callback);
                        };
                    });
                    async.parallel(results,function (err) {
                        if (err) return cb(err);
                        bloc.remove(cb);
                    });
                });
        });
};
module.exports.getGroupTeachers = function (data,cb) {
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID
        })
        .select('teachers name')
        .populate({
            path : 'teachers',
            select : 'firstName lastName'
        })
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(_Errors.notFound);
            cb(null,group);
        });
};
module.exports.getAvailableTeachersByGroupID = function (data,cb) {
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID
        })
        .select('teachers')
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(_Errors.notFound);
            Teacher
                .find({
                    centerID : data.centerID,
                    _id : {
                        $nin : group.teachers
                    }
                })
                .select('firstName lastName gender imageUrl')
                .exec(cb);
        });

};

var Group = require(__modelsPath + 'group'),
    Center = require(__modelsPath + 'center'),
    Teacher = require(__modelsPath + 'teacher'),
    Student = require(__modelsPath + 'student'),
    Bloc = require(__modelsPath + 'group-bloc'),
    _ = require('underscore'),
    _Schedule = require('admin/schedule'),
    _Helper = require('system/helper'),
    _Application = require('admin/application'),
    Constants = require('system/constants'),
    LoggerModule = require('system/logger'),
    _Errors = require('system/errors'),
    fs = require('fs'),
    fse = require('fs-extra'),
    path = require('path'),
    async = require('async'),
    crypto = require('crypto');




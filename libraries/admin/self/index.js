module.exports.info = function (data,cb) {
    Admin
        .findOne({
            _id : data.adminID,
            centerID : data.centerID
        })
        .populate([{
            path : "centerID"
        },{
            path : 'clientID',
            populate : [{
                path : 'tariffID'
            },{
                path : 'centers',
                select : 'name _id'
            }]
        }])
        .exec(function (err,admin) {
            if (err) return cb(err);
            if (!admin) return cb(Errors.notFoundError);
            ListModule.getListOfGroups(data,function (err,groups) {
                if (err) return cb(err);
                SubjectModule.getSubjects(data,function (err,subjects) {
                    if (err) return next(err);
                    return cb(null,{
                        admin : admin,
                        groups : groups,
                        subjects : subjects
                    });
                });
            });
        });
};
module.exports.updateCenterID = function (data,cb) {
    Admin
        .findOne({
            _id : data.adminID,
            clientID : data.clientID
        })
        .select('clientID centerID')
        .populate('clientID')
        .exec(function (err,admin) {
            if (err) return cb(err);
            if (!admin) return cb(Errors.objNotFound);
            var index = _.map(admin.clientID.centers,function (center) {
                return center.toString();
            }).indexOf(data.centerID.toString());
            if (index < 0) return cb(Errors.badRequest);
            admin.centerID = data.centerID;
            admin.save(cb);
        });
};
module.exports.updateProfile = function (data,cb) {
    Admin
        .findOne({
            centerID : data.centerID,
            _id : data.adminID
        })
        .select('email centerID')
        .populate({
            path : 'centerID'
        })
        .exec(function (err,admin) {
            if (err) return cb(err);
            if (!admin) return cb(Errors.notFoundError);
            admin.email = data.email;
            admin.save(function (err) {
                if (err) return cb(err);
                admin.centerID.name = data.name;
                return admin.centerID.save(cb);
            });
        });
};
module.exports.updatePassword = function (data,cb) {
    Admin
        .findOne({
            _id : data.adminID,
            centerID : data.centerID
        })
        .select('hash salt')
        .exec(function (err,admin) {
            if (err) return cb(err);
            if (!admin) return cb(Errors.notFoundError);
            data.user = admin;
            UserModule.updatePass(data,cb);
        });
};
module.exports.deleteImage = function (data,cb) {
    Admin
        .findOne({
            _id : data.adminID,
            centerID : data.centerID
        })
        .select('centerID')
        .populate({
            path : 'centerID'
        })
        .exec(function (err,admin) {
            if (err) return cb(err);
            if (!admin) return cb(Errors.notFoundError);
            if (admin.centerID.imageUrl == undefined) return cb(null);
            var pathToFile = path.normalize(path.join(__publicPath,admin.centerID.imageUrl));
            fs.stat(pathToFile,function (err,stats) {
                if (err) return cb(null);
                if (stats.isFile() == false) return cb(null);
                fse.remove(pathToFile,cb);
            });
        });
};
module.exports.updateImage = function (data,cb) {
    var image = data.image;
    exports.deleteImage(data,function (err) {
        if (err) return cb(err);
        Center
            .findOne({
                _id : data.centerID
            })
            .exec(function (err,center) {
                if (err) return cb(err);
                if (!center) return cb(Errors.notFoundError);
                var ext = image.split(';')[0].match(/jpeg|png|gif/)[0];
                var imageData = image.replace(/^data:image\/\w+;base64,/,'');
                var fileName = crypto.randomBytes(10).toString('hex') + '.' + ext;
                var publicImagePath = center.sourceUrl + fileName;
                var privateImagePath = center.getSourceUrl() + fileName;
                fs.writeFile(privateImagePath,imageData,{
                    encoding : 'base64'
                },function (err) {
                    if (err) return cb(err);
                    center.imageUrl = publicImagePath;
                    center.save(cb);
                });
            });
    });
};

var Admin = require(__modelsPath + 'admin'),
    Center = require(__modelsPath + 'center'),
    Group = require(__modelsPath + 'group'),


    Errors = require('system/errors'),
    UserModule = require('system/user'),
    ListModule = require('admin/list'),
    SubjectModule = require('admin/subject'),
    _ = require('underscore'),
    fs = require('fs'),
    path = require('path'),
    crypto = require('crypto'),
    fse = require('fs-extra');

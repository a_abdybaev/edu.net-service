module.exports.auth = function (data,cb) {
    Admin
        .findOne({
            'token.value' : data.token
        })
        .exec(function (err,admin) {
            if (err) return cb(err);
            if (!admin) return cb(ErrorsModule.invalidTokenError);
            UserModule.updateVisit({
                user : admin
            },function (err) {
                cb(err,admin)
            });
        });
};
module.exports.login = function (data,cb) {
    Admin
        .findOne({
            login : data.login
        })
        .select('hash salt clientID centerID')
        .populate('centerID')
        .exec(function (err,admin) {
            if (err) return cb(err);
            if (!admin) return cb(ErrorsModule.notFoundError);
            if (!admin.centerID) return admin.remove(function (err) {
                if (err) return (err);
                cb(ErrorsModule.objNotFound);
            });

            data.user = admin;
            if (!UserModule.isValidPass(data)) return cb(ErrorsModule.notFoundError);
            ClientModule.isPaid(admin,function (err,isPaid) {
                if (err) return cb(err);
                if (!isPaid) return cb(ErrorsModule.paymentRequiredError);
                UserModule.generateToken({
                    user : admin
                },cb);
            });
        });
};

var ErrorsModule = require('system/errors'),
    Admin = require(__modelsPath + 'admin'),
    Client = require(__modelsPath + 'client'),
    _ = require('underscore'),
    UserModule = require('system/user'),
    ClientModule = require('system/client');


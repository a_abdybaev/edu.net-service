module.exports.addClassroom = function (data,cb) {
    var classroom = new Classroom({
        name : data.name,
        centerID : data.centerID
    });
    classroom.save(cb);
};
module.exports.getClassrooms = function (data,cb) {
    Classroom
        .find({
            centerID : data.centerID
        })
        .exec(cb);
};
module.exports.updateClassroomByID = function (data,cb) {
    Classroom
        .findOne({
            _id : data.classroomID,
            centerID : data.centerID
        })
        .exec(function (err,classroom) {
            if (err) return cb(err);
            if (!classroom) return cb(ErrorsModule.objNotFound);
            classroom.name = data.name;
            classroom.save(cb);
        });
};
module.exports.getClassroomByID = function (data,cb) {
    Classroom
        .findOne({
            _id : data.classroomID,
            centerID : data.centerID
        })
        .exec(function (err,classroom) {
            if (err) return cb(err);
            if (!classroom) return cb(ErrorsModule.objNotFound);
            cb(null,classroom);
        });
};
module.exports.deleteClassroomByID = function (data,cb) {
    Classroom
        .findOne({
            _id : data.classroomID,
            centerID : data.centerID
        })
        .exec(function (err,classroom) {
            if (err) return cb(err);
            if (!classroom) return cb(ErrorsModule.objNotFound);
            LessonModule.deleteLessonsByClassroomID(data, function (err) {
                if (err) return cb(err);
                classroom.remove(cb);
            });
        });
};

var Classroom = require(__modelsPath + 'classroom'),
    LessonModule = require('admin/lesson'),
    ErrorsModule = require('system/errors');



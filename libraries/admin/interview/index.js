module.exports.startInterview = function (data,cb) {
    Group
        .findOne({
            centerID : data.centerID,
            _id : data.groupID
        })
        .select('interview name')
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.notFoundError);
            if (group.interviewResultID) return cb(ErrorsModule.notAcceptableError);
            Interview
                .findOne({
                    isDefault : true
                })
                .exec(function (err,interview) {
                    if (err) return cb(err);
                    if (!interview) return cb(ErrorsModule.notAcceptableError);
                    var interviewResult = new InterviewResult({
                        centerID : data.centerID,
                        groupID : data.groupID,
                        group : {
                            name : group.name
                        },
                        interviewID : interview._id,
                        students : [],
                        questions : []
                    });
                    _.each(interview.questions,function (question,i) {
                        if (question.answersType == ConstantsModule.INTERVIEW_ANSWERS_OUT_OF_TWO) {
                            interviewResult.questions[i] = {
                                analytics : [0,0]
                            };
                        } else if (question.answersType == ConstantsModule.INTERVIEW_ANSWERS_OUT_OF_FIVE) {
                            interviewResult.questions[i] = {
                                analytics : [0,0,0,0,0]
                            };
                        } else {
                            return cb({
                                error : "NOT READY FUNC"
                            });
                        }
                    });
                    interviewResult.save(function (err) {
                        if (err) return cb(err);
                        group.interviewResultID = interviewResult._id;
                        group.save(cb);
                    });
                });

        });
};

var ErrorsModule = require('system/errors'),
    InterviewResult = require(__modelsPath + 'interview-result'),
    Interview = require(__modelsPath + 'interview'),

    Group = require(__modelsPath + 'group'),
    ConstantsModule = require('system/constants'),
    _ = require('underscore');




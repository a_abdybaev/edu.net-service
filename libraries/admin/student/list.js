module.exports.getStudentByID = function (data,cb) {
    Student
        .findOne({
            centerID : data.centerID,
            _id : data.studentID
        })
        .populate([{
            path : 'groups',
            select : 'name price schedule createdAt teacherID',
            populate : [{
                path : 'schedule',
                populate : {
                    path : 'classroomID',
                    select : 'name'
                }
            },{
                path : 'teacherID',
                select : 'firstName lastName imageUrl'
            }]
        },{
            path : 'appearance'
        },{
            path : 'payments'
        },{
            path : 'subjectID'
        }])
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            cb(null,student);
        });
};

var Student = require(__modelsPath + 'student'),
    ErrorsModule = require('system/errors');
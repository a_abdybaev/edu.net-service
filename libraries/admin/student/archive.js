module.exports.getStudents = function (data,cb) {
    var query = {
        centerID : data.centerID,
        status : ConstantsModule.STUDENT_STATUS_ARCHIVE
    };
    if (!_.isEmpty(data.search)) {
        query.$or = [];
        _.each(data.search.split(' '),function (word) {
            query.$or.push({
                firstName : {
                    $regex : new RegExp(word.toLowerCase(),"i")
                }
            },{
                lastName : {
                    $regex : new RegExp(word.toLowerCase(),"i")
                }
            });
        });
    }
    Student
        .find(query)
        .skip((parseInt(data.page) - 1) * ConstantsModule.archiveStudentsPerPage)
        .limit(ConstantsModule.archiveStudentsPerPage)
        .exec(cb);
};
module.exports.deleteStudentByID = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            return student.remove(function (err) {
                if (err) return cb(err);
                return CenterModule.updateStudents.archive(data,cb);
            });
        });
};
module.exports.getStudentByID = function (data,cb) {
    Student
        .findOne({
            centerID : data.centerID,
            _id : data.studentID,
            status : ConstantsModule.STUDENT_STATUS_ARCHIVE
        })
        .select('firstName lastName email phone')
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            return cb(null,student);
        });
};
module.exports.updateStudentByID = function (data,cb) {
    Student
        .findOne({
            centerID : data.centerID,
            _id : data.studentID,
            status : ConstantsModule.STUDENT_STATUS_ARCHIVE
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            student.lastName = data.lastName;
            student.firstName = data.firstName;
            student.email = data.email;
            student.updatedAt = new Date();
            return student.save(cb);
        });
};
module.exports.getAvailableGroupsToAssignStudent = function (data,cb) {
    Group
        .find({
            centerID : data.centerID,
            $or : [{
                individual : true,
                students : {
                    $size : 0
                }
            },{
                individual : false
            }]
        })
        .select('name')
        .exec(cb);
};
module.exports.assignStudentForGroup = function (data,cb) {
    Student
        .findOne({
            centerID : data.centerID,
            _id : data.studentID
        })
        .select('groups updatedAt status')
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            var index = _.map(student.groups,function (groupID) {
                return groupID.toString();
            }).indexOf(data.groupID);
            if (index > -1) return cb(ErrorsModule.sendNotAcceptable("Student has given group"));
            student.groups.push(data.groupID);
            student.status = ConstantsModule.STUDENT_STATUS_ACTIVE;
            student.updatedAt = new Date();
            student.save(function (err) {
                if (err) return cb(err);
                console.log(student);
                HelperModule.addObjectInArray({
                    modelName : ConstantsModule.MODELS.GROUP,
                    modelID : data.groupID,
                    objectID : student._id,
                    objectPath : 'students',
                    centerID : data.centerID
                },function (err) {
                    if (err) return cb(err);
                    return CenterModule.updateStudents.active(data,function (err) {
                        if (err) return cb(err);
                        return CenterModule.updateStudents.archive(data,cb);
                    });
                });
            });
        });
};
module.exports.restoreStudentByID = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            status : ConstantsModule.STUDENT_STATUS_ARCHIVE,
            centerID : data.centerID
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            if (!_.isEmpty(data.groupType)) {
                student.groupType = data.groupType;
            }
            if (!_.isEmpty(data.ageType)) {
                student.ageType = data.ageType;
            }
            if (data.subjectID && data.levelID) {
                student.subjectID = data.subjectID;
                student.levelID = data.levelID;
            }
            student.studyDays = data.studyDays;
            student.studyTimes = data.studyTimes;
            student.updatedAt = new Date();
            student.status = ConstantsModule.STUDENT_STATUS_WAITING;
            return student.save(function (err,std) {
                if (err) return cb(err);
                CenterModule.updateStudents.archive(data,function (err) {
                    if (err) return cb(err);
                    return CenterModule.updateStudents.waiting(data,cb);
                });
            });
        });
};
var ConstantsModule = require('system/constants'),
    HelperModule = require('system/helper'),
    ErrorsModule = require('system/errors'),
    Student = require(__modelsPath + 'student'),
    Group = require(__modelsPath + 'group'),
    async = require('async'),
    _ = require('underscore'),
    CenterModule = require('system/center');
module.exports.addStudent = function (data,cb) {
    var student = new Student({
        firstName : data.firstName,
        lastName : data.lastName,
        phone : data.phone,
        email : data.email,
        centerID : data.centerID,
        clientID : data.clientID,
        status : ConstantsModule.STUDENT_STATUS_WAITING,
        studyTimes : data.studyTimes ,
        studyDays : data.studyDays

        // FILTER CONFIGS START...
    });
    if (!_.isEmpty(data.groupType)) {
        student.groupType = data.groupType;
    }
    if (!_.isEmpty(data.ageType)) {
        student.ageType = data.ageType;
    }
    if (data.subjectID && data.levelID) {
        student.subjectID = data.subjectID;
        student.levelID = data.levelID;
    }

    // FILTER CONFIGS END...
    UserModule.initUser({
        user : student
    },function (err) {
        if (err) return cb(err);
        student.save(function (err) {
            if (err) return cb(err);
            return CenterModule.updateStudents.waiting(data,cb);
        });
    });
};
module.exports.getStudents = function (data,cb) {
    var searchQuery = {
        centerID : data.centerID,
        status : ConstantsModule.STUDENT_STATUS_WAITING
    };
    if (!_.isEmpty(data.subjectID) && !_.isEmpty(data.levelID)) {
        searchQuery.subjectID = data.subjectID;
        searchQuery.levelID = data.levelID;
    }
    if (!_.isEmpty(data.ageType)) {
        searchQuery.ageType = data.ageType;
    }
    if (!_.isEmpty(data.groupType)) {
        searchQuery.groupType = data.groupType;
    }
    if (!_.isEmpty(data.studyTimes)) {
        searchQuery.studyTimes = {
            $all : data.studyTimes
        };
    }
    if (!_.isEmpty(data.studyDays)) {
        searchQuery.studyDays = {
            $all : data.studyDays
        };
    }
    if (!_.isEmpty(data.search)) {
        searchQuery.$or = [];
        _.each(data.search.split(' '),function (word) {
            searchQuery.$or.push({
                firstName : {
                    $regex : new RegExp(word.toLowerCase(),"i")
                }
            },{
                lastName : {
                    $regex : new RegExp(word.toLowerCase(),"i")
                }
            });
        });
    }
    Student
        .find(searchQuery)
        .sort({
            createdAt : -1
        })
        .skip((parseInt(data.page) - 1) * ConstantsModule.waitingStudentsPerPage)
        .limit(ConstantsModule.waitingStudentsPerPage)
        .select('-groups -appearance -centerID -groupsHistory -login -payments -__v')
        .populate([{
            path : 'subjectID',
            populate : {
                path : 'levels',
                match : {
                    _id : 'this.levelID'
                }
            }
        }])
        .exec(cb);
};
module.exports.assignStudents = function (data,cb) {
    Student
        .find({
            _id : {
                $in : data.students
            },
            centerID:  data.centerID
        })
        .select('status groups')
        .exec(function (err,students) {
            if (err) return cb(err);
            if (_.isEmpty(students)) return cb(ErrorsModule.notFoundError);
            TariffModule.isPossibleCreateStudentsByTariff({
                centerID : data.centerID,
                count : students.length
            },function (err,result) {
                if (err) return cb(err);
                if (result == false) return cb(ErrorsModule.tariffError);
                Group
                    .findOne({
                        centerID : data.centerID,
                        _id : data.groupID
                    })
                    .select('students individual')
                    .exec(function (err,group) {
                        if (err) return cb(err);
                        if (_.isEmpty(group)) return cb(ErrorsModule.notFoundError);
                        if (group.individual && group.students.length + students.length >1) return cb(ErrorsModule.badRequestError);
                        var results = [];
                        _.each(students,function (student,index) {
                            results[index] = function (callback) {
                                student.status = ConstantsModule.STUDENT_STATUS_ACTIVE;
                                student.updatedAt = new Date();
                                student.groups.push(data.groupID);
                                return student.save(function (err,student) {
                                    return callback(err,student._id);
                                });
                            };
                        });
                        async.series(results,function (err,studentsID) {
                            if (err) return cb(err);
                            group.students = group.students.concat(studentsID);
                            group.save(function (err) {
                                if (err) return cb(err);
                                CenterModule.updateStudents.active(data,function (err) {
                                    if (err) return cb(err);
                                    return CenterModule.updateStudents.waiting(data,cb);
                                });
                            });
                        });
                    });
            });

        });
};
module.exports.getAvailableGroupToAssignStudents = function (data,cb) {
    var query = {
        centerID : data.centerID
    };
    if (data.count > 1) {
        query.individual = false;
    } else {
        query['$or'] = [{
            individual : true,
            students : {
                $size : 0
            }
        },{
            individual : false
        }];
    }

    Group
        .find(query)
        .select('name')
        .exec(cb);
};
module.exports.formGroup = function (data,cb) {
    TariffModule.isPossibleCreateStudentsByTariff({
        centerID : data.centerID,
        count : data.students.length
    },function (err,result) {
        if (err) return cb(err);
        if (result == false) return cb(ErrorsModule.tariffError);
        GroupModule.addGroup(data,function (err,group) {
            if (err) return cb(err);
            Student
                .find({
                    _id : {
                        $in : data.students
                    },
                    centerID : data.centerID,
                    status : ConstantsModule.STUDENT_STATUS_WAITING
                })
                .select('status groups')
                .exec(function (err,students) {
                    if (err) return cb(err);
                    if (_.isEmpty(students)) return cb(ErrorsModule.notFoundError);
                    var results = [];
                    _.each(students,function (student,index) {
                        results[index] = function (callback) {
                            student.status = ConstantsModule.STUDENT_STATUS_ACTIVE;
                            student.groups = [group._id];
                            student.updatedAt = new Date();
                            student.save(function (err,student) {
                                return callback(err,student._id);
                            });
                        };
                    });
                    async.series(results,function (err,result) {
                        if (err) return cb(err);
                        group.students = result;
                        return group.save(function (err) {
                            if (err) return cb(err);
                            return CenterModule.updateStudents.active(data,function (err) {
                                if (err) return cb(err);
                                return CenterModule.updateStudents.waiting(data,cb);
                            });
                        });
                    });
                });
        });

    });
};
module.exports.updateStudentByID = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID,
            status : ConstantsModule.STUDENT_STATUS_WAITING
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            student.firstName = data.firstName;
            student.lastName = data.lastName;
            student.email = data.email;
            student.studyTimes = data.studyTimes;
            student.studyDays = data.studyDays;
            student.ageType =  data.ageType || undefined;
            student.updatedAt = new Date();
            if (!_.isEmpty(data.subjectID) && !_.isEmpty(data.levelID)) {
                student.subjectID = data.subjectID;
                student.levelID = data.levelID;
            } else {
                student.subjectID = undefined;
                student.levelID = undefined;
            }
            student.groupType = data.groupType || undefined;
            return student.save(cb);
        })
};
module.exports.getStudentByID = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,

            centerID : data.centerID,
            status : ConstantsModule.STUDENT_STATUS_WAITING
        })
        .select('-appearance -centerID -createdAt -groups -groupsHistory -login -payments')
        .populate({
            path : 'subjectID',
            populate : {
                path : 'levels',
                match : {
                    _id : 'this.levelID'
                }
            }
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            return cb(null,student);
        });
};
module.exports.archiveStudentByID = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            student.status = ConstantsModule.STUDENT_STATUS_ARCHIVE;
            student.updatedAt = new Date();
            return student.save(function (err) {
                if (err) return cb(err);
                CenterModule.updateStudents.archive(data,function (err) {
                    if (err) return cb(err);
                    CenterModule.updateStudents.waiting(data,cb);
                });
            });
        });
};
var TariffModule = require('admin/tariff'),
    UserModule = require('system/user'),
    ErrorsModule = require('system/errors'),
    ConstantsModule = require('system/constants'),
    CenterModule = require('system/center'),
    GroupModule = require('admin/group'),
    Student = require(__modelsPath + 'student'),
    Group = require(__modelsPath + 'group'),
    _ = require('underscore'),
    async = require('async');


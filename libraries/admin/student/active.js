module.exports.addStudent = function (data,cb) {
    TariffModule.isPossibleCreateStudentByTariff(data,function (err,result) {
        if (err) return cb(err);
        if (result == false) return cb(ErrorsModule.tariffError);
        GroupModule.isPossibleCreateStudentForGroup(data,function (err,result) {
            if (err) return cb(err);
            if (!result) return cb(ErrorsModule.badRequestError);
            var student = new Student({
                firstName : data.firstName,
                lastName : data.lastName,
                phone : data.phone,
                email : data.email,
                groups : [data.groupID],
                centerID : data.centerID,
                clientID : data.clientID,
                status : Constants.STUDENT_STATUS_ACTIVE
            });
            UserModule.initUser({
                user : student
            },function (err) {
                if (err) return cb(err);
                student.save(function (err) {
                    if (err) return cb(err);
                    Helper.addObjectInArray({
                        modelName : Constants.MODELS.GROUP,
                        modelID : data.groupID,
                        objectID : student._id,
                        objectPath : 'students'

                    },function (err) {
                        if (err) return cb(err);
                        CenterModule.updateStudents.active(data,cb);
                    });
                })
            });
        });
    });
};
module.exports.getStudentByID = function (data,cb) {
    Student
        .findOne({
            centerID : data.centerID,
            _id : data.studentID,
            status : Constants.STUDENT_STATUS_ACTIVE
        })
        .select('firstName lastName email phone')
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            return cb(null,student);
        });
};
module.exports.getStudents = function (data,cb) {
    var query = {
        centerID : data.centerID,
        status : Constants.STUDENT_STATUS_ACTIVE
    };

    if (!_.isEmpty(data.search)) {
        query.$or = [];
        _.each(data.search.split(' '),function (word) {
            query.$or.push({
                firstName : {
                    $regex : new RegExp(word.toLowerCase(),"i")
                }
            },{
                lastName : {
                    $regex : new RegExp(word.toLowerCase(),"i")
                }
            });
        });
    }
    Student
        .find(query)
        .populate({
            path : 'groups',
            select : 'name color'
        })
        .skip((parseInt(data.page) - 1) * Constants.activeStudentsPerPage)
        .limit(Constants.activeStudentsPerPage)
        .exec(cb);
};
module.exports.updateStudentByID = function (data,cb) {
    Student
        .findOne({
            centerID : data.centerID,
            _id : data.studentID,
            status : Constants.STUDENT_STATUS_ACTIVE
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            student.lastName = data.lastName;
            student.firstName = data.firstName;
            student.email = data.email;
            student.updatedAt = new Date();
            return student.save(cb);
        });
};
module.exports.assignStudentForGroup = function (data,cb) {
    exports.isPossibleAssignStudentForGroup(data,function (err) {
        if (err) return cb(err);
        Student
            .findOne({
                centerID : data.centerID,
                _id : data.studentID,
                status : Constants.STUDENT_STATUS_ACTIVE
            })
            .select('groups')
            .exec(function (err,student) {
                if (err) return cb(err);
                if (!student) return cb(ErrorsModule.notFoundError);
                var index = student.groups.map(function (groupID) {
                    return groupID.toString();
                }).indexOf(data.groupID);
                if (index > -1) return cb(ErrorsModule.badRequestError);
                student.groups.push(data.groupID);
                student.updatedAt = new Date();
                student.save(function (err) {
                    if (err) return cb(err);
                    Helper.addObjectInArray({
                        modelID : data.groupID,
                        modelName : Constants.MODELS.GROUP,
                        objectID : student._id,
                        objectPath : 'students'
                    },cb);
                });
            });
    });

};
module.exports.isPossibleAssignStudentForGroup = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .select('groups')
        .populate('groups')
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            if (student.groups.length >=5) return cb(ErrorsModule.conflictError);
            cb();
        });
};
module.exports.archiveStudentByID = function (data,cb) {
    Student
        .findOne({
            _id : data.studentID,
            centerID : data.centerID
        })
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            Helper.refreshGroupHistory({
                studentID : student._id
            },function (err) {
                if (err) return cb(err);
                var results = [];
                student.groups.forEach(function (groupID,i) {
                    results[i] = function (callback) {
                        Helper.deleteObjectFromArray({
                            modelName : 'Group',
                            modelID : groupID,
                            objectID : student._id,
                            objectPath : 'students',
                            centerID : student.centerID
                        },callback);
                    };
                });
                async.series(results,function (err) {
                    if (err) return cb(err);
                    student.status = Constants.STUDENT_STATUS_ARCHIVE;
                    student.groups = [];
                    student.updatedAt = new Date();
                    return student.save(function (err) {
                        if (err) return cb(err);
                        CenterModule.updateStudents.archive(data,function (err) {
                            if (err) return cb(err);
                            return CenterModule.updateStudents.active(data,cb);
                        });
                    });
                });
            });
        });
};
module.exports.getAvailableGroupsToAssignStudent = function (data,cb) {
    Student
        .findOne({
            centerID : data.centerID,
            _id : data.studentID
        })
        .select('groups')
        .exec(function (err,student) {
            if (err) return cb(err);
            if (!student) return cb(ErrorsModule.notFoundError);
            Group
                .find({
                    centerID : data.centerID,
                    _id : {
                        "$nin" : student.groups
                    },
                    $or : [{
                        individual : true,
                        students : {
                            $size : 0
                        }
                    },{
                        individual : false
                    }]
                })
                .select('name individual students')
                .exec(cb)
        });
};
var TariffModule = require('admin/tariff'),
    UserModule = require('system/user'),
    ErrorsModule = require('system/errors'),
    GroupModule = require('admin/group'),
    Constants = require('system/constants'),
    CenterModule = require('system/center'),
    Helper = require('system/helper'),
    Group = require(__modelsPath + 'group'),
    Student = require(__modelsPath + 'student'),
    async = require('async'),
    _ = require('underscore');


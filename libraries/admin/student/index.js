module.exports.archiveStudents = require('./archive');
module.exports.waitingStudents = require('./waiting');
module.exports.activeStudents = require('./active');
module.exports.listStudents = require('./list');
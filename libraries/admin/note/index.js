module.exports.updateNoteByID = function (data,cb) {
    Group
        .findOne({
            centerID : data.centerID,
            _id : data.groupID,
            'notes._id' : data.noteID
        })
        .select('notes')
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.objNotFound);
            var index = _.map(group.notes,function (note) {
                return note._id.toString();
            }).indexOf(data.noteID);
            if (index < -1) return cb(ErrorsModule.objNotFound);
            group.notes[index].value = data.content;
            group.save(cb);
        });

};
module.exports.addNoteByGroupID = function (data,cb) {
    Group
        .findOne({
            centerID : data.centerID,
            _id : data.groupID
        })
        .select('notes')
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.notFoundError);
            group.notes.push({
                value : data.content
            });
            group.save(cb);
        });
};
module.exports.getNoteByID = function (data,cb) {
    Group
        .findOne({
            centerID : data.centerID,
            _id : data.groupID,
            'notes._id' : data.noteID
        })
        .select('notes')
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.notFoundError);
            var index = group.notes.map(function (note) {
                return note._id.toString();
            }).indexOf(data.noteID.toString());
            if (index<-1) return cb(ErrorsModule.serverInternalError);
            cb(null,group.notes[index]);
        });
};
module.exports.deleteNoteByID = function (data,cb) {
    Group
        .findOne({
            id : data.groupID,
            centerID : data.centerID,
            'notes._id' : data.noteID
        })
        .select('notes')
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.notFoundError);
            var index = group.notes.map(function (note) {
                return note._id.toString();
            }).indexOf(data.noteID);
            if (index < -1) return cb(ErrorsModule.invalidBodyError);
            group.notes.splice(index,1);
            group.save(cb);
        });
};

module.exports.getNotesByGroupID = function (data,cb) {
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID
        })
        .select('notes')
        .exec(function (err,group) {
            if (err) return cb(err);
            if (!group) return cb(ErrorsModule.notFoundError);
            cb(null,group.notes);
        });
};


var Group = require(__modelsPath + 'group'),
    _ = require('underscore'),
    ErrorsModule = require('system/errors');
module.exports.getApplications = function (data,cb) {
    var query = {
        centerID : data.centerID
    };
    if (data.groupID != null) query.groupID = data.groupID;
    if (data.teacherID != null) query.teacherID = data.teacherID;
    Application
        .find(query)
        .sort({
            createdAt : -1
        })
        .populate([{
            path : 'answerID'
        },{
            path : 'groupID',
            select : 'name color'
        },{
            path : 'teacherID',
            select : 'firstName lastName imageUrl'
        }])
        .exec(cb);
};
module.exports.addAnswer = function (data,cb) {
    Application
        .findOne({
            centerID : data.centerID,
            _id : data.applicationID
        })
        .exec(function (err,application) {
            if (err) return cb(err);
            if (!application) return cb(ErrorsModule.notFoundError);
            var answer = new ApplicationAnswer({
                centerID:  data.centerID,
                applicationID : data.applicationID,
                content : data.content
            });
            answer.save(function (err) {
                if (err) return cb(err);
                application.answerID = answer._id;
                application.save(cb);
            });
        });
};
module.exports.deleteApplicationAnswer = function (data,cb) {
    Application
        .findOne({
            centerID : data.centerID,
            _id : data.applicationID
        })
        .exec(function (err,application) {
            if (err) return cb(err);
            if (!application) return cb(errors.notFoundError);
            ApplicationAnswer
                .findOne({
                    applicationID : data.applicationID,
                    centerID : data.centerID
                })
                .remove()
                .exec(function (err) {
                    if (err) return cb(err);
                    application.answerID = undefined;
                    application.save(cb);
                });
        });
};
module.exports.getApplicationByID = function (data,cb) {
    Application
        .findOne({
            centerID :  data.centerID,
            _id : data.applicationID
        })
        .populate([{
            path : 'answerID'
        },{
            path : 'groupID',
            select : 'name color'
        },{
            path : 'teacherID',
            select : 'firstName lastName'
        }])
        .exec(function (err,application) {
            if (err) return cb(err);
            if (!application) return cb(ErrorsModule.notFoundError);
            cb(null,application);
        });
};
module.exports.updateApplicationAnswerByID = function (data,cb) {
    ApplicationAnswer
        .findOne({
            centerID : data.centerID,
            applicationID : data.applicationID
        })
        .exec(function (err,answer) {
            if (err) return cb(err);
            if (!answer) return cb(ErrorsModule.notFoundError);
            answer.content = data.content;
            answer.save(cb);
        });
};
module.exports.deleteApplicationsByGroupID = function (data,cb) {
    Application
        .find({
            centerID : data.centerID,
            groupID : data.groupID
        })
        .populate('answerID')
        .exec(function (err,applications) {
            if (err) return cb(err);
            if (applications.length == 0) return cb(null);
            var results = [];
            applications.forEach(function (application,i) {
                results[i] = function (callback) {
                    application.remove(function (err) {
                        if (err) return callback(err);
                        if (!application.answerID) return callback(null);
                        application.answerID.remove(callback);
                    });
                };
            });
            async.series(results,cb);
        });
};


var Application = require(__modelsPath + 'application'),
    ApplicationAnswer = require(__modelsPath + 'application/application-answer'),
    ErrorsModule = require('system/errors'),
    async = require('async');
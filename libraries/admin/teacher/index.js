module.exports.getTeachers = function (data,cb) {
    Teacher
        .find({
            centerID : data.centerID
        })
        .exec(cb);
};
module.exports.addTeacher = function (data,cb) {
    var teacher = new Teacher({
        centerID : data.centerID,
        clientID : data.clientID,
        firstName : data.firstName,
        lastName : data.lastName,
        phone : data.phone,
        email : data.email,
        gender : data.gender
    });
    UserModule.initUser({
        user : teacher
    },function (err) {
        if (err) return cb(err);
        exports.saveImage(data,teacher, function (err,url) {
            if (err) return cb(err);
            teacher.imageUrl = url;
            teacher.save(cb);
        });
    });
};
module.exports.getScheduleByTeacherID = function (data,cb) {
    var data = {
        teacherID : data.teacherID,
        centerID : data.centerID
    };
    Teacher
        .findOne({
            _id : data.teacherID,
            centerID : data.centerID
        })
        .select('groups')
        .populate({
            path : 'groups',
            populate : {
                path : 'schedule',
                populate : [{
                    path : 'classroomID'
                },{
                    path : 'groupID',
                    populate : {
                        path : 'colorID'
                    }
                }]
            }
        })
        .exec(function (err,teacher) {
            if (err) return cb(err);
            if (!teacher) return cb(ErrorsModule.objNotFound);
            var lessons = [];
            if (_.isEmpty(teacher.groups)) return cb(null,lessons);
            teacher.groups.forEach(function (group) {
                if (group.schedule.length != 0) {
                    lessons = lessons.concat(group.schedule);
                }
            });
            cb(lessons);
        });
};

module.exports.saveImage = function (data,teacher,cb) {
    var image = data.image;
    if (image == null) return cb(null,undefined);
    Center
        .findOne({
            _id : teacher.centerID
        })
        .exec(function (err,center) {
            if (err) return cb(err);
            if (!center) return cb(ErrorsModule.objNotFound);
            var publicFolder = __publicPath;
            var randomName = crypto.randomBytes(10).toString('hex');
            randomName +='.png';
            var privateImagePath = path.join(publicFolder,'storage',center.subdomain,randomName);
            var publicImagePath = path.join('storage',center.subdomain,randomName);
            var data = image.replace(/^data:image\/\w+;base64,/, '');
            fs.writeFile(privateImagePath,data,{
                encoding : 'base64'
            }, function (err) {
                cb(err,publicImagePath);
            });
        });
};
module.exports.saveAvaForTeacher = function (teacher,data,cb) {

};

module.exports.updateTeacherByID = function (data,cb) {
    Teacher
        .findOne({
            _id : data.teacherID,
            centerID : data.centerID
        })
        .exec(function (err,teacher) {
            if (err) return cb(err);
            if (!teacher) return cb(ErrorsModule.objNotFound);
            teacher.firstName = data.firstName;
            teacher.lastName = data.lastName;
            teacher.email = data.email;
            exports.updateImage(data,teacher,function (err) {
                if (err) return cb(err);
                teacher.save(cb);
            });
        });
};
module.exports.updateImage = function (data,teacher,cb) {
    var image = data.image;
    if (!image) return cb();
    Center
        .findOne({
            _id : teacher.centerID
        })
        .exec(function (err,center) {
            if (err) return cb(err);
            if (!center) return cb(ErrorsModule.objNotFound);
            var randomName = crypto.randomBytes(10).toString('hex');
            randomName +='.png';
            var relPath = path.join(center.sourceUrl,crypto.randomBytes(10).toString('hex') + '.png'),
                absPath = path.join(__publicPath,relPath);
            console.log(relPath);
            var data = image.replace(/^data:image\/\w+;base64,/, '');
            fs.writeFile(absPath,data,{
                encoding : 'base64'
            }, function (err) {
                if (err) return cb(err);
                teacher.imageUrl = relPath;
                cb();
            });
        });
};

module.exports.deleteTeacherByID = function (data,cb) {
    exports.deleteTeacherSchedule(data,function (err) {
        if (err) return cb(err);
        Teacher
            .findOneAndRemove({
                _id : data.teacherID,
                centerID : data.centerID
            })
            .exec(cb);
    });

};
module.exports.deleteTeacherSchedule = function (data,cb) {
    Group
        .find({
            centerID : data.centerID,
            teacherID : data.teacherID
        })
        .exec(function (err,groups) {
            if (err) return cb(err);
            if (groups.length == 0) return cb(null);
            var results = [];
            groups.forEach(function (group,i) {
                results[i] = function (callback) {
                    ScheduleModule.deleteScheduleByGroupID({
                        groupID : group._id,
                        centerID : data.centerID
                    },callback);
                };
            });
            async.series(results,cb);
        });
};

//NEW CODE
module.exports.getTeacherByID = function (data,cb) {
    Teacher
        .findOne({
            centerID : data.centerID,
            _id : data.teacherID
        })
        .exec(function (err,teacher) {
            if (err) return cb(err);
            if (!teacher) return cb(ErrorsModule.notFoundError);
            Group
                .find({
                    teacherID : data.teacherID,
                    centerID : data.centerID
                })
                .select('color name createdAt updatedAt students')
                .populate({
                    path : 'students',
                    select : '_id'
                })
                .exec(function (err,groups) {
                    if (err) return cb(err);
                    cb(null,{
                        teacher : teacher,
                        groups : groups
                    });
                });
        });
};




var Group = require( __modelsPath  +'group'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Teacher = require(__modelsPath + 'teacher'),
    Center = mongoose.model('Center'),
    ErrorsModule = require('system/errors'),
    path = require('path'),
    crypto = require('crypto'),
    fs = require('fs'),
    async = require('async'),
    ScheduleModule = require('admin/schedule'),
    UserModule = require('system/user'),
    _ = require('underscore');
"use strict";


module.exports.isValidLessons = function (data,cb) {
    var res = [];
    _.each(data.lessons,function (lesson) {
        res.push(function (callback) {
            data.lesson = lesson;
            exports.isValidLesson(data,callback)
        });
    });
    async.series(res,cb);
};
module.exports.isValidEditableLessons = function (centerID,groupID,lessons,cb) {
    //REWRITE
    var results = [];
    lessons.forEach(function (lesson,i) {
        results[i] = function (callback) {
            lesson.centerID = centerID;
            exports.isValidEditableLesson(groupID,lesson,callback);
        }
    });
    async.series(results, cb);
};
module.exports.isValidEditableLesson = function (groupID,lesson,cb) {
    //REWRITE
    Classroom
        .findOne({
            centerID : lesson.centerID,
            _id : lesson.classroomID
        })
        .select('lessons')
        .populate({
            path: 'lessons'
        })
        .exec(function (err,classroom) {
            if (err) return cb(err);
            if (!classroom) return cb(_Errors.objNotFound);
            var results = [];
            _.each(classroom.lessons,function (existedLesson) {
                if (existedLesson.day == lesson.day && existedLesson.groupID.toString()!=groupID.toString()) {
                    results.push(existedLesson);
                }
            });
            var newLessonStartTime = exports.convertToMinutes(lesson.start.hour,lesson.start.minute);
            var newLessonEndTime = exports.convertToMinutes(lesson.end.hour,lesson.end.minute);
            if (newLessonStartTime >= newLessonEndTime) return cb(_Errors.notAcceptableError);
            if (_.isEmpty(results)) return cb();
            var isPossible = true;
            _.each(results,function (lesson) {
                var existedLessonStartTime = exports.convertToMinutes(lesson.start.hour,lesson.start.minute),
                    existedLessonEndTime = exports.convertToMinutes(lesson.end.hour,lesson.end.minute);
                if (newLessonEndTime > existedLessonStartTime && newLessonEndTime <= existedLessonEndTime || newLessonStartTime >=existedLessonStartTime && newLessonStartTime <existedLessonEndTime) {
                    isPossible = false;
                }
            });
            if (!isPossible) return cb(_Errors.notAcceptableError);
            cb();
        });
};
module.exports.isValidLesson = function (obj,cb) {
    exports.isValidLessonForClassroom(obj,function (err) {
        if (err) return cb(err);
        exports.isValidLessonForTeacher(obj,cb);
    });
};
module.exports.isValidLessonForTeacher = function (data,cb) {
    Lesson
        .find({
            teacherID : data.teacherID,
            day : data.lesson.day
        })
        .exec(function (err,lessons) {
            if (err) return cb(err);
            if (_.isEmpty(lessons)) return cb();
            exports.isHasPads({
                lessons : _.filter(lessons,function (lesson) {
                    return lesson.groupID.toString() != data.groupID.toString() || lesson.teacherID.toString() != data.teacherID.toString()
                }),
                start : data.lesson.start,
                end : data.lesson.end,
                context : Constants.TEACHER
            },cb);
        });
};
module.exports.isValidLessonForClassroom = function (data,cb) {
    var day = data.lesson.day,
        classroomID = data.lesson.classroomID,
        centerID = data.centerID;
    Lesson
        .find({
            centerID : centerID,
            classroomID : classroomID
        })
        .exec(function (err,lessons) {
            if (err) return cb(err);
            if (_.isEmpty(lessons)) return cb();
            var filtered = _.filter(lessons,function (lesson) {
                return (lesson.groupID.toString() != data.groupID || lesson.teacherID.toString() != data.teacherID.toString()) && lesson.day == day
            });
            exports.isHasPads({
                lessons : filtered,
                start : data.lesson.start,
                end : data.lesson.end,
                context : Constants.CLASSROOM
            },cb);
        });
};
module.exports.isHasPads = function (data,cb) {
    var start = data.start,
        end = data.end,
        lessons = data.lessons,
        error = null,
        newLessonStartTime = exports.convertToMinutes(start.hour,start.minute),
        newLessonEndTime = exports.convertToMinutes(end.hour,end.minute);
    if (newLessonStartTime >=newLessonEndTime) return cb(_Errors.invalidStartAndEndTimeError);
    if (_.isEmpty(lessons)) return cb();
    _.each(lessons,function (lesson) {
        var existedLessonStartTime = exports.convertToMinutes(lesson.start.hour,lesson.start.minute),
            existedLessonEndTime = exports.convertToMinutes(lesson.end.hour,lesson.end.minute);
        if (newLessonEndTime > existedLessonStartTime && newLessonEndTime <= existedLessonEndTime || newLessonStartTime >=existedLessonStartTime && newLessonStartTime < existedLessonEndTime) {
            error = _Errors.lessonHasPadsError;
            lesson.context = data.context;
            error.content = lesson;
            error.context = data.context;
        }
    });
    cb(error);
};

module.exports.createSchedule = function (data,cb) {
    var results = [];
    _.each(data.lessons,function (lesson,index) {
        results[index] = function (callback) {
            lesson.centerID = data.centerID;
            lesson.groupID = data.groupID;
            lesson.teacherID = data.teacherID;
            exports.addLesson(lesson,callback);
        };
    });
    async.series(results,function (err,lessonIDs) {
        if (err) return cb(err);
        Group
            .findOne({
                _id : data.groupID,
                centerID : data.centerID
            })
            .select('schedule')
            .exec(function (err,group) {
                if (err) return cb(err);
                if (!group) return cb(_Errors.badRequestError);
                group.schedule = group.schedule.concat(lessonIDs);
                group.save(cb);

            });
    });
};
module.exports.updateGroupSchedule = function (data,cb) {
    exports.isValidLessons(data,function (err) {
        if (err) return cb(err);
        Group
            .findOne({
                _id : data.groupID,
                centerID : data.centerID
            })
            .select('schedule teachers')
            .populate({
                path : 'teachers',
                select : '_id',
                match : {
                    _id : data.teacherID
                }
            })
            .exec(function (err,group) {
                if (err) return cb(err);
                if (!group) return cb(_Errors.notFound);
                if (_.isEmpty(group.teachers)) return cb(_Errors.badRequest);
                console.log(data.groupID);
                console.log(data.teacherID);
                exports.delLessonsByQuery({
                    groupID : data.groupID,
                    teacherID : data.teacherID
                },function (err) {
                    if (err) return cb(err);
                    var res = [];
                    _.each(data.lessons,function (lesson,index) {
                        res[index] = function (callback) {
                            lesson.centerID = data.centerID;
                            lesson.teacherID = data.teacherID;
                            lesson.groupID = data.groupID;
                            exports.addLesson(lesson,callback);
                        };
                    });
                    async.parallel(res,function (err,ids) {

                        if (err) return cb(err);
                        group.schedule = group.schedule.concat(ids);
                        group.save(cb);
                    });
                });
            });
    });

};

module.exports.addLesson = function (obj,cb) {
    // REWRITE
    var day = obj.day,
        start = obj.start,
        end = obj.end,
        classroomID = obj.classroomID,
        groupID = obj.groupID,
        centerID = obj.centerID;
    if (!day || !start || !end || !classroomID || !centerID || start.hour === null || start.minute === null || end.hour === null || end.minute === null) {
        cb(_Errors.invalidBodyError);
    } else {
        var lesson = new Lesson({
            day : day,
            start : start,
            end : end,
            classroomID : classroomID,
            centerID : centerID,
            groupID : groupID,
            teacherID : obj.teacherID
        });
        var startDate = new Date(0);
        startDate.setUTCHours(lesson.start.hour);
        startDate.setUTCMinutes(lesson.start.minute);
        startDate.setUTCDate(startDate.getDate() + lesson.day);
        var endDate = new Date(0);
        endDate.setUTCHours(lesson.end.hour);
        endDate.setUTCMinutes(lesson.end.minute);
        endDate.setUTCDate(endDate.getDate() + lesson.day);
        lesson.start.value = startDate.getTime()/1000;
        lesson.end.value = endDate.getTime()/1000;
        lesson.start.date = startDate;
        lesson.end.date = endDate;

        lesson.save(function (err) {
            if (err) return cb(err);
            cb(null,lesson._id);
        });
    }
};
module.exports.convertToMinutes = function (hour,minute) {
    return parseInt(hour) * 60 + parseInt(minute);
};
module.exports.delLessonsByQuery = function (data,cb) {
    if (!data.groupID && !data.teacherID) return cb(_Errors.badRequest);
    Lesson
        .remove(data,cb)
};
module.exports.deleteScheduleByGroupID = function (data,cb) {
    exports.delLessonsByQuery({
        centerID : data.centerID,
        teacherID : data.teacherID,
        groupID : data.groupID
    },function (err) {
        if (err) return cb(err);
        Group
            .findOne({
                centerID : data.centerID,
                _id : data.groupID
            })
            .exec(function (err,group) {
                if (err) return cb(err);
                if (!group) return cb(_Errors.notFoundError);
                group.schedule = [];
                group.save(cb);
            });
    });
};

module.exports.getAvailableGroupsForSchedule = function (data,cb) {
    Group
        .find({
            centerID : data.centerID
        })
        .select('schedule teachers name')
        .populate([{
            path : 'schedule'
        },{
            path : 'teachers',
            select : 'firstName lastName'
        }])
        .exec(function (err,groups) {
            if (err) return cb(err);
            cb(null,_.filter(groups,function (group) {
                if (_.isEmpty(group.schedule) || _.isEmpty(group.teachers)) return true;
                if (group.teachers.length == 1) return false;

                var teachersWithoutSchedule = [];
                _.each(group.teachers,function (teacher) {
                    var isContain = _.some(group.schedule,function (lesson) {
                        return lesson.teacherID.toString() == teacher._id.toString()
                    });
                    if (!isContain) {
                        teachersWithoutSchedule.push(teacher);
                    }
                });
                group.teachers = teachersWithoutSchedule;
                return !_.isEmpty(teachersWithoutSchedule);
            }));
        });
};
module.exports.validateAddScheduleBody = function (data) {
    if (!data.lessons || _.isEmpty(data.lessons) || !data.groupID || !data.teacherID) return _Errors.invalidBodyError;
    var error = null;
    _.each(data.lessons,function (lesson) {
        if (!lesson.day ||
            !lesson.start || !lesson.end || lesson.start.hour == null || lesson.start.minute == null
            || lesson.end.hour == null || lesson.end.minute == null || !lesson.classroomID) {
            error = _Errors.invalidBodyError;
        }
    });
    return error;
};
module.exports.addSchedule = function (data,cb) {
    exports.isValidLessons(data,function (err) {
        if (err) return cb(err);
        exports.createSchedule(data,cb);
    });
};

module.exports.getSchedule = function (data,cb) {
    var classroomID = data.classroomID,
        teacherID = data.teacherID;
    if (!classroomID && !teacherID) return cb(_Errors.invalidBodyError);
    if (!classroomID) return exports.getScheduleByTeacherID(data,cb);
    if (!teacherID) return exports.getScheduleByClassroomID(data,cb);
};
module.exports.getScheduleByClassroomID = function (data,cb) {
    Lesson
        .find({
            classroomID : data.classroomID,
            centerID : data.centerID
        })
        .populate([{
            path : 'classroomID'
        },{
            path : 'groupID',
            select : 'name color'
        },{
            path : 'teacherID',
            select : 'firstName lastName'
        }])
        .exec(function (err,lessons) {
            if (err) return cb(err);
            cb(null,lessons);
        });
};
module.exports.getScheduleByTeacherID = function (data,cb) {
    Lesson
        .find({
            teacherID : data.teacherID,
            centerID : data.centerID
        })
        .populate([{
            path : 'classroomID',
            select : 'name'
        },{
            path : 'teacherID',
            select : 'firstName lastName'
        },{
            path : 'groupID',
            select : 'name color'
        }])
        .exec(cb);
    // Group
    //     .find({
    //         teacherID : data.teacherID
    //     })
    //     .select('schedule')
    //     .populate({
    //         path : 'schedule',
    //         options : {
    //             $exists : true
    //         },
    //         populate : [{
    //             path : 'classroomID'
    //         },{
    //             path : 'groupID',
    //             select : 'name color'
    //         }]
    //     })
    //     .exec(function (err,groups) {
    //         if (err) return cb(err);
    //         if (groups.length === 0) return cb(null,[]);
    //         var results = [];
    //
    //         groups.forEach(function (group) {
    //             results = results.concat(group.schedule);
    //         });
    //         cb(null,results);
    //     });
};
module.exports.getScheduleByGroupID = function (data,cb) {
    var TeacherQuery = {
        path : 'teachers'
    };
    var ScheduleQuery = {
        path : 'schedule',
        populate : {
            path : 'classroomID'
        }
    };
    if (data.teacherID) {
        TeacherQuery.match = {
            _id : data.teacherID
        };
        ScheduleQuery.match = {
            teacherID : data.teacherID
        };
    }
    Group
        .findOne({
            _id : data.groupID,
            centerID : data.centerID
        })
        .select('teachers name schedule students color')
        .populate([TeacherQuery,ScheduleQuery])
        .exec(function (err,group) {
            if(err) return cb(err);
            if (!group) return cb(_Errors.notFoundError);
            cb(null,group);
        });
};
module.exports.getByQuery = function (data,cb) {
    Lesson
        .find({
            centerID : data.centerID,
            teacherID : data.teacherID,
            groupID : data.groupID
        })
        .populate({
            path : 'classroomID'
        })
        .exec(cb);
};

var Classroom = require(__modelsPath  + 'classroom'),
    Helper = require('system/helper'),
    Lesson = require(__modelsPath  + 'lesson'),
    Group = require(__modelsPath  + 'group'),
    GroupModule = require('admin/group'),
    ClassroomModule = require('admin/classroom'),
    _Errors = require('system/errors'),
    _ = require('underscore'),
    async = require('async'),
    Constants = require('system/constants'),
    LessonModule = require('admin/lesson');







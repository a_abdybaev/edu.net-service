module.exports.getAttendance = function (data,cb) {
    Attendance
        .find({
            centerID : data.centerID,
            groupID : data.groupID
        })
        .exec(cb);
};
var Attendance = require(__modelsPath + 'attendance');